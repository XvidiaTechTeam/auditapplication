
<!doctype html>
<html class="no-js" lang="">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Vowmee</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="${pageContext.request.contextPath}/images/favicon/favicon.ico">
    <!-- Stylesheets -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/font-awesome.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/animate.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/vowmee.css" />
   <%--  <link rel="stylesheet" type="text/css" href="<c:url value="/css/font-awesome.css" />">
    <link rel="stylesheet" type="text/css" href="<c:url value="/css/animate.css" />">
    <link rel="stylesheet" type="text/css" href="<c:url value="/css/bootstrap.min.css" />">
    <link rel="stylesheet" type="text/css" href="<c:url value="/css/vowmee.css" />"> --%>
    <style>
        video.intro {
            position: fixed;
            top: 50%;
            left: 50%;
            min-width: 100%;
            min-height: 100%;
            width: auto;
            height: auto;
            z-index: -100;
            transform: translateX(-50%) translateY(-50%);
            background: url('//demosthenes.info/assets/images/polina.jpg') no-repeat;
            background-size: cover;
            transition: 1s opacity;
        }
    </style>

</head>
<body>

    <!--[if lt IE 8]><p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p><![endif]-->
    <video class="intro" poster="${pageContext.request.contextPath}/images/poster.jpg" id="bgvid" playsinline autoplay muted loop>
        <source src=" ${pageContext.request.contextPath}/video/bg1.webm" type="video/webm">
        <source src=" ${pageContext.request.contextPath}/video/bg1.mp4" type="video/mp4">
    </video>

    <header id="home">
        <div class="overlay-light">
            <div class="container">
                <div class="menu-container">
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="logo">
                                <a href="#">
                                    <img src="${pageContext.request.contextPath}/images/logo/logo.png" height="80" width="80" alt="Live Logo">
                                </a>
                            </div><!-- .logo -->
                        </div><!-- .col-xs-6 -->
                        <div align="right" class="col-xs-6">
                            <a style="color:#fff; font-size:17px;"href="login">Account</a>

                        </div><!-- .col-xs-6 -->
                    </div><!-- .row -->
                </div><!-- .menu-container -->
                <div class="header-container">
                    <div class="row">
                        <!--<div class="col-md-6"></div>
                         <div class="col-md-6">
                             <img src="images/mobiles-intro.png" alt="App">
                         </div>-->
                        <div class="col-md-12">
                            <div class="header-items">
                                <div class="header-text">
                                    <h1 style="margin-bottom: 33px; font-size:45px;"> VOWMEE</h1> <h1 style="margin-bottom: 33px; font-size:36px;">Share your LIVE moments with your LOVED ones!</h1>
                                </div><!-- header-text -->
                                <div class="header-buttons">
                                    <a class="#" href="#">
                                        <img src="${pageContext.request.contextPath}/images/download-btn.png" />
                                    </a>

                                </div><!-- header-buttons -->
                                <div class="app-image-container">
                                    <img src="${pageContext.request.contextPath}/images/bane-mobile.png" alt="App Images">
                                </div> <!-- app-image-container -->
                            </div> <!-- .header-text -->
                        </div><!-- .col-md-12 -->
                    </div><!-- .row -->
                </div><!-- .header-container -->
            </div><!-- .container -->
        </div><!-- .overlay -->
    </header><!-- #home -->

    <section id="features" class="section light">
        <div class="section-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-heading-container">
                            <h2>
                                <span>Share Video</span> From Anywhere
                            </h2>
                            <!-- <p>
                                Lorem ipsum dolor sit. Incidunt laborum beatae earum nihil odio consequatur officiis tempore consequuntur officia ducimus unde doloribus quod unt repellat.
                            </p> -->
                        </div><!-- section-header-container -->
                    </div><!-- col-md-12 -->
                </div><!-- row -->
            </div><!-- container -->
        </div><!-- section-heading -->

        <div class="section-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <figure><img src="${pageContext.request.contextPath}/images/app-clip.jpg" alt="Nexus"></figure>
                    </div><!-- col-md-4 -->
                    <div class="col-md-8 col-sm-8">
                        <div class="feature-row">
                            <div class="row">
                                <div class="col-md-6 col-xs-12 wow fadeInLeft">

                                    <img src="${pageContext.request.contextPath}/images/live.png" />
                                    <h3>Live broadcast </h3>
                                    <p>Enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                    <div class="visible-xs">
                                        <br /><br /><br /><br />
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12 wow fadeInRight">
                                    <img src="${pageContext.request.contextPath}/images/video-call.png" />
                                    <h3>Video call</h3>
                                    <p>Enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                    <div class="visible-xs">
                                        <br /><br /><br /><br />
                                    </div>
                                </div>
                            </div> <!-- row -->
                        </div><!-- feature row -->
                        <div class="feature-row">
                            <div class="row">
                                <div class="col-md-6 col-xs-12 wow fadeInLeft" data-wow-delay="1s">
                                    <img src="${pageContext.request.contextPath}/images/video-char.png" />
                                    <h3>Video message</h3>
                                    <p>Enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                    <div class="visible-xs">
                                        <br /><br /><br /><br />
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12 wow fadeInRight" data-wow-delay="1s">
                                    <img src="${pageContext.request.contextPath}/images/groups.png" />
                                    <h3>Create groups</h3>
                                    <p>Enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                    <div class="visible-xs">
                                        <br /><br /><br /><br />
                                    </div>
                                </div>
                            </div><!-- row -->
                        </div><!-- feature row -->
                    </div><!-- col-md-8 -->
                </div><!-- row -->
            </div><!-- container -->
        </div><!-- section-container -->
    </section><!-- feature -->

    <section id="description" class="section cnt-section">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="section-heading-container-left">
                        <h2>
                            Explore with <span>Vowmee</span> App

                        </h2>
                        <p>
                            Lorem ipsum dolor sit. Incidunt laborum beatae earum nihil odio consequatur officiis tempore consequuntur officia ducimus unde doloribus quod unt repellat.
                        </p>
                        <div class="description-details">
                            <div class="row">
                                <div class="col-md-6 desc-left">
                                    <h4>Live Stream Video</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet.  </p>
                                </div><!-- col-md-6 -->
                                <div class="col-md-6">
                                    <h4>Enterprise App</h4>
                                    <p>Morem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod ut laoreet  tincidunt</p>
                                </div><!-- col-md-6 -->
                            </div><!-- row -->
                        </div> <!-- description details -->
                    <br />
                        <div class="download-btns">
                            <a class='btn-light-outlined' href="#"><i class="fa fa-android"></i> Play Store</a>
                            <a class="btn-light-outlined" href="#"><i class="fa fa-apple"></i> App Store</a>

                        </div>
                    </div><!-- section-header-container -->
                </div><!-- col-md-6 -->
                <div class="col-md-6">

                </div><!-- col-md-6 -->
            </div><!-- row -->
        </div><!-- container -->
    </section><!-- feature -->


    <section style="height:450px; position:relative;" id="section-bg" class="section dark section-full-screen-image">
        <div class="overlay-video">
            <div class="section-heading-container">
                <br /><br /><br /><br /><br /><br /><br />
                <h2 style="color:#fff;">
                    Share your happy  moments <br /><span> with loved ones </span>
                </h2>


            </div><!-- section-header-container -->
        </div>
        <video poster="${pageContext.request.contextPath}/images/poster.jpg" id="bgvid" playsinline autoplay muted loop>
            <!-- WCAG general accessibility recommendation is that media such as background video play through only once. Loop turned on for the purposes of illustration; if removed, the end of the video will fade in the same way created by pressing the "Pause" button  -->
            <source src=" ${pageContext.request.contextPath}/video/video.webm" type="video/webm">
            <source src=" ${pageContext.request.contextPath}/video/video.mp4" type="video/mp4">
        </video>
    </section><!-- vidoe -->


    <footer style="background:#3196eb;" class="section" id="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading-container">
                        <h2 style="color:#fff;">
                           <span> Subscribe newsletter </span>
                        </h2>
                       <p style="color:#fff;">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>

                        <div class="newsletter-div">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <input  type="text" placeholder="Enter Email ID" />
                                    </td>
                                    <td style="width:100px;">
                                        <a href="#"><img src="${pageContext.request.contextPath}/images/email.png" /></a>
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </div><!-- section-header-container -->
                </div><!-- col-md-12 -->


            </div><!-- row -->
        </div><!-- container -->
        <div >
            <div style="width:100%; background:#fff;" class="col-md-12">
                <div class="copyright text-center">
                    <p>&copy; 2016, Vowmee</p>
                </div><!-- copyright -->
            </div><!-- col-md-12 -->
        </div>
    </footer>

    <!-- Login -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div style="max-width:320px;" class="modal-dialog" role="document">
            <div class="modal-content modal-forms">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Login</h4>
                </div>
                <div class="modal-body">
                    <input type="text" placeholder="E-Mail ID" />
                    <input type="password" placeholder="Password" />
                    <a data-dismiss="modal" data-toggle="modal" data-target="#fpasswordModal" href="#">Forgot Password ?</a>
                </div>
                <div style="align-content: center;" align="center" class="modal-footer">
                    <a style="text-align:center;" href="#">
                        <img src="${pageContext.request.contextPath}/images/login-btn.png" />
                    </a>

                    <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                </div>
            </div>
        </div>
    </div>

    <!-- Create Account -->
    <div class="modal fade" id="cAccount" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div style="max-width:600px;" class="modal-dialog" role="document">
            <div class="modal-content modal-forms">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Create Account</h4>
                </div>
                <div class="modal-body">
                    <div style="margin: 0px; margin-top: 14px;" class="row">
                        <div class="col-md-6">
                            <input type="text" placeholder="Username" />
                            <input type="text" placeholder="Email ID" />
                            <input type="text" placeholder="First name" />
                            <input type="text" placeholder="Last name" />
                            <input type="text" placeholder="Street address" />
                        </div>
                        <div class="col-md-6">
                            <input type="text" placeholder="City" />
                            <input type="text" placeholder="State" />
                            <input type="text" placeholder="Postal" />
                            <input type="text" placeholder="Country" />
                            <input type="text" placeholder="Phone" />
                        </div>
                    </div>
                </div>
                <div style="align-content: center;" align="center" class="modal-footer">
                    <a style="text-align:center;" data-dismiss="modal" href="#">
                        <img src="${pageContext.request.contextPath}/images/create-account.png" />
                    </a>

                    <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                </div>
            </div>
        </div>
    </div>


    <!-- Scroll to Top
    ======================= -->
    <a href="#" id="scroll-to-top">Scroll to Top</a>
    <!-- Scripts -->
    
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
     <script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
     <script type="text/javascript" src="${pageContext.request.contextPath}/js/imagesloaded.pkgd.min.js"></script>
     <script type="text/javascript" src="${pageContext.request.contextPath}/js/main.js"></script>
   <%--  <script type="text/javascript" src="<c:url value="/js/jquery-1.11.3.min.js" />"></script>
    <script type="text/javascript" src="<c:url value="/js/bootstrap.min.js" />"></script>
    <script type="text/javascript" src="<c:url value="/js/imagesloaded.pkgd.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/main.js"/>"></script>
 --%>
</body>
</html>