<%@include file="include.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Vowmee</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/css/vowmee-design.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/css/oversheet.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/css/hover.css" rel="stylesheet" />
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    
     <script>
     
   
        function theFunction (id,firstName,lastName,mobile) {
        	console.log("hell");
        	console.log(id);
        	console.log(firstName);
        	var divID = '#'+id;
        	var divName = '#value'+id;
        	var divfirstName = '#firstName'+id;
        	var divlastName = '#lastName'+id;
        	var divmobile = '#mobile'+id;
        	
        	 $(divID).toggle("slow");
        	 $(divName).hide();
        	 $(divfirstName).val(firstName);
        	 $(divlastName).val(lastName);
        	 $(divmobile).val(mobile);
        	
        	
        	
       
        }
        
        function hideDisplay (id) {
        	console.log("hideDisplay(");
        	console.log(id);
        	var divID = '#'+id;
        	var divName = '#value'+id;
        	
        	
        	 $(divName).toggle("slow");
        	 $(divID).hide();
        	 
       
        }
        
     

    </script>

<style type="text/css">
.btn-link {
	border: none;
	outline: none;
	background: none;
	cursor: pointer;
	color: #0000EE;
	padding-left: 15px;
	text-decoration: underline;
	text-align: center;
	font-family: inherit;
	font-size: inherit;
}

.btn-link:active {
	color: #FF0000;
}
</style>

</head>
<body id="style-4" class="intro-bg">

	<%@include file="header.jsp"%>
<section>
        <div class="container">
            <div class="row">
                <div align="center" class="col-md-12">
                    <br /><br /><br />
                </div>
            </div>

            <div class="row">
                <div align="center" class="col-md-12">
                    <h3 class="heading">User Profile !</h3>
                </div>
            </div>
           <!--  <div class="row">
                <div class="col-md-12">
                    <div class="search-div">
                        <input type="text" placeholder="Search by user name" />
                    </div>
                </div>
               
            </div>

 -->
 
		<div class="row">
			<div class="col-md-12">
				<c:if test="${not empty userDetails}">
					<c:forEach var="userValue" items="${userDetails}">
						<div class="usercard-div">
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-6 no-pad">
										<p>
											<span>Name :</span>${userValue.firstName}
										</p>
										<p>
											<span>Email :</span>${userValue.name}
										</p>
									</div>
									<div align="right" class="col-md-6 no-pad">
										<a id="slide" href="#" onclick="return theFunction('${userValue.id}','${userValue.firstName}','${userValue.lastName}',
										'${userValue.mobile}');"><img class="hvr-rotate"
											src="${pageContext.request.contextPath}/webskin/edit.png" /></a>
									</div>
								</div>

							</div>
							<div class="row">
								<div style="border-bottom: 1px #ccc dotted; margin: 10px 0px;"
									class="col-md-12"></div>
							</div>
							<div class="row" id="value${userValue.id}">
							<div class="col-md-3">
									<span>First Name</span>
									<p>${userValue.firstName}</p>
								</div>
								<div class="col-md-3">
									<span>Last Name</span>
									<p>${userValue.lastName}</p>
								</div>
								<div class="col-md-3">
									<span>Mobile</span>
									<p>${userValue.mobile}</p>
								</div>
								<div class="col-md-3">
									<span>User Role</span>
									<p>${userValue.userRole}</p>
								</div>
								<div class="col-md-3">
									<span>User type</span>
									<p>${userValue.userType}</p>
								</div>
								<div class="col-md-3">
									<span>Status</span>
									<p>${userValue.userStatus}</p>
								</div>
								
								<div class="col-md-3">
									<span>Circle</span>
									<p>${userValue.userCircle}</p>
								</div>
								
							</div>
							<form:form id="editForm${userValue.id}" method="post" action="editProfileUser"	modelAttribute="editUserBean">
							<div id="${userValue.id}" style="display:none">
								<div class="row">
									<div style="border-bottom: 1px #ccc dotted; margin: 10px 0px;"
										class="col-md-12"></div>
									<div class="col-md-6">
									 <form:hidden path="id" value="${userValue.id}"></form:hidden>
										<%-- <span>First Name</span><br /> <input type="text" id="firstName${userValue.id}"/> --%>
										<span>First Name</span><br /><form:input id="firstName${userValue.id}" name="firstName"  path="" placeholder="First Name"/>
									</div>
									<div class="col-md-6">
									<span>Last Name</span><br /><form:input id="lastName${userValue.id}" name="lastName"  path="" placeholder="Last Name"/>
										<%-- <span>Last Name</span><br /> <input type="text" id="lastName${userValue.id}"/> --%>
									</div>
									<div class="col-md-6">
										<span>Mobile</span><br />  <form:input type="number" id="mobile${userValue.id}" placeholder="Mobile"  name="mobile" value="${signupBean.mobile}" path=""/>
										<%-- <input id="mobile${userValue.id}" type="text" /> --%>
									</div>
									
									
									<div class="col-md-12">
										<button type="submit" class="btn btn-primary">Save</button>
										<button type="button" class="btn btn-default" onclick="return hideDisplay('${userValue.id}');">Cancel</button>
									</div>
								</div>
							</div>
						</form:form>
						</div>
					</c:forEach>
				</c:if>
			</div>
		</div>


	</div>
    </section>
	
</body>

</html>