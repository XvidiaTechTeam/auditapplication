<%@include file="include.jsp"%>
<%@include file="header.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.vowmee.bean.UserBean" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Vowmee</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
<link href="${pageContext.request.contextPath}/css/bootstrap.min.css"
	rel="stylesheet" />
<link href="${pageContext.request.contextPath}/css/vowmee-design.css"
	rel="stylesheet" />
<link href="${pageContext.request.contextPath}/css/oversheet.css"
	rel="stylesheet" />
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
  <style type="text/css">.btn-link{
 border:none;
 outline:none;
 background:none;
 cursor:pointer;
 color:#0000EE;
 padding-left:10px;
 text-decoration:underline;
  text-align: center;
 font-family:inherit;
 font-size:inherit;
}
.btn-link:active{
 color:#FF0000;
}
</style>

<script>
$(document).ready(function () {
  
    $('#search').keyup(function(){
    	   var valThis = $(this).val().toLowerCase();
    	   console.log(valThis);
    	    if(valThis == ""){
    	        $('.searchAudit > div').show();           
    	    } else {
    	        $('.searchAudit > div').each(function(){
    	            var text = $(this).text().toLowerCase();
    	            console.log(text);
    	            (text.indexOf(valThis) >= 0) ? $(this).show() : $(this).hide();
    	        });
    	   };
    	});
 
});

</script>
 <% UserBean user = (UserBean) session.getAttribute("loginBean");%>
</head>
<body id="style-4" class="intro-bg">
	 <section>
        <div class="container">
            <div class="row">
                <div align="center" class="col-md-12">
                    <br /><br /><br />
                </div>
            </div>
                          
               <div class="row">
                <div align="center" class="col-md-12">
                
                    <h3 class="heading">Auditor: <%= user.getFirstName() %> | Circle : ${userCircle} </h3>
                    <p style="color:#fff;">Please do Audit for all the Category</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="search-div">
                        <input type="text" placeholder="Search by store name" id="search"/>
                    </div>
                </div>
            </div>
                <div class="row searchAudit">

                    <!--Store box start-->
                    <c:if test="${not empty storeList}">
                    <c:forEach var="storeValue" items="${storeList}">
	                    <div align="center" class="col-md-4 col-sm-6">
	                        <div class="store-bx">
	                            <table class="card-tbl">
	                           <tr>
	                          <%--  <td valign="middle" class="left-crd"><p>${storeValue.store_name} </p></td>
	                                    <td align="center" class="right-crd">
	                                    <a href="<c:url value='/createAudit/${storeValue.store_name}' />" >
	                                    <img src="${pageContext.request.contextPath}/webskin/pen.png" /><br />
	                                            Audit now
	                                    </a>
	                                    </td> --%>
	                            <c:choose>
	                               <%--  <c:when test="${storeValue.completedFlag == '(Completed)'}">
	                                    <td valign="middle" class="left-crd"><p>${storeValue.store_name}</p><p style="color:green;">${storeValue.completedFlag}</p></td>
	                                    <td align="center" class="right-crd">
	                                    <a href="<c:url value='/createAudit/${storeValue.store_name}' />" >
	                                    <img src="${pageContext.request.contextPath}/webskin/pen.png" /><br />
	                                    </a>
	                                    </td>
	                                </c:when> --%>
	                                <c:when test="${storeValue.completedFlag == '(Pending)'}">
	                                    <td valign="middle" class="left-crd"><p>${storeValue.store_name}</p><p style="color:red;">${storeValue.completedFlag}</p></td>
	                                    <td align="center" class="right-crd">
	                                    <a href="<c:url value='/continueAudit/${storeValue.store_name}' />" >
	                                    <img src="${pageContext.request.contextPath}/webskin/pen.png" /><br />
	                                            Continue Audit
	                                    </a>
	                                    </td>
	                                </c:when>
	                                <c:otherwise>
	                                    <td valign="middle" class="left-crd"><p>${storeValue.store_name}  ${storeValue.completedFlag}</p>
	                                   <!--  <p><a href="previousAudits" style="text-decoration:underline!important; color:blue;">View Previous Audits</a></p> --></td>
	                                    <td align="center" class="right-crd">
	                                    <a href="<c:url value='/createAudit/${storeValue.store_name}' />" >
	                                    <img src="${pageContext.request.contextPath}/webskin/pen.png" /><br />
	                                            Audit now
	                                    </a>
	                                    </td>
	                                </c:otherwise>
	                                </c:choose>
	                                </tr>
	                            </table>
	                        </div>
	                    </div>
                    </c:forEach>
                    </c:if>
                    <!--Store box end-->
                </div>
	        </div>
    </section>
	
		<%-- <div style="" class="container">
		<div class="frm-cnt">
			<div class="row">
				<div class="col-md-12">
					<br /> <br /> <br /> <br />
					<!-- Nav tabs -->
					<ul style="width: 200px; margin: auto;" class="nav nav-tabs"
						role="tablist">
						<li style="width: 100%;" role="presentation" class="active"><a
							href="#" style="text-align: center;" aria-controls="profile"
							role="tab" data-toggle="tab">Audit Screen</a></li>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="register">
							<div class="row reg-div">
								<div class="col-md-12">
									<h3>Let's Explore !</h3>
									<p>Lorem ipsum dolor sit amet, consectetuer adipiscing
										elit, sed diam</p>
									<br />
								</div>
								<form:form id="auditForm" method="post" action="createAudit"
									commandName="storeName">
									<div class="row">
										<div class="col-md-6">
											<form:select path="store" items="${storeList}" />
											<form:select path="store_name">
												<form:option value="0" label="Select Store" />
												<form:options items="${storeList}" />
											</form:select>
											<form:select path="store" id="storeSelect">
												<form:options items="${storeList}" itemValue="${storeList}"
													itemLabel="--- Select ---" />
											</form:select>
										</div>
										<div class="col-md-3">
											<div>
												<button type="submit" style="text-align: center;" role="tab">
													Create Audit</button>
												<br /> <br /> <br />
											</div>
										</div>
									</div>
								</form:form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
 --%>	

</body>
</html>