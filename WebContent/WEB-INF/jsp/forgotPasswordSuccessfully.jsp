<%@include file="include.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Activation Page</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
<link href="${pageContext.request.contextPath}/css/bootstrap.min.css"
	rel="stylesheet" />
<link href="${pageContext.request.contextPath}/css/vowmee-design.css"
	rel="stylesheet" />
<link href="${pageContext.request.contextPath}/css/oversheet.css"
	rel="stylesheet" />
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

</head>
<body id="style-4" class="intro-bg">
	 <div class="header-d">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-3 ">
                    <a href="..${pageContext.request.contextPath}/login"><img src="${pageContext.request.contextPath}/webskin/logo.png" /></a>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-8 ">
                    <h3 class="hidden-xs" style="text-align:center;">Vowmee Audit App</h3>
                    <h3 class="visible-xs" style="text-align: center; font-size: 17px; margin-top: 24px;">Vowmee Audit App</h3>
                </div>

                <div class="col-md-3 col-sm-3 col-xs-3 ">
                    
                </div>
            </div>
        </div>
    </div>

	<div style="" class="container">
		<div class="frm-cnt">
			<div class="row">
				<div class="col-md-12">
					<center style="padding-top: 121px;">
						<font color="white">Password Reset Successfull please login
							into your account</font> <a
							href="http://audit.xvidiaglobal.com:8080/idea/login"> login</a>
					</center>
				</div>
			</div>
		</div>
	</div>
</body>
</html>