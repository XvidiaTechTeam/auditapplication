<%@include file="include.jsp"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Vowmee</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/css/vowmee-design.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/css/oversheet.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/css/hover.css" rel="stylesheet" />
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
    <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
    <script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
    
</head>
<script type="text/javascript">
$(function() {
	 $("#fromDate").datepicker();
	 $("#toDate").datepicker();
	 var categoryList = ${categoryList}
	 var data1 = ${data}
	 var storeName = ${storeName}
	 console.log(categoryList);
	 console.log(data1);
	 var seriesData = [];
	 for(var i in data1){
		 var arr = [];
		 var val = {
				 name:i,
				 data:data1[i]
		 }
		 
		 seriesData.push(val);
		 console.log("arr " +arr);
	 }
	 console.log(seriesData);
	 $('#container').highcharts({
	        chart: {
	            type: 'column'
	        },
	        title: {
	            text: 'Score Card for '+ storeName
	        },
	        subtitle: {
	            text: ''
	        },
	        xAxis: {
	            categories: categoryList,
	            crosshair: true
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: 'Average Score %'
	            }
	        },
	        tooltip: {
	            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	                '<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>',
	            footerFormat: '</table>',
	            shared: true,
	            useHTML: true
	        },
	        plotOptions: {
	            column: {
	                pointPadding: 0.2,
	                borderWidth: 0
	            }
	        },
	        series: seriesData
	    });
});
</script>
<body style="overflow-x: hidden;  padding-top:20px;" class="intro-bg">
<%@include file="header.jsp"%>
    <!--<a href="login.html">
        <img class="logout-ic" src="webskin/logout-ic.png" />
    </a>-->
   <!--  <div class="header-d">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-3 ">
                    <a href="../index.html"><img src="webskin/logo.png" /></a>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6 ">
                    <h3 class="hidden-xs" style="text-align:center;">Vowmee Audit App</h3>
                    <h3 class="visible-xs" style="text-align: center; font-size: 17px; margin-top: 24px;">Vowmee Audit App</h3>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-3 ">
                    <table align="right" class="h-tbl">
                        <tr>
                            <td>
                                <a href="intro.html">
                                    <img src="webskin/home.png" />
                                </a>
                            </td>
                            <td>
                                <a href="login.html">
                                    <img src="webskin/logout-ic.png" />
                                </a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div> -->
    <section>
        <div class="container">
            <div class="row">
                <div align="center" class="col-md-12">
                    <br /><br /><br />
                </div>
            </div>
            <div class="row">
                <div align="center" class="col-md-12">
                    <h3 class="heading">Let's Explore !</h3>
                    <p style="color:#fff;">Please select the store which you would like to audit from the below list</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="search-div">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <select id="Select1">
                                    <option>Store 1</option>
                                    <option>Store 2</option>
                                    <option>Store 3</option>
                                    <option>Store 4</option>
                                </select>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <input type="text" id="fromDate" placeholder="Date From" />
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <input type="text" id="toDate" placeholder="Date To" />
                            </div>
                            <div align="right" class="col-md-12 col-sm-12 col-xs-12">
                                <button style=" margin-top: 5px;" type="submit" class="btn btn-success" onclick="generateReport();">Generate Report</button>
                                <button style=" margin-top: 5px;" type="button" class="btn btn-default">Clear</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">

                <!--Store box start-->
                <div align="center" class="col-md-12 col-sm-12">
                   <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </div><!--Store box end-->
            </div>

        </div>
    </section>
</body>
</html>