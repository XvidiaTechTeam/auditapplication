<%@include file="include.jsp"%>
<%@include file="header.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Completed Audits Page</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
<link href="${pageContext.request.contextPath}/css/bootstrap.min.css"
	rel="stylesheet" />
<link href="${pageContext.request.contextPath}/css/vowmee-design.css"
	rel="stylesheet" />
<link href="${pageContext.request.contextPath}/css/oversheet.css"
	rel="stylesheet" />
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
  <style type="text/css">.btn-link{
 border:none;
 outline:none;
 background:none;
 cursor:pointer;
 color:#0000EE;
 padding-left:10px;
 text-decoration:underline;
  text-align: center;
 font-family:inherit;
 font-size:inherit;
}
.btn-link:active{
 color:#FF0000;
}
</style>

</head>
<body id="style-4" class="intro-bg">
	 <section>
        <div class="container">
            <div class="row">
                <div align="center" class="col-md-12">
                    <br /><br /><br />
                </div>
            </div>
                          
                <div class="row">
                   <div align="center" class="col-md-12">
                   <br>
                       <p style="color:#fff;"><b>${firstname}'s completed audit details</b></p>
                   </div>
                </div>
                <div class="row searchAudit">

                    <!--Store box start-->
                    <c:if test="${not empty completedAuditList}">
                   
	                    <div align="center" class="col-md-4 col-sm-6" style="width:70%;style:align-center; margin-left: 15%">
	                        <div class="store-bx">
	                            <table class="card-tbl" border="1pt" >
	                           <tr style="background-color: rgba(0, 173, 255, 0.69); text-align: center;color:white;" >
	                           		<td nowrap style="width:20%; padding-top: 5pt; padding-bottom: 5pt; padding-left: 5pt; padding-right: 5pt;"><b>Action Performed</b></td>
	                           		<td nowrap style="width:40% ; padding-top: 5pt; padding-bottom: 5pt; padding-left: 5pt; padding-right: 5pt;"><b>New Value</b></td>
	                           		<td nowrap style="width:25 ; padding-top: 5pt; padding-bottom: 5pt; padding-left: 5pt; padding-right: 5pt;" ><b>Old Value</b></td>
	                           		<td nowrap style="width:15% ; padding-top: 5pt; padding-bottom: 5pt; padding-left: 5pt; padding-right: 5pt;"><b>Updated Date</b></td> 
	                           </tr>
	                            <c:forEach var="completedAudit" items="${completedAuditList}">
	                           <tr>
	                           		<td nowrap style="padding-top: 3pt; padding-bottom: 3pt; padding-left: 3pt; padding-right: 3pt;"><c:out value="${completedAudit.action}"></c:out></td>
	                           		<td nowrap style="padding-top: 3pt; padding-bottom: 3pt; padding-left: 3pt; padding-right: 3pt;"><c:out value="${completedAudit.newValue}"></c:out></td>
	                           		<td nowrap style="text-align: center; padding-top: 3pt; padding-bottom: 3pt;" ><c:out value="${completedAudit.oldValue}"></c:out></td>
	                           		<td nowrap style="text-align: center; padding-top: 3pt; padding-bottom: 3pt;" ><c:out value="${completedAudit.createdDate}"></c:out></td> 
	                           </tr>
	                           </c:forEach>
	                            </table>
	                        </div>
	                    </div>
                    
                    </c:if>
                    <!--Store box end-->
                </div>
	        </div>
    </section>

</body>
</html>