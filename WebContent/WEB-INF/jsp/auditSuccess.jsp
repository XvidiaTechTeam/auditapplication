<%@include file="include.jsp"%>
<%@include file="header.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Vowmee</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/css/vowmee-design.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/css/oversheet.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/css/hover.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/css/bootstrap-switch.css" rel="stylesheet" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" />
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap-switch.js"></script>
    <script src="${pageContext.request.contextPath}/js/init.js"></script>
</head>
<body style="overflow-x: hidden;  padding-top:20px;" class="intro-bg">
    <!--<a href="login.html">
        <img class="logout-ic" src="webskin/logout-ic.png" />
    </a>-->
    <section>
        <div class="container">
            <div class="row">
                <div align="center" class="col-md-12">
                    <br /><br /><br />
                </div>
            </div>

            <div class="row">
                <div align="center" class="col-md-12">
                    <h3 class="heading">You've successfully completed Audit for the Store : ${storeName}</h3>
                    <h3 class="heading">Audit Score - ${storeScore}%</h3>
                    <!-- <p style="color:#fff;">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.</p> -->
                </div>
            </div>
		</div>
        
    </section>
</body>
</html>