<%@include file="include.jsp"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Vowmee</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/css/vowmee-design.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/css/oversheet.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/css/hover.css" rel="stylesheet" />
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
      <style type="text/css">.btn-link{
 border:none;
 outline:none;
 background:none;
 cursor:pointer;
 color:#0000EE;
 padding-left:10px;
 text-decoration:underline;
  text-align: center;
 font-family:inherit;
 font-size:inherit;
}
.btn-link:active{
 color:#FF0000;
}
</style>
</head>
<body style="overflow-x: hidden;  padding-top:20px;" class="intro-bg">
  <%--  <form:form id="logOutForm" method="get" action="logout">
    <button type="submit" class="btn-link"><img class="logout-ic" src="webskin/logout-ic.png" /></button>
  
    </form:form> --%>
    <%@include file="header.jsp"%>
    
   
    <section>
        <div class="cotainer">
            <!-- <div class="row">
                <div align="center" class="col-md-12">
                    <img src="webskin/logo-big.png" />
                </div>
            </div> -->
            <div class="row">
                <div align="center" class="col-md-12">
                    <br /><br /><br /><br />
                </div>
            </div>
            <div class="boxes-outer">
                <br />
                <div class="row">
                    <div align="center" class="col-md-4">
                        <div class="box-div hvr-float-shadow">
                            <%-- <img class="box-div-img" src="webskin/user-icon.png" /> <br />
                            <h5>Welcome ! <br /> ${loggedInUser}</h5> --%>
                             <table class="bx-tbl">
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td>
                                                    <p>
                                                        Hi ${firstname} !<br />
                                                      <b>  Role : ${userRole}</b> <br />
                                                        Circle Name : ${userCircle} 
                                                         <c:if test="${sessionScope.auditor =='auditor'}">
                                                         <br/>Mobile : ${mobile}
                                                         
                                                         </c:if>

                                                    </p>
                                                </td>
                                                <td>
                                                    <img style="margin:15px 0px 0px 0px;" src="${pageContext.request.contextPath}/webskin/user-ic.png" />
                                                </td>
                                            </tr>
                                            
                                            <%-- <tr>
                                            
                                            <td><p><br/>${email}</p></td>
                                            </tr> --%>
                                        </table>
                                    </td>
                                </tr>
                                <%-- <tr><td><p><br/>Email : ${email}</p></td></tr> --%>
                                <tr>
                               <c:if test="${sessionScope.auditor =='auditor'}">
                                <td>
                                
                                <p><br/>Email : ${email}</p>
                               
                                </td>
                                 </c:if>
                                    <td style="height:138px;">
                                        <ul class="bx-link">
                                        <c:if test="${sessionScope.admin =='admin' || sessionScope.superAdmin =='superAdmin'}">
                                            <li><a href="usermanagement">User Management</a></li>
                                            <!-- <li><a href="signup">Add User</a></li> -->
                                            </c:if>
                                           <!--  <li><a href="usermanagement">User Profile</a></li> -->
                                           <!--  <li><a href="usermanagement">Edit User</a></li> -->
                                        </ul>
                                    </td>
                                </tr>

                            </table>
                            <div class="box-footer">
                                <div class="row">
                                    <div class="col-md-8 col-sm-8 col-xs-8">
                                        <h4>User info</h4>
                                    </div>
                                    <c:if test="${sessionScope.admin =='admin' || sessionScope.superAdmin =='superAdmin'}">
                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                        <a href="usermanagement">
                                            <img src="${pageContext.request.contextPath}/webskin/arrow.png" />
                                        </a>
                                    </div>
                                    </c:if>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                   
                     <c:if test="${sessionScope.auditor =='auditor'}">
                    <div align="center" class="col-md-4">
                        <div class="box-div hvr-float-shadow">
                            <!-- <img class="box-div-img" src="webskin/audit.png" /> -->
                            
                            <table class="bx-tbl">
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <img style="margin:15px 0px 0px 0px;" src="${pageContext.request.contextPath}/webskin/audit-ic.png" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height:138px;">
                                        <ul class="bx-link">
                                        <li><font color="red">In Progress (${sessionScope.getIncompletedCount})</font></li>
                                            <li><font color="green">Completed (${sessionScope.getCompletedCount})</font></li>
                                            <li><a href="previousAudits" style="text-decoration:underline!important; color:blue;">View Previous Audits</a></li>
                                        </ul>
                                    </td>
                                </tr>

                            </table>
                            
                            <div class="box-footer">
                                <div class="row">
                                    <div class="col-md-8 col-sm-8 col-xs-8">
                                        <h4>Audit</h4>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                        <a href="audit">
                                            <img src="${pageContext.request.contextPath}/webskin/arrow.png" />
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </c:if>
                     <c:if test="${sessionScope.management =='management'}">
                    <div align="center" class="col-md-4">
                        <div class="box-div hvr-float-shadow">
                            <!-- <img class="box-div-img" src="webskin/reports.png" /> -->
                             <table class="bx-tbl">
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <img style="margin:15px 0px 0px 0px;" src="${pageContext.request.contextPath}/webskin/reports-ic.png" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height:138px;">
                                        <ul class="bx-link">
												<li><font color="red">${storeMessage}</font></li>
												<li><a href="report">Store Wise Report</a></li>
												<li><a href="auditorReport">Auditor Wise Report</a></li>
												<li><a href="micoReport">MICO Wise Report</a></li>
												<li><a href="circleReport">Circle Wise Report</a></li>
											</ul>
                                    </td>
                                </tr>

                            </table>
                            <div class="box-footer">
                                <div class="row">
                                    <div class="col-md-8 col-sm-8 col-xs-8">
                                        <h4>Reports</h4>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                        <a href="report">
                                            <img src="${pageContext.request.contextPath}/webskin/arrow.png" />
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </c:if>
                </div>
            </div>
        </div>
    </section>
</body>
</html>
