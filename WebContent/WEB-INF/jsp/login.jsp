<%@include file="include.jsp"%>
<%-- <%@ include file="forgotPasswordSendEmail.jsp" %> --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Vowmee</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/css/vowmee-design.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/css/oversheet.css" rel="stylesheet" />
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
   <style type="text/css">
   
.btn-link{
 border:none;
 outline:none;
 background:none;
 cursor:pointer;
 color:#0000EE;
 padding-left:10px;
 text-decoration:underline;
  text-align: center;
 font-family:inherit;
 font-size:inherit;
}
.btn-link:active{
 color:#FF0000;
}
.btn-link1{
 border:none;
 outline:none;
 background:none;
 cursor:pointer;
 color:#0000EE;
 padding-right:20px;
 text-decoration:underline;
  text-align: center;
 font-family:inherit;
 font-size:inherit;
}
.btn-link1:active{
 color:#FF0000;
}

</style>

</head>
<body id="style-4" class="intro-bg">
    <div class="header-d">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-3 ">
                    <a href="..${pageContext.request.contextPath}/login"><img src="${pageContext.request.contextPath}/webskin/logo.png" /></a>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-8 ">
                    <h3 class="hidden-xs" style="text-align:center;">Vowmee Audit App</h3>
                    <h3 class="visible-xs" style="text-align: center; font-size: 17px; margin-top: 24px;">Vowmee Audit App</h3>
                </div>

                <div class="col-md-3 col-sm-3 col-xs-3 ">
                    
                </div>
            </div>
        </div>
    </div>

    <div style="" class="container">
		<div class="log-div">
			<br /><br /><br />
			<div class="row">
				<div class="col-md-12">
					<h3>
						Welcome to <br />Vowmee Audit App!
					</h3>
					<p>Login In with your credentials below</p>
				</div>
			</div>
			<!-- <div class="row">
				<div class="col-md-12">
			 -->		<!-- <br />
					<br />
					<br />
					<br />
					Nav tabs
					<ul style="width: 200px; margin: auto;" class="nav nav-tabs"
						role="tablist">
						<li style="width: 100%;" role="presentation" class="active"><a
							style="text-align: center;" href="login" aria-controls="home"
							role="tab" data-toggle="tab">Login</a></li>
						 <li style="width:50%;" role="presentation"><a href="signup" style="text-align:center;" role="tab">Register</a></li>
					</ul>
 -->
					<!-- Tab panes -->
					<!-- <div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="login">
							<div class="log-div">
						 -->	<!-- 	<div class="row">
									<div class="col-md-12">
										<h3>Hello !</h3>
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing
												elit, sed diam</p>
									</div>
								</div> -->
								<font color="red">${message}</font> <font color="red">${registered_message}</font>
								<font color="red">${yet_to_activate}</font>
								<font color="red">${invalidEmail}</font>

								<form:form id="loginForm" method="post" action="login"
									modelAttribute="loginBean">
									<div class="row">
										<div class="col-md-12">
											<!--<input type="text" />-->
											<div class="f-div">
												<table cellpadding="0" cellspacing="0" class="field-div">
													<tr>
														<td><form:input id="userName" name="name" path=""
																placeholder="Username / Email" /> <!-- <input type="text" placeholder="Username / Email" /> -->
														</td>
														<td><img src="${pageContext.request.contextPath}/webskin/user.png" /></td>
													</tr>
												</table>
											</div>

											<div class="f-div">
												<table cellpadding="0" cellspacing="0" class="field-div">
													<tr>
														<td><form:password id="password" name="password"
																path="" placeholder="Password" /> <!-- <input type="password" placeholder="Password" /> -->
														</td>
														<td><img src="${pageContext.request.contextPath}/webskin/lock.png" /></td>
													</tr>
												</table>
											</div>

										</div>
										<div align="center" class="col-md-12">
											<a style="color: #fff; text-align: center !important;"
												data-toggle="modal" data-target="#fpassword" href="#">Forgot
												password?</a>
											<div class="log-btn">
												<button type="submit" class="btn-link">
													<a>Login</a>
												</button>
											</div>
										</div>
									</div>
								</form:form>
						<!-- 	</div>

						</div>
					</div> -->
				<!-- </div>
			</div> -->
		</div>
	</div>
    
    <!-- Forgot Password -->
    <form:form id="forgotPasswordEmailForm" method="post" action="forgotPasswordEmail" modelAttribute="loginBean">
    <div class="modal fade" id="fpassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div style="max-width:320px;" class="modal-dialog" role="document">
            <div class="modal-content modal-forms">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Forgot Password</h4>
                </div>
                <div class="modal-body">
                    <p>Enter the email address associated with your account to reset your password</p>
                    <form:input id="username" name="name" path="" placeholder="E-Mail ID"/>
                    <!-- <input type="text" placeholder="E-Mail ID" /> -->

                </div>
                <div style="align-content: center;" align="center" class="modal-footer">
                    <div style="margin-top:0px;" class="log-btn">
                     <button type="submit" class="btn-link1"><a>Submit</a></button>
                        <!-- <a href="#">Submit</a> -->
                    </div>

                    <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                </div>
            </div>
        </div>
    </div>
</form:form>
</body>
</html>
