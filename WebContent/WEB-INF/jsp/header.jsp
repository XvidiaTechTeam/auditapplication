<div class="header-d">
        <div class="container">
            <div class="row">

                <div class="col-md-3 col-sm-3 col-xs-3 ">
                    <a href="${pageContext.request.contextPath}/login"><img src="${pageContext.request.contextPath}/webskin/logo.png" /></a>
                </div>


                <div class="col-md-6 col-sm-6 col-xs-6 ">
                    <h3 class="hidden-xs" style="text-align:center;">Vowmee Audit App </h3>
                    <h3 class="visible-xs" style="text-align: center; font-size: 17px; margin-top: 24px;">Vowmee Audit App</h3>
                </div>

                <div class="col-md-3 col-sm-3 col-xs-3 ">
                    <table align="right" class="h-tbl">
                        <tr>
                            <td>
                                <a href="homePage">
                                    <img src="${pageContext.request.contextPath}/webskin/home.png" />
                                </a>
                            </td>
                            <td>
                                <a href="logout">
                                    <img src="${pageContext.request.contextPath}/webskin/logout-ic.png" />
                                </a>
                            </td>
                        </tr>
                    </table>
                </div>

            </div>
        </div>
    </div>