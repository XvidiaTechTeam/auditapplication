<%@include file="include.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
<link href="${pageContext.request.contextPath}/css/bootstrap.min.css"
	rel="stylesheet" />
<link href="${pageContext.request.contextPath}/css/vowmee-design.css"
	rel="stylesheet" />
<link href="${pageContext.request.contextPath}/css/oversheet.css"
	rel="stylesheet" />
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	var data = ${auditPreviewJaon};
	console.log(data);
	for(var j = 0; j < data.length; j++)
	{
	 $("input[name='"+data[j].lookupId+"'][value='No']").attr('checked', true);
	 $("input[name='"+data[j].lookupId+"'][value='Yes']").attr('checked', true);
	}
	
});0
</script>
<body>

	<div class="row">
<div class="col-md-3">
			<form:form id="auditCategoryForm">
				<table>
					<tbody>
						<c:if test="${not empty auditCategoryList}">
							<c:forEach var="auditCategory1" items="${auditCategoryList}"
								varStatus="status">
								<tr>

									<td><a
										href="<c:url value='/previewAudit/${auditCategory1.audit_category}' />">
											${auditCategory1.audit_category} </a></td>

								</tr>
							</c:forEach>
						</c:if>
					</tbody>
				</table>
			</form:form>
		</div>
	<div class="col-md-3">
			<table>
				<tbody>
					<c:if test="${not empty auditPreviewList}">
						<c:forEach var="listValue" items="${auditPreviewList}">
							<tr>
								<td>${listValue.auditQuestion}</td>
								<td><input type="radio" name="${listValue.lookupId}"
									value="${listValue.auditYes}" disabled="disabled">Yes</td>
								<td><input type="radio" name="${listValue.lookupId}"
									value="${listValue.auditNo}" disabled="disabled">No</td>
							</tr>
						</c:forEach>
					</c:if>
				</tbody>
			</table>
		</div>
	</div>

	<div>
		<a href="submitAudit">Submit Audit</a>
	</div>
</body>
</html>