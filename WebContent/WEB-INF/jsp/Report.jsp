<%@include file="include.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>Vowmee</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/css/vowmee-design.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/css/oversheet.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/css/hover.css" rel="stylesheet" />
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
     <script src="${pageContext.request.contextPath}/js/highcharts.js"></script>
     <script src="https://code.highcharts.com/modules/exporting.js"></script>
     <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
    <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
   
    
</head>
<script type="text/javascript">
$(function() {
	 var categoryList = ${categoryList}
	 console.log(categoryList);
	 var data1 = ${data}
	 console.log(data1);
	 var storeName = ${storeName}
	 
	 
	 console.log(storeName);
	 var seriesData = [];
	 for(var i in data1){
		 var arr = [];
		 var val = {
				 name:i,
				 data:data1[i]
		 }
		 
		 seriesData.push(val);
		 console.log("arr " +arr);
	 }
	 console.log(seriesData);
	 var selectedStore = eval('${selectedStore}');
		$('#store_id option[value="'+selectedStore+'"]').prop('selected', true);
		var selectedMonth = eval('${selectedMonth}');
		$('#month option[value="'+selectedMonth+'"]').prop('selected', true);
	 $('#container').highcharts({
	        chart: {
	            type: 'column'
	        },
	        title: {
	            text: 'Score Card for '+ storeName
	        },
	        subtitle: {
	            text: ''
	        },
	        xAxis: {
	            categories: categoryList,
	            crosshair: true
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: 'Average Score %'
	            }
	        },
	        tooltip: {
	            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	                '<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>',
	            footerFormat: '</table>',
	            shared: true,
	            useHTML: true
	        },
	        plotOptions: {
	            column: {
	                pointPadding: 0.2,
	                borderWidth: 0
	            }
	        },
	        series: seriesData
	    });
	 
	 
	});

function generateReport(){
	var month = $("#month").val();
	if(month == 0 || month =="0"){
		alert("Please select mandatory fields");
		return false;
	}
	
}
function downloadReport(){
	var month = $("#month").val();
	if(month == 0 || month =="0"){
		alert("Please select mandatory fields");
		return false;
	}
}
</script>
<body style="overflow-x: hidden;  padding-top:20px;" class="intro-bg">
<%@include file="header.jsp"%>
    
    <!--<a href="login.html">
        <img class="logout-ic" src="webskin/logout-ic.png" />
    </a>-->
   <!--  <div class="header-d">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-3 ">
                    <a href="../index.html"><img src="webskin/logo.png" /></a>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6 ">
                    <h3 class="hidden-xs" style="text-align:center;">Vowmee Audit App</h3>
                    <h3 class="visible-xs" style="text-align: center; font-size: 17px; margin-top: 24px;">Vowmee Audit App</h3>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-3 ">
                    <table align="right" class="h-tbl">
                        <tr>
                            <td>
                                <a href="intro.html">
                                    <img src="webskin/home.png" />
                                </a>
                            </td>
                            <td>
                                <a href="login.html">
                                    <img src="webskin/logout-ic.png" />
                                </a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div> -->
    <section>
        <div class="container">
            <div class="row">
                <div align="center" class="col-md-12">
                    <br /><br /><br />
                </div>
            </div>
            <div class="row">
                <div align="center" class="col-md-12">
                 <%--  <c:if test="${not empty storeNameSelected}">
                <h3 class="heading">Store Name : ${storeNameSelected} Month : ${monthName}</h3>
                </c:if> --%>
                  
                    <p style="color:#fff;">Please select the store which you would like to audit from the below list</p>
                </div>
            </div>
            <form:form id="reportForm" method="post" action="reportGenerate"
									commandName="scoreCard">
            <div class="row">
                <div class="col-md-12">
                    <div class="search-div">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                               <form:select path="store_id">
												<form:option value="0" label="Select Store" />
												<form:options items="${storeList}" />
											</form:select>
                            </div>
                            <div class="col-md-6 col-sm-12">
                              	<form:select path="month">
									<form:option value="0" label="Select Month" />
									<form:option value="1" label="Jan" />
									<form:option value="2" label="Feb" />
									<form:option value="3" label="Mar" />
									<form:option value="4" label="Apr" />
									<form:option value="5" label="May" />
									<form:option value="6" label="Jun" />
									<form:option value="7" label="Jul" />
									<form:option value="8" label="Aug" />
									<form:option value="9" label="Sep" />
									<form:option value="10" label="Oct" />
									<form:option value="11" label="Nov" />
									<form:option value="12" label="Dec" />
									<%-- <form:options items="${storeList}" /> --%>
								</form:select>
                            </div>
                            <div align="right" class="col-md-12 col-sm-12 col-xs-12">
                            <button style=" margin-top: 5px;" type="submit" class="btn btn-success" onclick="return generateReport()">Generate Report</button>
                              <button style=" margin-top: 5px;" type="submit" class="btn btn-success" onclick="return downloadReport()" name="download">Download Report</button>
                                <!-- <button style=" margin-top: 5px;" type="button" class="btn btn-default">Clear</button> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </form:form>
            <div class="row">

                <!--Store box start-->
                <div align="center" class="col-md-12 col-sm-12">
                <c:if test="${not empty data}">
                   <!--  <h3 class="heading">Reports Placeholder</h3> -->
                     <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                     </c:if>
                </div><!--Store box end-->
            </div>

        </div>
    </section>
</body>
</html>