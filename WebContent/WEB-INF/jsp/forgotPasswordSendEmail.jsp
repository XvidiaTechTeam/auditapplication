<%@include file="include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Vowmee</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/css/vowmee-design.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/css/oversheet.css" rel="stylesheet" />
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
   <style type="text/css">.btn-link{
 border:none;
 outline:none;
 background:none;
 cursor:pointer;
 color:#0000EE;
 padding-left:15px;
 text-decoration:underline;
  text-align: center;
 font-family:inherit;
 font-size:inherit;
}
.btn-link:active{
 color:#FF0000;
}</style>

</head>
<body id="style-4" class="intro-bg">
     <div class="header-d">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-3 ">
                    <a href="..${pageContext.request.contextPath}/login"><img src="${pageContext.request.contextPath}/webskin/logo.png" /></a>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-8 ">
                    <h3 class="hidden-xs" style="text-align:center;">Vowmee Audit App</h3>
                    <h3 class="visible-xs" style="text-align: center; font-size: 17px; margin-top: 24px;">Vowmee Audit App</h3>
                </div>

                <div class="col-md-3 col-sm-3 col-xs-3 ">
                    
                </div>
            </div>
        </div>
    </div>


    <!-- Forgot Password -->
    <form:form id="forgotPasswordEmailForm" method="post" action="forgotPasswordEmail">
    <div class="modal fade" id="fpassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div style="max-width:320px;" class="modal-dialog" role="document">
            <div class="modal-content modal-forms">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Forgot Password</h4>
                </div>
                <div class="modal-body">
                    <p>Enter the email address associated with your account to reset your password</p>
                    <form:input id="username" name="name" path="" placeholder="E-Mail ID"/>
                    <!-- <input type="text" placeholder="E-Mail ID" /> -->

                </div>
                <div style="align-content: center;" align="center" class="modal-footer">
                    <div style="margin-top:0px;" class="log-btn">
                     <button type="submit" class="btn-link"><a>Submit</a></button>
                        <!-- <a href="#">Submit</a> -->
                    </div>

                    <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                </div>
            </div>
        </div>
    </div>
</form:form>
</body>
</html>
