<%@include file="include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html><head>
    <title>Vowmee</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/css/vowmee-design.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/css/oversheet.css" rel="stylesheet" />
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <style type="text/css">
label {display:inline-block; width:250px;}
</style>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js" type="text/javascript"></script>
    <style type="text/css">.btn-link{
 border:none;
 outline:none;
 background:none;
 cursor:pointer;
 color:#0000EE;
 padding-left:15px;
 text-decoration:underline;
  text-align: center;
 font-family:inherit;
 font-size:inherit;
}
.btn-link:active{
 color:#FF0000;
}</style>

</head>
<body id="style-4" class="intro-bg">
  <%--   <div class="header-d">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-7">
                    <a href="..${pageContext.request.contextPath}/vowmee.jsp"><img src="${pageContext.request.contextPath}/webskin/logo.png" /></a>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-5 ">
                    <h3 class="hidden-xs" style="text-align:right;">Vowmee Audit App</h3>
                </div>
            </div>
        </div>
    </div>
 --%>
 
  <%@include file="header.jsp"%>
	<div style="" class="container">
		<div class="log-div">
			<br />
			<br />
			<br />
			<div class="row">
				<div class="col-md-12">
					<h3>
						Welcome to <br />Vowmee Audit App!
					</h3>
					<p>Create New User</p>
				</div>
			</div>

			<font color="red">${unregistered}</font>
			<form:form id="signupForm" method="post" action="signup"
				modelAttribute="editUserBean">
				<div class="row">
					<div class="col-md-12">
						<!--<input type="text" />-->
						<div class="f-div">
							<table cellpadding="0" cellspacing="0" class="field-div">
								<tr>
									<td><form:input id="firstName" name="firstName"  path="" placeholder="First Name" onkeyup="this.value=this.value.replace(/[^a-z]/g,'');"/>
									</td>
									<td><img src="${pageContext.request.contextPath}/webskin/user.png" /></td>
								</tr>
							</table>
						</div>
						<div class="f-div">
							<table cellpadding="0" cellspacing="0" class="field-div">
								<tr>
									<td><form:input id="lastName" name="lastName" path="" placeholder="Last Name"/>
									</td>
									<td><img src="${pageContext.request.contextPath}/webskin/user.png" /></td>
								</tr>
							</table>
						</div>
						<div class="f-div">
							<table cellpadding="0" cellspacing="0" class="field-div">
								<tr>
									<td><form:input id="username" class="required email" name="name" value="${signupBean.name}" path="" placeholder="Username"/>
									</td>
									<td><img src="${pageContext.request.contextPath}/webskin/user.png" /></td>
								</tr>
							</table>
						</div>

						<div class="f-div">
							<table cellpadding="0" cellspacing="0" class="field-div">
								<tr>
									<td> <form:input type="number" id="mobile" placeholder="Mobile"  name="mobile" value="${signupBean.mobile}" path=""/><!-- <input type="password" placeholder="Password" /> -->
									</td><td></td>
									<%-- <td><img src="${pageContext.request.contextPath}/webskin/lock.png" /></td> --%>
								</tr>
							</table>
						</div>

						<div class="f-div">
							<table cellpadding="0" cellspacing="0" class="field-div">
								<tr>
									<td><form:select path="user_type" style="width: 288px;height: 34px;">
												<form:option value="0" label="Select Type" />
												<form:options items="${typeList}"/>
											</form:select> <!-- <input type="password" placeholder="Password" /> -->
									</td>
									<td></td>
									<!-- <td><img src="webskin/lock.png" /></td> -->
								</tr>
							</table>
						</div>


						<div class="f-div">
							<table cellpadding="0" cellspacing="0" class="field-div">
								<tr>
									<td><form:select path="user_role" style="width: 288px;height: 34px;">
												<form:option value="0" label="Select Role" />
												<form:options items="${roleList}" />
											</form:select> <!-- <input type="password" placeholder="Password" /> -->
									</td>
									<td></td>
									<!-- <td><img src="webskin/lock.png" /></td> -->
								</tr>
							</table>
						</div>
						
						<div class="f-div">
							<table cellpadding="0" cellspacing="0" class="field-div">
								<tr>
									<td><form:select path="user_circle" style="width: 288px;height: 34px;">
												<form:option value="0" label="Select Circle" />
												<form:options items="${circleList}" />
											</form:select> <!-- <input type="password" placeholder="Password" /> -->
									</td>
									<!-- <td><img src="webskin/lock.png" /></td> -->
								</tr>
							</table>
						</div>
						
						<div class="f-div">
							<table cellpadding="0" cellspacing="0" class="field-div">
								<tr>
									<td><form:select path="user_type" style="width: 288px;height: 34px;">
												<form:option value="0" label="Select Type" />
												<form:options items="${typeList}"/>
											</form:select> <!-- <input type="password" placeholder="Password" /> -->
									</td>
									<td></td>
									<!-- <td><img src="webskin/lock.png" /></td> -->
								</tr>
							</table>
						</div>
						
						<div class="f-div">
							<table cellpadding="0" cellspacing="0" class="field-div">
								<tr>
									<td><form:select path="user_status" style="width: 288px;height: 34px;">
												<form:option value="0" label="Select Status" />
												<form:options items="${statusList}" />
											</form:select> <!-- <input type="password" placeholder="Password" /> -->
									</td>
									<td></td>
									<!-- <td><img src="webskin/lock.png" /></td> -->
								</tr>
							</table>
						</div>

						<div class="f-div">
							<table cellpadding="0" cellspacing="0" class="field-div">
								<tr>
									<td>  <form:password id="password" placeholder="Password" name="password" path=""/> <!-- <input type="password" placeholder="Password" /> -->
									</td>
									<td><img src="webskin/lock.png" /></td>
								</tr>
							</table>
						</div>


						<div class="f-div">
							<table cellpadding="0" cellspacing="0" class="field-div">
								<tr>
									<td><form:password id="confirmPassword" placeholder="ConfirmPassword" name="confirmPassword" path="" /> <!-- <input type="password" placeholder="Password" /> -->
									</td>
									<td><img src="webskin/lock.png" /></td>
								</tr>
							</table>
						</div>

						<div align="center" class="col-md-12">
							
							<div class="log-btn">
								<button type="submit" class="btn-link">
									<a>Create</a>
								</button>
							</div>
						</div>

					</div>


				</div>
			</form:form>
			<!-- 	</div>

						</div>
					</div> -->
			<!-- </div>
			</div> -->
		</div>
	</div>


	<%-- <div style="" class="container">
        <div class="frm-cnt">
            <div class="row">
                <div class="col-md-12">
                    <br /><br /><br /><br />
                    <!-- Nav tabs -->
                    <ul style="width:200px; margin:auto;" class="nav nav-tabs" role="tablist">
                        <li style="width:50%;" role="presentation"><a style="text-align:center;" href="login" role="tab">Login</a></li>
                        <li style="width:50%;" role="presentation" class="active"><a href="register" style="text-align:center;" aria-controls="profile" role="tab" data-toggle="tab">Register</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <font color="red">${unregistered}</font>
                  <form:form id="signupForm" method="post" action="signup" modelAttribute="signupBean">  
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="register">
                            <div class="row reg-div">
                                <div class="col-md-12">
                                    <h3>Let's Explore !</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam </p>
                                    <br />
                                </div>
                                <div class="row">
                                <div class="col-md-3">
                                </div>
                                    <div class="col-md-6">
                                    <form:input id="username" class="required email" name="name" value="${signupBean.name}" path="" placeholder="Username"/>
                                      <!--   <input type="text" placeholder="Username" /> -->
                                    </div>
                                  <!--   <div class="col-md-6">
                                        <input type="text" placeholder="Email" />
                                    </div> -->
                                </div>

                                <div class="row">
                                <div class="col-md-3">
                                </div>
                                    <div class="col-md-6">
                                    <form:input type="number" id="mobile" placeholder="Mobile" name="mobile" value="${signupBean.mobile}" path="" style="margin-bottom: 6px;"/>
                                       <!--  <input type="text" placeholder="First name" /> -->
                                    </div>
                                   <!--  <div class="col-md-6">
                                        <input type="text" placeholder="last name" />
                                    </div> -->
                                </div>

                                <div class="row">
                                <div class="col-md-3">
                                </div>
                                    <div class="col-md-6">
                                    <form:password id="password" placeholder="Password" name="password" path="" style="margin-bottom: 6px;"/>
                                       <!--  <input type="text" placeholder="Street address" /> -->
                                    </div>
                                   <!--  <div class="col-md-6">
                                        <input type="text" placeholder="City" />
                                    </div> -->
                                </div>

                                <div class="row">
                                <div class="col-md-3">
                                </div>
                                    <div class="col-md-6">
                                    <form:password id="confirmPassword" placeholder="ConfirmPassword" name="confirmPassword" path="" />
                                        <!-- <input type="text" placeholder="State" /> -->
                                    </div>
                                   <!--  <div class="col-md-6">
                                        <input type="text" placeholder="Postal" />
                                    </div> -->
                                </div>

                               <!--  <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" placeholder="Country" />
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" placeholder="Phone" />
                                    </div>
                                </div> -->
                                
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="log-btn">
                                         <button type="submit" class="btn-link"><a>Register</a></button>
                                      <!--   <button type="submit" class="btn-link"><a>Register</a></button>
 -->                                            <!-- <a href="#">Register</a> -->
                                            <br /><br /><br />
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    </form:form>
                </div>
            </div>
        </div>
    </div> --%>
  
  <script type="text/javascript">
  $( "#signupForm" ).validate({
	  rules: {
		  password: {
	          required: true,
	          minlength: 8,
	          mypassword:true
	      },
	    confirmPassword: {
	      equalTo: "#password"
	    },
	    mobile:{
	    	maxlength: 10
	    }
	  }
	});  
	$.validator.addMethod('mypassword', function(value, element) {
	    return this.optional(element) || (value.match(/[a-zA-Z]/) && value.match(/[0-9]/));
	},
	'Password must contain at least one numeric and one alphabetic character.');

  </script>
</body>
</html>
