<%@include file="include.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Vowmee</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
<script src="${pageContext.request.contextPath}/js/chosen.jquery.min.js"></script>
 <!--  <script type="text/javascript">
    var config = {
      '.chosen-select'           : {},
      '.chosen-select-deselect'  : {allow_single_deselect:true},
      '.chosen-select-no-single' : {disable_search_threshold:10},
      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chosen-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }
  </script> -->
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/css/vowmee-design.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/css/oversheet.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/css/hover.css" rel="stylesheet" />
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <link href="${pageContext.request.contextPath}/css/chosen.min.css" rel="stylesheet" />
      <style type="text/css">
label {display:inline-block; width:150px; color:red}
</style>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js" type="text/javascript"></script>
  
				<script type="text/javascript">
				function setValue()
				
					{
					document.getElementById("newfield").value = $('#selectChoice1').val();
					}
				
				 function changeFunc()
				 {
					 if($('#selectChoice1').val()== "CreateStore"){
					 $(".storeDiv").show();
					 $(".userDiv").hide();
					 }
					 
					 if($('#selectChoice1').val()== "CreateAudit"){
						 $(".auditDiv").show();
					     $(".userDiv").hide();
					 }
					 if($('#selectChoice1').val()== "CreateUser"){
						 $(".userDiv").show();
					     $(".auditDiv").hide();
					     $(".storeDiv").hide();
					 }
				 }
				</script>
    
     <script>
     
     $(document).ready(function () {
    	
         $("#cuser").click(function () {
        	 document.getElementById("newfield").value=$('#selectChoice1').val();
        	 $("div.cuserfrm").show();
        	if($('#selectChoice1').val()== "CreateUser")
        		{
        		
        		$("div.cuserform").show();
        	$("div.createstorefrm").hide();
        	$("div.createauditfrm").hide();
        
        		}
        	if($('#selectChoice1').val()== "CreateStore")
    		{
        		
    	$("div.createstorefrm").show();
    	$("div.cuserform").hide();
    	$("div.createauditfrm").hide();
    
    		}
        	if($('#selectChoice1').val()== "CreateAudit")
    		{
        		
        		$("div.createstorefrm").hide();
            	$("div.cuserform").hide();
            	$("div.createauditfrm").show();
    		}
        	
        	/* if($('#selectChoice1').val()== "CreateStore")
        	$("#createstorefrm").slideToggle("slow"); */
         });
         
         $('#search').keyup(function(){
         	   var valThis = $(this).val().toLowerCase();
         	   console.log(valThis);
         	    if(valThis == ""){
         	        $('.searchUser > div').show();           
         	    } else {
         	        $('.searchUser > div').each(function(){
         	            var text = $(this).text().toLowerCase();
         	            console.log(text);
         	            (text.indexOf(valThis) >= 0) ? $(this).show() : $(this).hide();
         	        });
         	   };
         	});
         
   window.saveStore = function (divuserStore,diveditForm,userId,divselectedUserStore,divremoveSelectedUserStore)
   {
        	 $(diveditForm).validate({
        		  rules: {
        			 mobile:{
        		    	maxlength: 10,
        		    	minlength:10
        		    }
        		  }
        		});  
        	 
        	 
        	 
        	 $.getJSON('${pageContext.request.contextPath}/storesUserId', {
        		 userId : userId,
					ajax : 'true'
				}, function(data) {
					console.log("success");
					console.log(data);
					var select = $(divselectedUserStore);
					var removeSelected = $(divremoveSelectedUserStore);
			        select.find('option').remove();
			        removeSelected.find('option').remove();
					var html = '';
					var len = data.length;
					for ( var i = 0; i < len; i++) {
						html += '<option value="' + data[i].id + '">'
								+ data[i].store_name + '</option>';
					}
					html += '</option>';
	 
					$(divselectedUserStore).html(html);
					$(divremoveSelectedUserStore).html(html);
				});
        	
     	};
     	
     <!-- added by isha -->
     window.saveCircle = function (divuserCircle,divselectedUserCircle,divremoveSelectedUserCircle,userId)
     { 
          	 
          	 $.getJSON('${pageContext.request.contextPath}/getAllCircle', {
  					ajax : 'true'
  				}, function(data) {
  					console.log("success");
  					console.log(data);
  					var select = $(divuserCircle);
  					
  			        select.find('option').remove();
  					var html = '';
  					var len = data.length;
  					for ( var i = 0; i < len; i++) {
  						html += '<option value="' + data[i].id + '">'
  								+ data[i].circle_name + '</option>';
  					}
  					html += '</option>';
  	 
  					$(divuserCircle).html(html);
  				});
          	 
          	 $.getJSON('${pageContext.request.contextPath}/getCircle', {
          		 userId : userId,
  					ajax : 'true'
  				}, function(data) {
  					console.log("success");
  					console.log(data);
  					var select = $(divselectedUserCircle);
  					var removeSelected = $(divremoveSelectedUserCircle);
  			        select.find('option').remove();
  			        removeSelected.find('option').remove();
  					var html = '';
  					var len = data.length;
  					for ( var i = 0; i < len; i++) {
  						html += '<option value="' + data[i].id + '">'
  								+ data[i].circle_name + '</option>';
  					}
  					html += '</option>';
  	 
  					$(divselectedUserCircle).html(html);
  					$(divremoveSelectedUserCircle).html(html);
  				});
          	 
       	}
     <!-- closed by isha-->
         
         $('#addUserCircle').change(
        			function() {
        				
        				$.getJSON('${pageContext.request.contextPath}/stores', {
        					stateName : $(this).val(),
        					ajax : 'true'
        				}, function(data) {
        					console.log("success");
        					console.log(data);
        					var select = $('#addUserStore');
        			        select.find('option').remove();
        					var html = '';
        					var len = data.length;
        					for ( var i = 0; i < len; i++) {
        						html += '<option value="' + data[i].id + '">'
        								+ data[i].store_name + '</option>';
        					}
        					html += '</option>';
        	 
        					$('#addUserStore').html(html);
        				});
        			});
         
         $('#addUserRole').change(
     			function() {
     				/* var skillsSelect = document.getElementById("newSkill");
     				var selectedText = skillsSelect.options[skillsSelect.selectedIndex].text; */
     				var userRoleAdd = document.getElementById("addUserRole");
     				console.log(userRoleAdd);
     				var selectRoleText = userRoleAdd.options[userRoleAdd.selectedIndex].text;
     				var service_delivery="Service Delivery Head";
     				var service_excellence="Service Excellence Head";
     				var circle_training="Circle Training";
     				if(service_delivery==selectRoleText || service_excellence==selectRoleText ||circle_training==selectRoleText){
     					$("#adduserStoreForHide").hide();
     				}
     				/* $.getJSON('/Vowmee_Audit/stores', {
     					stateName : $(this).val(),
     					ajax : 'true'
     				}, function(data) {
     					console.log("success");
     					console.log(data);
     					var select = $('#addUserStore');
     			        select.find('option').remove();
     					var html = '';
     					var len = data.length;
     					for ( var i = 0; i < len; i++) {
     						html += '<option value="' + data[i].id + '">'
     								+ data[i].store_name + '</option>';
     					}
     					html += '</option>';
     	 
     					$('#addUserStore').html(html);
     				}); */
     				
     				
     			});
      
     });
  
        function theFunction (id,firstName,lastName,mobile,role,status,userRoleName) {
        	console.log("hell");
        	console.log(id);
        	console.log(firstName);
        	var divID = '#'+id;
        	var divName = '#value'+id;
        	var divfirstName = '#firstName'+id;
        	var divlastName = '#lastName'+id;
        	var divmobile = '#mobile'+id;
        	//var divuserType = '#userType'+id;
        	var divuserRole = '#userRole'+id;
        	var divuserStatus = '#userStatus'+id;
        	var divuserCircle='#userCircle'+id;
        	var divuserStore ='#userStore'+id;
        	var diveditForm ='#editForm'+id;
        	var divselectedUserStore ='#selectedUserStore'+id;
        	var divremoveSelectedUserStore ='#removeUserStore'+id;
        	var divselectedUserCircle='#selectedUserCircle'+id;
        	var divremoveSelectedUserCircle='#removeUserCircle'+id;
        	console.log(mobile);
        	//console.log(divuserType);
        	//console.log(type);
        	console.log(status);
        
        	
        	console.log(divID);
        	console.log(divName);
        	 $(divID).toggle("slow");
        	 $(divName).hide();
        	 $(divfirstName).val(firstName);
        	 $(divlastName).val(lastName);
        	 $(divmobile).val(mobile);
        	
        	
        	// $(divuserType).text = type;
        	//$(divuserType).val(1);
        	
        	  $(divuserRole).val(role);
        	  $(divuserStatus).val(status); 
        	  saveCircle(divuserCircle,divselectedUserCircle,divremoveSelectedUserCircle,id)
        	  saveStore(divuserStore,diveditForm,id,divselectedUserStore,divremoveSelectedUserStore);
        	  $(divuserCircle).change(
        			function() {
        				saveStore(divuserStore,diveditForm,id,divselectedUserStore,divremoveSelectedUserStore);
        			});
        	 /*  var service_delivery="Service Delivery Head";
				var service_excellence="Service Excellence Head";
				var circle_training="Circle Training";
				if(service_delivery==userRoleName || service_excellence==userRoleName ||circle_training==userRoleName){
					$(userStoreHide).hide();
					$(selectedUserStoreExisted).hide();
					$(divuserStore).hide();
					$(divselectedUserStore).hide();
				}else{
					$(userStoreHide).show();
					$(selectedUserStoreExisted).show();
					saveStore(circle,divuserStore,diveditForm,id,divselectedUserStore);
		        	  $(divuserCircle).change(
		        			function() {
		        				saveStore($(this).val(),divuserStore,diveditForm,id,divselectedUserStore);
		        			});
		        	 
				} */
        	 //var selectRoleTextUpdate = userRoleUpdate.options[role].text;
        	
        	// $(divName).hide();
       
        }
        
        function theFunctionStore (id,storeName,status,circle) {
        	
        	console.log("hell");
        	console.log(id);
        	alert("isha");
        	alert(status);
           
        	var divID = '#'+id;
        	var divName = '#value'+id;
        	var divstoreName = '#storeName'+id;
        	var divstoreCircle = '#storeCircle'+id;
        	//var divuserType = '#userType'+id;
            var divstoreStatus='#storeStatus'+id;
        	var diveditStoreForm ='#editStoreForm'+id;
        	
        	 $(divID).toggle("slow");
        	 $(divName).hide();
        	 $(divstoreName).val(storeName);
        	/*  $(divstoreCircle).val(circle); */
        	 //alert(document.getElementById('storeCircle'+id).value);
//         	var box= document.getElementById('storeCircle'+id);
//         	alert(box)
        	/* 
            $('storeCircle'+id).append('<option >'+circle+'</option>');
        	 $(divstoreStatus).val(status); */
        	
        	 
        	
       
        }
        
        function hideDisplay (id) {
        	console.log("hideDisplay(");
        	console.log(id);
        	var divID = '#'+id;
        	var divName = '#value'+id;
        	
        	
        	 $(divName).toggle("slow");
        	 $(divID).hide();
        	 
       
        }
        
        function hideCreate () {
        	 $(cuserfrm).hide();
        }

        function hideCreateStore () {
        	
        	// $(divName).toggle("slow");
        	 $(createstorefrm).hide();
        	 
       
        }
       
      
    </script>

<style type="text/css">
.btn-link {
	border: none;
	outline: none;
	background: none;
	cursor: pointer;
	color: #0000EE;
	padding-left: 15px;
	text-decoration: underline;
	text-align: center;
	font-family: inherit;
	font-size: inherit;
}

.btn-link:active {
	color: #FF0000;
}
.image-upload > input
{
    display: none;
}

.image-upload img
{
   
    cursor: pointer;
}
</style>
<script type="text/javascript">
function codeAddress() {
	var urlPost = "${pageContext.request.contextPath}/circleList";
	$.ajax({
         url: urlPost,
         type: "GET",
         contentType: "application/json",
		 async:false,
		 success: function(data)
         {
			 
			  
   var options = "";
   console.log(data.length);
   
   for (var i=0; i<data.length;i++){
  
   //console.log(pieces[0]+" "+pieces[1]);
   options+= "<option value='"+data[i].id+"'>"+data[i].circle_name+"</option>"
   }
   $('#addCircle').html(options);
   $("#addCircle").trigger("chosen:updated");
		 },
		 error: function (xhr, errorType, exception) {
        
            
            var result = "errorType: " + errorType + " exception :" + exception;
                  console.log(result);
				 alert("Failed to get Device List. Please try after some time.");
            
        }
});
}
window.onload = codeAddress;
</script>

</head>
<body id="style-4" class="intro-bg">

	<%@include file="header.jsp"%>
<section>
        <div class="container">
            <div class="row">
                <div align="center" class="col-md-12">
                    <br /><br /><br />
                </div>
            </div>

            <div class="row">
                <div align="center" class="col-md-12">
                    <h3 class="heading">User Profile Manager !</h3>
                </div>
            </div>
            <div class="row">
                <div align="left" class="col-md-12">
                    <font color="red">${unregistered}</font>
                </div>
            </div>
         <form:form id="filterForm" method="post" action="filterMonth" modelAttribute="selectTimePeriod">
             <div class="row">
             <div class="col-md-12">
                    <div class="search-div">
                       <form:select path="filterTimeForSelectUser" id="selectTime" multiple="false" style="width:843px; margin-right:60px">
												<%-- <form:options items="${circleList}" /> --%>
						<form:option value="0">Select TimePeriod</form:option>
						<form:option value="Last 7 Days">Last 7 Days</form:option>
						<form:option value="Last 1 month">Last 1 month</form:option>
						<form:option value="Last 2 months">Last 2 months</form:option>
						<form:option value="All">All</form:option>
						
						</form:select>
						 <button type="submit" class="btn btn-primary">Search</button>
                    </div>
                <!-- <div class="col-md-4 creat-user">
                    <div style="padding: 12px;">
                        <h4>Create User</h4>
                        <button type="submit" class="btn btn-primary">Search</button>
                    </div>
                </div> -->
                </div>
            </div>
            </form:form>
            <div class="row">
                <div class="col-md-8">
                    <div class="search-div">
                        <input type="text" placeholder="Search by user name" id="search"/>
                    </div>
                </div>
                <div class="col-md-4 creat-user">
                    <div style="padding: 12px;" class="c-usercard-div">
                        <!--<h4>Create User</h4>-->
                       <form:form method="POST" action="uploadFile"
								enctype="multipart/form-data" onsubmit="setValue()">
                        <table class="create-user-tbl">
                            <tr>
                                <td>
                                  <select  id="selectChoice1" style="width:120px" onchange="changeFunc()">
												<%-- <form:options items="${circleList}" /> --%>
						<option value="CreateUser">Create User</option>
						<option value="CreateStore">Create Store</option>
						<!-- <option value="CreateAudit">Create Audit</option> -->
						
						
						</select>
						  <input type='hidden' id='newfield' name='newfield' value=''/>
                                </td>
                                <td>
                                    <img class="uploadimg" id="cuser" style="cursor:pointer; margin-left:10px" src="webskin/add-user.png" />
                                </td>
							
								<td>
                               
								<span class="image-upload">
								<label for="file-input" style="width: 123px;"> <img class="uploadimg"
										style="float: right;"
										src="${pageContext.request.contextPath}/webskin/excel-upload.png" />
									</label> <input id="file-input" type="file" name="file" />
								
								
								</span>	
							
                                </td>
                                
                                <td ><button type="submit" class="btn btn-primary">Save</button></td>
                            </tr>
                        </table>
                        </form:form>
                    </div>
                </div>
            </div>
            


 <!--Ceate User-->
<!--  <div id="createstorefrm">
                <div class="c-usercard-div">
                    <div class="row">
                    
			 

					
				</div>
                </div>
            </div> -->
            <div class="cuserfrm">
                <div class="c-usercard-div">
                    <div class="row">
                    <div class="createstorefrm">
                    <form:form id="signupForm" method="post" action="signupforStore" modelAttribute="editStoreBean">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <h5 style="font-weight:bold;">Create Store</h5>
                        </div>
								
					<div style="border-bottom:1px #ccc dotted; margin:10px 0px; " class="col-md-12">
                        </div>
                       
						<div class="col-md-6">
									
										<%-- <span>First Name</span><br /> <input type="text" id="firstName${userValue.id}"/> --%>
										<span>Store Name</span><br /><form:input id="addStoreName" name="store_name"  path="store_name" placeholder="Store Name" onkeyup="this.value=this.value.replace(/[^a-zA-Z ]/g,'');"/>
									</div>
									<div class="col-md-6">
									<span>Store Code</span><br /><form:input id="addStoreId" name="store_code"  path="store_code" placeholder="Store Code" onkeyup="this.value=this.value.replace(/[^a-zA-Z ]/g,'');"/>
									
									</div>
									<div class="col-md-6">
									<span>Location</span><br />
										<form:select path="location" id="addlocation" style="width: 288px;height: 34px;">
												<form:option value="0" label="Select Location" />
												<form:options items="${locationList}" />
											</form:select>
									</div>
									<div class="col-md-6">
									<span>User Circle</span><br />
										<form:select path="circle" id="addUserCircle"  style="width: 288px;height: 34px;">
												<form:option value="0" label="Select Circle" />
												<form:options items="${circleList}" />
											</form:select>
									</div>
									
									
									
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-default" onclick="return hideCreateStore();">Cancel</button>
                        </div>
                        </form:form>
                    </div>
                    <div class="createauditfrm">
                    <form:form id="signupForm" method="post" action="signupforAudit" modelAttribute="editAuditBean">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <h5 style="font-weight:bold;">Create Category/Questions</h5>
                        </div>
								
					<div style="border-bottom:1px #ccc dotted; margin:10px 0px; " class="col-md-12">
                        </div>
                       
						
									<div class="col-md-6">
									<span>Category</span><br />
										<form:select path="audit_category" id="addcategory" style="width: 288px;height: 34px;">
												<form:option value="0" label="Select Category" />
												<form:options items="${auditList}" />
											</form:select>
									</div>
									<div class="col-md-6">
									
										<%-- <span>First Name</span><br /> <input type="text" id="firstName${userValue.id}"/> --%>
										<span>Question</span><br /><form:input id="addQuestion" name="audit_name"  path="audit_name" placeholder="Question" onkeyup="this.value=this.value.replace(/[^a-zA-Z ]/g,'');"/>
									</div>
									<div class="col-md-6">
									<span>User Circle</span><br />
										<form:select path="circle" id="addUserCircle" style="width: 288px;height: 34px;">
												<form:option value="0" label="Select Circle" />
												<form:options items="${circleList}" />
											</form:select>
									</div>
									<div class="col-md-6">
									<span>Mandatory</span><br />
									<form:select path="flag" id="flag"  style="width:288px; margin-right:34px">
												<%-- <form:options items="${circleList}" /> --%>
						<form:option value="Y">Yes</form:option>
						<form:option value="N">No</form:option>
					
						</form:select>
									</div>
									
									
									
									
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-default" onclick="return hideCreateStore();">Cancel</button>
                        </div>
                        </form:form>
                    </div>
                    <div class="cuserform">
			 <form:form id="signupForm" method="post" action="signup" modelAttribute="editUserBean">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <h5 style="font-weight:bold;">Create User</h5>
                        </div>
								
					<div style="border-bottom:1px #ccc dotted; margin:10px 0px; " class="col-md-12">
                        </div>
                       
						<div class="col-md-6">
									
										<%-- <span>First Name</span><br /> <input type="text" id="firstName${userValue.id}"/> --%>
										<span>First Name</span><br /><form:input id="addFirstName" name="firstName"  path="" placeholder="First Name" required="required" onkeyup="this.value=this.value.replace(/[^a-zA-Z ]/g,'');"/>
									</div>
									<div class="col-md-6">
									<span>Last Name</span><br /><form:input id="addLastName" name="lastName"  path="" placeholder="Last Name" required="required" onkeyup="this.value=this.value.replace(/[^a-zA-Z ]/g,'');"/>
										<%-- <span>Last Name</span><br /> <input type="text" id="lastName${userValue.id}"/> --%>
									</div>
									<div class="col-md-6">
									<span>Email</span><br /><form:input id="username" required="required" class="required email" name="name" path="" placeholder="Email"/>
										<%-- <span>Last Name</span><br /> <input type="text" id="lastName${userValue.id}"/> --%>
									</div>
									
									
									<div class="col-md-6">
										<span>Mobile</span><br />  <form:input type="number" id="addMobile" placeholder="Mobile"  name="mobile" value="${signupBean.mobile}" path=""/>
										<%-- <input id="mobile${userValue.id}" type="text" /> --%>
									</div>
								
									
									<div class="col-md-6">
									<span>User Role</span><br />
										<form:select path="user_role" required="required" id="addUserRole" style="width: 288px;height: 34px;" name="addUserRole" class="required">
												<form:option value="0" label="Select Role" />
												<form:options items="${roleList}" />
											</form:select>
									</div>
									
									<div class="col-md-6">
									<span>User Circle</span><br/>
									<form:select path="userCircle" required="required" multiple="multiple" id="addCircle" onchange="getStoreForChangeInCircle();">
									</form:select>
										</div>
									
									<div class="col-md-6" id="adduserStoreForHide">
									<span>User Store</span><br />
										<form:select path="storeName" required="required" id="addUserStore" style="width: 288px;height: 62px;" multiple="true">
												<%-- <form:options items="${circleList}" /> --%>
												<form:option value="0">Store</form:option>
											</form:select>
									</div>
									
									
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-default" onclick="return hideCreate();">Cancel</button>
                        </div>
                        </form:form>
</div>
					<!-- <form method="POST" action="uploadFile"
						enctype="multipart/form-data">
						File to upload: <input type="file" name="file">  
						 <img class="uploadimg" id="cuser" style="cursor:pointer;" src="webskin/excel-upload.png" /> <input type="submit"
							value="Upload"> Press here to upload the file!
					</form> -->
				</div>
                </div><!--Ceate User-->
            </div>
            <!-- create store -->
              
<!-- store -->
<div class="storeDiv" style="display:none;">
 <%-- <div class="row">
			<div class="col-md-12 searchUser">
				<c:if test="${not empty storeDetails}">
					<c:forEach var="storeValue" items="${storeDetails}">
					   <div class="usercard-div" >
						<c:choose>
						 <c:when test="${storeValue.storeStatus == 'Active'}">  
							 <div class="row">
								<div class="col-md-12" >
									<div class="col-md-6 no-pad">
										<p >
											<span>Store Code:</span>${storeValue.store_code}
										</p>
										<p>
											<span>Store Name:</span>${storeValue.store_name}
										</p>
									</div>
									 <div align="right" class="col-md-6 no-pad">
										<a id="slide" href="#" onclick="return theFunctionStore('${storeValue.store_code}','${storeValue.store_name}','${storeValue.store_status}','${storeValue.circle_name}');"><img class="hvr-rotate"
											src="${pageContext.request.contextPath}/webskin/edit.png" /></a>
									</div> 
								</div>

							</div>
							 <div class="row">
								<div style="border-bottom: 1px #ccc dotted; margin: 10px 0px;"
									class="col-md-12"></div>
							</div> 
							<div class="row " id="value${storeValue.store_code}">
							 <div class="col-md-3">
									<span>Store Name</span>
									<p>${storeValue.store_name}</p>
								</div>
								
								
								<div class="col-md-3">
									<span>Circle</span>
									<p>${storeValue.circle_name}</p>
								</div>

							</div> 
							</c:when>
							</c:choose>
							<form:form id="editStoreForm${storeValue.store_code}" method="post" action="editStore" modelAttribute="editStoreBean">
							<div id="${storeValue.store_code}" style="display:none">
								<div class="row">
									<div style="border-bottom: 1px #ccc dotted; margin: 10px 0px;"
										class="col-md-12"></div>
									<div class="col-md-6">
									 <form:hidden path="id" value="${storeValue.store_code}"></form:hidden>
										<span>First Name</span><br /> <input type="text" id="firstName${userValue.id}"/>
										<span>Store Name</span><br />
										<form:input id="storeName${storeValue.store_code}" name="storeName"  path="" placeholder="Store Name" onkeyup="this.value=this.value.replace(/[^a-zA-Z ]/g,'');"/>
										<form:hidden path="old_store_name" value="${storeValue.store_name}"></form:hidden>
									</div>
									
									
									<div class="col-md-6">
									<span>Store Circle</span><br />
										<form:select path="circle_name" id="storeCircle${storeValue.store_code}" style="width: 288px;height: 34px;">
												<form:option value="0" label="Select Circle" />
												 <c:forEach items="${circleList}" var="st">
			   <c:choose>
			   <c:when test="${storeValue.circle_name  == st.value}">
			  <form:option value="${st.value}" selected="selected" label="${st.value}" />
			  </c:when> 
			  <c:otherwise>
			  <form:option value="${st.value}" label="${st.value}" />
					</c:otherwise>
					</c:choose>
				</c:forEach>
												
											</form:select>
											<form:hidden path="old_circle_name" value="${storeValue.circle_name}"/>
									</div>
									<div class="col-md-6">
									<span>Store Status</span><br />
										<form:select path="store_status" id="storeStatus${storeValue.store_code}" style="width: 288px;height: 34px;">
												<c:forEach items="${statusList}" var="st">
			   <c:choose>
			   <c:when test="${storeValue.store_status  == st.key}">
			  <form:option value="${st.key}" selected="selected" label="${st.value}" />
			  </c:when> 
			  <c:otherwise>
			  <form:option value="${st.key}" label="${st.value}" />
					</c:otherwise>
					</c:choose>
				</c:forEach>
											</form:select>
											<form:hidden path="old_store_status" value="${storeValue.store_status}"/>
									</div> 
									
									
									<div class="col-md-12">
										<button id="submit" type="submit" class="btn btn-primary">Save</button>
										<button type="button" class="btn btn-default" onclick="return hideDisplay('${storeValue.id}');">Cancel</button>
									</div>
								</div>
							</div>
						</form:form>
							
						</div>
						 
					</c:forEach>
				</c:if>
			</div>
		</div>  --%>
		<div class="row">
			<div class="col-md-12 searchUser">
				<c:if test="${not empty storeDetails}">
					<c:forEach var="storeValue" items="${storeDetails}">
						<div class="usercard-div" >
							
							<c:choose>
							
							
							 <c:when test="${storeValue.storeStatus == 'Active'}">
							 <div class="row">
								<div class="col-md-12" >
									<div class="col-md-6 no-pad">
										<p>
											<span>Store Code :</span>${storeValue.store_code}
										</p>
										<p>
											<span>Store Name :</span>${storeValue.store_name}
										</p>
									</div>
									<div align="right" class="col-md-6 no-pad">
										<a id="slide" href="#" onclick="return theFunctionStore('${storeValue.id}','${storeValue.store_name}','${storeValue.circle_name}',
											'${storeValue.location}');"><img class="hvr-rotate"
											src="${pageContext.request.contextPath}/webskin/edit.png" /></a>
									</div>
								</div>

							</div>
							<div class="row">
								<div style="border-bottom: 1px #ccc dotted; margin: 10px 0px;"
									class="col-md-12"></div>
							</div>
							<div class="row " id="value${storeValue.id}">
							<div class="col-md-3">
									<span>Store Name</span>
									<p>${storeValue.store_name}</p>
								</div>
								
								<%-- <div class="col-md-3">
									<span>User type</span>
									<p>${userValue.userType}</p>
								</div> --%>
								<div class="col-md-3">
									<span>Status</span>
									<p>${storeValue.storeStatus}</p>
								</div>
								
								<div class="col-md-3">
									<span>Circle</span>
									<p>${storeValue.circle_name}</p>
								</div>
								
								<%-- <c:if test="${not empty userValue.storeName}">
									<div class="col-md-3">
										<span>Store</span>
										<c:forEach var="userStoreName" items="${userValue.storeName}">
											<div class="row">
												<p>${userStoreName}</p>
											</div>

										</c:forEach>
									</div>
								</c:if> --%>
								
								<%-- <div class="col-md-3">
									<span><a href="userAuditHystory/${userValue.id}" style="text-decoration:underline!important; color:blue;">AUDIT</a></span>
								</div> --%>

							</div>
							</c:when>
							</c:choose>
							<form:form id="editStoreForm${storeValue.id}" method="post" action="editStore" modelAttribute="editStoreBean">
							<div id="${storeValue.id}" style="display:none">
								<div class="row">
									<div style="border-bottom: 1px #ccc dotted; margin: 10px 0px;"
										class="col-md-12"></div>
									<div class="col-md-6">
									 <form:hidden path="id" value="${storeValue.id}"></form:hidden>
									  <form:hidden path="store_code" value="${storeValue.store_code}"></form:hidden>
										<%-- <span>First Name</span><br /> <input type="text" id="firstName${userValue.id}"/> --%>
										<span>Store Name</span><br /><form:input id="storeName${storeValue.id}" name="storeName"  path="store_name" placeholder="Store Name" onkeyup="this.value=this.value.replace(/[^a-zA-Z ]/g,'');"/>
										<form:hidden path="old_store_name" value="${storeValue.store_name}"></form:hidden>
									</div>
									
									
			
									
									
									
									<div class="col-md-6">
									<span>Store Circle</span><br />
										<form:select path="circle" id="storeCircle${storeValue.store_code}" style="width: 288px;height: 34px;">
												<form:option value="0" label="Select Circle" />
												 <c:forEach items="${circleList}" var="st">
			   <c:choose>
			   <c:when test="${storeValue.circle_name  == st.value}">
			  <form:option value="${st.key}" selected="selected" label="${st.value}" />
			  </c:when> 
			  <c:otherwise>
			  <form:option value="${st.key}" label="${st.value}" />
					</c:otherwise>
					</c:choose>
				</c:forEach>
												
											</form:select>
											<form:hidden path="old_circle_name" value="${storeValue.circle_name}"/>
									</div>
									
									<div class="col-md-6">
									<span>Store Status</span><br />
										<form:select path="storeStatus" id="storeStatus${storeValue.id}" style="width: 288px;height: 34px;">
												<%-- <form:option value="0" label="Select Status" /> --%>
												<form:options items="${statusList}" />
											</form:select>
											<form:hidden path="old_store_status" value="${storeValue.storeStatus}"/>
									</div>
									<!-- <div class="col-md-3">
										<span>User Role</span><br /> <input type="text" />

									</div>
									<div class="col-md-3">
										<span>User type</span><br /> <input type="text" />
									</div>
									<div class="col-md-3">
										<span>Status</span><br /> <select id="Select1">
											<option>Active</option>
											<option>Deactive</option>
										</select>
									</div> -->
									<div class="col-md-12">
										<button type="submit" class="btn btn-primary">Save</button>
										<button type="button" class="btn btn-default" onclick="return hideDisplay('${storeValue.id}');">Cancel</button>
									</div>
								</div>
							</div>
						</form:form>
						</div>
					</c:forEach>
				</c:if>
			</div>
			</div>
		 <!-- <form id="jjj" action = "editStore" method = "POST">
         First Name: <input type = "text" name = "first_name">
         <br />
         Last Name: <input type = "text" name = "last_name" />
         <input type = "submit" value = "Submit" />
      </form> -->
</div>
            <!-- end of create store -->
         <div class="userDiv"> 
		<div class="row">
			<div class="col-md-12 searchUser">
				<c:if test="${not empty userDetails}">
					<c:forEach var="userValue" items="${userDetails}">
						<div class="usercard-div" >
							
							<c:choose>
							 <c:when test="${userValue.userStatus == 'Inactive'}">
							 <div class="row">
								<div class="col-md-12" >
									<div class="col-md-6 no-pad">
										<p style="color:red;">
											<span>Name :</span>${userValue.firstName}
										</p>
										<p style="color:red;">
											<span>Email :</span>${userValue.name}
										</p>
									</div>
									<div align="right" class="col-md-6 no-pad">
										<a id="slide" href="#" onclick="return theFunction('${userValue.id}','${userValue.firstName}','${userValue.lastName}',
										'${userValue.mobile}','${userValue.user_role}','${userValue.user_status}','${userValue.userRole}');"><img class="hvr-rotate"
											src="${pageContext.request.contextPath}/webskin/edit.png" /></a>
									</div>
								</div>

							</div>
							<div class="row">
								<div style="border-bottom: 1px #ccc dotted; margin: 10px 0px;"
									class="col-md-12"></div>
							</div>
							<div class="row " id="value${userValue.id}">
							<div class="col-md-3">
									<span style="color:red;">First Name</span>
									<p style="color:red;">${userValue.firstName}</p>
								</div>
								<div class="col-md-3">
									<span style="color:red;">Last Name</span>
									<p style="color:red;">${userValue.lastName}</p>
								</div>
								<div class="col-md-3">
									<span style="color:red;">Mobile</span>
									<p style="color:red;">${userValue.mobile}</p>
								</div>
								<div class="col-md-3">
									<span style="color:red;">User Role</span>
									<p style="color:red;">${userValue.userRole}</p>
								</div>
								<%-- <div class="col-md-3">
									<span>User type</span>
									<p>${userValue.userType}</p>
								</div> --%>
								<div class="col-md-3">
									<span style="color:red;">Status</span>
									<p style="color:red;">${userValue.userStatus}</p>
								</div>
								<c:if test="${not empty userValue.userCircle}">
								<div class="col-md-3">
									<span style="color:red;">Circle</span>
									<c:forEach var="userCircle" items="${userValue.userCircle}">
											<div class="row">
												<p style="color:red;">${userCircle}</p>
											</div>

										</c:forEach>
								</div>
								</c:if>
								<c:if test="${not empty userValue.storeName}">
									<div class="col-md-3">
										<span style="color:red;">Store</span>
										<c:forEach var="userStoreName" items="${userValue.storeName}">
											<div class="row">
												<p style="color:red;">${userStoreName}</p>
											</div>

										</c:forEach>
									</div>
								</c:if>

							</div>
							</c:when>
							
							 <c:when test="${userValue.userStatus == 'Active'}">
							 <div class="row">
								<div class="col-md-12" >
									<div class="col-md-6 no-pad">
										<p>
											<span>Name :</span>${userValue.firstName}
										</p>
										<p>
											<span>Email :</span>${userValue.name}
										</p>
									</div>
									<div align="right" class="col-md-6 no-pad">
										<a id="slide" href="#" onclick="return theFunction('${userValue.id}','${userValue.firstName}','${userValue.lastName}',
										'${userValue.mobile}','${userValue.user_role}','${userValue.user_status}','${userValue.userRole}');"><img class="hvr-rotate"
											src="${pageContext.request.contextPath}/webskin/edit.png" /></a>
									</div>
								</div>

							</div>
							<div class="row">
								<div style="border-bottom: 1px #ccc dotted; margin: 10px 0px;"
									class="col-md-12"></div>
							</div>
							<div class="row " id="value${userValue.id}">
							<div class="col-md-3">
									<span>First Name</span>
									<p>${userValue.firstName}</p>
								</div>
								<div class="col-md-3">
									<span>Last Name</span>
									<p>${userValue.lastName}</p>
								</div>
								<div class="col-md-3">
									<span>Mobile</span>
									<p>${userValue.mobile}</p>
								</div>
								<div class="col-md-3">
									<span>User Role</span>
									<p>${userValue.userRole}</p>
								</div>
								<%-- <div class="col-md-3">
									<span>User type</span>
									<p>${userValue.userType}</p>
								</div> --%>
								<div class="col-md-3">
									<span>Status</span>
									<p>${userValue.userStatus}</p>
								</div>
								
								<c:if test="${not empty userValue.userCircle}">
								<div class="col-md-3">
									<span>Circle</span>
									<c:forEach var="userCircle" items="${userValue.userCircle}">
											<div class="row">
												<p >${userCircle}</p>
											</div>

										</c:forEach>
								</div>
								</c:if>
								<c:if test="${not empty userValue.storeName}">
									<div class="col-md-3">
										<span>Store</span>
										<c:forEach var="userStoreName" items="${userValue.storeName}">
											<div class="row">
												<p>${userStoreName}</p>
											</div>

										</c:forEach>
									</div>
								</c:if>
								
								<div class="col-md-3">
									<span><a href="userAuditHystory/${userValue.id}" style="text-decoration:underline!important; color:blue;">AUDIT</a></span>
								</div>

							</div>
							</c:when>
							</c:choose>
							<form:form id="editForm${userValue.id}" method="post" action="editStore" modelAttribute="editUserBean">
							<div id="${userValue.id}" style="display:none">
								<div class="row">
									<div style="border-bottom: 1px #ccc dotted; margin: 10px 0px;"
										class="col-md-12"></div>
									<div class="col-md-6">
									 <form:hidden path="id" value="${userValue.id}"></form:hidden>
										<%-- <span>First Name</span><br /> <input type="text" id="firstName${userValue.id}"/> --%>
										<span>First Name</span><br /><form:input id="firstName${userValue.id}" name="firstName"  path="" placeholder="First Name" onkeyup="this.value=this.value.replace(/[^a-zA-Z ]/g,'');"/>
										<form:hidden path="old_firstName" value="${userValue.firstName}"></form:hidden>
									</div>
									<div class="col-md-6">
									<span>Last Name</span><br /><form:input id="lastName${userValue.id}" name="lastName"  path="" placeholder="Last Name" onkeyup="this.value=this.value.replace(/[^a-zA-Z ]/g,'');"/>
									<form:hidden path="old_lastName" value="${userValue.lastName}"></form:hidden>
										<%-- <span>Last Name</span><br /> <input type="text" id="lastName${userValue.id}"/> --%>
									</div>
									<div class="col-md-6">
										<span>Mobile</span><br />  <form:input type="number" id="mobile${userValue.id}" placeholder="Mobile"  name="mobile" value="${signupBean.mobile}" path=""/>
										<form:hidden path="old_mobile" value="${userValue.mobile}"/>
										<%-- <input id="mobile${userValue.id}" type="text" /> --%>
									</div>
									
									
									<%-- <div class="col-md-6">
									<span>User Type</span><br />
										<form:select path="user_type" id="userType${userValue.id}" style="width: 288px;height: 34px;">
												<form:option value="0" label="Select Type" />
												<form:options items="${typeList}"/></form:select>
									</div> --%>
									
									<div class="col-md-6">
									<span>User Role</span><br />
										<form:select path="user_role" id="userRole${userValue.id}" style="width: 288px;height: 34px;">
												<%-- <form:option value="0" label="Select Role" /> --%>
												<form:options items="${roleList}" />
											</form:select>
											<form:hidden path="old_userRole" value="${userValue.user_role}"/>
									</div>
									
									<div class="col-md-6">
									<span>Assign New Circles to User</span><br />
										<form:select path="userCircle" id="userCircle${userValue.id}" multiple="true" onchange="getStoreForEditCircle(${userValue.id})" style="width: 288px;height: 34px;">
												
											</form:select>
											<%-- <form:hidden path="old_userCircle" value="${userValue.user_circle}"/> --%>
									</div>
									
									<div class="col-md-6">
									<span id="userStoreHide">Assign New Stores to User</span><br/>
										<form:select path="storeName" id="userStore${userValue.id}" style="width: 288px;height: 62px;" multiple="true">
												<%-- <form:options items="${circleList}" /> --%>
												<form:option value="">Store</form:option>
											</form:select>
									</div>
									<div class="col-md-6">
									<span id="selectedUserStoreExisted">Existed User Circle</span><br />
										<form:select path="userCircle" id="selectedUserCircle${userValue.id}"  disabled="true" style="width: 288px;height: 62px;" multiple="true">
												<%-- <form:options items="${circleList}" /> --%>
												<form:option value="">Circle</form:option>
											</form:select>
											<%-- <form:hidden path="old_firstName" value="${userValue.firstName}"/> --%>
									</div>
									
									<div class="col-md-6">
									<span id="selectedUserStoreExisted">Existed User Stores</span><br />
										<form:select path="storeName" id="selectedUserStore${userValue.id}"  disabled="true" style="width: 288px;height: 62px;" multiple="true">
												<%-- <form:options items="${circleList}" /> --%>
												<form:option value="">Store</form:option>
											</form:select>
											<%-- <form:hidden path="old_firstName" value="${userValue.firstName}"/> --%>
									</div>
									<div class="col-md-6">
									<span id="removeUserCircleExisted">Remove Circle assigned to User</span><br />
										<form:select path="removeCircleName" id="removeUserCircle${userValue.id}"  style="width: 288px;height: 62px;" onchange="removeStores('${userValue.id}')" multiple="multiple">
												<%-- <form:options items="${circleList}" /> --%>
												<form:option value="">Circle</form:option>
											</form:select>
											<%-- <form:hidden path="old_firstName" value="${userValue.firstName}"/> --%>
									</div>
									
									<div class="col-md-6">
									<span id="removeUserStoreExisted">Remove Stores assigned to User</span><br />
										<form:select path="removeStoreName" id="removeUserStore${userValue.id}"  style="width: 288px;height: 62px;" multiple="true">
												<%-- <form:options items="${circleList}" /> --%>
												<form:option value="">Store</form:option>
											</form:select>
											<%-- <form:hidden path="old_firstName" value="${userValue.firstName}"/> --%>
									</div>
									
									<div class="col-md-6">
									<span>User Status</span><br />
										<form:select path="user_status" id="userStatus${userValue.id}" style="width: 288px;height: 34px;">
												<%-- <form:option value="0" label="Select Status" /> --%>
												<form:options items="${statusList}" />
											</form:select>
											<form:hidden path="old_userStatus" value="${userValue.user_status}"/>
									</div>
									<!-- <div class="col-md-3">
										<span>User Role</span><br /> <input type="text" />

									</div>
									<div class="col-md-3">
										<span>User type</span><br /> <input type="text" />
									</div>
									<div class="col-md-3">
										<span>Status</span><br /> <select id="Select1">
											<option>Active</option>
											<option>Deactive</option>
										</select>
									</div> -->
									<div class="col-md-12">
										<button type="submit" class="btn btn-primary">Save</button>
										<button type="button" class="btn btn-default" onclick="return hideDisplay('${userValue.id}');">Cancel</button>
									</div>
								</div>
							</div>
						</form:form>
						</div>
					</c:forEach>
				</c:if>
			</div>
		</div>

</div>



<!-- for audit questions -->
<div class="auditDiv" style="display:none;" >
<!-- store -->

<div class="row">
			<div class="col-md-12 searchUser">
				<c:if test="${not empty questionsList}">
					<c:forEach var="questionValue" items="${questionsList}">
						<div class="usercard-div" >
							 <div class="row">
								<div class="col-md-12" >
									<div class="col-md-6 no-pad">
										<p>
											<span>Category:</span>${questionValue.key}
										</p>
										
									</div>
									<%--  <div align="right" class="col-md-6 no-pad">
										<a id="slide" href="#" onclick="return theFunctionStore('${storeValue.store_code}','${storeValue.store_name}','${storeValue.circle_name}',
										'${storeValue.location}');"><img class="hvr-rotate"
											src="${pageContext.request.contextPath}/webskin/edit.png" /></a>
									</div>  --%>
								</div>

							</div>
							 <div class="row">
								<div style="border-bottom: 1px #ccc dotted; margin: 10px 0px;"
									class="col-md-12"></div>
							</div> 
							<div class="row " id="value${questionValue.key}">
							 <div class="col-md-3">
									<span>Category</span>
									<p >${questionValue.key}</p>
									 <input type='hidden' id='auditQuestion' name='auditQuestion' value='${questionValue.key}'/>
								</div>
								
								
								<div class="col-md-3">
								
										<span>Questions</span>
										<%-- <select name='question'>
										<c:forEach var="questionList" items="${questionValue.value}">
											
											<option> ${questionList.audit_name} </option>
											

										</c:forEach>
										</select> --%>
										<button id="previewBtn"  style="margin-top: 5px;" data-toggle="modal"
							data-target="#myModal" type="button" class="btn btn-primary"
							onclick="previewList()">Open Question</button>

<!-- The Modal -->
						
									
								</div>

							</div> 
							<%-- <form:form id="editForm${storeValue.store_code}" method="post" action="editStore" modelAttribute="editStoreBean">
							<div id="${storeValue.store_code}" style="display:none">
								<div class="row">
									<div style="border-bottom: 1px #ccc dotted; margin: 10px 0px;"
										class="col-md-12"></div>
									<div class="col-md-6">
									 <form:hidden path="id" value="${storeValue.store_code}"></form:hidden>
										<span>First Name</span><br /> <input type="text" id="firstName${userValue.id}"/>
										<span>Store Name</span><br /><form:input id="storeName${storeValue.store_name}" name="storeName"  path="" placeholder="First Name" onkeyup="this.value=this.value.replace(/[^a-zA-Z ]/g,'');"/>
										<form:hidden path="" value="${userValue.firstName}"></form:hidden>
									</div>
									
									<div class="col-md-12">
										<button type="submit" class="btn btn-primary">Save</button>
										<button type="button" class="btn btn-default" onclick="return hideDisplay('${userValue.id}');">Cancel</button>
									</div>
								</div>
							</div>
						</form:form>
							 --%>
						</div>
					</c:forEach>
				</c:if>
			</div>
		</div>
</div>
	
    </section>
    
		<!-- <div class="modal fade" id="myModal" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						 <h4 class="modal-title" id="myModalLabel">Questions</h4>
						
					</div>
					<div class="modal-body">
						<ul class="value-ul">
						<li style="padding:0px; border:0px;">
                 
                        </li>
						
						</ul>
						<div id="result"></div>
					</div>
				</div>
			</div>
		</div> -->
		
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						 <h4 class="modal-title" id="myModalLabel">Question Details</h4>
						
					</div>
					<%-- <div class="row">

						<div class="col-md-3">
							<!-- <span>First Name</span><br /> <input type="text" id="firstName"/> -->
							<span style="margin-left: 10px;"><b>Agent Name</b></span><br />
							<form:input id="agent_name" name="agent_name" path=""
								style="margin-left: 10px;" placeholder="Agent Name"
								onkeyup="this.value=this.value.replace(/[^a-zA-Z ]/g,'');" />
						</div>
						<div class="col-md-3">
							<span style="margin-left: 47px;"><b>Wizdom ID</b></span><br />
							<form:input id="agent_email" name="agent_email" path=""
								style="margin-left: 47px;" onkeyup="this.value=this.value.replace(/[^a-zA-Z 0-9]/g,'');"
								placeholder="Wizdom ID" />
							<!-- <span>Last Name</span><br /> <input type="text" id="lastName"/> -->
						</div>
						<div class="col-md-3">
							<span style="margin-left: 85px;"><b>Mobile</b></span><br />
							<!-- <input type="text" id="mobile"/> -->
							<form:input type="text" id="agent_mobile"
								style="margin-left: 85px;" placeholder="Mobile"
								name="agent_mobile" value="" path=""
								onkeypress='return event.charCode>=48&&event.charCode<=57' />
						</div>
					</div> --%>
					<%-- <div class="modal-header">

						<h4 class="modal-title" id="myModalLabel">${storeName}: Audit Preview</h4>
						*<font style="color: red; font-weight: bold">Red</font>  - Pending audit category &nbsp;&nbsp; *
						<font style="color: green; font-weight: bold" >Green</font> - Completed audit category
					</div> --%>
					<div class="modal-body">
						<ul class="value-ul">
						<li style="padding:0px; border:0px;">
                           
                            <div class="row pop-form">
                                <div class="col-md-12">
                                    <p>Select Question you want to edit</p>
                                </div>
                                
                               
                               
                             
                               
                            </div>
                        </li>
						
						</ul>
						<div id="result"></div>
					</div>


					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Continue
							Edit</button>
						<button type="button" class="btn btn-primary"
							id="previewSubmit" onclick="previewSubmitCheck()">Submit Audit</button>
							<!-- <button type="button" data-toggle="modal"
							data-target="#submitModal" class="btn btn-primary"
							id="previewSubmit">Submit Audit</button> -->
					</div>
				</div>
			</div>
		</div>

		
    <script type="text/javascript">
    $("div.cuserfrm").hide();
    </script>
    <script>
// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>
	
	 <script type="text/javascript">
  $( "#signupForm" ).validate({
	  rules: {
		 mobile:{
	    	maxlength: 10,
	    	minlength:10
	    },
	    firstName:{
	    	required : true
	    }
	    ,
	    lastName:{
	    	required : true
	    }
	    ,
	    username:{
	    	required : true
	    }
	    ,
	    user_role:{
	    	selectcheck: true
	    }
	    ,
	    user_circle:{
	    	selectcheck : true
	    }
	    ,
	    storeName:{
	    	selectcheck : true
	    }
	   
	  }
	});  
  
  $( "#filterForm" ).validate({
	  rules: {
		  filterTimeForSelectUser:{
	    	selectcheck : true
	    }
	   
	  }
	});  
	 $.validator.addMethod('mypassword', function(value, element) {
		 console.log(this.optional(element));
	    return (value.match(/[a-zA-Z]/));
	},
	'Must contains only alphabetic character.'); 
	 
	  $.validator.addMethod('selectcheck', function (value) {
		  console.log(value);
	        return (value != '0');
	    }, "This field is required.");
	 
  </script>
  <script type="text/javascript">
  function previewList(){
		getPreviewData();
	}
  </script>
  <script type="text/javascript">
  window.getPreviewData = function (){
	  var auditCat=$("#auditQuestion").val();
	  
		$.ajax({
  		type : "GET",
  		url : "${pageContext.request.contextPath}/getAuditQuestion?auditCat="+auditCat,
  		timeout : 100000,
  		success : function(data) {
  			//var button = document.createElement("button");

  			function doSomething() {
  			   
  			}

  		/* 	button.id = "myButton";
  			button.textContent = "Click me";
  			button.addEventListener("click", doSomething, false); */
  			/* var something = document.getElementById("result");
  			

  			something.appendChild(button); */
  			var newHTML = [];
  			//var Options ='<table>';
			for(var c=0;c<data.length;c++)
			{
				var button = document.createElement("button");
				button.id = "myButton";
	  			button.textContent = "Click me";
	  			button.addEventListener("click", doSomething, false);
				/*  newHTML.push('<span>' + data[c].audit_name + '</span>');
				 newHTML.push */
				 $('#result').append('<span>' + data[c].audit_name + '</span>');
				 $('#result').append(button);
				/* Options += '<tr><td>'+data[c].audit_name+'</td>';
				Options +='<td> <button type="button" class="btn btn-default" onclick="getAuditQuestionFromPop()">Submit</button>'+'<br>'+'</td></tr>';
			 */}
			//Options +='</table>';
			
			/*  $("#result").html(newHTML.join(""));  */
  		},
  		error : function(e) {
  			console.log("ERROR: ", e);
  		},
  		done : function(e) {
  			console.log("DONE");
  		}
  	});
	};
	</script>
	  <script type="text/javascript">
	  function removeStores(abc)
	  {
		  
	  
		  var fld=document.getElementById('removeUserCircle'+abc);
		  var circle='';
			for (var i = 0; i < fld.options.length; i++) {
			
				  if (fld.options[i].selected) {
					  circle+=fld.options[i].value+",";
				  }
			} 
		  var storeList=document.getElementById('removeStoreName'+abc);
		  $.ajax({
				url:"${pageContext.request.contextPath}/stores?circle="+circle,
				success:function(cityGrp){
				
				
				var Options='';
					for(var c=0;c<cityGrp.length;c++)
					{
						if()
						Options += '<option value="'+cityGrp[c].id+'">'+cityGrp[c].store_name+'</option>';
					}
					
					 $("#userStore"+abc).html(Options);  
					
			}
			});
				
	  }
	 
	
	
	</script> 
	<script type="text/javascript">
	 function getStoreForEditCircle(abc)
		{
		  
		  var fld = document.getElementById('userCircle'+abc);
			 
		 	var circle='';
			for (var i = 0; i < fld.options.length; i++) {
			
				  if (fld.options[i].selected) {
					  circle+=fld.options[i].value+",";
				  }
			} 
			
			$.ajax({
			url:"${pageContext.request.contextPath}/stores?circle="+circle,
			success:function(cityGrp){
			
			
			var Options='';
				for(var c=0;c<cityGrp.length;c++)
				{
					
					Options += '<option value="'+cityGrp[c].id+'">'+cityGrp[c].store_name+'</option>';
				}
				
				 $("#userStore"+abc).html(Options);  
				
		}
		});
			
		
		}
		
	</script>
	<script type="text/javascript">
	function getStoreForChangeInCircle()
	{
	 
	 
		
		 var fld = document.getElementById('addCircle');
		
	 	var circle='';
		for (var i = 0; i < fld.options.length; i++) {
		
			  if (fld.options[i].selected) {
				  circle+=fld.options[i].value+",";
			  }
		}
		
		$.ajax({
		url:"${pageContext.request.contextPath}/stores?circle="+circle,
		success:function(cityGrp){
		
		
		var Options='';
			for(var c=0;c<cityGrp.length;c++)
			{
				
				Options += '<option value="'+cityGrp[c].id+'">'+cityGrp[c].store_name+'</option>';
			}
			
			 $("#addUserStore").html(Options);  
			
	}
	});
		
	
	}
	
	
	
	
	
	</script>
	<!-- <script type="text/javascript">
	$(.chosen-select).chosen();
	
	</script> -->
	<script type="text/javascript">
 	function getStore(){
		
		$.ajax({
	         url: "${pageContext.request.contextPath}/stores",
	         type: "GET",
	         contentType: "application/json",
			 async:false,
			 success: function(data)
	         {
				 
				  
	   var options = "";
	   console.log(data.length);
	   for (var i=0; i<data.length;i++){
	  
	   //console.log(pieces[0]+" "+pieces[1]);
	   options+= "<option value='"+data[i].boxId+"'>"+data[i].boxName+"</option>"
	   }
	   $('#boxSelect').html(options);
	   $("#boxSelect").trigger("chosen:updated");
			 },
			 error: function (xhr, errorType, exception) {
	        
	            
	            var result = "errorType: " + errorType + " exception :" + exception;
	                  console.log(result);
					 alert("Failed to get Device List. Please try after some time.");
	            
	        }
	});
		}
		 
		
	</script>
</body>

