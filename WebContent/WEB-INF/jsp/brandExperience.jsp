<%@include file="include.jsp"%>
<%@include file="header.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="com.vowmee.bean.UserBean"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Vowmee</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
<link href="${pageContext.request.contextPath}/css/bootstrap.min.css"
	rel="stylesheet" />
<link href="${pageContext.request.contextPath}/css/vowmee-design.css"
	rel="stylesheet" />
<link href="${pageContext.request.contextPath}/css/oversheet.css"
	rel="stylesheet" />
<link href="${pageContext.request.contextPath}/css/hover.css"
	rel="stylesheet" />
<link href="${pageContext.request.contextPath}/css/bootstrap-switch.css"
	rel="stylesheet" />
<link
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"
	rel="stylesheet" />
	 <link href="${pageContext.request.contextPath}/css/media-q.css" rel="stylesheet" />
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap-switch.js"></script>
<script src="${pageContext.request.contextPath}/js/init.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style type="text/css">
label {
	display: inline-block;
	width: 270px;
	style: "margin-left: 49px;";
	color: red
}

</style>
<% UserBean user = (UserBean) session.getAttribute("loginBean");%>
<script
	src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"
	type="text/javascript"></script>

</head>
<script type="text/javascript">
var lookupCount = 0;
var cate = null;
$(document).ready(function() {
	var previewFlagVal = eval('${previewFlag}');
	if(previewFlagVal){
		document.getElementById("previewBtn").disabled = false;
		document.getElementById("displaymessage").style.display = 'none';
	}
	/* if((${auditPreviewJaon})!=null){
		
	} */
	
	
	var auditPreviewJaon = eval('${auditPreviewJaon}');
	console.log(auditPreviewJaon);
	console.log(auditPreviewJaon!=undefined);
	var getCategory = eval('${getCategory}');
	
	cate = getCategory;
	
	console.log(cate);
	$('#auditCategoryList option[value="'+getCategory+'"]').attr('selected','selected');
	
	//var auditPreviewJaon = ${auditPreviewJaon};
	
	window.saveAudit = function (data){
		
		$.ajax({
    		type : "POST",
    		contentType : "application/json",
    		url : "${pageContext.request.contextPath}/saveAudit",
    		data : JSON.stringify(data),
    		dataType : 'json',
    		timeout : 100000,
    		success : function(data) {
    			if(data.previewFlag){
    				document.getElementById("previewBtn").disabled = false;
    				document.getElementById("displaymessage").style.display = 'none';
    				document.getElementById("optionSpace").style.display = 'block';
    			}
    			console.log("Success");
    		},
    		error : function(e) {
    			console.log("ERROR: ", e);
    		},
    		done : function(e) {
    			console.log("DONE");
    		}
    	});
	};
	 $( "#cancelId" ).click(function(){
			 $('#auditCategoryList option[value="'+cate+'"]').prop('selected', true)}); 

	window.setCategory = function(data){
		$('#auditCategoryList option[value="'+data+'"]').prop('selected', true);
	};
	
	window.saveCategory = function (data){
		
	
		
		
		$.ajax({
    		type : "POST",
    		contentType : "application/json",
    		url : "${pageContext.request.contextPath}/selectCategory",
    		data : JSON.stringify(data),
    		dataType : 'json',
    		timeout : 100000,
    		success : function(data) {
    			console.log("Success");
    			window.location = "${pageContext.request.contextPath}/getCategory";
    		},
    		error : function(e) {
    			console.log("ERROR: ", e);
    		},
    		done : function(e) {
    			console.log("DONE");
    		}
    	});
	};
	
	window.getPreviewData = function (){
		$.ajax({
    		type : "GET",
    		url : "${pageContext.request.contextPath}/getPreviewData",
    		timeout : 100000,
    		success : function(data) {
    			console.log(data);
    			 $('#result').html(data);
    		},
    		error : function(e) {
    			console.log("ERROR: ", e);
    		},
    		done : function(e) {
    			console.log("DONE");
    		}
    	});
	};
		
	var auditPreviewJaon = eval('${auditPreviewJaon}');
//	console.log(auditPreviewJaon);
	if(auditPreviewJaon!= null){
		for(var j = 0; j < auditPreviewJaon.length; j++)
		{
			console.log("inside for");
			if(auditPreviewJaon[j].auditCategory == $('#auditCategoryList').val()){
				console.log(auditPreviewJaon[j].auditCategory);
				console.log($('#auditCategoryList').val());
				lookupCount+=1;
				console.log(lookupCount);
				}
			console.log(auditPreviewJaon[j].auditValue);
		if(auditPreviewJaon[j].auditFlag==1){
			$("input[name='"+auditPreviewJaon[j].lookupId+"'][value='Yes']").attr('checked', true);
		}else if(auditPreviewJaon[j].auditFlag==0){
			$("input[name='"+auditPreviewJaon[j].lookupId+"'][value='No']").attr('checked', true);
		}else if(auditPreviewJaon[j].auditFlag==-1){
			$("input[name='"+auditPreviewJaon[j].lookupId+"'][value='N/A']").attr('checked', true);
		}
		}
	}
});

var auditLookupSize = ${auditLookupListJson};
function selectCancelCategory(){
	alert("inside");
	alert(cate);
	setCategory(cate);
}
function selectFunction(functionName, name, value){
	
	
	console.log(lookupCount);
	console.log(auditLookupSize.length);
	if(functionName == "selectCategory"){
		if(auditLookupSize.length == lookupCount){
			selectCatagory();
		}else{
			$( '.bs-example-modal-sm' ).modal('show');
			/*  $( "#dialog-confirm" ).dialog({
			      resizable: false,
			      height: "auto",
			      width: 400,
			      modal: true,
			      buttons: {
			        "Proceed": function() {
			        	$( "#Proceed" ).click(
			        	selectCatagory());
			        	$( this ).dialog( "close" );
			         
			        },
			        Cancel: function() {
			        	$( "#Cancel" ).click(
			        			setCategory(cate));
			        	$( this ).dialog( "close" );
			        }
			      }
			    }); */
			
			
		}
		
	}else if(functionName == "showAlert"){
		lookupCount +=1;
		showAlert(name, value);
	}
	
}

function showAlert(name, value){
	var flag;
	if(value.value == "Yes"){
		flag = true;
	}else{
		flag = false;
	}
	data ={
		"auditLookup":name,
		"auditValue":flag,
		"auditAnswer":value.value
	}
	
	saveAudit(data);

}


function selectCatagory(){
	
	var category = $('#auditCategoryList').val();
	saveCategory(category);

}

function changeColor(){
	alert("inside change color");
	var category = $('#auditCategoryList').val();
	/* $('.auditCategoryList option:eq('+category+')').prop('selected', true) */
	$('#auditCategoryList option[value="'+category+'"]').css({"color":"blue"});
	

}

function previewList(){
	getPreviewData();
}

function saveAndSubmit(){
	window.location = "${pageContext.request.contextPath}/submitAudit";
}
function previewSubmitCheck(){
	var agentName = $('#agent_name').val();
	var agentId = $('#agent_email').val();
	var mobile = $('#agent_mobile').val();
	if(agentName=='' || agentId=='' || mobile==''){
		alert("Please select mandatory Fields");
	}else{
		$( '#submitModal' ).modal('show');
	}
}

function dismissNo(){
	$( '#submitModal' ).modal('hide');
}
</script>
<body style="overflow-x: hidden; padding-top: 20px;" class="intro-bg">
	<!--<a href="login.html">
        <img class="logout-ic" src="webskin/logout-ic.png" />
    </a>-->
    
	<section>
		<div class="container">
			<div class="row">
				<div align="center" class="col-md-12">
					<br />
					<br />
					<br />
				</div>
			</div>

			<div class="row">
				<div align="center" class="col-md-12">
					<h3 class="heading">
						Auditor:
						<%= user.getFirstName() %>
						| Circle : ${userCircle} | Store : ${storeName}
					</h3>
					<p style="color: #fff;">Please do Audit for all the Category</p>
				</div>
			</div>

			<%-- 	<div class="row">
                <div class="col-md-12">
                    <div class="audit-r-div">
                      <span><b>Select Category</b></span><br /> 
                       <form:select id="auditCategoryList" path="auditCategoryList" items="${auditCategoryList}" onchange="selectCatagory()"/>
                       <form:select id="auditCategoryList" path="auditCategoryList" items="${auditCategoryList}" onchange="selectFunction('selectCategory')"/>
                   
                    </div>

                </div>
                  
            </div> --%>
			<div class="row">
				<div class="col-md-12">
					<div class="audit-r-div">
						
						<div>
						<b style="margin-left: 10px">Select Audit Category</b>
						<font style="padding-left:500pt;">*</font><font style="color: red; font-weight: bold">Red</font>  - Pending &nbsp;&nbsp; *
						<font style="color: green; font-weight: bold" >Green</font> - Completed
						
						</div>
						<form:select id="auditCategoryList" path="auditCategoryList"
							onchange="selectFunction('selectCategory')">
							<c:forEach var="auditCategory"
								items="${auditCategoryList.keySet()}">
								<c:if test="${auditCategoryMap.get(auditCategory)!=null}">
									<form:option value="${auditCategory }"
										cssStyle="color:${auditCategoryMap.get(auditCategory)} !important"> ${auditCategory } </form:option>
								</c:if>
								<c:if test="${auditCategoryMap.get(auditCategory)==null}">
									<form:option value="${auditCategory }"
										cssStyle="color:red !important"> ${auditCategory } </form:option>
								</c:if>

							</c:forEach>

						</form:select>
					</div>

				</div>
			</div>


			<div class="row">
				<div class="col-md-12">
					<div class="audit-r-div">
						<ul class="value-ul">
							<c:if test="${not empty auditLookupList}">
								<c:forEach var="auditLookupList" items="${auditLookupList}">
									<li>
										<table>
											<tr>
												<td style="width: 100%;">
													<p>${auditLookupList.audit_name}</p>
												</td>
												<td>
													<table align="right">
														<%--  <tr>
                                                    <td><input type="radio" name="${auditLookupList.id}" value="Yes" onclick="showAlert(${auditLookupList.id},this)"></td>
                                                    <td><span>Yes</span></td>
                                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                                    <td><input type="radio" name="${auditLookupList.id}" value="No" onclick="showAlert(${auditLookupList.id},this)"></td>
                                                    <td><span>No</span></td>
                                                </tr> --%>
														<tr>
															<td><input type="radio" name="${auditLookupList.id}"
																value="Yes"
																onclick="selectFunction('showAlert',${auditLookupList.id},this)"></td>
															<td><span>Yes</span></td>
															<td>&nbsp;&nbsp;&nbsp;</td>
															<td><input type="radio" name="${auditLookupList.id}"
																value="No"
																onclick="selectFunction('showAlert',${auditLookupList.id},this)"></td>
															<td><span>No</span></td>
															<td>&nbsp;&nbsp;&nbsp;</td>
															<td><input type="radio" name="${auditLookupList.id}"
																value="N/A"
																onclick="selectFunction('showAlert',${auditLookupList.id},this)"></td>
															<td><span>N/A</span></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</li>
								</c:forEach>
							</c:if>
						</ul>
					</div>
				</div>
			</div>
			<div class="search-div">
				<div class="row">
				 <c:if test="${previewFlag ==true}">
					<div align="left" class="col-md-6 col-sm-6 col-xs-6" id="optionSpace">
						
					</div>
					</c:if>
					<div align="left" id="displaymessage" class="col-md-6">
						<!--   <button style=" margin-top: 5px;" type="button" class="btn btn-success" onclick="saveAndSubmit()">Submit</button> -->
						<p style="color:#910909; " align="left"><b>NOTE: Auditors can  Preview and Submit Audit only after completion of all category line items.</b></p>
						
					</div>
					<div align="right" id="previewBtnHide" class="col-md-6 col-sm-6 col-xs-6">
						<!--   <button style=" margin-top: 5px;" type="button" class="btn btn-success" onclick="saveAndSubmit()">Submit</button> -->
						<!-- <p id="displaymessage" style="color:#910909; " align="left"><b>NOTE: Auditors can  Preview and Submit Audit only after completion of all category line items.</b></p> -->
						<button id="previewBtn" disabled="true" style="margin-top: 5px;" data-toggle="modal"
							data-target="#myModal" type="button" class="btn btn-primary"
							onclick="previewList()">Preview Audit</button>
						<!--  <button style=" margin-top: 5px;" type="button" class="btn btn-default">Cancel</button> -->
					</div>
				</div>
			</div>
		</div>
		<%--  <div class="col-md-1col-sm-1 col-xs-1" style="padding-left: 600px;">
				<a href="previewAudit"> <img src="${pageContext.request.contextPath}/webskin/arrow.png">
				</a>
			</div> --%>
	</section>
	<div style="margin-top:15%;" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                        <div class="modal-dialog modal-small" role="document">
                            <div  class="modal-content">
                                <h4 style="padding-top: 23px;padding-left: 26px;">Do you want continue ?</h4>
                                <p style="padding-left: 26px;width: 494px;">You have pending items left in this category. Do you want to continue with other audit category?</p>
                                <button type="button" class="btn btn-primary" style="margin-bottom:21px; margin-left:25px" onclick="selectCatagory()">Proceed</button>
                                <button data-dismiss="modal" id="cancelId" class="btn btn-default" type="submit" style="margin-bottom: 21px;">Cancel</button>
                            </div>
                        </div>
                    </div>
	<!-- <div id="dialog-confirm" title="Do you want to continue?" style="display:none;">
		<p>
			<span class="ui-icon ui-icon-alert"
				style="float: left; margin: 12px 12px 20px 0;"></span>You have
			pending items left in this category. Do you want to continue with
			other audit category?
		</p>
	</div> -->
	<form:form id="agentSubmitForm" method="post" action="submitAudit"
		modelAttribute="agentModel">
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						 <h4 class="modal-title" id="myModalLabel">Agent Details</h4>
						
					</div>
					<%-- <div class="row">

						<div class="col-md-3">
							<!-- <span>First Name</span><br /> <input type="text" id="firstName"/> -->
							<span style="margin-left: 10px;"><b>Agent Name</b></span><br />
							<form:input id="agent_name" name="agent_name" path=""
								style="margin-left: 10px;" placeholder="Agent Name"
								onkeyup="this.value=this.value.replace(/[^a-zA-Z ]/g,'');" />
						</div>
						<div class="col-md-3">
							<span style="margin-left: 47px;"><b>Wizdom ID</b></span><br />
							<form:input id="agent_email" name="agent_email" path=""
								style="margin-left: 47px;" onkeyup="this.value=this.value.replace(/[^a-zA-Z 0-9]/g,'');"
								placeholder="Wizdom ID" />
							<!-- <span>Last Name</span><br /> <input type="text" id="lastName"/> -->
						</div>
						<div class="col-md-3">
							<span style="margin-left: 85px;"><b>Mobile</b></span><br />
							<!-- <input type="text" id="mobile"/> -->
							<form:input type="text" id="agent_mobile"
								style="margin-left: 85px;" placeholder="Mobile"
								name="agent_mobile" value="" path=""
								onkeypress='return event.charCode>=48&&event.charCode<=57' />
						</div>
					</div> --%>
					<%-- <div class="modal-header">

						<h4 class="modal-title" id="myModalLabel">${storeName}: Audit Preview</h4>
						*<font style="color: red; font-weight: bold">Red</font>  - Pending audit category &nbsp;&nbsp; *
						<font style="color: green; font-weight: bold" >Green</font> - Completed audit category
					</div> --%>
					<div class="modal-body">
						<ul class="value-ul">
						<li style="padding:0px; border:0px;">
                            <p style="font-size: 16px; padding: 15px; color:#2c7dfb; font-weight:bold; background:#fff;">${storeName} (${userName}): Audit Preview</p>
                            <div class="row pop-form">
                                <div class="col-md-12">
                                    <p>Enter Your Details Below</p>
                                </div>
                                
                               
                                <div class="col-md-4">
                                 <!--  <span><b>Agent Name</b></span><br /> -->
							<form:input id="agent_name" name="agent_name" path=""
								placeholder="Agent Name"
								onkeyup="this.value=this.value.replace(/[^a-zA-Z ]/g,'');" />

                                </div>
                                <div class="col-md-4">
                                   <!--  <span><b>Wizdom ID</b></span><br /> -->
							<form:input id="agent_email" name="agent_email" path=""
								onkeyup="this.value=this.value.replace(/[^a-zA-Z 0-9]/g,'');"
								placeholder="Wizdom ID" />

                                </div>
                                <div class="col-md-4">
                                  <!--  <span><b>Mobile</b></span><br /> -->
							<!-- <input type="text" id="mobile"/> -->
							<form:input type="text" id="agent_mobile"
								placeholder="Mobile"
								name="agent_mobile" value="" path=""
								onkeypress='return event.charCode>=48&&event.charCode<=57' />

                                </div>
                               
                            </div>
                        </li>
						
						</ul>
						<div id="result"></div>
					</div>


					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Continue
							Edit</button>
						<button type="button" class="btn btn-primary"
							id="previewSubmit" onclick="previewSubmitCheck()">Submit Audit</button>
							<!-- <button type="button" data-toggle="modal"
							data-target="#submitModal" class="btn btn-primary"
							id="previewSubmit">Submit Audit</button> -->
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="submitModal" tabindex="-1" role="dialog"
			aria-labelledby="submitModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="submitModalLabel">Confirmation</h4>
					</div>
					<div class="modal-body">
						<div id="confirmationId">Audit once Submitted cannot be
							Edited again. Please Click 'Yes' to Submit Or 'No' to Continue
							Audit.</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" onclick="dismissNo()">No</button>
						<button type="submit" class="btn btn-primary"
							onclick="checkingAgent()">Yes</button>
					</div>
				</div>
			</div>
		</div>
	</form:form>

 <script type="text/javascript">
  $( "#agentSubmitForm" ).validate({
	  rules: {
		  agent_mobile:{
	    	maxlength: 10,
	    	minlength:10,
	    	required : true
	    },
	    agent_name:{
	    	required : true
	    }
	       
	  }
	});  
	

  </script>

</body>
</html>