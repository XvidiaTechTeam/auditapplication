<%@include file="include.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Activation Page</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
<link href="${pageContext.request.contextPath}/css/bootstrap.min.css"
	rel="stylesheet" />
<link href="${pageContext.request.contextPath}/css/vowmee-design.css"
	rel="stylesheet" />
<link href="${pageContext.request.contextPath}/css/oversheet.css"
	rel="stylesheet" />
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

</head>
<body id="style-4" class="intro-bg">
	<div class="header-d">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-4 col-xs-7">
					<a href="..${pageContext.request.contextPath}/login"><img
						src="${pageContext.request.contextPath}/webskin/logo.png" /></a>
				</div>
				<div class="col-md-8 col-sm-8 col-xs-5 ">
					<h3 class="hidden-xs" style="text-align: right;">Vowmee Audit
						App</h3>
				</div>
			</div>
		</div>
	</div>

	<div style="" class="container">
		<div class="frm-cnt">
			<div class="row">
				<div class="col-md-12">
					<center style="padding-top: 121px;">
						<font color="white">${getEmail} Registered Successfully
							Please go to your email to activate your account <br> <a
							href="https://mail.google.com/mail/">gmail</a>
						</font>
					</center>
				</div>
			</div>
		</div>
	</div>





</body>
</body>
</html>