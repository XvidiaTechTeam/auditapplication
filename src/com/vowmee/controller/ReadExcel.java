package com.vowmee.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.vowmee.bean.AuditLookupBean;
import com.vowmee.bean.LocationBean;
import com.vowmee.bean.Store;
import com.vowmee.bean.StoreBean;
import com.vowmee.bean.UserCircleVO;
import com.vowmee.bean.UserManagementBean;
import com.vowmee.bean.UserRoleBean;
import com.vowmee.bean.UserStatusBean;
import com.vowmee.bean.UserTypeBean;
import com.vowmee.dao.UserManagementDao;
import com.vowmee.service.RegisterService;

public class ReadExcel {

	@Autowired
	private UserManagementDao usermanagementDao;
	@Autowired
	RegisterService registerService;

	public List<UserManagementBean> readExcel(CommonsMultipartFile inputfile, String filename, String filePath,
			List<UserStatusBean> userstatusList, List<UserTypeBean> usertypeList, List<UserRoleBean> userRoleList,
			 List<UserCircleVO> userCircle,List<Store> storeList,int statusId) {
		List<UserManagementBean> userUploadList = new ArrayList<UserManagementBean>();
		
		System.out.println("file name " + inputfile);
		String fileType = filename.substring(filename.lastIndexOf("."), filename.length());
		try {
			// getFilePath(filename);
			userUploadList = buildUserDetailsFromExcel(com.vowmee.utill.ExcelSheetReader.returnRowDataList(fileType, 0, inputfile),userRoleList,userCircle,storeList);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userUploadList;
	}
	
	public List<StoreBean> readExcel(CommonsMultipartFile inputfile, String filename, String filePath,List<LocationBean> locationList,
			 List<UserCircleVO> userCircle) {
		List<StoreBean> storeUploadList = new ArrayList<StoreBean>();
		
		System.out.println("file name " + inputfile);
		String fileType = filename.substring(filename.lastIndexOf("."), filename.length());
		try {
			// getFilePath(filename);
			storeUploadList = buildStoreDetailsFromExcel(com.vowmee.utill.ExcelSheetReader.returnRowDataList(fileType, 0, inputfile),locationList,userCircle);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return storeUploadList;
	}
	
	public List<AuditLookupBean> readExcel(CommonsMultipartFile inputfile, String filename, String filePath,
			 List<UserCircleVO> userCircle) {
		List<AuditLookupBean> auditUploadList = new ArrayList<AuditLookupBean>();
		
		System.out.println("file name " + inputfile);
		String fileType = filename.substring(filename.lastIndexOf("."), filename.length());
		try {
			// getFilePath(filename);
			auditUploadList = buildAuditDetailsFromExcel(com.vowmee.utill.ExcelSheetReader.returnRowDataList(fileType, 0, inputfile),userCircle);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return auditUploadList;
	}
	
	
	public static List<UserManagementBean> getFailedRecords(List<UserManagementBean> userDetails) {
		List<UserManagementBean> failedUserList = new ArrayList<UserManagementBean>();
		try {
	
			for (UserManagementBean user : userDetails) {
				if(user.getReasonForFail()!=null){
					failedUserList.add(user);
				}
			}
			
			System.out.println("user final : "+failedUserList);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return failedUserList;

	}
	public static List<StoreBean> getFailedRecordsForStore(List<StoreBean> store) {
		List<StoreBean> failedStoreList = new ArrayList<StoreBean>();
		try {
	
			for (StoreBean storeVo : store) {
				if(storeVo.getReasonForFail()!=null){
					failedStoreList.add(storeVo);
				}
			}
			
			System.out.println("user final : "+failedStoreList);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return failedStoreList;

	}
	public static List<AuditLookupBean> getFailedRecordsForAudit(List<AuditLookupBean> audit) {
		List<AuditLookupBean> failedAuditList = new ArrayList<AuditLookupBean>();
		try {
	
			for (AuditLookupBean storeVo : audit) {
				if(storeVo.getReasonForFail()!=null){
					failedAuditList.add(storeVo);
				}
			}
			
			System.out.println("user final : "+failedAuditList);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return failedAuditList;

	}
	
	public static Map<String,String> getPassword(List<UserManagementBean> userDetails){
		Map<String,String> userPassword = new HashMap<>();
		for(UserManagementBean user:userDetails){
			userPassword.put(user.getEmail(), user.getPassword());
		}
		return userPassword;
	}

	public static List<UserManagementBean> getUserDetails(List<UserManagementBean> userDetails,
			List<UserRoleBean> userRoleList,int statusId) {
		List<UserManagementBean> saveToDbuserDetails = new ArrayList<>();
		try {
			
			
			Set<String> userEmails = new HashSet<String>();
			
			for (UserManagementBean user : userDetails) {
				userEmails.add(user.getEmail());
			}
			for (String email : userEmails) {
				List<String> userstores = new ArrayList<String>();
				List<String> userCircles = new ArrayList<String>();
				int randomNumber = (int) (Math.random() * 90000);
				String randomString = String.valueOf(randomNumber);
				UserManagementBean userfinal = new UserManagementBean();
				for (UserManagementBean user : userDetails) {
					if (email != null && email.equalsIgnoreCase(user.getEmail())) {
						userstores.add(user.getStoreName());
						userCircles.add(user.getCircleName());
						userfinal.setRoleName(user.getRoleName());
						userfinal.setFirstName(user.getFirstName());
						userfinal.setLastName(user.getLastName());
						userfinal.setEmail(user.getEmail());
						userfinal.setMobile(user.getMobile());
					}
				}
				userfinal.setStoreList(userstores);
				userfinal.setCircleList(userCircles);
				userfinal.setPassword(randomString);
				userfinal.setStatus(statusId);
				saveToDbuserDetails.add(userfinal);
			}
			
			System.out.println("user final : "+saveToDbuserDetails);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return saveToDbuserDetails;

	}
	
	public static List<StoreBean> getUserDetails(List<StoreBean> storeDetails) {
		List<StoreBean> saveToDbuserDetails = new ArrayList<>();
		try {
			
	
			StoreBean storefinal = new StoreBean();
				for (StoreBean store : storeDetails) {
					
						
					    storefinal.setStore_code(store.getStore_code());
					    storefinal.setStore_name(store.getStore_name());
					    storefinal.setLocation(store.getLocation());
					    storefinal.setCircle_name(store.getCircle_name());
						
				         saveToDbuserDetails.add(storefinal);
				}
			
			System.out.println("user final : "+saveToDbuserDetails);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return saveToDbuserDetails;

	}
	public static List<AuditLookupBean> getAuditDetails(List<AuditLookupBean> auditDetails) {
		List<AuditLookupBean> saveToDbuserDetails = new ArrayList<>();
		try {
			
	
			AuditLookupBean auditfinal = new AuditLookupBean();
				for (AuditLookupBean audit : auditDetails) {
					
						
					auditfinal.setAudit_category(audit.getAudit_category());
					auditfinal.setAudit_name(audit.getAudit_name());
					auditfinal.setCircle(audit.getCircle());
					auditfinal.setFlag(audit.getFlag());
						
				         saveToDbuserDetails.add(auditfinal);
				}
			
			System.out.println("user final : "+saveToDbuserDetails);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return saveToDbuserDetails;

	}

	/*
	 * private void getFilePath(String name) {
	 * 
	 * File serverFile = null; BufferedOutputStream stream; try { byte[] bytes =
	 * name.getBytes(); String rootPath = System.getProperty("catalina.home");
	 * 
	 * File dir = new File(rootPath + File.separator + "tmpFiles"); if
	 * (!dir.exists()) dir.mkdirs(); serverFile = new File(dir.getAbsolutePath()
	 * + File.separator + name); // Create the file on server Path path =
	 * Paths.get(dir.getAbsolutePath() + File.separator + name);
	 * Files.write(path, bytes);
	 * 
	 * Files.write(path, bytes);
	 * 
	 * stream = new BufferedOutputStream( new FileOutputStream(serverFile));
	 * stream.write(bytes); stream.close(); } catch (Exception e) { // TODO
	 * Auto-generated catch block e.printStackTrace(); }
	 * 
	 * 
	 * }
	 */
	public List<StoreBean> buildStoreDetailsFromExcel(List<List<org.apache.poi.ss.usermodel.Cell>> rowDataList,
			List<LocationBean> locationList,List<UserCircleVO> userCircle )
			throws Exception {

		try {
			List<StoreBean> storeList = new ArrayList<StoreBean>();
			
			String circle = null;
			String storeName = null;
			String location = null;
			String storeCode = null;
			if (rowDataList != null) {
				for (int idx = 1; idx < rowDataList.size(); idx++) {
					StringBuilder reasonForFail=new StringBuilder();
					List<org.apache.poi.ss.usermodel.Cell> row = rowDataList.get(idx);

					StoreBean store = new StoreBean();

					if (rowDataList.get(0).get(0).toString().equalsIgnoreCase("Store Code")) {
						storeCode = (com.vowmee.utill.ExcelSheetReader.getValue(row, "Store Code") != null)
								? com.vowmee.utill.ExcelSheetReader.getValue(row, "Store Code").trim() : "";
					} else {
						storeCode = "null";
					}

					if (rowDataList.get(0).get(1).toString().equalsIgnoreCase("Store Name")) {
						storeName = (com.vowmee.utill.ExcelSheetReader.getValue(row, "Store Name") != null)
								? com.vowmee.utill.ExcelSheetReader.getValue(row, "Store Name").trim() : "";
					} else {
						storeName = "null";
					}
/*
					if (rowDataList.get(0).get(3).toString().equalsIgnoreCase("Store Type")) {
						storeType = (com.vowmee.utill.ExcelSheetReader.getValue(row, "Store Type") != null)
								? com.vowmee.utill.ExcelSheetReader.getValue(row, "Store Type").trim() : "";
					} else {
						storeType = "null";
					}
*/
				
					if (rowDataList.get(0).get(2).toString().equalsIgnoreCase("Location")) {
						location = (com.vowmee.utill.ExcelSheetReader.getValue(row, "Location") != null)
								? com.vowmee.utill.ExcelSheetReader.getValue(row, "Location").trim() : "";
					} else {
						location = "null";
					}

					if (rowDataList.get(0).get(3).toString().equalsIgnoreCase("Circle")) {
						circle = (com.vowmee.utill.ExcelSheetReader.getValue(row, "Circle") != null)
								? com.vowmee.utill.ExcelSheetReader.getValue(row, "Circle").trim() : "";
					} else {
						circle = "null";
					}

					

					if (circle != null && circle.trim().length() != 0) {
						for (UserCircleVO roleBean : userCircle) {
							if (roleBean.getCircleName().trim().equalsIgnoreCase(circle.trim())
									|| roleBean.getCircleCode().trim().equalsIgnoreCase(circle.trim())) {
								store.setCircle_name(roleBean.getId()+"");
							}
						}
					}
					
					if (location != null && location.trim().length() != 0) {
						for (LocationBean roleBean : locationList) {
							if (roleBean.getName().trim().equalsIgnoreCase(location.trim())) {
								store.setLocation(roleBean.getId()+"");
							}
						}
					}
					/*
					 * else{ user = null; break; }
					 */
					
					/*
					 * else{ user = null; break; }
					 */
					
					/*
					 * else{ user = null; break; }
					 */
					
					/*
					 * else{ user = null; break; }
					 */
					if (storeCode != null && storeCode.trim().length() != 0) {
						store.setStore_code(storeCode);
					}
					/*
					 * else{ user = null; break; }
					 */
					if (storeName != null && storeName.trim().length() != 0)
						store.setStore_name(storeName);
					/*
					 * else{ user = null; break; }
					 */

					

					if(store.getStore_name()!=null && store.getCircle_name()!=null && store.getStore_code()!=null 
							&& store.getLocation()!=null ){
						storeList.add(store);
					} else {
						if (store.getStore_code() == null) {
							reasonForFail.append("The record has no value in Store Code \n");
							if (store.getStore_name() == null) {
								reasonForFail.append("The record has no value in Store Name \n");
							}
							if (store.getCircle_name() == null) {
								reasonForFail.append("The record has no value in Circle \n");
							}
							if (store.getLocation() == null) {
								reasonForFail.append("The record has no value in Location \n");
							}
						} else
							reasonForFail.append("The record in the attachment contains invalid/empty values");

						store.setReasonForFail(reasonForFail.toString());
						storeList.add(store);
					}
				}
			}
			return storeList;

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	public List<AuditLookupBean> buildAuditDetailsFromExcel(List<List<org.apache.poi.ss.usermodel.Cell>> rowDataList,List<UserCircleVO> userCircle )
			throws Exception {

		try {
			List<AuditLookupBean> auditList = new ArrayList<AuditLookupBean>();
			
			String circle = null;
			String auditCategory = null;
			String auditName = null;
			String flag = null;
			if (rowDataList != null) {
				for (int idx = 1; idx < rowDataList.size(); idx++) {
					StringBuilder reasonForFail=new StringBuilder();
					List<org.apache.poi.ss.usermodel.Cell> row = rowDataList.get(idx);

					AuditLookupBean audit = new AuditLookupBean();

					if (rowDataList.get(0).get(0).toString().equalsIgnoreCase("Audit category")) {
						auditCategory = (com.vowmee.utill.ExcelSheetReader.getValue(row, "Audit category") != null)
								? com.vowmee.utill.ExcelSheetReader.getValue(row, "Audit category").trim() : "";
					} else {
						auditCategory = "null";
					}

					if (rowDataList.get(0).get(1).toString().equalsIgnoreCase("Audit name")) {
						auditName = (com.vowmee.utill.ExcelSheetReader.getValue(row, "Audit name") != null)
								? com.vowmee.utill.ExcelSheetReader.getValue(row, "Audit name").trim() : "";
					} else {
						auditName = "null";
					}
				
					if (rowDataList.get(0).get(2).toString().equalsIgnoreCase("Circle")) {
						circle = (com.vowmee.utill.ExcelSheetReader.getValue(row, "Circle") != null)
								? com.vowmee.utill.ExcelSheetReader.getValue(row, "Circle").trim() : "";
					} else {
						circle = "null";
					}

					if (rowDataList.get(0).get(3).toString().equalsIgnoreCase("Mandatory")) {
						flag = (com.vowmee.utill.ExcelSheetReader.getValue(row, "Mandatory") != null)
								? com.vowmee.utill.ExcelSheetReader.getValue(row, "Mandatory").trim() : "";
					} else {
						flag = "null";
					}

					

					if (circle != null && circle.trim().length() != 0) {
						for (UserCircleVO roleBean : userCircle) {
							if (roleBean.getCircleName().trim().equalsIgnoreCase(circle.trim())
									|| roleBean.getCircleCode().trim().equalsIgnoreCase(circle.trim())) {
								audit.setCircle(roleBean.getId()+"");
							}
						}
					}
					
					if (auditCategory != null && auditCategory.trim().length() != 0) {
						audit.setAudit_category(auditCategory);
					}
					/*
					 * else{ user = null; break; }
					 */
					if (auditName != null && auditName.trim().length() != 0)
						audit.setAudit_name(auditName);
					
					if (flag != null && flag.trim().length() != 0)
						audit.setFlag(flag);
					/*
					 * else{ user = null; break; }
					 */

					

					if(audit.getAudit_category()!=null && audit.getAudit_name()!=null && audit.getCircle()!=null && audit.getFlag()!=null){
						auditList.add(audit);
					} else {
						if (audit.getAudit_category() == null) {
							reasonForFail.append("The record has no value in Audit Category \n");
							if (audit.getAudit_name() == null) {
								reasonForFail.append("The record has no value in Audit Question \n");
							}
							if (audit.getCircle() == null) {
								reasonForFail.append("The record has no value in Circle \n");
							}
							if (audit.getFlag() == null) {
								reasonForFail.append("The record has no value in Mandatory \n");
							}
						} else
							reasonForFail.append("The record in the attachment contains invalid/empty values");

						audit.setReasonForFail(reasonForFail.toString());
						auditList.add(audit);
					}
				}
			}
			return auditList;

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	public List<UserManagementBean> buildUserDetailsFromExcel(List<List<org.apache.poi.ss.usermodel.Cell>> rowDataList, List<UserRoleBean> userRoleList
			,List<UserCircleVO> userCircle,List<Store> storeList )
			throws Exception {

		try {
			List<UserManagementBean> userList = new ArrayList<UserManagementBean>();
			
			String circle = null;
			String storeName = null;
			String storeType = null;
			String roleName = null;
			String firstName = null;
			String lastName = null;
			String email = null;
			String mobile = null;
			if (rowDataList != null) {
				for (int idx = 1; idx < rowDataList.size(); idx++) {
					StringBuilder reasonForFail=new StringBuilder();
					List<org.apache.poi.ss.usermodel.Cell> row = rowDataList.get(idx);

					UserManagementBean user = new UserManagementBean();

					if (rowDataList.get(0).get(0).toString().equalsIgnoreCase("Circle")) {
						circle = (com.vowmee.utill.ExcelSheetReader.getValue(row, "Circle") != null)
								? com.vowmee.utill.ExcelSheetReader.getValue(row, "Circle").trim() : "";
					} else {
						circle = "null";
					}

					if (rowDataList.get(0).get(1).toString().equalsIgnoreCase("Store Name")) {
						storeName = (com.vowmee.utill.ExcelSheetReader.getValue(row, "Store Name") != null)
								? com.vowmee.utill.ExcelSheetReader.getValue(row, "Store Name").trim() : "";
					} else {
						storeName = "null";
					}
/*
					if (rowDataList.get(0).get(3).toString().equalsIgnoreCase("Store Type")) {
						storeType = (com.vowmee.utill.ExcelSheetReader.getValue(row, "Store Type") != null)
								? com.vowmee.utill.ExcelSheetReader.getValue(row, "Store Type").trim() : "";
					} else {
						storeType = "null";
					}
*/
				
					if (rowDataList.get(0).get(2).toString().equalsIgnoreCase("FST Name")) {
						firstName = (com.vowmee.utill.ExcelSheetReader.getValue(row, "FST Name") != null)
								? com.vowmee.utill.ExcelSheetReader.getValue(row, "FST Name").trim() : "";
					} else {
						firstName = "null";
					}

					if (rowDataList.get(0).get(3).toString().equalsIgnoreCase("LST NAME")) {
						lastName = (com.vowmee.utill.ExcelSheetReader.getValue(row, "LST NAME") != null)
								? com.vowmee.utill.ExcelSheetReader.getValue(row, "LST NAME").trim() : "";
					} else {
						lastName = "null";
					}

					if (rowDataList.get(0).get(4).toString().equalsIgnoreCase("EMAIL")) {
						email = (com.vowmee.utill.ExcelSheetReader.getValue(row, "EMAIL") != null)
								? com.vowmee.utill.ExcelSheetReader.getValue(row, "EMAIL").trim() : "";
					} else {
						email = "null";
					}

					if (rowDataList.get(0).get(5).toString().equalsIgnoreCase("MOBILE")) {
						mobile = (com.vowmee.utill.ExcelSheetReader.getValue(row, "MOBILE") != null)
								? com.vowmee.utill.ExcelSheetReader.getValue(row, "MOBILE").trim() : "";
						
					} else {
						mobile = "null";
					}
					if (rowDataList.get(0).get(6).toString().equalsIgnoreCase("User Role")) {
						roleName = (com.vowmee.utill.ExcelSheetReader.getValue(row, "User Role") != null)
								? com.vowmee.utill.ExcelSheetReader.getValue(row, "User Role").trim() : "";
					} else {
						roleName = "null";
					}


					if (circle != null && circle.trim().length() != 0) {
						for (UserCircleVO roleBean : userCircle) {
							if (roleBean.getCircleName().trim().equalsIgnoreCase(circle.trim())
									|| roleBean.getCircleCode().trim().equalsIgnoreCase(circle.trim())) {
								user.setCircleName(roleBean.getId()+"");
							}
						}
					}
					/*
					 * else{ user = null; break; }
					 */
					if (storeName != null && storeName.trim().length() != 0) {
						for (Store roleBean : storeList) {
							
							
							if ((roleBean.getStore_name().trim()).equalsIgnoreCase(storeName.trim())) {
								user.setStoreName(roleBean.getId()+"");
							
							}
							
							
						}
						
					}
					/*
					 * else{ user = null; break; }
					 */
					if (storeType != null && storeType.trim().length() != 0) {
						user.setStoreType(storeType);
					}
					/*
					 * else{ user = null; break; }
					 */
					if (roleName != null && roleName.trim().length() != 0) {
						for (UserRoleBean roleBean : userRoleList) {
							if ((roleBean.getRole().trim()).equalsIgnoreCase(roleName.trim())) {
								user.setRoleName(roleBean.getId()+"");
							}
						}
						
					}
					/*
					 * else{ user = null; break; }
					 */
					if (firstName != null && firstName.trim().length() != 0) {
						user.setFirstName(firstName);
					}
					/*
					 * else{ user = null; break; }
					 */
					if (lastName != null)
						user.setLastName(lastName);
					/*
					 * else{ user = null; break; }
					 */

					if (email != null)
						user.setEmail(email);
					/*
					 * else{ user = null; break; }
					 */
					if (mobile != null)
						user.setMobile(new BigDecimal(mobile).longValue()+"");
					/*
					 * else{ user = null; break; }
					 */

					if(user.getStoreName()!=null && user.getCircleName()!=null && user.getRoleName()!=null 
							&& user.getFirstName()!=null && user.getLastName()!=null && user.getEmail()!=null
							&& user.getMobile()!=null){
						userList.add(user);
					}else{
						if(user.getCircleName()==null)
						{
							reasonForFail.append("The record has no value in Circle \n");
						if(user.getStoreName()==null)
						{
							reasonForFail.append("The record has no value in Store \n");
							if(user.getRoleName()==null)
								reasonForFail.append("The record has no value in Role \n");
						}
						}
						else
							reasonForFail.append("The record in the attachment contains invalid/empty values");
								
		                user.setReasonForFail(reasonForFail.toString());
						userList.add(user);
					}
				}
			}
			return userList;

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public List<UserManagementBean> readExcelUser(CommonsMultipartFile inputfile, List<UserStatusBean> userstatusList,
			List<UserTypeBean> usertypeList, List<UserRoleBean> userRoleList) {
		List<UserManagementBean> userUploadList = new ArrayList<UserManagementBean>();

		try {
			// FileInputStream file = new FileInputStream(new File(inputfile));
			/*
			 * List<UserStatusBean> userstatusList = new
			 * ArrayList<UserStatusBean>(); List<UserTypeBean> usertypeList =
			 * new ArrayList<UserTypeBean>(); List<UserRoleBean> userRoleList =
			 * new ArrayList<UserRoleBean>(); userstatusList =
			 * usermanagementDao.selectAllUserStatus(); usertypeList =
			 * usermanagementDao.selectAllUserType(); userRoleList =
			 * usermanagementDao.selectAllRole();
			 */
			// Create Workbook instance holding reference to .xlsx file
			XSSFWorkbook workbook = new XSSFWorkbook(inputfile.getInputStream());

			// Get first/desired sheet from the workbook
			XSSFSheet sheet = workbook.getSheetAt(0);

			// Iterate through each rows one by one
			Iterator<Row> rowIterator = sheet.iterator();
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				// For each row, iterate through all the columns
				Iterator<Cell> cellIterator = row.cellIterator();
				UserManagementBean usermanagaementBean = new UserManagementBean();
				if (row.getRowNum() != 0) {
					while (cellIterator.hasNext()) {
						Cell cell = cellIterator.next();
						int columnIndex = cell.getColumnIndex();
						// Check the cell type and format accordingly
						switch (columnIndex) {
						case 0:
							usermanagaementBean.setEmail((String) getCellValue(cell));
							break;
						case 1:
							usermanagaementBean.setPassword((String) getCellValue(cell));
							break;
						case 2:
							for (UserStatusBean stausbean : userstatusList) {
								String status = (String) getCellValue(cell);
								if (stausbean.getUserStatus().toUpperCase().equals(status.toUpperCase())) {
									usermanagaementBean.setStatus(stausbean.getId());
								}
							}
							break;

						case 3:
							for (UserTypeBean typeBean : usertypeList) {
								String type = (String) getCellValue(cell);
								if (typeBean.getType_name().equals(type.toUpperCase())) {
									usermanagaementBean.setUsertype(typeBean.getId());
								}
							}
							break;

						case 4:
							for (UserRoleBean roleBean : userRoleList) {
								String role = (String) getCellValue(cell);
								if (roleBean.getRole().trim().toUpperCase().equals(role.trim().toUpperCase())) {
									usermanagaementBean.setUserrole(roleBean.getId());
									System.out.println(roleBean.getId());

								}
							}
							break;

						case 5:
							Double mob = (Double) getCellValue(cell);
							usermanagaementBean.setMobile(String.valueOf(mob));
							break;
						}
					}
					userUploadList.add(usermanagaementBean);
				}

				System.out.println(usermanagaementBean);
			}
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userUploadList;
	}

	private Object getCellValue(Cell cell) {
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_STRING:
			return cell.getStringCellValue();

		case Cell.CELL_TYPE_BOOLEAN:
			return cell.getBooleanCellValue();

		case Cell.CELL_TYPE_NUMERIC:
			return cell.getNumericCellValue();
		}

		return null;
	}
}
