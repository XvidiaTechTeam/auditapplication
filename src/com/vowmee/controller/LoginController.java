package com.vowmee.controller;



import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.vowmee.auditHistory.AuditHistoryService;
import com.vowmee.bean.AuditHistoryBean;
import com.vowmee.bean.LoginBean;
import com.vowmee.bean.UserBean;
import com.vowmee.service.LoginService;
import com.vowmee.utill.AuditHistoryUtil;


@Controller
public class LoginController {
	@Autowired
	private LoginService loginService;
	
	@Autowired
	private AuditHistoryService auditHistoryService;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView displayLogin(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView model = new ModelAndView("login");
		LoginBean loginBean = new LoginBean();
		model.addObject("loginBean", loginBean);
		return model;
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ModelAndView executeLogin(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("loginBean") UserBean loginBean) {
		ModelAndView model = null;
		AuditHistoryUtil loginAuditHistoryUtil = new AuditHistoryUtil(); 
		AuditHistoryBean auditHistoryBean = new AuditHistoryBean();
		try {
			request.getSession().invalidate();
			String path= request.getRequestURI();
			boolean isValidUser = loginService.isValidUser(loginBean.getName(), loginBean.getPassword(), request);
			
			System.out.println("valid user "+isValidUser);
			System.out.println("session value "+ request.getSession().getAttribute("inactive"));
			String status = (String) request.getSession().getAttribute("inactive");
			String yet_to_activate = (String) request.getSession().getAttribute("yet_to_activate");
			String registered = (String) request.getSession().getAttribute("registered");
			String auditor = (String) request.getSession().getAttribute("auditor");
			String admin = (String) request.getSession().getAttribute("admin");
			String superAdmin = (String) request.getSession().getAttribute("superAdmin");
			Integer auditUserId = (Integer) request.getSession().getAttribute("userId");
			if (isValidUser) {
				System.out.println("User Login Successful");
				
				request.getSession().setAttribute("loggedInUser", loginBean.getFirstName());
				if(admin=="admin" || superAdmin=="superAdmin"){
					
					auditHistoryBean=loginAuditHistoryUtil.auditHistoryBean(auditUserId, auditUserId, "User Loggined", "", "");
					auditHistoryService.insertAuditHistory(auditHistoryBean);
					
					
					model = new ModelAndView("homePage");
					//model = new ModelAndView("redirect:usermanagement");
				}else if(auditor=="auditor"){
					auditHistoryBean=loginAuditHistoryUtil.auditHistoryBean(auditUserId, auditUserId, "User Loggined", "", "");
					auditHistoryService.insertAuditHistory(auditHistoryBean);
					
					model = new ModelAndView("homePage");
					//model = new ModelAndView("redirect:audit");
				}else if(registered=="registered"){
					model = new ModelAndView("welcome");
				}else{
					model = new ModelAndView("homePage");
					auditHistoryBean=loginAuditHistoryUtil.auditHistoryBean(auditUserId, auditUserId, "User Loggined", "", "");
					auditHistoryService.insertAuditHistory(auditHistoryBean);
				}
				
			}else if(status =="inactive"){
				loginBean=new UserBean();
				model = new ModelAndView("login");
				model.addObject("loginBean", loginBean);
				request.setAttribute("registered_message", "Your account is not in activation");
			}else if(yet_to_activate=="yet_to_activate"){
				loginBean=new UserBean();
				model = new ModelAndView("login");
				model.addObject("loginBean", loginBean);
				request.setAttribute("yet_to_activate", "Please activate your account");
			}else {
				loginBean=new UserBean();
				request.getSession().invalidate();
				model = new ModelAndView("login");
				model.addObject("loginBean", loginBean);
				request.setAttribute("message", "Invalid credentials!!");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return model;
	}
	

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public ModelAndView logOut(HttpServletRequest request, HttpServletResponse response) {
		request.getSession().invalidate();
		String path= request.getContextPath();
		String url = "http://audit.xvidiaglobal.com:8080"+path+"/login";
		ModelAndView model = new ModelAndView("redirect:"+url);
		return model;
	}
	
	@RequestMapping(value = "createAudit/homePage", method = RequestMethod.GET)
	public ModelAndView homePage(HttpServletRequest request, HttpServletResponse response) {
		//request.getSession().invalidate();
		ModelAndView model = null;
		try{
			String path= request.getContextPath();
			UserBean userBean = (UserBean) request.getSession().getAttribute("loginBean");
			String getCompletedCount = loginService.getCompletedCount(userBean.getId());
			String getIncompletedCount = loginService.getIncompletedCount(userBean.getId());
			request.getSession().setAttribute("getCompletedCount", getCompletedCount);
			request.getSession().setAttribute("getIncompletedCount", getIncompletedCount);
			String url = "http://audit.xvidiaglobal.com:8080"+path+"/homePage";
			 model = new ModelAndView("redirect:"+url);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return model;
	}
	
	@RequestMapping(value = "continueAudit/homePage", method = RequestMethod.GET)
	public ModelAndView continueAudithomePage(HttpServletRequest request, HttpServletResponse response) {
		//request.getSession().invalidate();
		ModelAndView model = null;
		try{
			String path= request.getContextPath();
			UserBean userBean = (UserBean) request.getSession().getAttribute("loginBean");
			String getCompletedCount = loginService.getCompletedCount(userBean.getId());
			String getIncompletedCount = loginService.getIncompletedCount(userBean.getId());
			request.getSession().setAttribute("getCompletedCount", getCompletedCount);
			request.getSession().setAttribute("getIncompletedCount", getIncompletedCount);
			String url = "http://audit.xvidiaglobal.com:8080"+path+"/homePage";
			 model = new ModelAndView("redirect:"+url);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return model;
	}
	
	@RequestMapping(value = "createAudit/logout", method = RequestMethod.GET)
	public ModelAndView createAuditlogOut(HttpServletRequest request, HttpServletResponse response) {
		request.getSession().invalidate();
		String path= request.getContextPath();
		String url = "http://audit.xvidiaglobal.com:8080"+path+"/login";
		ModelAndView model = new ModelAndView("redirect:"+url);
		return model;
	}
	
	@RequestMapping(value = "continueAudit/logout", method = RequestMethod.GET)
	public ModelAndView continueAuditlogOut(HttpServletRequest request, HttpServletResponse response) {
		request.getSession().invalidate();
		String path= request.getContextPath();
		String url = "http://audit.xvidiaglobal.com:8080"+path+"/login";
		ModelAndView model = new ModelAndView("redirect:"+url);
		return model;
	}
	
	@RequestMapping(value = "continueAudit/login", method = RequestMethod.GET)
	public ModelAndView continueAuditlogIn(HttpServletRequest request, HttpServletResponse response) {
		request.getSession().invalidate();
		String path= request.getContextPath();
		String url = "http://audit.xvidiaglobal.com:8080"+path+"/login";
		ModelAndView model = new ModelAndView("redirect:"+url);
		return model;
	}
	
	
}
