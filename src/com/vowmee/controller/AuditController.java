package com.vowmee.controller;

import com.google.gson.Gson;
import com.vowmee.auditHistory.AuditHistoryService;
import com.vowmee.bean.AuditBean;
import com.vowmee.bean.AuditCategory;
import com.vowmee.bean.AuditHistoryBean;
import com.vowmee.bean.AuditLookupBean;
import com.vowmee.bean.AuditMaster;
import com.vowmee.bean.CompletedAuditsVO;
import com.vowmee.bean.ScoreCardBO;
import com.vowmee.bean.Store;
import com.vowmee.bean.UserBean;
import com.vowmee.bean.UserPreviousAuditHytory;
import com.vowmee.service.AuditService;
import com.vowmee.service.EmailService;
import com.vowmee.service.LoginService;
import com.vowmee.utill.AuditHistoryUtil;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.activation.DataSource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AuditController
{
  @Autowired
  private AuditService auditService;
  @Autowired
  private EmailService emailService;
  @Autowired
  private JavaMailSender mailSender;
  @Autowired
  private LoginService loginService;
  @Autowired
  private AuditHistoryService auditHistoryService;
  private String category;
  private String previewCategory;
  
  public String getPreviewCategory()
  {
    return this.previewCategory;
  }
  
  public void setPreviewCategory(String previewCategory)
  {
    this.previewCategory = previewCategory;
  }
  
  public String getCategory()
  {
    return this.category;
  }
  
  public void setCategory(String category)
  {
    this.category = category;
  }
  
  @RequestMapping(value={"/audit"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public ModelAndView createAudit(HttpServletRequest request, HttpServletResponse response)
  {
    ModelAndView model = null;
    try
    {
      UserBean user = (UserBean)request.getSession().getAttribute("loginBean");
      int userId = this.auditService.getUserId(user.getName());
      

      List<String> storeIdList = this.auditService.getStoreIdList(userId);
      if (storeIdList.size() != 0)
      {
        StringBuilder build = new StringBuilder();
        for (String storeId : storeIdList) {
          if (build.length() == 0) {
            build.append("'").append(storeId).append("'");
          } else {
            build.append(",'").append(storeId).append("'");
          }
        }
        List<Store> storeList = this.auditService.selectStoreById(build);
        Object storeCompletedList = new ArrayList();
        List<AuditMaster> incompleteStore = this.auditService.getIncompletedCount(build, userId);
        System.out.println("incompleteStore" + incompleteStore);
        for (Store store : storeList)
        {
          for (AuditMaster auditMaster : incompleteStore) {
            if (store.getId() == auditMaster.getStore()) {
              if (auditMaster.isAuditComplete()) {
                store.setCompletedFlag("(Completed)");
              } else {
                store.setCompletedFlag("(Pending)");
              }
            }
          }
          ((List)storeCompletedList).add(store);
        }
        System.out.println("storeCompletedList" + storeCompletedList);
        model = new ModelAndView("audit");
        




        model.addObject("storeList", storeCompletedList);
        model.addObject("storeName", new Store());
      }
      else
      {
        model = new ModelAndView("homePage");
        request.setAttribute("storeMessage", "No Store was Mapped to you. Please Contact Admin.");
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return model;
  }
  
  @RequestMapping(value={"/createAudit/{storeName}"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public ModelAndView displayCategory(HttpServletRequest request, HttpServletResponse response, @PathVariable("storeName") String storeName)
  {
    ModelAndView model = null;
    AuditHistoryUtil loginAuditHistoryUtil = new AuditHistoryUtil();
    AuditHistoryBean auditHistoryBean = new AuditHistoryBean();
    try
    {
      List<AuditLookupBean> auditLookupList = new ArrayList();
      
      Gson gson = new Gson();
      UserBean userBean = (UserBean)request.getSession().getAttribute("loginBean");
      long auditMasterId = this.auditService.insertAuditMaster(storeName, userBean.getId());
      Integer auditUserId = (Integer)request.getSession().getAttribute("userId");
      String firstname = (String)request.getSession().getAttribute("firstname");
      auditHistoryBean = loginAuditHistoryUtil.auditHistoryBean(auditUserId.intValue(), auditUserId.intValue(), "started Audit", "", "");
      this.auditHistoryService.insertAuditHistory(auditHistoryBean);
      int storeId = this.auditService.getStoreIdByName(storeName);
      request.getSession().setAttribute("storeId", Integer.valueOf(storeId));
      request.getSession().setAttribute("auditMasterId", Long.valueOf(auditMasterId));
      request.getSession().setAttribute("storeName", storeName);
      List<String> auditCategoryList1 = this.auditService.getAuditCategory(auditMasterId);
      List<AuditBean> auditPreviewList = new ArrayList();
      Map<String, String> auditCategoryList = new LinkedHashMap();
      if (auditCategoryList1 != null) {
        setCategory((String)auditCategoryList1.get(0));
      }
      for (String auditCategory : auditCategoryList1) {
        auditCategoryList.put(auditCategory, auditCategory);
      }
      System.out.println(auditLookupList);
      auditLookupList = this.auditService.getAuditQuestions((String)auditCategoryList1.get(0));
      System.out.println(auditLookupList);
      model = new ModelAndView("brandExperience");
      model.addObject("auditLookupList", auditLookupList);
      model.addObject("auditLookupSize", Integer.valueOf(auditLookupList.size()));
      model.addObject("auditPreviewJaon", gson.toJson(auditPreviewList));
      model.addObject("auditLookupListJson", gson.toJson(auditLookupList));
      model.addObject("auditCategoryList", auditCategoryList);
      model.addObject("storeName", storeName);
      model.addObject("userName", firstname);
      model.addObject("getCategory", gson.toJson(getCategory()));
      model.addObject("agentModel", new AuditMaster());
      model.addObject("auditCategoryMap", this.auditService.getAuditCategoryMap());
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return model;
  }
  
  @RequestMapping(value={"/getAuditQuestion/{name}"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public ModelAndView displayLogin(HttpServletRequest request, HttpServletResponse response, @PathVariable("name") String auditCategory)
  {
    List<AuditLookupBean> auditLookupList = new ArrayList();
    long auditMasterId = ((Long)request.getSession().getAttribute("auditMasterId")).longValue();
    List<String> auditCategoryList1 = this.auditService.getAuditCategory(auditMasterId);
    List<AuditCategory> auditCategoryList = new ArrayList();
    for (String auditCategoryName : auditCategoryList1)
    {
      AuditCategory audit = new AuditCategory();
      audit.setAudit_category(auditCategoryName);
      auditCategoryList.add(audit);
    }
    System.out.println("value " + request.getParameter("audit_category") + " wr " + auditCategory);
    auditLookupList = this.auditService.getAuditQuestions(auditCategory);
    ModelAndView model = new ModelAndView("brandExperience");
    model.addObject("auditLookupList", auditLookupList);
    model.addObject("auditCategoryList", auditCategoryList);
    model.addObject("auditCategory", new AuditCategory());
    model.addObject("agentModel", new AuditMaster());
    model.addObject("auditCategoryMap", this.auditService.getAuditCategoryMap());
    return model;
  }
  
  @RequestMapping(value={"/saveAudit"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  @ResponseBody
  public String ajaxInsertAuditData(@RequestBody String json, HttpServletRequest request)
    throws JSONException
  {
    JSONObject obj = new JSONObject();
    long auditMasterId = ((Long)request.getSession().getAttribute("auditMasterId")).longValue();
    this.auditService.insertAuditData(json, auditMasterId);
    List<Integer> auditCategoryList = this.auditService.getAuditLookUpCount();
    List<Integer> auditMasterList = this.auditService.getMasterLookUpCount(auditMasterId);
    List<Integer> auditMasterForOptionalList = this.auditService.getMasterLookUpCountForOptional(auditMasterId);
    System.out.println(auditCategoryList.get(0) + " " + auditMasterList.get(0));
    if ((auditCategoryList != null) && (auditMasterList != null) && (auditMasterForOptionalList != null) && 
      (auditCategoryList.get(0) >= auditMasterList.get(0)) && (((Integer)auditMasterForOptionalList.get(0)).intValue() > 0)) {
      obj.put("previewFlag", true);
    }
    System.out.println(obj);
    return obj.toString();
  }
  
  @RequestMapping(value={"getAuditQuestion/previewAudit"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public ModelAndView auditPreview(HttpServletRequest request, HttpServletResponse response)
  {
    long auditMasterId = ((Long)request.getSession().getAttribute("auditMasterId")).longValue();
    List<AuditCategory> auditCategoryList = new ArrayList();
    List<AuditBean> auditPreviewList = new ArrayList();
    List<String> auditCategoryList1 = this.auditService.getAuditPreviewList(auditMasterId);
    String storeName = (String)request.getSession().getAttribute("storeName");
    for (String audit : auditCategoryList1)
    {
      AuditCategory audit1 = new AuditCategory();
      audit1.setAudit_category(audit);
      auditCategoryList.add(audit1);
    }
    auditPreviewList = this.auditService.selectAuditPreview(auditMasterId, ((AuditCategory)auditCategoryList.get(0)).getAudit_category());
    
    Gson gson = new Gson();
    ModelAndView model = new ModelAndView("auditPreview");
    model.addObject("auditPreviewList", auditPreviewList);
    model.addObject("auditPreviewJaon", gson.toJson(auditPreviewList));
    model.addObject("auditCategoryList", auditCategoryList);
    model.addObject("storeName", storeName);
    return model;
  }
  
  @RequestMapping(value={"/createAudit/previewAudit"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public ModelAndView auditPreviewWithOutGetAudit(HttpServletRequest request, HttpServletResponse response)
  {
    long auditMasterId = ((Long)request.getSession().getAttribute("auditMasterId")).longValue();
    List<AuditBean> auditPreviewList = new ArrayList();
    List<String> auditCategoryList1 = this.auditService.getAuditPreviewList(auditMasterId);
    Map<String, String> auditCategoryList = new LinkedHashMap();
    String storeName = (String)request.getSession().getAttribute("storeName");
    String category = (String)auditCategoryList1.get(0);
    for (String auditCategory : auditCategoryList1) {
      auditCategoryList.put(auditCategory, auditCategory);
    }
    auditPreviewList = this.auditService.selectAuditPreview(auditMasterId, category);
    
    Gson gson = new Gson();
    ModelAndView model = new ModelAndView("auditPreview");
    model.addObject("auditPreviewList", auditPreviewList);
    model.addObject("auditPreviewJaon", gson.toJson(auditPreviewList));
    model.addObject("auditCategoryList", auditCategoryList);
    model.addObject("storeName", storeName);
    return model;
  }
  
  @RequestMapping(value={"/previewAudit"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public ModelAndView auditPreviewWithOutCreateAudit(HttpServletRequest request, HttpServletResponse response)
  {
    long auditMasterId = ((Long)request.getSession().getAttribute("auditMasterId")).longValue();
    List<AuditBean> auditPreviewList = new ArrayList();
    List<String> auditCategoryList1 = this.auditService.getAuditPreviewList(auditMasterId);
    Map<String, String> auditCategoryList = new LinkedHashMap();
    String storeName = (String)request.getSession().getAttribute("storeName");
    String category = (String)auditCategoryList1.get(0);
    for (String auditCategory : auditCategoryList1) {
      auditCategoryList.put(auditCategory, auditCategory);
    }
    auditPreviewList = this.auditService.selectAuditPreview(auditMasterId, category);
    
    Gson gson = new Gson();
    ModelAndView model = new ModelAndView("auditPreview");
    model.addObject("auditPreviewList", auditPreviewList);
    model.addObject("auditPreviewJaon", gson.toJson(auditPreviewList));
    model.addObject("auditCategoryList", auditCategoryList);
    model.addObject("storeName", storeName);
    return model;
  }
  
  @RequestMapping(value={"previewAudit/{name}"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public ModelAndView auditPreviewForEachQue(HttpServletRequest request, HttpServletResponse response, @PathVariable("name") String auditCategory)
  {
    long auditMasterId = ((Long)request.getSession().getAttribute("auditMasterId")).longValue();
    

    List<AuditCategory> auditCategoryList = new ArrayList();
    List<AuditBean> auditPreviewList = new ArrayList();
    List<String> auditCategoryList1 = this.auditService.getAuditPreviewList(auditMasterId);
    String storeName = (String)request.getSession().getAttribute("storeName");
    for (String audit : auditCategoryList1)
    {
      AuditCategory audit1 = new AuditCategory();
      audit1.setAudit_category(audit);
      auditCategoryList.add(audit1);
    }
    System.out.println("value " + request.getParameter("audit_category") + " wr " + auditCategory);
    auditPreviewList = this.auditService.selectAuditPreview(auditMasterId, auditCategory);
    
    Gson gson = new Gson();
    ModelAndView model = new ModelAndView("auditPreview");
    model.addObject("auditPreviewList", auditPreviewList);
    model.addObject("auditPreviewJaon", gson.toJson(auditPreviewList));
    model.addObject("auditCategoryList", auditCategoryList);
    model.addObject("storeName", storeName);
    return model;
  }
  
  @RequestMapping(value={"previewAudit/submitAudit"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public ModelAndView auditSubmit(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("agentModel") AuditMaster auditMaster)
  {
    long auditMasterId = ((Long)request.getSession().getAttribute("auditMasterId")).longValue();
    String userName = (String)request.getSession().getAttribute("username");
    String storeName = (String)request.getSession().getAttribute("storeName");
    Integer auditUserId = (Integer)request.getSession().getAttribute("userId");
    AuditHistoryUtil loginAuditHistoryUtil = new AuditHistoryUtil();
    AuditHistoryBean auditHistoryBean = new AuditHistoryBean();
    String path = request.getContextPath();
    auditHistoryBean = loginAuditHistoryUtil.auditHistoryBean(auditUserId.intValue(), auditUserId.intValue(), "Audit Completed", "", "");
    this.auditHistoryService.insertAuditHistory(auditHistoryBean);
    
    this.auditService.updateAuditStatus(auditMasterId, auditMaster.getAgent_name(), auditMaster.getAgent_email(), auditMaster.getAgent_mobile());
    insertStoreCardCalculation(request);
    int storeId = ((Integer)request.getSession().getAttribute("storeId")).intValue();
    List<String> percentageList = this.auditService.getScorePercentage(storeId);
    float scorePercentage = 0.0F;
    for (String percentage : percentageList) {
      scorePercentage += Float.valueOf(percentage).floatValue();
    }
    String htmlBody = "<html><body>Hi " + userName + ",<br><br> You have successfully completed audit." + 
      "<br/><br> Average Audit Score : " + scorePercentage / percentageList.size() + 
      "<br/><br> For Store : " + storeName + 
      "<br/><br>http://audit.xvidiaglobal.com:8080" + path + "</body></html>";
    String subject = storeName + ": Vowmee - Audit completed";
    this.emailService.commonSendEmail(this.mailSender, userName, htmlBody, subject);
    ModelAndView model = new ModelAndView("auditSuccess");
    model.addObject("storeName", storeName);
    return model;
  }
  
  @RequestMapping(value={"/submitAudit"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public ModelAndView auditSubmitWithOutAudit(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("agentModel") AuditMaster auditMaster)
  {
    ModelAndView model = new ModelAndView("auditSuccess");
    AuditHistoryUtil loginAuditHistoryUtil = new AuditHistoryUtil();
    AuditHistoryBean auditHistoryBean = new AuditHistoryBean();
    try
    {
      System.out.println("audit Master " + auditMaster);
      long auditMasterId = ((Long)request.getSession().getAttribute("auditMasterId")).longValue();
      String userName = (String)request.getSession().getAttribute("username");
      String path = request.getContextPath();
      Integer auditUserId = (Integer)request.getSession().getAttribute("userId");
      String storeName = (String)request.getSession().getAttribute("storeName");
      int storeId = ((Integer)request.getSession().getAttribute("storeId")).intValue();
      this.auditService.updateAuditStatus(auditMasterId, auditMaster.getAgent_name(), auditMaster.getAgent_email(), auditMaster.getAgent_mobile());
      insertStoreCardCalculation(request);
      auditHistoryBean = loginAuditHistoryUtil.auditHistoryBean(auditUserId.intValue(), auditUserId.intValue(), "Audit Completed", "", "");
      this.auditHistoryService.insertAuditHistory(auditHistoryBean);
      List<String> percentageList = this.auditService.getScorePercentage(storeId);
      float scorePercentage = 0.0F;
      for (String percentage : percentageList) {
        if (percentage != null) {
          scorePercentage += Float.valueOf(percentage).floatValue();
        }
      }
      List<ScoreCardBO> scoreCardList = this.auditService.scoreCardCalculation(auditMasterId);
      Object auditValueList = this.auditService.getAuditValue(auditMasterId);
      DataSource datasource = this.auditService.getAuditDataSource(scoreCardList, (List)auditValueList);
      String htmlBody = "<html><body>Hi " + userName + ",<br><br> You have successfully completed audit." + 
        "<br/><br> Average Audit Score : " + scorePercentage / percentageList.size() + 
        "<br/><br> For Store : " + storeName;
      String subject = storeName + ": Vowmee - Audit completed";
      
      this.emailService.commonSendEmailAttachment(this.mailSender, userName, htmlBody, subject, datasource, request);
      
      model.addObject("storeName", storeName);
      model.addObject("storeScore", Float.valueOf(scorePercentage / percentageList.size()));
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return model;
  }
  
  @RequestMapping(value={"/continueAudit/submitAudit"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public ModelAndView auditContinueSubmitWithOutAudit(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("agentModel") AuditMaster auditMaster)
  {
    ModelAndView model = new ModelAndView("auditSuccess");
    AuditHistoryUtil loginAuditHistoryUtil = new AuditHistoryUtil();
    AuditHistoryBean auditHistoryBean = new AuditHistoryBean();
    try
    {
      System.out.println("audit Master " + auditMaster);
      long auditMasterId = ((Long)request.getSession().getAttribute("auditMasterId")).longValue();
      String userName = (String)request.getSession().getAttribute("username");
      String path = request.getContextPath();
      String storeName = (String)request.getSession().getAttribute("storeName");
      this.auditService.updateAuditStatus(auditMasterId, auditMaster.getAgent_name(), auditMaster.getAgent_email(), auditMaster.getAgent_mobile());
      insertStoreCardCalculation(request);
      Integer auditUserId = (Integer)request.getSession().getAttribute("userId");
      auditHistoryBean = loginAuditHistoryUtil.auditHistoryBean(auditUserId.intValue(), auditUserId.intValue(), "Audit Completed", "", "");
      this.auditHistoryService.insertAuditHistory(auditHistoryBean);
      int storeId = ((Integer)request.getSession().getAttribute("storeId")).intValue();
      List<String> percentageList = this.auditService.getScorePercentage(storeId);
      float scorePercentage = 0.0F;
      for (String percentage : percentageList) {
        if (percentage != null) {
          scorePercentage += Float.valueOf(percentage).floatValue();
        }
      }
      List<ScoreCardBO> scoreCardList = this.auditService.scoreCardCalculation(auditMasterId);
      Object auditValueList = this.auditService.getAuditValue(auditMasterId);
      DataSource datasource = this.auditService.getAuditDataSource(scoreCardList, (List)auditValueList);
      String htmlBody = "<html><body>Hi " + userName + ",<br><br> You have successfully completed audit." + 
        "<br/><br> Average Audit Score : " + scorePercentage / percentageList.size() + 
        "<br/><br> For Store : " + storeName;
      String subject = storeName + ": Vowmee - Audit completed";
      
      this.emailService.commonSendEmailAttachment(this.mailSender, userName, htmlBody, subject, datasource, request);
      
      model.addObject("storeName", storeName);
      model.addObject("storeScore", Float.valueOf(scorePercentage / percentageList.size()));
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return model;
  }
  
  public AuditService getAuditService()
  {
    return this.auditService;
  }
  
  public void setAuditService(AuditService auditService)
  {
    this.auditService = auditService;
  }
  
  @RequestMapping(value={"/dummyCreate"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public ModelAndView brandExperience(HttpServletRequest request, HttpServletResponse response)
  {
    ModelAndView model = new ModelAndView("brandExperience");
    model.addObject("agentModel", new AuditMaster());
    return model;
  }
  
  @RequestMapping(value={"/homePage"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public ModelAndView homePage(HttpServletRequest request, HttpServletResponse response)
  {
    ModelAndView model = null;
    try
    {
      UserBean userBean = (UserBean)request.getSession().getAttribute("loginBean");
      String getCompletedCount = this.loginService.getCompletedCount(userBean.getId());
      String getIncompletedCount = this.loginService.getIncompletedCount(userBean.getId());
      request.getSession().setAttribute("getCompletedCount", getCompletedCount);
      request.getSession().setAttribute("getIncompletedCount", getIncompletedCount);
      model = new ModelAndView("homePage");
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return model;
  }
  
  @RequestMapping(value={"/selectCategory"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  @ResponseBody
  public String selectCategory(@RequestBody String json, HttpServletRequest request, HttpServletResponse response)
  {
    System.out.println(json);
    setCategory(json);
    
    return json;
  }
  
  @RequestMapping(value={"/getCategory"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  @ResponseBody
  public ModelAndView getCategoryList(HttpServletRequest request, HttpServletResponse response)
  {
    long auditMasterId = ((Long)request.getSession().getAttribute("auditMasterId")).longValue();
    List<AuditLookupBean> auditLookupList = new ArrayList();
    System.out.println("getCategory()" + getCategory());
    auditLookupList = this.auditService.getAuditQuestionsReplace(getCategory());
    List<String> auditCategoryList1 = this.auditService.getAuditCategory(auditMasterId);
    Map<String, String> auditCategoryList = new LinkedHashMap();
    String storeName = (String)request.getSession().getAttribute("storeName");
    List<AuditBean> auditPreviewList = new ArrayList();
    Map<String, List<AuditBean>> auditPreviewList1 = new HashMap();
    auditPreviewList1 = this.auditService.selectAllAuditPreviewList(auditMasterId);
    List<AuditBean> auditList = (List)auditPreviewList1.get("auditList");
    for (String auditCategory : auditCategoryList1) {
      auditCategoryList.put(auditCategory, auditCategory);
    }
    ModelAndView model = new ModelAndView("brandExperience");
    auditPreviewList1 = this.auditService.selectAllAuditPreviewList(auditMasterId);
    auditPreviewList = this.auditService.selectAuditPreview(auditMasterId, getCategory());
    Gson gson = new Gson();
    List<Integer> auditLookupCountList = this.auditService.getAuditLookUpCount();
    List<Integer> auditMasterList = this.auditService.getMasterLookUpCount(auditMasterId);
    List<Integer> auditMasterForOptionalList = this.auditService.getMasterLookUpCountForOptional(auditMasterId);
    model.addObject("auditPreviewList", auditPreviewList);
    System.out.println("auditPreviewList" + auditPreviewList);
    System.out.println("auditPreviewList" + gson.toJson(auditList));
    model.addObject("auditPreviewJaon", gson.toJson(auditList));
    model.addObject("auditLookupSize", Integer.valueOf(auditLookupList.size()));
    model.addObject("auditLookupListJson", gson.toJson(auditLookupList));
    model.addObject("auditLookupList", auditLookupList);
    model.addObject("auditCategoryList", auditCategoryList);
    model.addObject("auditCategory", new AuditCategory());
    model.addObject("getCategory", getCategory());
    model.addObject("storeName", storeName);
    String firstname = (String)request.getSession().getAttribute("firstname");
    model.addObject("userName", firstname);
    model.addObject("agentModel", new AuditMaster());
    model.addObject("auditCategoryMap", this.auditService.getAuditCategoryMap());
    if ((auditLookupCountList != null) && (auditMasterList != null) && (auditMasterForOptionalList != null) && (auditLookupCountList.get(0) >= auditMasterList.get(0)) && (((Integer)auditMasterForOptionalList.get(0)).intValue() > 0)) {
      model.addObject("previewFlag", Boolean.valueOf(true));
    }
    return model;
  }
  
  @RequestMapping(value={"/selectPreviewCategory"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  @ResponseBody
  public String selectPreviewCategory(@RequestBody String json, HttpServletRequest request, HttpServletResponse response)
  {
    System.out.println(json);
    setPreviewCategory(json);
    
    return json;
  }
  
  @RequestMapping(value={"/getPreviewCategory"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  @ResponseBody
  public ModelAndView getPreviewCategoryList(HttpServletRequest request, HttpServletResponse response)
  {
    long auditMasterId = ((Long)request.getSession().getAttribute("auditMasterId")).longValue();
    List<AuditBean> auditPreviewList = new ArrayList();
    List<String> auditCategoryList1 = this.auditService.getAuditPreviewList(auditMasterId);
    Map<String, String> auditCategoryList = new LinkedHashMap();
    String storeName = (String)request.getSession().getAttribute("storeName");
    for (String auditCategory : auditCategoryList1) {
      auditCategoryList.put(auditCategory, auditCategory);
    }
    auditPreviewList = this.auditService.selectAuditPreviewList(auditMasterId, getPreviewCategory());
    Gson gson = new Gson();
    ModelAndView model = new ModelAndView("auditPreview");
    model.addObject("auditPreviewList", auditPreviewList);
    model.addObject("auditPreviewJaon", gson.toJson(auditPreviewList));
    model.addObject("auditCategoryList", auditCategoryList);
    model.addObject("getPreviewCategory", getPreviewCategory());
    model.addObject("storeName", storeName);
    return model;
  }
  
  @RequestMapping(value={"/createAudit/submitAudit"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public ModelAndView auditSubmitAudit(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("agentModel") AuditMaster auditMaster)
  {
    ModelAndView model = new ModelAndView("auditSuccess");
    AuditHistoryUtil loginAuditHistoryUtil = new AuditHistoryUtil();
    AuditHistoryBean auditHistoryBean = new AuditHistoryBean();
    try
    {
      System.out.println("audit Master " + auditMaster);
      Integer auditUserId = (Integer)request.getSession().getAttribute("userId");
      long auditMasterId = ((Long)request.getSession().getAttribute("auditMasterId")).longValue();
      String userName = (String)request.getSession().getAttribute("username");
      String path = request.getContextPath();
      String storeName = (String)request.getSession().getAttribute("storeName");
      int storeId = ((Integer)request.getSession().getAttribute("storeId")).intValue();
      this.auditService.updateAuditStatus(auditMasterId, auditMaster.getAgent_name(), auditMaster.getAgent_email(), auditMaster.getAgent_mobile());
      insertStoreCardCalculation(request);
      auditHistoryBean = loginAuditHistoryUtil.auditHistoryBean(auditUserId.intValue(), auditUserId.intValue(), "Audit Completed", "", "");
      this.auditHistoryService.insertAuditHistory(auditHistoryBean);
      
      List<String> percentageList = this.auditService.getScorePercentage(storeId);
      float scorePercentage = 0.0F;
      for (String percentage : percentageList) {
        if (percentage != null) {
          scorePercentage += Float.valueOf(percentage).floatValue();
        }
      }
      List<ScoreCardBO> scoreCardList = this.auditService.scoreCardCalculation(auditMasterId);
      Object auditValueList = this.auditService.getAuditValue(auditMasterId);
      DataSource datasource = this.auditService.getAuditDataSource(scoreCardList, (List)auditValueList);
      String htmlBody = "<html><body>Hi " + userName + ",<br><br> You have successfully completed audit." + 
        "<br/><br> Average Audit Score : " + scorePercentage / percentageList.size() + 
        "<br/><br> For Store : " + storeName;
      String subject = storeName + ": Vowmee - Audit completed";
      
      this.emailService.commonSendEmailAttachment(this.mailSender, userName, htmlBody, subject, datasource, request);
      
      model.addObject("storeName", storeName);
      model.addObject("storeScore", Float.valueOf(scorePercentage / percentageList.size()));
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return model;
  }
  
  public void insertStoreCardCalculation(HttpServletRequest request)
  {
    try
    {
      long auditMasterId = ((Long)request.getSession().getAttribute("auditMasterId")).longValue();
      int storeId = ((Integer)request.getSession().getAttribute("storeId")).intValue();
      List<ScoreCardBO> scoreCardList = this.auditService.scoreCardCalculation(auditMasterId);
      this.auditService.insertScoreCardCal(scoreCardList, storeId);
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
  
  @RequestMapping(value={"/getPreviewData"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  @ResponseBody
  public String getTime(HttpServletRequest request, HttpServletResponse response)
  {
    long auditMasterId = ((Long)request.getSession().getAttribute("auditMasterId")).longValue();
    Map<String, List<AuditBean>> auditPreviewList = new LinkedHashMap();
    auditPreviewList = this.auditService.selectAllAuditPreviewList(auditMasterId);
    List<AuditBean> auditList = (List)auditPreviewList.get("auditList");
    List<AuditBean> auditCategoryList = (List)auditPreviewList.get("auditCategoryList");
    StringBuilder str = null;
    System.out.println(auditList);
    this.auditService.getAuditCategory(auditMasterId);
    for (AuditBean auditBean : auditCategoryList)
    {
      String colorCode = (String)this.auditService.getAuditCategoryMap().get(auditBean.getAuditCategory());
      if (str == null) {
        str = new StringBuilder("<ul class='value-ul'>");
      } else {
        str.append("<ul class='value-ul'>");
      }
      if (colorCode != null)
      {
        if (colorCode.equalsIgnoreCase("red")) {
          str.append("<li class='state-pending' style='padding:0px; border:0px;'><h4>" + auditBean.getAuditCategory() + "</h4></li>");
        } else if (colorCode.equalsIgnoreCase("green")) {
          str.append("<li class='satate-completed' style='padding:0px; border:0px;'><h4>" + auditBean.getAuditCategory() + "</h4>\t</li>");
        }
      }
      else {
        str.append("<li style='padding:0px; color: white; font-weight: bold; border: 2px " + colorCode + " solid !important'<h4>" + auditBean.getAuditCategory() + "</h4></li>");
      }
      String category = auditBean.getAuditCategory();
      for (AuditBean audit : auditList) {
        if (category.equals(audit.getAuditCategory()))
        {
          str.append("<li><table class='pop-res-tble'><tr><td style='width:80%;'><p>" + audit.getAuditQuestion() + "</p></td><td>&nbsp;</td><td style='width:80%;'><table align='right'>");
          if ((audit.getAuditYes() != null) && (audit.getAuditYes() == "Yes")) {
            str.append("<tr><td><label style='color:#4dbe10; padding-left:150pt;'>Yes</label></td></tr></table></td></tr></table></li>");
          } else if ((audit.getAuditNo() != null) && (audit.getAuditNo() == "No")) {
            str.append("<tr><td><td><label style='color:#ff0000; padding-left:150pt;'>No</label></td></td></tr></table></td></tr></table></li>");
          } else {
            str.append("<tr><td><td><label style='color:#808080; padding-left:150pt;'>N/A</label></td></td></tr></table></td></tr></table></li>");
          }
        }
      }
      str.append("</ul>");
    }
    System.out.println(str);
    return str.toString();
  }
  
  @RequestMapping(value={"/report"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public ModelAndView reportGenerate(HttpServletRequest request, HttpServletResponse response)
  {
    ModelAndView model = null;
    try
    {
      UserBean user = (UserBean)request.getSession().getAttribute("loginBean");
      int userId = this.auditService.getUserId(user.getName());
      


      List<String> storeIdList = this.auditService.getAllCompletedStoreListByCircle(userId);
      StringBuilder build = new StringBuilder();
      if (storeIdList.size() != 0)
      {
        for (String storeId : storeIdList) {
          if (build.length() == 0) {
            build.append("'").append(storeId).append("'");
          } else {
            build.append(",'").append(storeId).append("'");
          }
        }
        List<Store> storeList = this.auditService.selectStoreById(build);
        
        model = new ModelAndView("Report");
        Object stores = new LinkedHashMap();
        for (Store store : storeList) {
          ((Map)stores).put(Integer.valueOf(store.getId()), store.getCircleName() + " - " + store.getStore_name());
        }
        List<String> data = new ArrayList();
        model.addObject("storeList", stores);
        model.addObject("scoreCard", new ScoreCardBO());
        model.addObject("data", data);
      }
      else
      {
        model = new ModelAndView("homePage");
        request.setAttribute("storeMessage", "No Store was Mapped to you. Please Contact Admin.");
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return model;
  }
  
  @RequestMapping(value={"/continueAudit/{storeName}"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public ModelAndView continueAudit(HttpServletRequest request, HttpServletResponse response, @PathVariable("storeName") String storeName)
  {
    ModelAndView model = null;
    AuditHistoryUtil loginAuditHistoryUtil = new AuditHistoryUtil();
    AuditHistoryBean auditHistoryBean = new AuditHistoryBean();
    try
    {
      List<AuditLookupBean> auditLookupList = new ArrayList();
      
      UserBean userBean = (UserBean)request.getSession().getAttribute("loginBean");
      int storeId = this.auditService.getStoreIdByName(storeName);
      long auditMasterId = this.auditService.selectAuditId(storeId);
      Map<String, List<AuditBean>> auditPreviewList1 = new HashMap();
      auditHistoryBean = loginAuditHistoryUtil.auditHistoryBean(userBean.getId(), userBean.getId(), "started Audit", "", "");
      this.auditHistoryService.insertAuditHistory(auditHistoryBean);
      
      request.getSession().setAttribute("storeId", Integer.valueOf(storeId));
      request.getSession().setAttribute("auditMasterId", Long.valueOf(auditMasterId));
      request.getSession().setAttribute("storeName", storeName);
      List<String> auditCategoryList1 = this.auditService.getAuditCategory(auditMasterId);
      Map<String, String> auditCategoryList = new LinkedHashMap();
      auditPreviewList1 = this.auditService.selectAllAuditPreviewList(auditMasterId);
      List<AuditBean> auditList = (List)auditPreviewList1.get("auditList");
      Gson gson = new Gson();
      if (auditCategoryList1 != null) {
        setCategory((String)auditCategoryList1.get(0));
      }
      for (String auditCategory : auditCategoryList1) {
        auditCategoryList.put(auditCategory, auditCategory);
      }
      System.out.println(auditLookupList);
      auditLookupList = this.auditService.getAuditQuestions((String)auditCategoryList1.get(0));
      System.out.println(auditLookupList);
      List<Integer> auditLookupCountList = this.auditService.getAuditLookUpCount();
      Object auditMasterList = this.auditService.getMasterLookUpCount(auditMasterId);
      List<Integer> auditMasterForOptionalList = this.auditService.getMasterLookUpCountForOptional(auditMasterId);
      
      model = new ModelAndView("brandExperience");
      model.addObject("auditPreviewJaon", gson.toJson(auditList));
      model.addObject("auditLookupList", auditLookupList);
      model.addObject("auditCategoryList", auditCategoryList);
      model.addObject("storeName", storeName);
      String firstname = (String)request.getSession().getAttribute("firstname");
      model.addObject("userName", firstname);
      model.addObject("agentModel", new AuditMaster());
      model.addObject("auditLookupSize", Integer.valueOf(auditLookupList.size()));
      model.addObject("auditLookupListJson", gson.toJson(auditLookupList));
      model.addObject("getCategory", gson.toJson(getCategory()));
      model.addObject("auditCategoryMap", this.auditService.getAuditCategoryMap());
      if ((auditLookupCountList != null) && (auditMasterList != null) && (auditMasterForOptionalList != null) && 
        (((Integer)auditLookupCountList.get(0)).toString().equalsIgnoreCase(((Integer)((List)auditMasterList).get(0)).toString())) && 
        (((Integer)auditMasterForOptionalList.get(0)).intValue() > 0)) {
        model.addObject("previewFlag", Boolean.valueOf(true));
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return model;
  }
  
  @RequestMapping(value={"/previousAudits"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public ModelAndView previousAudits(HttpServletRequest request, HttpServletResponse response)
  {
    ModelAndView model = null;
    try
    {
      UserBean user = (UserBean)request.getSession().getAttribute("loginBean");
      int userId = this.auditService.getUserId(user.getName());
      List<CompletedAuditsVO> completedAuditList = this.auditService.getCompletedAuditDetails(userId);
      model = new ModelAndView("previousAudits");
      model.addObject("completedAuditList", completedAuditList);
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return model;
  }
  
  @RequestMapping(value={"/userAuditHystory/{parentId}"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public ModelAndView getUserPreviousAuditHystory(HttpServletRequest request, HttpServletResponse response, @PathVariable("parentId") String parentId)
  {
    ModelAndView model = null;
    try
    {
      if (parentId.equalsIgnoreCase("homepage"))
      {
        String path = request.getContextPath();
        String url = "http://audit.xvidiaglobal.com:8080" + path + "/homePage";
        model = new ModelAndView("redirect:" + url);
      }
      else if (parentId.equalsIgnoreCase("logout"))
      {
        request.getSession().invalidate();
        String path = request.getContextPath();
        String url = "http://audit.xvidiaglobal.com:8080" + path + "/logout";
        model = new ModelAndView("redirect:" + url);
      }
      else
      {
        List<UserPreviousAuditHytory> completedAuditList = this.auditService.getUserAuditDetails(parentId);
        model = new ModelAndView("userAuditHystory");
        model.addObject("completedAuditList", completedAuditList);
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return model;
  }
}