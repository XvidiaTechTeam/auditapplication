package com.vowmee.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.vowmee.bean.AuditBean;
import com.vowmee.bean.ReportsBean;
import com.vowmee.bean.ScoreCardBO;
import com.vowmee.bean.Store;
import com.vowmee.bean.UserBean;
import com.vowmee.service.AuditService;

@Controller
public class ReportController {
	
	private Map<Integer, String> monthMap = new TreeMap<>();
	@PostConstruct
	void setMonths(){
		monthMap.put(1, "Jan");
		monthMap.put(2, "Feb");
		monthMap.put(3, "Mar");
		monthMap.put(4, "Apr");
		monthMap.put(5, "May");
		monthMap.put(6, "Jun");
		monthMap.put(7, "Jul");
		monthMap.put(8, "Aug");
		monthMap.put(9, "Sep");
		monthMap.put(10, "Oct");
		monthMap.put(11, "Nov");
		monthMap.put(12, "Dec");
	}
	
	@Autowired
	private AuditService auditService;
	
/*	@RequestMapping(value="/generateReport",method=RequestMethod.POST)
	@ResponseBody
	public String ajaxInsertAuditData(@RequestBody String json, HttpServletRequest request){
		//long auditMasterId = (long) request.getSession().getAttribute("auditMasterId");
		System.out.println(json);
		return json;
	}*/
	/*@RequestMapping(value = "/reportGenerate", method = RequestMethod.POST)
	public ModelAndView reportGenerate(HttpServletRequest request, HttpServletResponse response,@ModelAttribute ScoreCardBO scoreCard) {
		ModelAndView model=null;
		try{
			System.out.println("store id" + scoreCard);
			if(scoreCard.getMonth() != null){
				UserBean user = (UserBean) request.getSession().getAttribute("loginBean");
				int userId = auditService.getUserId(user.getName());
				Map<String,List> data = new LinkedHashMap<String,List>();
				List<String> storeIdList = auditService.getAllCompletedStoreList();
				StringBuilder build = new StringBuilder();
				List<String> categoryList = new ArrayList<String>();
				List<Integer> janpercentage = new ArrayList<Integer>();
				List<Integer> febpercentage = new ArrayList<Integer>();
				List<Integer> marpercentage = new ArrayList<Integer>();
				List<Integer> aprpercentage = new ArrayList<Integer>();
				List<Integer> maypercentage = new ArrayList<Integer>();
				List<Integer> junpercentage = new ArrayList<Integer>();
				List<Integer> julpercentage = new ArrayList<Integer>();
				List<Integer> augpercentage = new ArrayList<Integer>();
				List<Integer> septpercentage = new ArrayList<Integer>();
				List<Integer> octpercentage = new ArrayList<Integer>();
				List<Integer> novpercentage = new ArrayList<Integer>();
				List<Integer> decpercentage = new ArrayList<Integer>();

				for(String storeId:storeIdList){
					if(build.length()==0){
						build.append("'").append(storeId).append("'");
					}else{
						build.append(",'").append(storeId).append("'");
					}
				}
				String monthName = null;
				 List<ReportsBean> reportList = auditService.scoreCardList(scoreCard);
				for(ReportsBean reportBean: reportList){
					categoryList.add(reportBean.getAudit_category());
					if(reportBean.getAudit_month() == 1){
						janpercentage.add((int) reportBean.getAudit_percentage());
						monthName = "Jan";
					}
					if(reportBean.getAudit_month() == 2){
						febpercentage.add((int) reportBean.getAudit_percentage());
						monthName = "Feb";
					}
					if(reportBean.getAudit_month() == 3){
						marpercentage.add((int) reportBean.getAudit_percentage());
						monthName = "Mar";
					}
					if(reportBean.getAudit_month() == 4){
						aprpercentage.add((int) reportBean.getAudit_percentage());
						monthName = "Apr";
					}
					if(reportBean.getAudit_month() == 5){
						maypercentage.add((int) reportBean.getAudit_percentage());
						monthName = "May";
					}
					if(reportBean.getAudit_month() == 6){
						junpercentage.add((int) reportBean.getAudit_percentage());
						monthName = "Jun";
					}
					if(reportBean.getAudit_month() == 7){
						julpercentage.add((int) reportBean.getAudit_percentage());
						monthName = "Jul";
					}
					if(reportBean.getAudit_month() == 8){
						augpercentage.add((int) reportBean.getAudit_percentage());
						monthName = "Aug";
					}
					if(reportBean.getAudit_month() == 9){
						septpercentage.add((int) reportBean.getAudit_percentage());
						monthName = "Sep";
					}
					if(reportBean.getAudit_month() == 10){
						octpercentage.add((int) reportBean.getAudit_percentage());
						monthName = "Oct";
					}
					if(reportBean.getAudit_month() == 11){
						novpercentage.add((int) reportBean.getAudit_percentage());
						monthName = "Nov";
					}
					if(reportBean.getAudit_month() == 12){
						decpercentage.add((int) reportBean.getAudit_percentage());
						monthName = "Dec";
					}
					
					
				}
				//categoryList.remove(0);
				if(janpercentage.size() != 0){
					data.put("January", janpercentage);
				}
				if(febpercentage.size() != 0){
					data.put("February", febpercentage);
				}
				if(marpercentage.size() != 0){
					data.put("March", marpercentage);
				}
				if(aprpercentage.size() != 0){
					data.put("April", aprpercentage);
				}
				if(maypercentage.size() != 0){
					data.put("May", maypercentage);
				}
				if(junpercentage.size() != 0){
					data.put("June", junpercentage);
				}
				if(julpercentage.size() != 0){
					data.put("July", julpercentage);
				}
				if(augpercentage.size() != 0){
					data.put("August", augpercentage);
				}
				if(septpercentage.size() != 0){
					data.put("September", septpercentage);
				}
				if(octpercentage.size() != 0){
					data.put("October", octpercentage);
				}
				if(novpercentage.size() != 0){
					data.put("November", novpercentage);
				}
				if(decpercentage.size() != 0){
					data.put("December", decpercentage);
				}
				

				List<Store> storeList = auditService.selectStoreById(build);
				String storeNameSelected = null;
				model= new ModelAndView("Report");
				 Map< Integer, String > stores = new HashMap<Integer, String>(); 
				 for(Store store:storeList){
					 stores.put(store.getId(), store.getStore_name());
					 if(store.getId()==scoreCard.getStore_id()){
						 storeNameSelected=store.getStore_name();
					 }
				 }
				 
				 Gson gson = new Gson();
				
							
				// List<ScoreCardBO> scoreCardList = auditService.scoreCardList(scoreCard);
				// model.addObject("scoreCardListJson",gson.toJson(scoreCardList));
				Set<String> categorySet = new LinkedHashSet<>(categoryList);
				model.addObject("storeList", stores);
				model.addObject("data",gson.toJson(data));
				model.addObject("categoryList",gson.toJson(categorySet));
				model.addObject("selectedStore", scoreCard.getStore_id());
				  model.addObject("selectedMonth", scoreCard.getMonth());
				  model.addObject("monthName", monthName);
				  model.addObject("storeNameSelected", storeNameSelected);
				model.addObject("storeName",gson.toJson(auditService.getStoreNameById(scoreCard.getStore_id())));
				model.addObject("scoreCard", new ScoreCardBO());
				
			}else{
				model= new ModelAndView("Report");
				UserBean user = (UserBean) request.getSession().getAttribute("loginBean");
				int userId = auditService.getUserId(user.getName());
				StringBuilder build = new StringBuilder();
				List<String> storeIdList = auditService.getAllCompletedStoreList();
				for(String storeId:storeIdList){
					if(build.length()==0){
						build.append("'").append(storeId).append("'");
					}else{
						build.append(",'").append(storeId).append("'");
					}
				}
				List<Store> storeList = auditService.selectStoreById(build);
				 Map< Integer, String > stores = new HashMap<Integer, String>(); 
				 for(Store store:storeList){
					 stores.put(store.getId(), store.getStore_name());
				 }
				 
				 Gson gson = new Gson();
				 model.addObject("storeList", stores);
				 model.addObject("scoreCard", new ScoreCardBO());
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return model;
	}*/
	
	@RequestMapping(value = "/reportGenerate", method = RequestMethod.POST)
	public ModelAndView reportGenerate(HttpServletRequest request, HttpServletResponse response,@ModelAttribute ScoreCardBO scoreCard) {
		ModelAndView model=null;
		try{
			System.out.println("store id" + scoreCard);
			if(scoreCard.getMonth() != null){
				//UserBean user = (UserBean) request.getSession().getAttribute("loginBean");
			//	int userId = auditService.getUserId(user.getName());
				Map<String,List<Float>> data = new LinkedHashMap<>();
				UserBean user = (UserBean) request.getSession().getAttribute("loginBean");
				int userId = auditService.getUserId(user.getName());
//				List<String> storeIdList = auditService.getStoreIdList(userId);
				
				
				List<String> storeIdList = auditService.getAllCompletedStoreListByCircle(userId);
				StringBuilder build = new StringBuilder();
				List<String> categoryList = new ArrayList<String>();

				for(String storeId:storeIdList){
					if(build.length()==0){
						build.append("'").append(storeId).append("'");
					}else{
						build.append(",'").append(storeId).append("'");
					}
				}
				String monthName = null;
				 List<ReportsBean> reportList = auditService.scoreCardList(scoreCard);
				for(ReportsBean reportBean: reportList){
					categoryList.add(reportBean.getAudit_category());
					if(reportBean.getAudit_month() == 1){
						monthName = "Jan";
						data.put(monthName, new ArrayList<Float>());
					}
					if(reportBean.getAudit_month() == 2){
						monthName = "Feb";
						data.put(monthName, new ArrayList<Float>());
					}
					if(reportBean.getAudit_month() == 3){
						monthName = "Mar";
						data.put(monthName, new ArrayList<Float>());
					}
					if(reportBean.getAudit_month() == 4){
						monthName = "Apr";
						data.put(monthName, new ArrayList<Float>());
					}
					if(reportBean.getAudit_month() == 5){
						monthName = "May";
						data.put(monthName, new ArrayList<Float>());
					}
					if(reportBean.getAudit_month() == 6){
						monthName = "Jun";
						data.put(monthName, new ArrayList<Float>());
					}
					if(reportBean.getAudit_month() == 7){
						monthName = "Jul";
						data.put(monthName, new ArrayList<Float>());
					}
					if(reportBean.getAudit_month() == 8){
						monthName = "Aug";
						data.put(monthName, new ArrayList<Float>());
					}
					if(reportBean.getAudit_month() == 9){
						monthName = "Sep";
						data.put(monthName, new ArrayList<Float>());
					}
					if(reportBean.getAudit_month() == 10){
						monthName = "Oct";
						data.put(monthName, new ArrayList<Float>());
					}
					if(reportBean.getAudit_month() == 11){
						monthName = "Nov";
						data.put(monthName, new ArrayList<Float>());
					}
					if(reportBean.getAudit_month() == 12){
						monthName = "Dec";
						data.put(monthName, new ArrayList<Float>());
					}
					reportBean.setMonth(monthName);
				}
				
				List<Store> storeList = auditService.selectStoreById(build);
				String storeNameSelected = null;
				model= new ModelAndView("Report");
				Map< Integer, String > stores = new LinkedHashMap<Integer, String>(); 
				 for(Store store:storeList){
					 stores.put(store.getId(), store.getCircleName()+" - "+store.getStore_name());
					 if(store.getId()==scoreCard.getStore_id()){
						 storeNameSelected=store.getStore_name();
					 }
				 }
				 
				 Set<String> categorySet = new LinkedHashSet<>(categoryList);				 
				 
				 for(String category: categorySet){
						for(String val: data.keySet() ){
							ReportsBean reportsBean = new ReportsBean();
							reportsBean.setAudit_category(category);
							reportsBean.setMonth(val);
							if(reportList.contains(reportsBean)){
								ReportsBean rBean=reportList.get(reportList.indexOf(reportsBean));
								List<Float> categoriesList = data.get(rBean.getMonth());
								categoriesList.add(rBean.getAudit_percentage());
								data.put(rBean.getMonth(), categoriesList);
							}else{
								List<Float> categoriesList = data.get(val);
								categoriesList.add(0.00f);
								data.put(val, categoriesList);							
								}
						}
					}
				 
				 Gson gson = new Gson();
				
							
				// List<ScoreCardBO> scoreCardList = auditService.scoreCardList(scoreCard);
				// model.addObject("scoreCardListJson",gson.toJson(scoreCardList));
				
				model.addObject("storeList", stores);
				model.addObject("data",gson.toJson(data));
				model.addObject("categoryList",gson.toJson(categorySet));
				model.addObject("selectedStore", scoreCard.getStore_id());
				  model.addObject("selectedMonth", scoreCard.getMonth());
				  model.addObject("monthName", monthName);
				  model.addObject("storeNameSelected", storeNameSelected);
				model.addObject("storeName",gson.toJson(stores.get(scoreCard.getStore_id())));
				model.addObject("scoreCard", new ScoreCardBO());
				
			}else{
				model= new ModelAndView("Report");
				//UserBean user = (UserBean) request.getSession().getAttribute("loginBean");
				//int userId = auditService.getUserId(user.getName());
				StringBuilder build = new StringBuilder();
				UserBean user = (UserBean) request.getSession().getAttribute("loginBean");
				int userId = auditService.getUserId(user.getName());
//				List<String> storeIdList = auditService.getStoreIdList(userId);
				
				
				List<String> storeIdList = auditService.getAllCompletedStoreListByCircle(userId);
				for(String storeId:storeIdList){
					if(build.length()==0){
						build.append("'").append(storeId).append("'");
					}else{
						build.append(",'").append(storeId).append("'");
					}
				}
				List<Store> storeList = auditService.selectStoreById(build);
				Map< Integer, String > stores = new LinkedHashMap<Integer, String>(); 
				 for(Store store:storeList){
					 stores.put(store.getId(), store.getCircleName()+" - "+store.getStore_name());
				 }
				 
				 model.addObject("storeList", stores);
				 model.addObject("scoreCard", new ScoreCardBO());
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return model;
	}
	@RequestMapping(value="/reportGenerate",params="download",method=RequestMethod.POST)
    public ModelAndView downloadReports(HttpServletRequest request, HttpServletResponse response,@ModelAttribute ScoreCardBO scoreCard) throws Exception
    {
		//String userName = (String) request.getSession().getAttribute("firstname");
		List<Store> storeNameList = new ArrayList<>();
		storeNameList.addAll(auditService.selectStoreReportById(scoreCard.getStore_id()));
		ModelAndView model = new ModelAndView("reportListExcel");
		if(storeNameList.size()>0){
			response.setHeader("Content-disposition", "attachment; filename=Store Wise Report_"+storeNameList.get(0).getCircleName()+" - "+storeNameList.get(0).getStore_name()+"_"+monthMap.get(Integer.parseInt(scoreCard.getMonth()))+"_"+new Date()+".xls");
			List<ReportsBean> reportList = new ArrayList<ReportsBean>();
			List<AuditBean> auditDetailsList = new ArrayList<>();
			if(scoreCard.getMonth() != null){
				reportList = auditService.scoreCardList(scoreCard);
				auditDetailsList= auditService.getAuditDetailsToExport(scoreCard);
			}
			Collections.sort(auditDetailsList, new Comparator<AuditBean>(){
				@Override
			    public int compare(AuditBean a1, AuditBean a2) {
			        return a1.getAuditCategory().compareTo(a2.getAuditCategory());
			    }
			});
			model.addObject("monthMap", monthMap);
			model.addObject("circleStore", storeNameList.get(0).getCircleName()+" - "+storeNameList.get(0).getStore_name());
		    model.addObject("reportList", reportList);
		    model.addObject("auditDetailsList", auditDetailsList);
		}
		
	 
		return model;
    }
	
	@RequestMapping(value = "/circleReport", method = RequestMethod.GET)
	public ModelAndView circleWiseReport(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView model=null;
		try{
			UserBean user = (UserBean) request.getSession().getAttribute("loginBean");
			int userId = auditService.getUserId(user.getName());
			//UserBean user = (UserBean) request.getSession().getAttribute("loginBean");
			//int userId = auditService.getUserId(user.getName());
			List<ScoreCardBO> circleList = auditService.getCircleListForUser(userId);
			List<String> categoryList = auditService.getCategoryList();
			if(circleList!=null && categoryList!=null){
				
				model= new ModelAndView("circleReport");
				 Map< Integer, String > circles = new LinkedHashMap<Integer, String>(); 
				 Map< String, String > categories = new LinkedHashMap<String, String>(); 
				 for(ScoreCardBO score:circleList){
					 circles.put(score.getCircle_id(),score.getCircle_name());
				 }
				 for(String category : categoryList){
					 categories.put(category,category);
				 }
				 categories.put("Overall Score", "Overall Score");
				//Store store = new Store();
				model.addObject("circleList", circles);
				model.addObject("categorieList", categories);
				model.addObject("scoreCard", new ScoreCardBO());
			}else{
				model = new ModelAndView("homePage");
				request.setAttribute("storeMessage", "No Circle was Mapped to you. Please Contact Admin.");
			}
			
			//request.setAttribute("storeList", storeList);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return model;
	}
	
	
	@RequestMapping(value = "/circleReportGenerate", method = RequestMethod.POST)
	public ModelAndView generateCircleReport(HttpServletRequest request, HttpServletResponse response,@ModelAttribute ScoreCardBO scoreCard) {
		ModelAndView model=null;
		try{
			List<ScoreCardBO> auditorReportListFinal = auditService.getCircleScoreList(scoreCard.getCircle_id(), scoreCard.getMonth(), scoreCard.getAudit_category());
			UserBean user = (UserBean) request.getSession().getAttribute("loginBean");
			int userId = auditService.getUserId(user.getName());
			//UserBean user = (UserBean) request.getSession().getAttribute("loginBean");
			//int userId = auditService.getUserId(user.getName());
			List<ScoreCardBO> circleList = auditService.getCircleListForUser(userId);
			List<String> categoryList = auditService.getCategoryList();
			Gson gson = new Gson();
			String circleName = null;
			model = new ModelAndView("circleReport");
			Map<Integer, String> circles = new LinkedHashMap<Integer, String>();
			Map<String, String> categories = new LinkedHashMap<String, String>();
			Map<String, List<Float>> data = new LinkedHashMap<>();
			Set<String> stores = new TreeSet<>();
			String monthName ="";
			for(ScoreCardBO scoreBo: auditorReportListFinal){
				stores.add(scoreBo.getStoreName());
				
				if(scoreBo.getMonth().equals("1")){
					monthName = "Jan";
					data.put(monthName, new ArrayList<Float>());
				}
				if(scoreBo.getMonth().equals("2")){
					monthName = "Feb";
					data.put(monthName, new ArrayList<Float>());
				}
				if(scoreBo.getMonth().equals("3")){
					monthName = "Mar";
					data.put(monthName, new ArrayList<Float>());
				}
				if(scoreBo.getMonth().equals("4")){
					monthName = "Apr";
					data.put(monthName, new ArrayList<Float>());
				}
				if(scoreBo.getMonth().equals("5")){
					monthName = "May";
					data.put(monthName, new ArrayList<Float>());
				}
				if(scoreBo.getMonth().equals("6")){
					monthName = "Jun";
					data.put(monthName, new ArrayList<Float>());
				}
				if(scoreBo.getMonth().equals("7")){
					monthName = "Jul";
					data.put(monthName, new ArrayList<Float>());
				}
				if(scoreBo.getMonth().equals("8")){
					monthName = "Aug";
					data.put(monthName, new ArrayList<Float>());
				}
				if(scoreBo.getMonth().equals("9")){
					monthName = "Sep";
					data.put(monthName, new ArrayList<Float>());
				}
				if(scoreBo.getMonth().equals("10")){
					monthName = "Oct";
					data.put(monthName, new ArrayList<Float>());
				}
				if(scoreBo.getMonth().equals("11")){
					monthName = "Nov";
					data.put(monthName, new ArrayList<Float>());
				}
				if(scoreBo.getMonth().equals("12")){
					monthName = "Dec";
					data.put(monthName, new ArrayList<Float>());
				}
				scoreBo.setMonth(monthName);
			}
			
			
			for(String store: stores){
				for(String val: data.keySet() ){
					ScoreCardBO cardBO = new ScoreCardBO();
					cardBO.setStoreName(store);
					cardBO.setMonth(val);
					if(auditorReportListFinal.contains(cardBO)){
						ScoreCardBO score=auditorReportListFinal.get(auditorReportListFinal.indexOf(cardBO));
						List<Float> scoreList = data.get(score.getMonth());
						scoreList.add(Float.parseFloat(score.getPercentage()));
						data.put(score.getMonth(), scoreList);
					}else{
						List<Float> scoreList = data.get(cardBO.getMonth());
						scoreList.add(0.00f);
						data.put(cardBO.getMonth(), scoreList);
					}
				}
			}
			
			for (ScoreCardBO score : circleList) {
				circles.put(score.getCircle_id(), score.getCircle_name());
				if(score.getCircle_id()==scoreCard.getCircle_id()){
					circleName = score.getCircle_name();
				}
			}
			for (String category : categoryList) {
				categories.put(category, category);
			}
			
			categories.put("Overall Score", "Overall Score");
			model.addObject("circleList", circles);
			model.addObject("categorieList", categories);
			model.addObject("scoreCard", new ScoreCardBO());
			model.addObject("getCategory",gson.toJson(scoreCard.getAudit_category()));
			model.addObject("circleName",gson.toJson(circleName));
			model.addObject("circleIdForDropDown",gson.toJson(scoreCard.getCircle_id()));
			model.addObject("selectedMonth",gson.toJson(scoreCard.getMonth()));
			
			model.addObject("data",gson.toJson(data));
			model.addObject("storeSet",gson.toJson(new ArrayList<>(stores)));
			request.setAttribute("selectedCircle", circleName);
			request.setAttribute("selectedCategory", scoreCard.getAudit_category());
			model.addObject("auditorReportListFinal", gson.toJson(auditorReportListFinal));
			/*categories.put("Overall Score", "Overall Score");
			model.addObject("circleList", circles);
			model.addObject("categorieList", categories);
			model.addObject("scoreCard", new ScoreCardBO());
			model.addObject("getCategory",gson.toJson(scoreCard.getAudit_category()));
			model.addObject("circleName",gson.toJson(scoreCard.getCircle_id()));
			model.addObject("selectedMonth",gson.toJson(scoreCard.getMonth()));
			
			model.addObject("data",gson.toJson(data));
			model.addObject("storeSet",gson.toJson(new ArrayList<>(stores)));
			request.setAttribute("selectedCircle", circleName);
			request.setAttribute("selectedCategory", scoreCard.getAudit_category());
			model.addObject("auditorReportListFinal", gson.toJson(auditorReportListFinal));
*/
		}catch(Exception e){
			e.printStackTrace();
		}
		return model;
	}
	
	@RequestMapping(value = "/auditorReport", method = RequestMethod.GET)
	public ModelAndView reportGenerate(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView model=null;
		try{
			UserBean user = (UserBean) request.getSession().getAttribute("loginBean");
			int userId = auditService.getUserId(user.getName());
			List<String> storeIdList = auditService.getAllCompletedStoreListByCircle(userId);
			List<UserBean> auditorList = auditService.getUserListforReportStore(storeIdList);
			if(auditorList.size() != 0){
				model= new ModelAndView("auditorReport");
				 Map< Integer, String > auditorMap = new LinkedHashMap<Integer, String>(); 
				 for(UserBean auditor:auditorList){
					 StringBuffer val = new StringBuffer();
					 if(auditor.getFirstName()!=null){
						 val.append(auditor.getFirstName());
					 }else{
						 val.append("");
					 }if(auditor.getLastName()!=null){
						 val.append(" "+auditor.getLastName());
					 }else{
						 val.append(" ");
					 }if(auditor.getName()!=null){
						 val.append(" (Email: "+auditor.getName()+" )");
					 }else{
						 val.append("");
					 }
					 
					 System.out.println(val.toString());
					 auditorMap.put(auditor.getId(), val.toString());
				 }
				//Store store = new Store();
				 List<String> data = new ArrayList<>();
				model.addObject("auditor", auditorMap);
				model.addObject("scoreCard", new ScoreCardBO());
				model.addObject("data",data);
			}else{
				model = new ModelAndView("homePage");
				request.setAttribute("storeMessage", "No Store was Mapped to you. Please Contact Admin.");
			}
			
			//request.setAttribute("storeList", storeList);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return model;
	}
	
	@RequestMapping(value = "/generateAuditorReport", method = RequestMethod.POST)
	public ModelAndView generateAuditorReport(HttpServletRequest request, HttpServletResponse response,@ModelAttribute ScoreCardBO scoreCard) {
		ModelAndView model=null;
		try{
			List<ScoreCardBO> auditorReportList = new ArrayList<ScoreCardBO>();
			List<ScoreCardBO> auditorReportListFinal = new ArrayList<ScoreCardBO>();
			Gson gson = new Gson();
			auditorReportList = auditService.getAuditorReport(scoreCard.getStore_id(), scoreCard.getMonth());
			List<Store> storeList = auditService.getStoreObject();
			System.out.println("auditorReportList "+auditorReportList);
			System.out.println("storeList "+storeList);
			Map<String,List<Float>> data = new LinkedHashMap<String,List<Float>>();
			String monthName = "";
			for(ScoreCardBO scoreBo : auditorReportList){
				ScoreCardBO storeBo = new ScoreCardBO();
				for(Store store : storeList){
					if(scoreBo.getStore_id() == store.getId()){
						storeBo.setPercentage(scoreBo.getPercentage());
						storeBo.setStoreName(store.getStore_name());
						if(scoreBo.getMonth().equals("1")){
							monthName = "Jan";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("2")){
							monthName = "Feb";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("3")){
							monthName = "Mar";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("4")){
							monthName = "Apr";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("5")){
							monthName = "May";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("6")){
							monthName = "Jun";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("7")){
							monthName = "Jul";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("8")){
							monthName = "Aug";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("9")){
							monthName = "Sep";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("10")){
							monthName = "Oct";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("11")){
							monthName = "Nov";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("12")){
							monthName = "Dec";
							data.put(monthName, new ArrayList<Float>());
						}
						storeBo.setMonth(monthName);
						auditorReportListFinal.add(storeBo);
					}
				}
			}
			Set<String> categorySet =new TreeSet<>(); 
			for(ScoreCardBO bo: auditorReportListFinal){
				categorySet.add(bo.getStoreName());
			}
			for(String store: categorySet){
				for(String val: data.keySet() ){
					ScoreCardBO cardBO = new ScoreCardBO();
					cardBO.setStoreName(store);
					cardBO.setMonth(val);
					if(auditorReportListFinal.contains(cardBO)){
						ScoreCardBO score=auditorReportListFinal.get(auditorReportListFinal.indexOf(cardBO));
						List<Float> scoreList = data.get(score.getMonth());
						scoreList.add(Float.parseFloat(score.getPercentage()));
						data.put(score.getMonth(), scoreList);
					}else{
						List<Float> scoreList = data.get(cardBO.getMonth());
						scoreList.add(0.00f);
						data.put(cardBO.getMonth(), scoreList);
					}
				}
			}
			
			/*for(ScoreCardBO bo: auditorReportListFinal){
				categorySet.add(bo.getStoreName());
				if(data.containsKey(bo.getMonth())){
					List<Float> scoreList = data.get(bo.getMonth());
					scoreList.add(Float.parseFloat(bo.getPercentage()));
					data.put(bo.getMonth(), scoreList);
				}else{
					List<Float> scoreList = new ArrayList<>();
					scoreList.add(Float.parseFloat(bo.getPercentage()));
					data.put(bo.getMonth(), scoreList);
				}
				
			}*/
			UserBean user = (UserBean) request.getSession().getAttribute("loginBean");
			int userId = auditService.getUserId(user.getName());	
			Map< Integer, String > auditorMap = new LinkedHashMap<Integer, String>(); 
			//List<UserBean> auditorList = auditService.getUserListforReport();
			List<String> storeIdList = auditService.getAllCompletedStoreListByCircle(userId);
			List<UserBean> auditorList = auditService.getUserListforReportStore(storeIdList);
			String auditorNameSelected = null;
			if(auditorList.size() != 0){
				
				 for(UserBean auditor:auditorList){
					 StringBuffer val = new StringBuffer();
					 if(auditor.getFirstName()!=null){
						 val.append(auditor.getFirstName());
					 }else{
						 val.append("");
					 }if(auditor.getLastName()!=null){
						 val.append(" "+auditor.getLastName());
					 }else{
						 val.append(" ");
					 }if(auditor.getName()!=null){
						 val.append(" (Email: "+auditor.getName()+" )");
					 }else{
						 val.append("");
					 }
					 
					 auditorMap.put(auditor.getId(), val.toString());
					 if(auditor.getId()==scoreCard.getStore_id()){
						 auditorNameSelected = val.toString(); 
					 }
				 }
			}
			
			model= new ModelAndView("auditorReport");
			model.addObject("auditor", auditorMap);
			model.addObject("data",gson.toJson(data));
			model.addObject("scoreCard", new ScoreCardBO());
			model.addObject("selectedAuditor", scoreCard.getStore_id());
			request.setAttribute("selectedAuditorName", auditorNameSelected);
			request.setAttribute("selectedMonthName", monthName);
			  model.addObject("selectedMonth", scoreCard.getMonth());
			  model.addObject("categorySet", gson.toJson(new ArrayList<>(categorySet)));
			model.addObject("auditorReportListFinal", gson.toJson(auditorReportListFinal));
			model.addObject("auditorDetails", gson.toJson(auditorNameSelected));
		}catch(Exception e){
			e.printStackTrace();
		}
		return model;
	}
	
	@RequestMapping(value="/generateAuditorReport",params="download",method=RequestMethod.POST)
    public ModelAndView downloadAuditorReports(HttpServletRequest request, HttpServletResponse response,@ModelAttribute ScoreCardBO scoreCard) throws Exception
    {
		ModelAndView model=null;
		try{
			List<ScoreCardBO> auditorReportList = new ArrayList<ScoreCardBO>();
			List<ScoreCardBO> auditorReportListFinal = new ArrayList<ScoreCardBO>();
			Gson gson = new Gson();
			auditorReportList = auditService.getAuditorReport(scoreCard.getStore_id(), scoreCard.getMonth());
			List<Store> storeList = auditService.getStoreObject();
			Map<String,List<Float>> data = new LinkedHashMap<String,List<Float>>();
			String monthName = "";
			String user = auditService.getAuditorNameById(scoreCard.getStore_id());
			response.setHeader("Content-disposition", "attachment; filename=Auditor Wise Report_"+user+"_"+monthMap.get(Integer.parseInt(scoreCard.getMonth()))+"_"+new Date()+".xls");
			for(ScoreCardBO scoreBo : auditorReportList){
				ScoreCardBO storeBo = new ScoreCardBO();
				for(Store store : storeList){
					if(scoreBo.getStore_id() == store.getId()){
						storeBo.setPercentage(scoreBo.getPercentage());
						storeBo.setStoreName(store.getStore_name());
						if(scoreBo.getMonth().equals("1")){
							monthName = "Jan";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("2")){
							monthName = "Feb";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("3")){
							monthName = "Mar";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("4")){
							monthName = "Apr";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("5")){
							monthName = "May";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("6")){
							monthName = "Jun";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("7")){
							monthName = "Jul";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("8")){
							monthName = "Aug";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("9")){
							monthName = "Sep";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("10")){
							monthName = "Oct";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("11")){
							monthName = "Nov";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("12")){
							monthName = "Dec";
							data.put(monthName, new ArrayList<Float>());
						}
						storeBo.setMonth(monthName);
						auditorReportListFinal.add(storeBo);
					}
				}
			}
			Set<String> categorySet =new TreeSet<>(); 
			for(ScoreCardBO bo: auditorReportListFinal){
				categorySet.add(bo.getStoreName());
			}
			for(String store: categorySet){
				for(String val: data.keySet() ){
					ScoreCardBO cardBO = new ScoreCardBO();
					cardBO.setStoreName(store);
					cardBO.setMonth(val);
					if(auditorReportListFinal.contains(cardBO)){
						ScoreCardBO score=auditorReportListFinal.get(auditorReportListFinal.indexOf(cardBO));
						List<Float> scoreList = data.get(score.getMonth());
						scoreList.add(Float.parseFloat(score.getPercentage()));
						data.put(score.getMonth(), scoreList);
					}else{
						List<Float> scoreList = data.get(cardBO.getMonth());
						scoreList.add(0.00f);
						data.put(cardBO.getMonth(), scoreList);
					}
				}
			}
			
			/*for(ScoreCardBO bo: auditorReportListFinal){
				categorySet.add(bo.getStoreName());
				if(data.containsKey(bo.getMonth())){
					List<Float> scoreList = data.get(bo.getMonth());
					scoreList.add(Float.parseFloat(bo.getPercentage()));
					data.put(bo.getMonth(), scoreList);
				}else{
					List<Float> scoreList = new ArrayList<>();
					scoreList.add(Float.parseFloat(bo.getPercentage()));
					data.put(bo.getMonth(), scoreList);
				}
				
			}*/
				
			Map< Integer, String > auditorMap = new LinkedHashMap<Integer, String>(); 
			List<UserBean> auditorList = auditService.getUserListforReport();
			String auditorNameSelected = null;
			if(auditorList.size() != 0){
				
				 for(UserBean auditor:auditorList){
					 StringBuffer val = new StringBuffer();
					 if(auditor.getFirstName()!=null){
						 val.append(auditor.getFirstName());
					 }else{
						 val.append("");
					 }if(auditor.getLastName()!=null){
						 val.append(" "+auditor.getLastName());
					 }else{
						 val.append(" ");
					 }if(auditor.getName()!=null){
						 val.append(" (Email: "+auditor.getName()+" )");
					 }else{
						 val.append("");
					 }
					 
					 auditorMap.put(auditor.getId(), val.toString());
					 if(auditor.getId()==scoreCard.getStore_id()){
						 auditorNameSelected = val.toString(); 
					 }
				 }
			}
			Collections.sort(auditorReportListFinal, new Comparator<ScoreCardBO>(){
				@Override
			    public int compare(ScoreCardBO a1, ScoreCardBO a2) {
			        return a1.getStoreName().compareTo(a2.getStoreName());
			    }
			});
			
			model= new ModelAndView("auditorReportExcelBuild");
			model.addObject("user",user);
			model.addObject("auditorReportExportList", auditorReportListFinal);
			model.addObject("auditor", auditorMap);
			model.addObject("data",gson.toJson(data));
			model.addObject("scoreCard", new ScoreCardBO());
			model.addObject("selectedAuditor", scoreCard.getStore_id());
			request.setAttribute("selectedAuditorName", auditorNameSelected);
			request.setAttribute("selectedMonthName", monthName);
			  model.addObject("selectedMonth", scoreCard.getMonth());
			  model.addObject("categorySet", gson.toJson(new ArrayList<>(categorySet)));
			model.addObject("auditorReportListFinal", gson.toJson(auditorReportListFinal));
			model.addObject("auditorDetails", gson.toJson(auditorNameSelected));
		}catch(Exception e){
			e.printStackTrace();
		}
		return model;
    }
	
	
	@RequestMapping(value="/circleReportGenerate",params="download",method=RequestMethod.POST)
    public ModelAndView downloadCircleReports(HttpServletRequest request, HttpServletResponse response,@ModelAttribute ScoreCardBO scoreCard) throws Exception
    {
		ModelAndView model=null;
		try{
			List<ScoreCardBO> CircleReport = new ArrayList<ScoreCardBO>();
			List<ScoreCardBO> auditorReportListFinal = auditService.getCircleScoreList(scoreCard.getCircle_id(), scoreCard.getMonth(), scoreCard.getAudit_category());
			List<ScoreCardBO> circleList = auditService.getCircleList();
			List<String> categoryList = auditService.getCategoryList();
			Gson gson = new Gson();
			String circleName = null;
			model = new ModelAndView("circleExport");
			Map<Integer, String> circles = new LinkedHashMap<Integer, String>();
			Map<String, String> categories = new LinkedHashMap<String, String>();
			Map<String, List<Float>> data = new LinkedHashMap<>();
			Set<String> stores = new TreeSet<>();
			String monthName ="";
			
			for(ScoreCardBO scoreBo: auditorReportListFinal){
				
				ScoreCardBO storeBo = new ScoreCardBO();
				double roundOff = Math.round(new Double(scoreBo.getPercentage()) * 100.0) / 100.0;
				storeBo.setPercentage(roundOff+"");
				storeBo.setStoreName(scoreBo.getStoreName());
				stores.add(scoreBo.getStoreName());
				
				if(scoreBo.getMonth().equals("1")){
					monthName = "Jan";
					data.put(monthName, new ArrayList<Float>());
				}
				if(scoreBo.getMonth().equals("2")){
					monthName = "Feb";
					data.put(monthName, new ArrayList<Float>());
				}
				if(scoreBo.getMonth().equals("3")){
					monthName = "Mar";
					data.put(monthName, new ArrayList<Float>());
				}
				if(scoreBo.getMonth().equals("4")){
					monthName = "Apr";
					data.put(monthName, new ArrayList<Float>());
				}
				if(scoreBo.getMonth().equals("5")){
					monthName = "May";
					data.put(monthName, new ArrayList<Float>());
				}
				if(scoreBo.getMonth().equals("6")){
					monthName = "Jun";
					data.put(monthName, new ArrayList<Float>());
				}
				if(scoreBo.getMonth().equals("7")){
					monthName = "Jul";
					data.put(monthName, new ArrayList<Float>());
				}
				if(scoreBo.getMonth().equals("8")){
					monthName = "Aug";
					data.put(monthName, new ArrayList<Float>());
				}
				if(scoreBo.getMonth().equals("9")){
					monthName = "Sep";
					data.put(monthName, new ArrayList<Float>());
				}
				if(scoreBo.getMonth().equals("10")){
					monthName = "Oct";
					data.put(monthName, new ArrayList<Float>());
				}
				if(scoreBo.getMonth().equals("11")){
					monthName = "Nov";
					data.put(monthName, new ArrayList<Float>());
				}
				if(scoreBo.getMonth().equals("12")){
					monthName = "Dec";
					data.put(monthName, new ArrayList<Float>());
				}
				scoreBo.setMonth(monthName);
				storeBo.setMonth(monthName);
				CircleReport.add(storeBo);
			}
			
			
			for(String store: stores){
				for(String val: data.keySet() ){
					ScoreCardBO cardBO = new ScoreCardBO();
					cardBO.setStoreName(store);
					cardBO.setMonth(val);
					if(auditorReportListFinal.contains(cardBO)){
						ScoreCardBO score=auditorReportListFinal.get(auditorReportListFinal.indexOf(cardBO));
						List<Float> scoreList = data.get(score.getMonth());
						scoreList.add(Float.parseFloat(score.getPercentage()));
						data.put(score.getMonth(), scoreList);
					}else{
						List<Float> scoreList = data.get(cardBO.getMonth());
						scoreList.add(0.00f);
						data.put(cardBO.getMonth(), scoreList);
					}
				}
			}
			
			for (ScoreCardBO score : circleList) {
				circles.put(score.getCircle_id(), score.getCircle_name());
				if(score.getCircle_id()==scoreCard.getCircle_id()){
					circleName = score.getCircle_name();
				}
			}
			for (String category : categoryList) {
				categories.put(category, category);
			}
			
			Collections.sort(CircleReport, new Comparator<ScoreCardBO>(){
				@Override
			    public int compare(ScoreCardBO a1, ScoreCardBO a2) {
			        return a1.getStoreName().compareTo(a2.getStoreName());
			    }
			});
			
			response.setHeader("Content-disposition", "attachment; filename= Circle Wise Report_"+circleName+"_"+scoreCard.getAudit_category()+"_"+new Date()+".xls");
			categories.put("Overall Score", "Overall Score");
			model.addObject("circleList", circles);
			model.addObject("categorieList", categories);
			model.addObject("scoreCard", new ScoreCardBO());
			model.addObject("getCategory",gson.toJson(scoreCard.getAudit_category()));
			model.addObject("circleName",gson.toJson(scoreCard.getCircle_id()));
			model.addObject("selectedMonth",gson.toJson(scoreCard.getMonth()));
			
			model.addObject("data",gson.toJson(data));
			model.addObject("storeSet",gson.toJson(new ArrayList<>(stores)));
			request.setAttribute("selectedCircle", circleName);
			request.setAttribute("selectedCategory", scoreCard.getAudit_category());
			model.addObject("auditorReportListFinal", gson.toJson(auditorReportListFinal));
			model.addObject("CircleReport",CircleReport);
			model.addObject("selectedCircleDownload",circleName);
			

		}catch(Exception e){
			e.printStackTrace();
		}
		return model;
    }
	@RequestMapping(value = "/micoReport", method = RequestMethod.GET)
	public ModelAndView micoReport(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView model=null;
		try{
			UserBean user = (UserBean) request.getSession().getAttribute("loginBean");
			int userId = auditService.getUserId(user.getName());
			List<String> storeIdList = auditService.getAllCompletedStoreListByCircle(userId);
			Map< Integer, String > micoUserMap = new LinkedHashMap<Integer, String>(); 
			List<UserBean> micoUserList = auditService.getMICOUserListReport(storeIdList);
			if(micoUserList!=null && micoUserList.size() != 0){
				
				 for(UserBean mico:micoUserList){
					 StringBuffer val = new StringBuffer();
					 if(mico.getFirstName()!=null){
						 val.append(mico.getFirstName());
					 }else{
						 val.append("");
					 }if(mico.getLastName()!=null){
						 val.append(" "+mico.getLastName());
					 }else{
						 val.append(" ");
					 }if(mico.getName()!=null){
						 val.append(" (Email: "+mico.getName()+")");
					 }else{
						 val.append("");
					 }
					 micoUserMap.put(mico.getId(), val.toString());
				 }
				 	List<String> data = new ArrayList<>();
					model= new ModelAndView("micoReport");
					model.addObject("scoreCard", new ScoreCardBO());
					model.addObject("micoUserMap", micoUserMap);
					model.addObject("data", data);
			}
			else{
				model = new ModelAndView("homePage");
				request.setAttribute("storeMessage", "No Mico was Mapped to you. Please Contact Admin.");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return model;
	}
	
	@RequestMapping(value = "/generateMicoReport", method = RequestMethod.POST)
	public ModelAndView generateMicoReport(HttpServletRequest request, HttpServletResponse response,@ModelAttribute ScoreCardBO scoreCard) {
		ModelAndView model=null;
		try{
			
			
			Map< Integer, String > micoUserMap = new LinkedHashMap<Integer, String>(); 
			UserBean user = (UserBean) request.getSession().getAttribute("loginBean");
			int userId = auditService.getUserId(user.getName());
			List<String> storeIdList = auditService.getAllCompletedStoreListByCircle(userId); 
			List<UserBean> micoUserList = auditService.getMICOUserListReport(storeIdList);
			if(micoUserList!=null && micoUserList.size() != 0){
				
				 for(UserBean mico:micoUserList){
					 StringBuffer val = new StringBuffer();
					 if(mico.getFirstName()!=null){
						 val.append(mico.getFirstName());
					 }else{
						 val.append("");
					 }if(mico.getLastName()!=null){
						 val.append(" "+mico.getLastName());
					 }else{
						 val.append(" ");
					 }if(mico.getName()!=null){
						 val.append(" (Email: "+mico.getName()+")");
					 }else{
						 val.append("");
					 }
					 micoUserMap.put(mico.getId(), val.toString());
				 }
			}
			
			model= new ModelAndView("micoReport");
			model.addObject("micoUserMap", micoUserMap);
			
			
			List<ScoreCardBO> auditorReportList = new ArrayList<ScoreCardBO>();
			List<ScoreCardBO> auditorReportListFinal = new ArrayList<ScoreCardBO>();
			Gson gson = new Gson();
			auditorReportList = auditService.getMICOReport(scoreCard.getStore_id(), scoreCard.getMonth());
			List<Store> storeList = auditService.getStoreObject();
			Map<String,List<Float>> data = new LinkedHashMap<String,List<Float>>();
			String monthName = "";
			for(ScoreCardBO scoreBo : auditorReportList){
				ScoreCardBO storeBo = new ScoreCardBO();
				for(Store store : storeList){
					if(scoreBo.getStore_id() == store.getId()){
						storeBo.setPercentage(scoreBo.getPercentage());
						storeBo.setStoreName(store.getStore_name());
						if(scoreBo.getMonth().equals("1")){
							monthName = "Jan";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("2")){
							monthName = "Feb";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("3")){
							monthName = "Mar";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("4")){
							monthName = "Apr";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("5")){
							monthName = "May";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("6")){
							monthName = "Jun";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("7")){
							monthName = "Jul";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("8")){
							monthName = "Aug";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("9")){
							monthName = "Sep";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("10")){
							monthName = "Oct";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("11")){
							monthName = "Nov";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("12")){
							monthName = "Dec";
							data.put(monthName, new ArrayList<Float>());
						}
						storeBo.setMonth(monthName);
						auditorReportListFinal.add(storeBo);
					}
				}
			}
			Set<String> categorySet =new TreeSet<>(); 
			for(ScoreCardBO bo: auditorReportListFinal){
				categorySet.add(bo.getStoreName());
			}
			for(String store: categorySet){
				for(String val: data.keySet() ){
					ScoreCardBO cardBO = new ScoreCardBO();
					cardBO.setStoreName(store);
					cardBO.setMonth(val);
					if(auditorReportListFinal.contains(cardBO)){
						ScoreCardBO score=auditorReportListFinal.get(auditorReportListFinal.indexOf(cardBO));
						List<Float> scoreList = data.get(score.getMonth());
						scoreList.add(Float.parseFloat(score.getPercentage()));
						data.put(score.getMonth(), scoreList);
					}else{
						List<Float> scoreList = data.get(cardBO.getMonth());
						scoreList.add(0.00f);
						data.put(cardBO.getMonth(), scoreList);
					}
				}
			}
				
			Map< Integer, String > auditorMap = new LinkedHashMap<Integer, String>(); 
			List<UserBean> auditorList = auditService.getMICOUserList();
			String auditorNameSelected = null;
			if(auditorList.size() != 0){
				
				 for(UserBean auditor:auditorList){
					 StringBuffer val = new StringBuffer();
					 if(auditor.getFirstName()!=null){
						 val.append(auditor.getFirstName());
					 }else{
						 val.append("");
					 }if(auditor.getLastName()!=null){
						 val.append(" "+auditor.getLastName());
					 }else{
						 val.append(" ");
					 }if(auditor.getName()!=null){
						 val.append(" (Email: "+auditor.getName()+")");
					 }else{
						 val.append("");
					 }
					 
					 auditorMap.put(auditor.getId(), val.toString());
					 if(auditor.getId()==scoreCard.getStore_id()){
						 auditorNameSelected = val.toString(); 
					 }
				 }
			}
			
			model.addObject("auditor", auditorMap);
			model.addObject("data",gson.toJson(data));
			model.addObject("scoreCard", new ScoreCardBO());
			model.addObject("selectedAuditor", scoreCard.getStore_id());
			request.setAttribute("selectedAuditorName", auditorNameSelected);
			request.setAttribute("selectedMonthName", monthName);
			  model.addObject("selectedMonth", scoreCard.getMonth());
			  model.addObject("categorySet", gson.toJson(new ArrayList<>(categorySet)));
			model.addObject("auditorReportListFinal", gson.toJson(auditorReportListFinal));
			model.addObject("auditorDetails", gson.toJson(auditorNameSelected));
		}catch(Exception e){
			e.printStackTrace();
		}
		return model;
	}
	
	@RequestMapping(value="/generateMicoReport",params="download",method=RequestMethod.POST)
    public ModelAndView downloadMicoReport(HttpServletRequest request, HttpServletResponse response,@ModelAttribute ScoreCardBO scoreCard) throws Exception
    {
		ModelAndView model=null;
		try{
			List<ScoreCardBO> auditorReportList = new ArrayList<ScoreCardBO>();
			List<ScoreCardBO> auditorReportListFinal = new ArrayList<ScoreCardBO>();
			Gson gson = new Gson();
			auditorReportList = auditService.getMICOReport(scoreCard.getStore_id(), scoreCard.getMonth());
			List<Store> storeList = auditService.getStoreObject();
			Map<String,List<Float>> data = new LinkedHashMap<String,List<Float>>();
			String monthName = "";
			String user = auditService.getAuditorNameById(scoreCard.getStore_id());
			response.setHeader("Content-disposition", "attachment; filename=MICO Wise Report_"+user+"_"+monthMap.get(Integer.parseInt(scoreCard.getMonth()))+"_"+new Date()+".xls");
			for(ScoreCardBO scoreBo : auditorReportList){
				ScoreCardBO storeBo = new ScoreCardBO();
				for(Store store : storeList){
					if(scoreBo.getStore_id() == store.getId()){
						storeBo.setPercentage(scoreBo.getPercentage());
						storeBo.setStoreName(store.getStore_name());
						if(scoreBo.getMonth().equals("1")){
							monthName = "Jan";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("2")){
							monthName = "Feb";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("3")){
							monthName = "Mar";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("4")){
							monthName = "Apr";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("5")){
							monthName = "May";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("6")){
							monthName = "Jun";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("7")){
							monthName = "Jul";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("8")){
							monthName = "Aug";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("9")){
							monthName = "Sep";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("10")){
							monthName = "Oct";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("11")){
							monthName = "Nov";
							data.put(monthName, new ArrayList<Float>());
						}
						if(scoreBo.getMonth().equals("12")){
							monthName = "Dec";
							data.put(monthName, new ArrayList<Float>());
						}
						storeBo.setMonth(monthName);
						auditorReportListFinal.add(storeBo);
					}
				}
			}
			Set<String> categorySet =new TreeSet<>(); 
			for(ScoreCardBO bo: auditorReportListFinal){
				categorySet.add(bo.getStoreName());
			}
			for(String store: categorySet){
				for(String val: data.keySet() ){
					ScoreCardBO cardBO = new ScoreCardBO();
					cardBO.setStoreName(store);
					cardBO.setMonth(val);
					if(auditorReportListFinal.contains(cardBO)){
						ScoreCardBO score=auditorReportListFinal.get(auditorReportListFinal.indexOf(cardBO));
						List<Float> scoreList = data.get(score.getMonth());
						scoreList.add(Float.parseFloat(score.getPercentage()));
						data.put(score.getMonth(), scoreList);
					}else{
						List<Float> scoreList = data.get(cardBO.getMonth());
						scoreList.add(0.00f);
						data.put(cardBO.getMonth(), scoreList);
					}
				}
			}
			
			/*for(ScoreCardBO bo: auditorReportListFinal){
				categorySet.add(bo.getStoreName());
				if(data.containsKey(bo.getMonth())){
					List<Float> scoreList = data.get(bo.getMonth());
					scoreList.add(Float.parseFloat(bo.getPercentage()));
					data.put(bo.getMonth(), scoreList);
				}else{
					List<Float> scoreList = new ArrayList<>();
					scoreList.add(Float.parseFloat(bo.getPercentage()));
					data.put(bo.getMonth(), scoreList);
				}
				
			}*/
				
			Map< Integer, String > auditorMap = new LinkedHashMap<Integer, String>(); 
			List<UserBean> auditorList = auditService.getMICOUserList();
			String auditorNameSelected = null;
			if(auditorList.size() != 0){
				
				 for(UserBean auditor:auditorList){
					 StringBuffer val = new StringBuffer();
					 if(auditor.getFirstName()!=null){
						 val.append(auditor.getFirstName());
					 }else{
						 val.append("");
					 }if(auditor.getLastName()!=null){
						 val.append(" "+auditor.getLastName());
					 }else{
						 val.append(" ");
					 }if(auditor.getName()!=null){
						 val.append(" (Email: "+auditor.getName()+" )");
					 }else{
						 val.append("");
					 }
					 
					 auditorMap.put(auditor.getId(), val.toString());
					 if(auditor.getId()==scoreCard.getStore_id()){
						 auditorNameSelected = val.toString(); 
					 }
				 }
			}
			
			Collections.sort(auditorReportListFinal, new Comparator<ScoreCardBO>(){
				@Override
			    public int compare(ScoreCardBO a1, ScoreCardBO a2) {
			        return a1.getStoreName().compareTo(a2.getStoreName());
			    }
			});
			
			
			model= new ModelAndView("micoExport");
			model.addObject("user",user);
			model.addObject("auditorReportExportList", auditorReportListFinal);
			model.addObject("auditor", auditorMap);
			model.addObject("data",gson.toJson(data));
			model.addObject("scoreCard", new ScoreCardBO());
			model.addObject("selectedAuditor", scoreCard.getStore_id());
			request.setAttribute("selectedAuditorName", auditorNameSelected);
			request.setAttribute("selectedMonthName", monthName);
			  model.addObject("selectedMonth", scoreCard.getMonth());
			  model.addObject("categorySet", gson.toJson(new ArrayList<>(categorySet)));
			model.addObject("auditorReportListFinal", gson.toJson(auditorReportListFinal));
			model.addObject("auditorDetails", gson.toJson(auditorNameSelected));
		}catch(Exception e){
			e.printStackTrace();
		}
		return model;
    }


}
