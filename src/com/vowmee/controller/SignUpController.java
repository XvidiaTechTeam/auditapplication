
package com.vowmee.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.vowmee.auditHistory.AuditHistoryService;
import com.vowmee.bean.AuditHistoryBean;
import com.vowmee.bean.AuditLookupBean;
import com.vowmee.bean.CircleBean;
import com.vowmee.bean.StoreBean;
import com.vowmee.bean.UserBean;
import com.vowmee.bean.UserCircleBean;
import com.vowmee.bean.UserStoreMap;
import com.vowmee.service.RegisterService;
import com.vowmee.utill.AuditHistoryUtil;

@Controller
public class SignUpController {
	@Autowired
    private JavaMailSender mailSender;
	 @Autowired
	RegisterService registerService;
	 @Autowired
		private AuditHistoryService auditHistoryService;

   
	@RequestMapping(value="/signup",method=RequestMethod.GET)
	public ModelAndView register(HttpServletRequest request, HttpServletResponse response)
	{
		ModelAndView model = new ModelAndView("signup");
		UserBean userBean = new UserBean();
		/*List<String> roleList = registerService.getRoleList();*/
		List<UserBean> roleList = registerService.getRoleList();
		List<UserBean> typeList = registerService.getTypeList();
		List<UserBean> statusList = registerService.getStatusList();
		List<CircleBean> circleList = registerService.getCircleList();
		 Map< Integer, String > roles = new HashMap<Integer, String>(); 
		 Map< Integer, String > types = new HashMap<Integer, String>(); 
		 Map< Integer, String > statuses = new HashMap<Integer, String>(); 
		 Map< Integer, String > circles = new HashMap<Integer, String>(); 
		 for(UserBean role:roleList){
			 roles.put(role.getUser_role(), role.getUserRole());
		 }
		 for(UserBean type:typeList){
			 if(!type.getUserType().equalsIgnoreCase("admin")){
				 types.put(type.getUser_type(), type.getUserType());
			 }
			
		 }
		 for(UserBean status:statusList){
			 
			 statuses.put(status.getUser_status(), status.getUserStatus());
		 }
		for(CircleBean circle:circleList){
			 circles.put(circle.getId(), circle.getCircle_name());
		 }
		//Store store = new Store();
		model.addObject("roleList", roles);
		model.addObject("typeList", types);
		model.addObject("statusList", statuses);
		model.addObject("circleList", circles);
		model.addObject("editUserBean", userBean);
		model.addObject("selectTimePeriod", userBean);
		return model;
	}
	
	
	
	@RequestMapping(value="/activeuser/{id}",method=RequestMethod.GET)
	public ModelAndView activeUser(HttpServletRequest request, HttpServletResponse response, @PathVariable("id")int id)
	{
		ModelAndView model=null;
		try{
			int statusId = registerService.getStatusId("RE");
			registerService.updateStatusAfterActivation(id, statusId);
			String url = request.getContextPath();
			System.out.println("url "+url);
			model = new ModelAndView("registerActivation");
		
			request.setAttribute("activateMessage", "Your account has been activated please login into your account");
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return model;
		
	}
	@RequestMapping(value="/signup",method=RequestMethod.POST)
	public ModelAndView registerUser(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("editUserBean")UserBean userBean
			)
	{
		ModelAndView model= null;
		AuditHistoryUtil loginAuditHistoryUtil = new AuditHistoryUtil(); 
		AuditHistoryBean auditHistoryBean = new AuditHistoryBean();
		try
		{
			
			System.out.println("user Bean "+userBean);
			System.out.println("user Bean "+userBean.getUser_role());
			String path= request.getRequestURI();
			boolean exist = registerService.checkForExistingUser(userBean.getName());
			Integer auditAdminUserId = (Integer) request.getSession().getAttribute("userId");
			if(!exist){
				String url = request.getContextPath();
				/*int randomNumber = (int)(Math.random()*90000);
				String randomString = String.valueOf(randomNumber);*/
				//userBean.setOtp(randomString);
				//int statusId = registerService.getStatusId("CR");
				//int userTypeId = registerService.getUserTypeId("NM");
				//int userRoleId = registerService.getUserRoleId("AD");
				//userBean.setUser_role(userRoleId);
				//userBean.setUser_type(userTypeId);
				int randomNumber = (int)(Math.random()*90000);
				String randomString = String.valueOf(randomNumber);
				int statusId = registerService.getUserStatusByiD("Active");
				userBean.setPassword(randomString);
				userBean.setUser_status(statusId);
				System.out.println("user bean "+userBean);
				long id = registerService.saveUser(userBean);
				String newUserId = id+"";
				int newUserIdInt = Integer.parseInt(newUserId);
				String roleName = registerService.getUserRoleName(userBean.getUser_role());
				
				List<UserStoreMap> userStoreMapList = new ArrayList<UserStoreMap>();
				List<UserCircleBean> userCircleBeanList=new ArrayList<UserCircleBean>();
				if(roleName.equalsIgnoreCase("Service Delivery Head") ||roleName.equalsIgnoreCase("Service Excellence Head")
						|| roleName.equalsIgnoreCase("Circle Training")){
					for(String s:userBean.getUserCircle()){
					UserCircleBean userCircleBean=new UserCircleBean();
					userCircleBean.setCircleid(Integer.parseInt(s));
					userCircleBean.setUserid(id);
					userCircleBeanList.add(userCircleBean);
					List<UserStoreMap> userStoreMapping = registerService.getStoreList(Integer.parseInt(s));
					if(userStoreMapping != null){
						for(UserStoreMap storeId:userStoreMapping){
							UserStoreMap userStoreMap = new UserStoreMap();
							userStoreMap.setUserid(id);
							userStoreMap.setStoreid(storeId.getId());
							userStoreMapList.add(userStoreMap);
						}
					}
					
					}
					
					
				}
				else{
					if(userBean!=null && userBean.getUserCircle().size()>0)
					{
						for(String ucerCircle:userBean.getUserCircle())
						{
							UserCircleBean usercircle=new UserCircleBean();
							usercircle.setUserid(id);
							usercircle.setCircleid(Integer.parseInt(ucerCircle));
							userCircleBeanList.add(usercircle);
						}
					}
					if(userBean != null && userBean.getStoreName().size()>0){
						for(String storeId:userBean.getStoreName()){
							UserStoreMap userStoreMap = new UserStoreMap();
							userStoreMap.setUserid(id);
							userStoreMap.setStoreid(Integer.parseInt(storeId));
							userStoreMapList.add(userStoreMap);
						}
					}
				}
				if(userCircleBeanList.size()>0)
				{
					registerService.userCircleMapping(userCircleBeanList);
				}
				
				if(userStoreMapList.size()>0){
					registerService.userStoreMapping(userStoreMapList);
				}
				
				
				if(id!=0){
					request.setAttribute("getEmail", userBean.getName());
					registerService.sendEmail(mailSender, userBean, url, id, randomString);
					auditHistoryBean=loginAuditHistoryUtil.auditHistoryBean(newUserIdInt, auditAdminUserId, "New User Created", "created", "");
					auditHistoryService.insertAuditHistory(auditHistoryBean);
					request.getSession().setAttribute("unregistered", "Registered Successfully");
					
					model = new ModelAndView("redirect:usermanagement");
				}else{
					System.out.println("User Successful"+userBean.getName());
					request.getSession().setAttribute("unregistered", "User Not Registered");
					model = new ModelAndView("redirect:usermanagement");
				}
				
				System.out.println("User Registered Successful"+userBean.getName());
				

			}else{
				request.getSession().setAttribute("unregistered", "Already registered");
				/*userBean=new UserBean();
				model = new ModelAndView("signup");
				model.addObject("signupBean", userBean);*/
				model = new ModelAndView("redirect:usermanagement");
			}
			
							/*if((!userBean.getPassword().isEmpty()) &&
						(!userBean.getUsername().isEmpty()) &&
						(!userBean.getMobile().isEmpty() ) && 
						(!userBean.getConfirmPassword().isEmpty() )){
					
					if(userBean.getPassword().equals(userBean.getConfirmPassword())){
						String url = request.getContextPath();
						int randomNumber = (int)(Math.random()*90000);
						String randomString = String.valueOf(randomNumber);
						userBean.setOtp(randomString);
						registerService.sendEmail(mailSender, userBean, url);
						registerService.saveUser(userBean);
						model = new ModelAndView("welcome");
					}else{
						
						request.setAttribute("confirmation", "Password and Confirm Password didn't match");
						model = new ModelAndView("signup");
						model.addObject("loginBean", userBean);
					}
					
				}else{
					request.setAttribute("mandatory", "Please provide all mandatory fields");
					model = new ModelAndView("signup");
					model.addObject("loginBean", userBean);
					//request.setAttribute("message", "Invalid credentials!!");
					//model = new ModelAndView("redirect:signup");
				}
				*/
				
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		return model;
	}
@RequestMapping(value="/signupforStore",method=RequestMethod.POST)
	public ModelAndView registerStore(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("editStoreBean")StoreBean storeBean
			)
	{
		ModelAndView model= null;
		AuditHistoryUtil loginAuditHistoryUtil = new AuditHistoryUtil(); 
		AuditHistoryBean auditHistoryBean = new AuditHistoryBean();
		try
		{
			
			System.out.println("user Bean "+storeBean);
			
			String path= request.getRequestURI();
			boolean exist = registerService.checkForExistingStore(storeBean.getStore_code());
			Integer auditAdminUserId = (Integer) request.getSession().getAttribute("userId");
			
			if(!exist){
				String url = request.getContextPath();
				
				int statusId = registerService.getStoreStatusByiD("Active");
				System.out.println("user bean "+storeBean);
				storeBean.setStore_status(statusId);
				long id = registerService.saveStore(storeBean);
				String newStoreId = id+"";
				int newStoreIdInt = Integer.parseInt(newStoreId);
				
				
				
				
				
				if(id!=0){
				
					auditHistoryBean=loginAuditHistoryUtil.auditHistoryBean(newStoreIdInt, auditAdminUserId, "New Store Created", "created", "");
					auditHistoryService.insertAuditHistory(auditHistoryBean);
					request.getSession().setAttribute("unregistered", "Registered Successfully");
					
					model = new ModelAndView("redirect:usermanagement");
				}else{
					System.out.println("Store Successful"+storeBean.getStore_name());
					request.getSession().setAttribute("unregistered", "Store Not Registered");
					model = new ModelAndView("redirect:usermanagement");
				}
				
				System.out.println("Store Registered Successful"+storeBean.getStore_name());
				

			}else{
				request.getSession().setAttribute("unregistered", "Already registered");
				storeBean=new StoreBean();
				model = new ModelAndView("signup");
				model.addObject("signupBean", storeBean);
				model = new ModelAndView("redirect:usermanagement");
			}
			
						
				
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		return model;
	}
	
@RequestMapping(value="/signupforAudit",method=RequestMethod.POST)
public ModelAndView registerAudit(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("editAuditBean")AuditLookupBean auditLookupBean
		)
{
	ModelAndView model= null;
	AuditHistoryUtil loginAuditHistoryUtil = new AuditHistoryUtil(); 
	AuditHistoryBean auditHistoryBean = new AuditHistoryBean();
	try
	{
		
		System.out.println("user Bean "+auditLookupBean);
		
		String path= request.getRequestURI();
		boolean exist = registerService.checkForExistingAudit(auditLookupBean.getAudit_category(),auditLookupBean.getAudit_name());
		Integer auditAdminUserId = (Integer) request.getSession().getAttribute("userId");
		
		if(!exist){
			String url = request.getContextPath();
			
			
			System.out.println("user bean "+auditLookupBean);
			long id = registerService.saveAudit(auditLookupBean);
			String newAuditLookupId = id+"";
			int newAuditLookupIdInt = Integer.parseInt(newAuditLookupId);
			
			
			
			
			
			if(id!=0){
			
				auditHistoryBean=loginAuditHistoryUtil.auditHistoryBean(newAuditLookupIdInt, auditAdminUserId, "New Category/Questions Created", "created", "");
				auditHistoryService.insertAuditHistory(auditHistoryBean);
				request.getSession().setAttribute("unregistered", "Registered Successfully");
				
				model = new ModelAndView("redirect:usermanagement");
			}else{
				//System.out.println("Store Successful"+auditLookupBean.getStore_name());
				request.getSession().setAttribute("unregistered", "User Not Registered");
				model = new ModelAndView("redirect:usermanagement");
			}
			
			//System.out.println("User Registered Successful"+auditLookupBean.getStore_name());
			

		}else{
			request.getSession().setAttribute("unregistered", "Already registered");
			auditLookupBean=new AuditLookupBean();
			model = new ModelAndView("signup");
			model.addObject("signupBean", auditLookupBean);
			model = new ModelAndView("redirect:usermanagement");
		}
		
					
			
	
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}

	return model;
}

	

}
