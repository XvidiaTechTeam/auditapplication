package com.vowmee.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.vowmee.bean.UserBean;
import com.vowmee.service.EmailService;
import com.vowmee.service.LoginService;
import com.vowmee.service.RegisterService;
import com.vowmee.service.SetPasswordService;

@Controller
public class SetPasswordController {
	@Autowired
	private SetPasswordService passwordService;
	@Autowired
    private JavaMailSender mailSender;
	@Autowired
	private EmailService emailService;
	 @Autowired
		RegisterService registerService;
	 @Autowired
		private LoginService loginService;
	
	@RequestMapping(value="/setpassword",method=RequestMethod.GET)
	public ModelAndView setPassword(HttpServletRequest request, HttpServletResponse response)
	{
		ModelAndView model = new ModelAndView("setpassword");
		UserBean userBean = new UserBean();
		model.addObject("setPassword", userBean);
		return model;
	}
	

	@RequestMapping(value="/forgotPasswordSendEmail",method=RequestMethod.GET)
	public ModelAndView forGotPasswordGet(HttpServletRequest request, HttpServletResponse response)
	{
		ModelAndView model = new ModelAndView("forgotPasswordSendEmail");
		
		UserBean userBean = new UserBean();
		model.addObject("loginBean", userBean);
		return model;
	}
	
	@RequestMapping(value="/forgotPassword/{id}",method=RequestMethod.GET)
	public ModelAndView forgotPassword(HttpServletRequest request, HttpServletResponse response,@PathVariable("id")int id)
	{
		ModelAndView model = new ModelAndView("forgotPassword");
		UserBean userBean = passwordService.getUserDetailById(id);
		//UserBean userBean = new UserBean();
		model.addObject("forgotPasswordAction", userBean);
		return model;
	}
	@RequestMapping(value="/forgotPasswordSuccessfully",method=RequestMethod.GET)
	public ModelAndView forgotPasswordSuccess(HttpServletRequest request, HttpServletResponse response)
	{
		ModelAndView model = new ModelAndView("forgotPasswordSuccessfully");
		return model;
	}
	
	@RequestMapping(value="forgotPassword/forgotPasswordAction",method=RequestMethod.POST)
	public ModelAndView forgotPasswordPost(HttpServletRequest request, HttpServletResponse response,@ModelAttribute("forgotPasswordAction")UserBean userBean)
	{
		ModelAndView model = null;
		try{
			int user_status = registerService.getStatusId("AC");
			userBean.setUser_status(user_status);
			int result = passwordService.updateRegisterSetPassword(userBean);
			if(result!=0){
				
				
				String path= request.getContextPath();
				String url = "http://audit.xvidiaglobal.com:8080"+path+"/forgotPasswordSuccessfully";
				//model = new ModelAndView("redirect:"+url);
				
				request.getSession().invalidate();
				
				boolean isValidUser = loginService.isValidUser(userBean.getName(), userBean.getPassword(), request);
				
				System.out.println("valid user "+isValidUser);
				System.out.println("session value "+ request.getSession().getAttribute("inactive"));
				String status = (String) request.getSession().getAttribute("inactive");
				String yet_to_activate = (String) request.getSession().getAttribute("yet_to_activate");
				String registered = (String) request.getSession().getAttribute("registered");
				String auditor = (String) request.getSession().getAttribute("auditor");
				String admin = (String) request.getSession().getAttribute("admin");
				if (isValidUser) {
					System.out.println("User Login Successful");
					
					request.getSession().setAttribute("loggedInUser", userBean.getFirstName());
					if(admin=="admin"){
						url = "http://audit.xvidiaglobal.com:8080"+path+"/homePage";
						//model = new ModelAndView("homePage");
						model = new ModelAndView("redirect:"+url);
					}else if(auditor=="auditor"){
						url = "http://audit.xvidiaglobal.com:8080"+path+"/homePage";
						//model = new ModelAndView("homePage");
						model = new ModelAndView("redirect:"+url);
					}else if(registered=="registered"){
						url = "http://audit.xvidiaglobal.com:8080"+path+"/welcome";
						//model = new ModelAndView("welcome");
						model = new ModelAndView("redirect:"+url);
					}else{
						url = "http://audit.xvidiaglobal.com:8080"+path+"/homePage";
						model = new ModelAndView("homePage");
						model = new ModelAndView("redirect:"+url);
					}
					
				}else if(status =="inactive"){
					url = "http://audit.xvidiaglobal.com:8080"+path+"/login";
					userBean=new UserBean();
					model = new ModelAndView("redirect:"+url);
					//model = new ModelAndView("login");
					model.addObject("loginBean", userBean);
					request.setAttribute("registered_message", "Your account is not in activation");
				}else if(yet_to_activate=="yet_to_activate"){
					url = "http://audit.xvidiaglobal.com:8080"+path+"/login";
					userBean=new UserBean();
					//model = new ModelAndView("login");
					model = new ModelAndView("redirect:"+url);
					model.addObject("loginBean", userBean);
					request.setAttribute("yet_to_activate", "Please activate your account");
				}else {
					url = "http://audit.xvidiaglobal.com:8080"+path+"/login";
					userBean=new UserBean();
					model = new ModelAndView("redirect:"+url);
					//model = new ModelAndView("login");
					model.addObject("loginBean", userBean);
					
					request.setAttribute("message", "Invalid credentials!!");
				}
				
			}
			
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
	
		return model;
	}
	
	@RequestMapping(value="/forgotPasswordEmail",method=RequestMethod.POST)
	public ModelAndView forgotPasswordSendEmail(HttpServletRequest request, HttpServletResponse response,@ModelAttribute("loginBean")UserBean userBean)
	{
		ModelAndView model=null;
		try{
			String userName = userBean.getName();
			String path = request.getContextPath();
			int randomNumber = (int)(Math.random()*90000);
			String randomString = String.valueOf(randomNumber);
			int id=passwordService.getUserDetailByName(userName);
			if(id>0){
				int updateId=passwordService.updateSecurityCodeId(id,randomString);
				String htmlBody = "<html><body> Please find the following details: "
						+ "<br/><br/> user name : "+userBean.getName()
						+ "<br/><br/> Security Code : "+randomString
						+ " <br/> <br/> please click the below link to reset your Password"
						+ "<br/> <br/>http://audit.xvidiaglobal.com:8080"+path+"/forgotPassword/"+id+" </body></html>";
				String subject = "Vowmee - Password Reset";
				emailService.commonSendEmail(mailSender, userName, htmlBody, subject);
				 model = new ModelAndView("forgotPasswordSentEmailSuccess");
			}else{
				userBean=new UserBean();
				model = new ModelAndView("login");
				model.addObject("loginBean", userBean);
				request.setAttribute("invalidEmail", "Invalid Email!!");
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return model;
	}
	
}
