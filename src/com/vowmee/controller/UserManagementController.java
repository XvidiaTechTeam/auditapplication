package com.vowmee.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.sun.org.apache.bcel.internal.generic.NEWARRAY;
import com.vowmee.auditHistory.AuditHistoryService;
import com.vowmee.bean.AuditBean;
import com.vowmee.bean.AuditHistoryBean;
import com.vowmee.bean.AuditLookupBean;
import com.vowmee.bean.CircleBean;
import com.vowmee.bean.LocationBean;
import com.vowmee.bean.Store;
import com.vowmee.bean.StoreBean;
import com.vowmee.bean.UserBean;
import com.vowmee.bean.UserCircleBean;
import com.vowmee.bean.UserCircleVO;
import com.vowmee.bean.UserManagementBean;
import com.vowmee.bean.UserRoleBean;
import com.vowmee.bean.UserStatusBean;
import com.vowmee.bean.UserStoreMap;
import com.vowmee.bean.UserTypeBean;
import com.vowmee.dao.UserManagementDao;
import com.vowmee.service.AuditService;
import com.vowmee.service.EmailService;
import com.vowmee.service.RegisterService;
import com.vowmee.service.SetPasswordService;
import com.vowmee.utill.AuditHistoryUtil;

@Controller
public class UserManagementController {
	
	@Autowired
	private UserManagementDao usermanagementDao;
	@Autowired
	private SetPasswordService passwordService;
	@Autowired
    private JavaMailSender mailSender;
	
	@Autowired
	private EmailService emailService;
	private boolean flag;
	
	 @Autowired
		RegisterService registerService;
	 @Autowired
		AuditService auditService;
	 
	 @Autowired
		private AuditHistoryService auditHistoryService;
	
	@RequestMapping(value="/usermanagement",method=RequestMethod.GET)
	public ModelAndView displayLogin(HttpServletRequest request, HttpServletResponse response) throws JsonGenerationException, JsonMappingException, IOException
	{
	//	Map<String,List> usermanagement = new HashMap<String,List>();
		//List<String> existUser = new ArrayList<String>();
		//UserManagementService usermanagementService = new UserManagementService();
	//	ObjectMapper mapper = new ObjectMapper();
	//	Gson gson = new Gson();
		String auditor = (String) request.getSession().getAttribute("auditor");
		String admin = (String) request.getSession().getAttribute("admin");
		String superAdmin = (String) request.getSession().getAttribute("superAdmin");
		ModelAndView model = null;
		
		List<UserBean> roleList = registerService.getRoleList();
		List<UserBean> typeList = registerService.getTypeList();
		List<UserBean> statusList = registerService.getStatusList();
		List<CircleBean> circleList = registerService.getCircleList();
		List<AuditLookupBean> auditList=registerService.getAuditLookUp();
		List<LocationBean> locationList=registerService.getLocationList();
		
		UserBean userBean1 = (UserBean) request.getSession().getAttribute("loginBean");
		 Map< Integer, String > roles = new HashMap<Integer, String>(); 
		 Map< Integer, String > types = new HashMap<Integer, String>(); 
		 Map< Integer, String > statuses = new HashMap<Integer, String>(); 
		 Map< Integer, String > circles = new HashMap<Integer, String>(); 
		 Map< Integer, String > audit = new HashMap<Integer, String>(); 
		 Map< Integer, String > location = new HashMap<Integer, String>(); 
		/* for(UserBean role:roleList){
			 if(!role.getUserRole().equalsIgnoreCase("admin")&& !role.getUserRole().equalsIgnoreCase("superAdmin")){
				 roles.put(role.getUser_role(), role.getUserRole());
			 }
			 
		 }*/
		 for(LocationBean loc:locationList)
		 {
			 location.put(loc.getId(), loc.getName());
		 }
		 for(UserBean type:typeList){
			 /*if(!type.getUserType().equalsIgnoreCase("admin")){
				 types.put(type.getUser_type(), type.getUserType());
			 }*/
			 types.put(type.getUser_type(), type.getUserType());
			
		 }
		 int count=1;
		 for(AuditLookupBean auditLookup:auditList)
		 {
			 audit.put(count, auditLookup.getAudit_name());
			 count++;
		 }
		 for(UserBean status:statusList){
			 
			 statuses.put(status.getUser_status(), status.getUserStatus());
		 }
		 for(CircleBean circle:circleList){
			 circles.put(circle.getId(), circle.getCircle_name());
		 }
		
		 UserBean userBean = new UserBean();
		 StoreBean storeBean=new StoreBean();
		 AuditLookupBean auditLookUpBean=new AuditLookupBean();
		if(admin=="admin"){
			
			 for(UserBean role:roleList){
				 if(!role.getUserRole().equalsIgnoreCase("admin")&& !role.getUserRole().equalsIgnoreCase("superAdmin")){
					 roles.put(role.getUser_role(), role.getUserRole());
				 }
				 
			 }
			 List<AuditLookupBean> auditQuestionList=usermanagementDao.getQuestionsList();
			 Map<String,List<AuditLookupBean>> auditQuestion =new HashMap<>();
			 List<AuditLookupBean> auditLookupBeanList=new ArrayList<>();
			 for(AuditLookupBean question:auditQuestionList)
			 {
                 		 
				 if(auditQuestion.containsKey(question.getAudit_category()))
				 { 
					 auditLookupBeanList.add(question);
					auditQuestion.remove(question.getAudit_category());
					auditQuestion.put(question.getAudit_category(), auditLookupBeanList);
				 }
				 else
				 {
					 auditLookupBeanList=new ArrayList<>();
					 auditQuestion.put(question.getAudit_category(),null);
					 auditLookupBeanList.add(question);
					 
				 }
			 }
			//model = new ModelAndView("homePage");
			model = new ModelAndView("manageUser");
			model.addObject("userDetails", usermanagementDao.getUserListFor1Week(userBean1.getId(),7));
			model.addObject("storeDetails", usermanagementDao.getStoreList());
			model.addObject("questionsList", auditQuestion);
			userBean.setFilterTimeForSelectUser("Last 7 Days");
			model.addObject("roleList", roles);
			model.addObject("typeList", types);
			model.addObject("statusList", statuses);
			model.addObject("circleList", circles);
			model.addObject("editUserBean", userBean);
			model.addObject("editStoreBean", storeBean);
			model.addObject("selectTimePeriod", userBean);
			model.addObject("editAuditBean", auditLookUpBean);
			model.addObject("auditList", audit);
			model.addObject("locationList", location);
			//usermanagement.put("userDetails", usermanagementDao.getUserList());
			/*usermanagement.put("userDetails", usermanagementDao.selectAllUser());
			usermanagement.put("roleDetial", usermanagementDao.selectAllRole());
			usermanagement.put("userType", usermanagementDao.selectAllUserType());
			usermanagement.put("userStatus", usermanagementDao.selectAllUserStatus());
			if(this.flag){
				existUser.add("true");
				usermanagement.put("existUser", existUser);
			}
			
			System.out.println("gson.toJson(usermanagement)" + gson.toJson(usermanagement));
			request.setAttribute("userList",gson.toJson(usermanagement));*/
			//model = new ModelAndView("redirect:usermanagement");
		}else if(auditor=="auditor"){
			UserBean user = (UserBean) request.getSession().getAttribute("loginBean");
			//model = new ModelAndView("manageUser");
			
			//usermanagement.put("userDetails", usermanagementDao.selectPerticularUser(user));
			//request.setAttribute("userList",gson.toJson(usermanagement));
			
			model = new ModelAndView("profile");
			model.addObject("userDetails", usermanagementDao.selectPerticularUserForProfile(user));
			model.addObject("editUserBean", userBean);
			//model = new ModelAndView("redirect:audit");
		}else if(superAdmin=="superAdmin"){
			
			 for(UserBean role:roleList){
				 if(!role.getUserRole().equalsIgnoreCase("superAdmin")){
					 roles.put(role.getUser_role(), role.getUserRole());
				 }
				 
			 }
			//model = new ModelAndView("homePage");
			model = new ModelAndView("manageUser");
			model.addObject("userDetails", usermanagementDao.getAdminUserListFor1Week(userBean1.getId(),7));
			model.addObject("storeDetails", usermanagementDao.getStoreList());
			model.addObject("questionsList", usermanagementDao.getQuestionsList());
			userBean.setFilterTimeForSelectUser("Last 7 Days");
			model.addObject("roleList", roles);
			model.addObject("typeList", types);
			model.addObject("statusList", statuses);
			model.addObject("circleList", circles);
			model.addObject("editUserBean", userBean);
			model.addObject("editStoreBean", storeBean);
			model.addObject("selectTimePeriod", userBean);
			model.addObject("editAuditBean", auditLookUpBean);
			model.addObject("auditList", audit);
			//usermanagement.put("userDetails", usermanagementDao.getUserList());
			/*usermanagement.put("userDetails", usermanagementDao.selectAllUser());
			usermanagement.put("roleDetial", usermanagementDao.selectAllRole());
			usermanagement.put("userType", usermanagementDao.selectAllUserType());
			usermanagement.put("userStatus", usermanagementDao.selectAllUserStatus());
			if(this.flag){
				existUser.add("true");
				usermanagement.put("existUser", existUser);
			}
			
			System.out.println("gson.toJson(usermanagement)" + gson.toJson(usermanagement));
			request.setAttribute("userList",gson.toJson(usermanagement));*/
			//model = new ModelAndView("redirect:usermanagement");
		}
		
		return model;
	}
	
	
	
	@RequestMapping(value="/insertUser",method=RequestMethod.POST)
	public String ajaxSaveData(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("usermanagementbean")UserManagementBean usermanagement){
		Gson gson = new Gson();
		 this.flag = usermanagementDao.checkForExistingUser(usermanagement.getEmail());
		if(!this.flag){
			usermanagementDao.insertUser(gson.toJson(usermanagement));
			String userName = usermanagement.getEmail();
			String path = request.getContextPath();
			int id=passwordService.getUserDetailByName(userName);
			int randomNumber = (int)(Math.random()*90000);
			String randomString = String.valueOf(randomNumber);
			int updateId=passwordService.updateSecurityCodeId(id,randomString);
			String htmlBody = "<html><body> Please find the following details: "
					+ "<br/><br/> user name : "+userName
					+ "<br/><br/> Security Code : "+randomString
					+ " <br/><br> please click the below link to set your Password"
					+ "<br/><br> http://audit.xvidiaglobal.com:8080"+path+"/forgotPassword/"+id+" </body></html>";
			String subject = "Vowmee - Registeration Success - Set Password";
			emailService.commonSendEmail(mailSender, userName, htmlBody, subject);
		}
		
		return "redirect:usermanagement";
	}
	
	@RequestMapping(value="/updateUser",method=RequestMethod.POST)
	@ResponseBody
	public String ajaxUpdateUserData(@RequestBody String json, HttpServletRequest request){
		usermanagementDao.updateUser(json);
		System.out.println(json);
		return json;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	
	@RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
	public ModelAndView uploadFileHandler(@RequestParam("file") CommonsMultipartFile file, HttpSession session,
			HttpServletRequest request, HttpServletResponse response) {
		List<UserManagementBean> userUploadList = new ArrayList<UserManagementBean>();
		List<StoreBean> storeUploadList=new ArrayList<StoreBean>(); 
		List<AuditLookupBean> auditLookupBeanList=new ArrayList<AuditLookupBean>();
		ModelAndView model= null;
		AuditHistoryUtil loginAuditHistoryUtil = new AuditHistoryUtil(); 
		AuditHistoryBean auditHistoryBean = new AuditHistoryBean();
		String flag=request.getParameter("newfield");
	
		try {
			if (!file.isEmpty()) {
                if(flag.equals("CreateUser"))
                {
				List<UserStatusBean> userstatusList = new ArrayList<UserStatusBean>();
				List<UserTypeBean> usertypeList = new ArrayList<UserTypeBean>();
				List<UserRoleBean> userRoleList = new ArrayList<UserRoleBean>();
				List<UserManagementBean> userUploadListFinal = new ArrayList<UserManagementBean>();
				List<UserManagementBean> failedUserUploadList = new ArrayList<UserManagementBean>();
				List<UserManagementBean> failedUserUploadListFinal = new ArrayList<UserManagementBean>();
				Set<UserManagementBean> failedUserUploadListFinalSet = new HashSet<UserManagementBean>();
				List<UserStoreMap> userStoreMapList = new ArrayList<UserStoreMap>();
				List<UserCircleBean> userCircleMapList = new ArrayList<UserCircleBean>();
				userstatusList = usermanagementDao.selectAllUserStatus();
				usertypeList = usermanagementDao.selectAllUserType();
				userRoleList = usermanagementDao.selectAllRole();
				List<UserCircleVO> userCircle = usermanagementDao.getCircle();
				List<Store> storeList = usermanagementDao.selectAllStores();
				String path = session.getServletContext().getRealPath("/");
				String filename = file.getOriginalFilename();
				String filePath = path + filename;
				System.out.println(path + filename);
				ReadExcel readExcel = new ReadExcel();
				int statusId = registerService.getUserStatusByiD("Active");
				userUploadList = readExcel.readExcel(file, filename, filePath, userstatusList, usertypeList,
						userRoleList, userCircle, storeList, statusId);
				failedUserUploadList = ReadExcel.getFailedRecords(userUploadList);
				userUploadListFinal=ReadExcel.getUserDetails(userUploadList, userRoleList,statusId);
				
				System.out.println("user Uploaded data " + userUploadListFinal);
				List<UserManagementBean> existedUsers = usermanagementDao.getExistedUsers(userUploadListFinal);
				Map<String, Long> userFinalIds = usermanagementDao.insertNewUsers(userUploadListFinal);
				Map<String, String> userPassword = ReadExcel.getPassword(userUploadListFinal);
				
				if(failedUserUploadList!=null && failedUserUploadList.size()>0){
					failedUserUploadListFinal.addAll(failedUserUploadList);
				}
				
				if(existedUsers!=null && existedUsers.size()>0){
					failedUserUploadListFinal.addAll(existedUsers);
				}
				
				if(failedUserUploadListFinal!=null && failedUserUploadListFinal.size()>0){
					failedUserUploadListFinalSet.addAll(failedUserUploadListFinal);
					failedUserUploadListFinal = new ArrayList<>();
					failedUserUploadListFinal.addAll(failedUserUploadListFinalSet);
				}
				
				System.out.println("failed records list : "+failedUserUploadListFinal);
				for(UserManagementBean userStoreFinal: userUploadListFinal){
					if(userFinalIds.get(userStoreFinal.getEmail())!=null){
						for(String storeId:userStoreFinal.getStoreList()){
							if(storeId!=null){
								UserStoreMap userStoreMap = new UserStoreMap();
								userStoreMap.setUserid(userFinalIds.get(userStoreFinal.getEmail()));
								
								userStoreMap.setStoreid(Integer.parseInt(storeId));
								userStoreMapList.add(userStoreMap);
							}
							
						}
						for(String circleId:userStoreFinal.getCircleList()){
							if(circleId!=null){
								UserCircleBean userCircleMap = new UserCircleBean();
								userCircleMap.setUserid(userFinalIds.get(userStoreFinal.getEmail()));
								
								userCircleMap.setCircleid(Integer.parseInt(circleId));
								userCircleMapList.add(userCircleMap);
							}
							
						}
					}
				}
				if(userCircleMapList.size()>0)
				{
					registerService.userCircleMapping(userCircleMapList);
				}
				if(userStoreMapList.size()>0){
					registerService.userStoreMapping(userStoreMapList);
				}
				
				String url = request.getContextPath();
				UserBean userBean = (UserBean) request.getSession().getAttribute("loginBean");		
				if(userFinalIds!=null && userFinalIds.size()>0){
				
					registerService.sendUploadedEmail(mailSender, userBean,userUploadList,failedUserUploadListFinal);
					Iterator it = userFinalIds.entrySet().iterator();
				    while (it.hasNext()) {
				        Map.Entry pair = (Map.Entry)it.next();
				        System.out.println(pair.getKey() + " = " + pair.getValue());
				        String email = (String) pair.getKey();
				        long id=(long) pair.getValue();
				        UserBean userBeanForEmail =new UserBean();
				        userBeanForEmail.setName(email);
				        registerService.sendEmail(mailSender, userBeanForEmail, url, id, userPassword.get(email));
				    }
					auditHistoryBean=loginAuditHistoryUtil.auditHistoryBean(0, userBean.getId(), "Uploaded Multiple Users", "created", "");
					auditHistoryService.insertAuditHistory(auditHistoryBean);
					request.getSession().setAttribute("unregistered", "Registered Successfully");
					
					model = new ModelAndView("redirect:usermanagement");
				}else{
					registerService.sendUploadedEmail(mailSender, userBean, userUploadList,failedUserUploadListFinal);
					System.out.println("User Successful"+userBean.getName());
					request.getSession().setAttribute("unregistered", "Users Existed");
					model = new ModelAndView("redirect:usermanagement");
				}

			}
                else if(flag.equals("CreateStore"))
				{

					List<LocationBean> locationList = new ArrayList<LocationBean>();
					List<StoreBean> storeUploadListFinal = new ArrayList<StoreBean>();
					List<StoreBean> failedStoreUploadList = new ArrayList<StoreBean>();
					List<StoreBean> failedStoreUploadListFinal = new ArrayList<StoreBean>();
					Set<StoreBean> failedStoreUploadListFinalSet = new HashSet<StoreBean>();
					locationList = usermanagementDao.selectAllLocation();
                    List<UserCircleVO> userCircle = usermanagementDao.getCircle();
					String path = session.getServletContext().getRealPath("/");
					String filename = file.getOriginalFilename();
					String filePath = path + filename;
					System.out.println(path + filename);
					ReadExcel readExcel = new ReadExcel();
					storeUploadList = readExcel.readExcel(file, filename, filePath, locationList,userCircle);
					failedStoreUploadList = ReadExcel.getFailedRecordsForStore(storeUploadList);
					storeUploadListFinal = ReadExcel.getUserDetails(storeUploadList);

					System.out.println("user Uploaded data " + storeUploadListFinal);
					List<StoreBean> existedStore = usermanagementDao.getExistedStores(storeUploadListFinal);
					Map<String, Long> userFinalIds = usermanagementDao.insertNewStores(storeUploadListFinal);
					

					if (failedStoreUploadList != null && failedStoreUploadList.size() > 0) {
						failedStoreUploadListFinal.addAll(failedStoreUploadList);
					}

					if (existedStore != null && existedStore.size() > 0) {
						failedStoreUploadListFinal.addAll(existedStore);
					}

					if (failedStoreUploadListFinal != null && failedStoreUploadListFinal.size() > 0) {
						failedStoreUploadListFinalSet.addAll(failedStoreUploadListFinal);
						failedStoreUploadListFinal = new ArrayList<>();
						failedStoreUploadListFinal.addAll(failedStoreUploadListFinalSet);
					}

					System.out.println("failed records list : " + failedStoreUploadListFinal);
					
					
					String url = request.getContextPath();
					UserBean userBean = (UserBean) request.getSession().getAttribute("loginBean");
					if (userFinalIds != null && userFinalIds.size() > 0) {

						registerService.sendUploadedEmailForStore(mailSender, userBean, storeUploadList,
								failedStoreUploadListFinal);
					
						auditHistoryBean = loginAuditHistoryUtil.auditHistoryBean(0, userBean.getId(),
								"Uploaded Multiple Stores", "created", "");
						auditHistoryService.insertAuditHistory(auditHistoryBean);
						request.getSession().setAttribute("unregistered", "Registered Successfully");

						model = new ModelAndView("redirect:usermanagement");
					} else {
						registerService.sendUploadedEmailForStore(mailSender, userBean, storeUploadList,
						failedStoreUploadListFinal);
						System.out.println("Store Successful" + userBean.getName());
						request.getSession().setAttribute("unregistered", "Stores Existed");
						model = new ModelAndView("redirect:usermanagement");
					}

				}
                else
                {
					List<AuditLookupBean> auditLookupUploadListFinal = new ArrayList<AuditLookupBean>();
					List<AuditLookupBean> failedAuditLookupUploadList = new ArrayList<AuditLookupBean>();
					List<AuditLookupBean> failedAuditLookupUploadListFinal = new ArrayList<AuditLookupBean>();
					Set<AuditLookupBean> failedAuditLookupUploadListFinalSet = new HashSet<AuditLookupBean>();
                    List<UserCircleVO> userCircle = usermanagementDao.getCircle();
					String path = session.getServletContext().getRealPath("/");
					String filename = file.getOriginalFilename();
					String filePath = path + filename;
					System.out.println(path + filename);
					ReadExcel readExcel = new ReadExcel();
					auditLookupBeanList = readExcel.readExcel(file, filename, filePath,userCircle);
					failedAuditLookupUploadList = ReadExcel.getFailedRecordsForAudit(auditLookupBeanList);
					auditLookupUploadListFinal = ReadExcel.getAuditDetails(auditLookupBeanList);

					System.out.println("Audit Uploaded data " + auditLookupUploadListFinal);
					List<AuditLookupBean> existedAudit = usermanagementDao.getExistedAudit(auditLookupUploadListFinal);
					Map<String, Long> userFinalIds = usermanagementDao.insertNewAudit(auditLookupUploadListFinal);
					

					if (failedAuditLookupUploadList != null && failedAuditLookupUploadList.size() > 0) {
						failedAuditLookupUploadListFinal.addAll(failedAuditLookupUploadList);
					}

					if (existedAudit != null && existedAudit.size() > 0) {
						failedAuditLookupUploadListFinal.addAll(existedAudit);
					}

					if (failedAuditLookupUploadListFinal != null && failedAuditLookupUploadListFinal.size() > 0) {
						failedAuditLookupUploadListFinalSet.addAll(failedAuditLookupUploadListFinal);
						failedAuditLookupUploadListFinal = new ArrayList<>();
						failedAuditLookupUploadListFinal.addAll(failedAuditLookupUploadListFinalSet);
					}

					System.out.println("failed records list : " + failedAuditLookupUploadListFinal);
					
					
					String url = request.getContextPath();
					UserBean userBean = (UserBean) request.getSession().getAttribute("loginBean");
					if (userFinalIds != null && userFinalIds.size() > 0) {

						registerService.sendUploadedEmailForAudit(mailSender, userBean, auditLookupBeanList,
								failedAuditLookupUploadListFinal);
				
						auditHistoryBean = loginAuditHistoryUtil.auditHistoryBean(0, userBean.getId(),
								"Uploaded Multiple Users", "created", "");
						auditHistoryService.insertAuditHistory(auditHistoryBean);
						request.getSession().setAttribute("unregistered", "Registered Successfully");

						model = new ModelAndView("redirect:usermanagement");
					} else {
						registerService.sendUploadedEmailForAudit(mailSender, userBean, auditLookupBeanList,
								failedAuditLookupUploadListFinal);
						System.out.println("User Successful" + userBean.getName());
						request.getSession().setAttribute("unregistered", "Users Existed");
						model = new ModelAndView("redirect:usermanagement");
					}

				
                }
			} else {
				request.getSession().setAttribute("unregistered", "Failed to Upload ");
				model = new ModelAndView("redirect:usermanagement");
			}
		} catch (Exception e) {
			System.out.println("Exception " + e);
		}
		return model;
	}

	@RequestMapping(value="/editUser",method=RequestMethod.POST)
	public String editUserAdmin(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("editUserBean")UserBean userBean)
	{
		ModelAndView model= null;
		AuditHistoryUtil loginAuditHistoryUtil = new AuditHistoryUtil(); 
		AuditHistoryBean auditHistoryBean = new AuditHistoryBean();
		try
		{
			Integer auditAdminUserId = (Integer) request.getSession().getAttribute("userId");
			if(!userBean.getFirstName().isEmpty()){
				request.getSession().setAttribute("firstname", userBean.getFirstName());
			}
			if(!userBean.getOld_firstName().equalsIgnoreCase(userBean.getFirstName())){
				auditHistoryBean=loginAuditHistoryUtil.auditHistoryBean(userBean.getId(), auditAdminUserId, "Updated User first name", userBean.getOld_firstName(), userBean.getFirstName());
				auditHistoryService.insertAuditHistory(auditHistoryBean);
			}
			
			if(!userBean.getOld_lastName().equalsIgnoreCase(userBean.getLastName())){
				auditHistoryBean=loginAuditHistoryUtil.auditHistoryBean(userBean.getId(), auditAdminUserId, "Updated User lastname", userBean.getOld_lastName(), userBean.getLastName());
				auditHistoryService.insertAuditHistory(auditHistoryBean);
			}
			
			if(!userBean.getOld_mobile().equalsIgnoreCase(userBean.getMobile())){
				auditHistoryBean=loginAuditHistoryUtil.auditHistoryBean(userBean.getId(), auditAdminUserId, "Updated User mobile", userBean.getOld_mobile(), userBean.getMobile());
				auditHistoryService.insertAuditHistory(auditHistoryBean);
			}
			
			if(Integer.parseInt(userBean.getOld_userRole())!=userBean.getUser_role()){
				auditHistoryBean=loginAuditHistoryUtil.auditHistoryBean(userBean.getId(), auditAdminUserId, "Updated User role", userBean.getOld_userRole(), new Integer(userBean.getUser_role()).toString());
				auditHistoryService.insertAuditHistory(auditHistoryBean);
			}
			
			if(Integer.parseInt(userBean.getOld_userStatus())!=userBean.getUser_status()){
				auditHistoryBean=loginAuditHistoryUtil.auditHistoryBean(userBean.getId(), auditAdminUserId,"Updated User status", userBean.getOld_userStatus(), new Integer(userBean.getUser_status()).toString());
				auditHistoryService.insertAuditHistory(auditHistoryBean);
			}
			System.out.println("userOld firstname"+userBean.getOld_firstName());
			System.out.println("userOld lastName"+userBean.getOld_lastName());
			System.out.println("Remove store"+userBean.getRemoveStoreName());
			List<UserCircleBean> userCircleMapList = new ArrayList<UserCircleBean>();
			if(userBean != null && userBean.getUserCircle().size()>0)
			{

				for(String circleId:userBean.getUserCircle()){
					UserCircleBean userCircleMap = new UserCircleBean();
					userCircleMap.setUserid(userBean.getId());
					userCircleMap.setCircleid(Integer.parseInt(circleId));
					userCircleMapList.add(userCircleMap);
				}
			
			}
			if(userBean.getRemoveCircleName()!=null && userBean.getRemoveCircleName().size()>0){
				List <Store> storeList=registerService.removeUserCircleMapping(userBean.getRemoveCircleName(),userBean.getId());
				List<String> storeNameList=new ArrayList<>();
				for(Store s:storeList)
				{
					storeNameList.add(String.valueOf(s.getId()));
				}
				userBean.getRemoveStoreName().addAll(storeNameList);
				
			}
			
			if(userCircleMapList.size()>0){
				registerService.userCircleMapping(userCircleMapList);
			}
			List<UserStoreMap> userStoreMapList = new ArrayList<UserStoreMap>();
			if(userBean != null && userBean.getStoreName().size()>0){
				for(String storeId:userBean.getStoreName()){
					UserStoreMap userStoreMap = new UserStoreMap();
					userStoreMap.setUserid(userBean.getId());
					userStoreMap.setStoreid(Integer.parseInt(storeId));
					userStoreMapList.add(userStoreMap);
				}
			}
			if(userBean.getRemoveStoreName()!=null && userBean.getRemoveStoreName().size()>0){
				registerService.removeUserStoreMapping(userBean.getRemoveStoreName(),userBean.getId());
			}
			
			if(userStoreMapList.size()>0){
				registerService.userStoreMapping(userStoreMapList);
			}
			//UPDATE `vowmeeasymp`.`vowmee_user_store_mapping` SET `storeid`='1' WHERE `id`='17';
			//UPDATE `vowmeeasymp`.`vowmee_user_store_mapping` SET `storeid`='10' WHERE `id`='18';
			
			usermanagementDao.updateUser(userBean);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		return "redirect:usermanagement";
	}
	@RequestMapping(value="/editStore",method=RequestMethod.POST)
	public String editStore(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("editStoreBean")StoreBean storeBean)
	{
		ModelAndView model= null;
		AuditHistoryUtil loginAuditHistoryUtil = new AuditHistoryUtil(); 
		AuditHistoryBean auditHistoryBean = new AuditHistoryBean();
		try
		{
			StoreBean oldStoreBean= usermanagementDao.getStoreforStoreCode(storeBean.getStore_code()).get(0);
			Integer auditAdminUserId = (Integer) request.getSession().getAttribute("userId");
			if(!storeBean.getStore_code().isEmpty()){
				request.getSession().setAttribute("Storename", storeBean.getStore_name());
			}
			if(!oldStoreBean.getStore_name().equalsIgnoreCase(storeBean.getStore_name())){
				auditHistoryBean=loginAuditHistoryUtil.auditHistoryBean(storeBean.getId(), auditAdminUserId, "Updated Store name", oldStoreBean.getStore_name(), storeBean.getStore_name());
				auditHistoryService.insertAuditHistory(auditHistoryBean);
			}
			
			if(!String.valueOf(oldStoreBean.getCircle()).equalsIgnoreCase(storeBean.getCircle_name())){
				auditHistoryBean=loginAuditHistoryUtil.auditHistoryBean(storeBean.getId(), auditAdminUserId, "Updated Store Circle", oldStoreBean.getCircle_name(), storeBean.getCircle_name());
				auditHistoryService.insertAuditHistory(auditHistoryBean);
			}
			
			
			/*System.out.println("userOld firstname"+userBean.getOld_firstName());
			System.out.println("userOld lastName"+userBean.getOld_lastName());
			System.out.println("Remove userName"+userBean.getRemoveStoreName());*/
			
			//UPDATE `vowmeeasymp`.`vowmee_user_store_mapping` SET `storeid`='1' WHERE `id`='17';
			//UPDATE `vowmeeasymp`.`vowmee_user_store_mapping` SET `storeid`='10' WHERE `id`='18';
			
			usermanagementDao.updateStore(storeBean);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		return "redirect:usermanagement";
	}
	@RequestMapping(value="/editProfileUser",method=RequestMethod.POST)
	public String editProfile(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("editUserBean")UserBean userBean)
	{
		ModelAndView model= null;
		try
		{
			if(!userBean.getFirstName().isEmpty()){
				request.getSession().setAttribute("firstname", userBean.getFirstName());
			}
			usermanagementDao.updateUserProfile(userBean);
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		return "redirect:usermanagement";
	}
	
	@RequestMapping(value = "/stores", method = RequestMethod.GET)
	public @ResponseBody
	Set<Store> citiesForState(@RequestParam("circle") String circle) {
	//	System.out.println("finding cities for state " + circle);
		Set<Store> storeList =new HashSet<>();
		try{
			
			String [] arr=circle.split(",");
			for(int i=0;i<arr.length;i++)
			{
				List<Store> storeUserList=new ArrayList<>();
				storeUserList = usermanagementDao.findStoreForCircle(arr[i]);
				storeList.addAll(storeUserList);
			}
			
		}catch(Exception e){ 
			e.printStackTrace();
		}
		
		return storeList;
	}
	
	/*@RequestMapping(value = "/storesForCircle", method = RequestMethod.GET)
	public @ResponseBody
	Set<Store> storesForCircle(@RequestParam("circle") List<CircleBean> circle) {
	//	System.out.println("finding cities for state " + circle);
		Set<Store> storeList =new HashSet<>();
		try{
			
			
			for(int i=0;i<circle.size();i++)
			{
				List<Store> storeUserList=new ArrayList<>();
				storeUserList = usermanagementDao.findStoreForCircle(circle.get(i).getCircle_name());
				storeList.addAll(storeUserList);
			}
			
		}catch(Exception e){ 
			e.printStackTrace();
		}
		
		return storeList;
	}*/
	@RequestMapping(value = "/circleList", method = RequestMethod.GET)
	public @ResponseBody List<CircleBean> getCircle()
	{
		List<CircleBean> circleList = registerService.getCircleList();
		return circleList;
	}
	
	@RequestMapping(value = "/getAuditQuestion", method = RequestMethod.GET)
    public @ResponseBody List<AuditLookupBean> getTime(@RequestParam("auditCat") String auditCat,HttpServletRequest request, HttpServletResponse response) {
		List<AuditLookupBean>questionList=auditService.getAuditQuestions(auditCat);
        return questionList;
		
    }

	@RequestMapping(value = "/CircleUserId", method = RequestMethod.GET)
	public @ResponseBody
	Set<CircleBean> CircleListBasedUser(@RequestParam(value = "userId", required = true) String userId) {
		//System.out.println("finding cities for state " + userId);
		Set<CircleBean> circleSet = new HashSet<>();
		try{
			List<CircleBean> storeList = usermanagementDao.findCircleForUserId(userId);
			circleSet.addAll(storeList);
		}catch(Exception e){ 
			e.printStackTrace();
		}
		
		return circleSet;
	}
	
	
	@RequestMapping(value = "/storesUserId", method = RequestMethod.GET)
	public @ResponseBody
	Set<Store> StoreListBasedUser(@RequestParam(value = "userId", required = true) String userId) {
		//System.out.println("finding cities for state " + userId);
		Set<Store> storeSet = new HashSet<>();
		try{
			List<Store> storeList = usermanagementDao.findStoreForUserId(userId);
			storeSet.addAll(storeList);
		}catch(Exception e){ 
			e.printStackTrace();
		}
		
		return storeSet;
	}
	
	@RequestMapping(value = "/getAllCircle", method = RequestMethod.GET)
	public @ResponseBody
	Set<CircleBean> getAllCircle() {
		//System.out.println("finding cities for state " + userId);
		Set<CircleBean> circleSet = new HashSet<>();
		try{
			List<CircleBean> circleList = usermanagementDao.selectAllCircle();
			circleSet.addAll(circleList);
		}catch(Exception e){ 
			e.printStackTrace();
		}
		
		return circleSet;
	}
	
	@RequestMapping(value = "/getCircle", method = RequestMethod.GET)
	public @ResponseBody
	Set<CircleBean> getCircleListBasedUser(@RequestParam(value = "userId", required = true) String userId) {
		//System.out.println("finding cities for state " + userId);
		Set<CircleBean> circleSet = new HashSet<>();
		try{
			List<CircleBean> circleList = usermanagementDao.findCircleForUserId(userId);
			circleSet.addAll(circleList);
		}catch(Exception e){ 
			e.printStackTrace();
		}
		
		return circleSet;
	}
	
	

	@RequestMapping(value="/filterMonth",method=RequestMethod.POST)
	public ModelAndView filterMonth(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("editUserBean")UserBean selectTimePeriod)
	{
		ModelAndView model= null;
		try
		{
			String admin = (String) request.getSession().getAttribute("admin");
			String superAdmin = (String) request.getSession().getAttribute("superAdmin");
			 StoreBean storeBean=new StoreBean();
			 AuditLookupBean auditLookUpBean=new AuditLookupBean();
			List<UserBean> roleList = registerService.getRoleList();
			List<UserBean> typeList = registerService.getTypeList();
			List<UserBean> statusList = registerService.getStatusList();
			List<CircleBean> circleList = registerService.getCircleList();
			List<UserBean> userDetails = new ArrayList<UserBean>();
			UserBean userBean1 = (UserBean) request.getSession().getAttribute("loginBean");
			 Map< Integer, String > roles = new HashMap<Integer, String>(); 
			 Map< Integer, String > types = new HashMap<Integer, String>(); 
			 Map< Integer, String > statuses = new HashMap<Integer, String>(); 
			 Map< Integer, String > circles = new HashMap<Integer, String>(); 
			
			 for(UserBean type:typeList){
				 types.put(type.getUser_type(), type.getUserType());
				
			 }
			 for(UserBean status:statusList){
				 
				 statuses.put(status.getUser_status(), status.getUserStatus());
			 }
			 for(CircleBean circle:circleList){
				 circles.put(circle.getId(), circle.getCircle_name());
			 }
			
				if(admin=="admin"){
					
					 for(UserBean role:roleList){
						 if(!role.getUserRole().equalsIgnoreCase("admin")&& !role.getUserRole().equalsIgnoreCase("superAdmin")){
							 roles.put(role.getUser_role(), role.getUserRole());
						 }
						 
					 }
					model = new ModelAndView("manageUser");
					if(selectTimePeriod!=null && selectTimePeriod.getFilterTimeForSelectUser().equalsIgnoreCase("Last 7 Days")){
						userDetails = usermanagementDao.getUserListFor1Week(userBean1.getId(),7);
					}else if(selectTimePeriod!=null && selectTimePeriod.getFilterTimeForSelectUser().equalsIgnoreCase("Last 1 month")){
						userDetails= usermanagementDao.getUserListForMonth(userBean1.getId(),1);
					}else if(selectTimePeriod!=null && selectTimePeriod.getFilterTimeForSelectUser().equalsIgnoreCase("Last 2 months")){
						userDetails = usermanagementDao.getUserListForMonth(userBean1.getId(),2);
					}else{
						userDetails = usermanagementDao.getUserList(userBean1.getId());
					}
					model.addObject("userDetails", userDetails);
					model.addObject("roleList", roles);
					model.addObject("typeList", types);
					model.addObject("statusList", statuses);
					model.addObject("circleList", circles);
					model.addObject("editUserBean", selectTimePeriod);
					model.addObject("selectTimePeriod", selectTimePeriod);
					model.addObject("editStoreBean", storeBean);
					model.addObject("editAuditBean", auditLookUpBean);
				}else if(superAdmin=="superAdmin"){
					
					 for(UserBean role:roleList){
						 if(!role.getUserRole().equalsIgnoreCase("superAdmin")){
							 roles.put(role.getUser_role(), role.getUserRole());
						 }
						 
					 }
					model = new ModelAndView("manageUser");
					if(selectTimePeriod!=null && selectTimePeriod.getFilterTimeForSelectUser().equalsIgnoreCase("Last 7 Days")){
						userDetails = usermanagementDao.getAdminUserListFor1Week(userBean1.getId(),7);
					}else if(selectTimePeriod!=null && selectTimePeriod.getFilterTimeForSelectUser().equalsIgnoreCase("Last 1 month")){
						userDetails= usermanagementDao.getAdminUserListForMonth(userBean1.getId(),1);
					}else if(selectTimePeriod!=null && selectTimePeriod.getFilterTimeForSelectUser().equalsIgnoreCase("Last 2 months")){
						userDetails = usermanagementDao.getAdminUserListForMonth(userBean1.getId(),2);
					}else{
						userDetails = usermanagementDao.getAdminUserList(userBean1.getId());
					}
					model.addObject("userDetails",userDetails);
					model.addObject("roleList", roles);
					model.addObject("typeList", types);
					model.addObject("statusList", statuses);
					model.addObject("circleList", circles);
					model.addObject("editUserBean", selectTimePeriod);
					model.addObject("selectTimePeriod", selectTimePeriod);
					model.addObject("editStoreBean", storeBean);
					model.addObject("editAuditBean", auditLookUpBean);
				}
		}catch(Exception e){
			e.printStackTrace();
		}
				return model;
	}
	
}
