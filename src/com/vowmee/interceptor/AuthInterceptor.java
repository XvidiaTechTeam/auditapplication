package com.vowmee.interceptor;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.vowmee.bean.UserBean;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.logging.Logger;

public class AuthInterceptor implements HandlerInterceptor {

    private static final Logger logger = Logger.getLogger("AuthInterceptor");

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
        logger.info(" Pre handle "+ httpServletRequest.getServletPath());
        logger.info(" Pre handle "+ (httpServletRequest.getServletPath().equals("/login")));
        UserBean user=(UserBean) httpServletRequest.getSession().getAttribute("loginBean");
        if (httpServletRequest.getServletPath().equals("/login")) { // BEWARE : to be adapted to your actual login page
        	 logger.info(" Pre handle "+ (httpServletRequest.getServletPath() == "/login"));
        	 if(user != null)
             {
                 System.err.println("Request Path : ");
                 httpServletResponse.sendRedirect("homePage");
                 return false;
             }
             else
             {
                 return true;
             }
        	
        }else{
        	
             System.out.println("user bean "+user);
             if(user == null)
             {
                 System.err.println("Request Path : ");
                 httpServletResponse.sendRedirect("login");
                 return false;
             }
             else
             {
                 return true;
             }
        }

       
       /* if(httpServletRequest.getMethod().equals("GET"))
            return true;
        else
            return false;*/
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {
        logger.info(" Post handle ");
      /*  if(modelAndView.getModelMap().containsKey("status")){
            String status = (String) modelAndView.getModelMap().get("status");
            if(status.equals("SUCCESS!")){
                status = "Authentication " + status;
                modelAndView.getModelMap().put("status",status);
            }
        }*/
    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {
        logger.info(" After Completion ");
    }
}