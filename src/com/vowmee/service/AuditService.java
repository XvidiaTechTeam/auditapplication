package com.vowmee.service;

/*import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.vowmee.bean.AuditBean;
import com.vowmee.bean.AuditLookupBean;
import com.vowmee.bean.AuditMaster;
import com.vowmee.bean.ReportsBean;
import com.vowmee.bean.ScoreCardBO;
import com.vowmee.bean.Store;
import com.vowmee.dao.AuditDAO;*/
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.activation.DataSource;

import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.google.gson.Gson;
import com.sun.istack.internal.ByteArrayDataSource;
import com.vowmee.bean.AuditBean;
import com.vowmee.bean.AuditLookupBean;
import com.vowmee.bean.AuditMaster;
import com.vowmee.bean.CompletedAuditsVO;
import com.vowmee.bean.ReportsBean;
import com.vowmee.bean.ScoreCardBO;
import com.vowmee.bean.Store;
import com.vowmee.bean.StoreBean;
import com.vowmee.bean.UserBean;
import com.vowmee.bean.UserPreviousAuditHytory;
import com.vowmee.dao.AuditDAO;

public class AuditService {

	AuditDAO auditDao;
	private Map<String, String> auditCategoryMap;
	
/*	public synchronized void load(InputStream inStream) throws IOException{
		
	}*/

	public AuditDAO getAuditDao() {
		return auditDao;
	}
	
	public List<Integer> getAuditLookUpCount() {
		return auditDao.getAuditLookUpCount();
	}

	public List<Integer> getMasterLookUpCount(long auditMasterId) {
		return auditDao.getMasterLookUpCount(auditMasterId);
	}
	public List<Integer> getMasterLookUpCountForOptional(long auditMasterId) {
		List<String>categoryList=new ArrayList<>();
		int count=0;
		List<Integer>optionalCount=new ArrayList<>();
		List<Integer>categoryCount=new ArrayList<>();
		List<Integer>auditCount=new ArrayList<>();
		
		categoryList=auditDao.getMasterLookUpCountForOptional(auditMasterId);
		for(String category:categoryList)
		{
			if(category.contains("Process Scenarios") && !category.contains("Product and process knowledge"))
			{
               			categoryCount=auditDao.getOptionLookUpCountforCategory(category);
               			auditCount=auditDao.getOptionAuditCountforCategory(category,auditMasterId);
               			if(categoryCount.get(0)==auditCount.get(0))
               				count++;
               			
			}
				
			
		}
		optionalCount.add(count);
		return optionalCount ;
	}
	public Map<String, String> getAuditCategoryMap() {
		return auditCategoryMap;
	}
	public void setAuditCategoryMap(Map<String, String> auditCategoryMap) {
		this.auditCategoryMap = auditCategoryMap;
	}

	public void setAuditDao(AuditDAO auditDao) {
		this.auditDao = auditDao;
	}
	
	public List<String> getStoreList(){
		List<String> storeList= null;
		try{
			storeList = auditDao.getStoreList();
		}catch(Exception e){
			e.printStackTrace();
		}
		return storeList;
	}
	
	public List<String> getStoreListById(StringBuilder buildId){
		List<String> auditList=null;
		try{
			auditList = auditDao.getStoreListById(buildId);
		}catch(Exception e){
			e.printStackTrace();
		}
		return auditList;
	}
	
	
	public List<Store> selectStoreById(StringBuilder buildId){
		List<Store> auditList=null;
		try{
			auditList = auditDao.selectStoreById(buildId);
		}catch(Exception e){
			e.printStackTrace();
		}
		return auditList;
	}
	
	public List<String> getStoreIdList(int userId){
		List<String> storeList= null;
		try{
			storeList = auditDao.getStoreIdList(userId);
		}catch(Exception e){
			e.printStackTrace();
		}
		return storeList;
	}
	public int getUserId(String email){
		int userId = 0;
		try{
			userId = auditDao.getUserId(email);
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		
		return userId;
	}
	
	public List<Store> selectStoreReportById(int buildId){
		List<Store> auditList=null;
		try{
			auditList = auditDao.selectStoreReportById(buildId);
		}catch(Exception e){
			e.printStackTrace();
		}
		return auditList;
	}
	
	/*public List<String> getAuditCategory(){
		List<String> auditCategoryList= null;
		try{
			auditCategoryList = auditDao.getAuditCategory();
		}catch(Exception e){
			e.printStackTrace();
		}
		return auditCategoryList;
	}
	*/

	public List<String> getAuditCategory(long auditMasterId){
		List<String> auditCategoryList= null;
		try{
			auditCategoryList = auditDao.getAuditCategory();
			Map<String, String> categoryMap = auditDao.getAuditCatergoryStatus(auditMasterId); 
			setAuditCategoryMap(categoryMap);
		}catch(Exception e){
			e.printStackTrace();
		}
		return auditCategoryList;
	}

	public int insertAuditData(String json, long auditMasterId) {

		AuditBean auditBean = new Gson().fromJson(json, AuditBean.class);
		boolean existedAuditBean = auditDao.getExistedAudit(auditMasterId, auditBean.getAuditLookup());
		String query = null;
		int val = 0;
		if (existedAuditBean) {
			if (!auditBean.getAuditAnswer().equalsIgnoreCase("N/A")) {
				query = "INSERT INTO `vowmeeasymp`.`vowmee_audit_detail`(`audit`,`audit_lkp`,`audit_value`)VALUES("
						+ auditMasterId + "," + auditBean.getAuditLookup() + "," + auditBean.isAuditValue() + ")";
				val = auditDao.insertAuditData(query);
			}else{
				query = "INSERT INTO `vowmeeasymp`.`vowmee_audit_detail`(`audit`,`audit_lkp`,`audit_value`)VALUES("
						+ auditMasterId + "," + auditBean.getAuditLookup() + ",'-1')";
				val = auditDao.insertAuditData(query);
			}

		} else {
			if (auditBean.getAuditAnswer().equalsIgnoreCase("N/A")) {
				/*query = "DELETE FROM vowmeeasymp.vowmee_audit_detail where audit='" + auditMasterId
						+ "' and audit_lkp = '" + auditBean.getAuditLookup() + "'";*/
				query = "update vowmeeasymp.vowmee_audit_detail set audit_value='-1'"
				+ " where audit='" + auditMasterId + "' and audit_lkp = '" + auditBean.getAuditLookup() + "'";
				auditDao.insertAuditData(query);
			} else {
				query = "update vowmeeasymp.vowmee_audit_detail set audit_value=" + auditBean.isAuditValue()
						+ " where audit='" + auditMasterId + "' and audit_lkp = '" + auditBean.getAuditLookup() + "'";
				val = auditDao.insertAuditData(query);
			}

		}

		System.out.println("existedAudit : " + existedAuditBean);
		return val;
	}
	public List<AuditLookupBean> selectAllAuditName() {
		List<AuditLookupBean> auditLookupList = new ArrayList<AuditLookupBean>();
		try{
			
			auditLookupList = auditDao.selectAllAuditName();
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		return auditLookupList;
	}

	
	public List<AuditLookupBean> getAuditQuestions(String auditCategory) {
		List<AuditLookupBean> auditLookupList = new ArrayList<AuditLookupBean>();
		try{
			
			auditLookupList = auditDao.getAuditQuestions(auditCategory);
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		return auditLookupList;
	}
	public long insertAuditMaster(String storeName,int id) {
		String storeNameUnique = storeName+"_"+new Date();
	//	AuditBean auditBean = new Gson().fromJson(json, AuditBean.class);
	//	String query = "INSERT INTO `vowmeeasymp`.`vowmee_audit_detail`(`audit`,`audit_lkp`,`audit_value`)VALUES(1,"+auditBean.getAuditLookup()+","+auditBean.isAuditValue()+")";
		int storeId=getStoreIdByName(storeName);
		long val = auditDao.insertAuditMaster(storeNameUnique,id,storeId);
		return val;
	}
	
	public int getStoreIdByName(String storeName){
		int id = auditDao.getStoreIdByName(storeName);
		return id;
		 
	}
	
	public String getStoreNameById(int storeId){
		String storeName = auditDao.getStoreNameById(storeId);
		return storeName;
		 
	}
	
	public void updateAuditStatus(long audit_name,String agentName, String agentEmail, String agentMobile) {
		try{
			auditDao.updateAuditStatus(audit_name,agentName,agentEmail,agentMobile);
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}
	public List<AuditBean> selectAuditPreview(long auditId, String auditCategory) {
		List<AuditBean> auditPreviewList = new ArrayList<AuditBean>();
		try{
			auditPreviewList = auditDao.selectAuditPreview(auditId, auditCategory);
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		return auditPreviewList;
	}
	
	public List<ScoreCardBO> scoreCardCalculation(long auditMasterId) {
		List<ScoreCardBO> scoreCardList = new ArrayList<ScoreCardBO>();
		try{
			scoreCardList = auditDao.scoreCardCalculation(auditMasterId);
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		return scoreCardList;
	}
	public List<String> getAuditPreviewList(long auditId) {
		List<String> auditPreviewList = new ArrayList<String>();
		try{
			auditPreviewList = auditDao.getAuditPreviewList(auditId);
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		return auditPreviewList;
	}
	
	public List<String> getScorePercentage(long storeId) {
		List<String> auditPreviewList = new ArrayList<String>();
		try{
			auditPreviewList = auditDao.getScorePercentage(storeId);
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		return auditPreviewList;
	}

	public List<AuditLookupBean> getAuditQuestionsReplace(String category) {
		List<AuditLookupBean> auditLookupList = new ArrayList<AuditLookupBean>();
		try{
			
			auditLookupList = auditDao.getAuditQuestionsReplace(category);
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		return auditLookupList;
	}

	public List<AuditBean> selectAuditPreviewList(long auditMasterId, String previewCategory) {
		List<AuditBean> auditPreviewList = new ArrayList<AuditBean>();
		try{
			auditPreviewList = auditDao.selectAuditPreviewList(auditMasterId, previewCategory);
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		return auditPreviewList;
	}

	public Map<String, List<AuditBean>> selectAllAuditPreviewList(long auditMasterId) {
		Map<String,List<AuditBean>> auditPreviewList = new LinkedHashMap<String,List<AuditBean>>();
		List<AuditBean> auditList = new ArrayList<AuditBean>();
		List<AuditBean> auditCategoryList = new ArrayList<AuditBean>();
		List<String> auditStringCategory = new ArrayList<String>();
		try{
			auditList = auditDao.selectAllAuditPreviewList(auditMasterId);
			auditStringCategory = auditDao.getAuditPreviewList(auditMasterId);
			for(String category : auditStringCategory){
				AuditBean audit = new AuditBean();
				audit.setAuditCategory(category);
				auditCategoryList.add(audit);
			}
			auditPreviewList.put("auditList", auditList);
			auditPreviewList.put("auditCategoryList", auditCategoryList);
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		
		return auditPreviewList;
	}
	public List<AuditMaster> getIncompletedCount(StringBuilder stores, long auditId) {
		List<AuditMaster> auditPreviewList = new ArrayList<AuditMaster>();
		try{
			auditPreviewList = auditDao.getIncompletedCount(stores, (int)auditId);
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		return auditPreviewList;
	}
	
	public void insertScoreCardCal(List<ScoreCardBO> scoreCardList, int storeId) {
		try{
			auditDao.insertScoreCardCal(scoreCardList, storeId);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public List<ReportsBean> scoreCardList(ScoreCardBO scoreBo) throws Exception {
		List<ReportsBean> scoreCardList = new ArrayList<ReportsBean>();
		try{
			scoreCardList = auditDao.scoreCardReportList(scoreBo);
			Map<Integer, List<Float>> overallPercentage=new LinkedHashMap<Integer, List<Float>> ();
			List<Float> percentageList = null;
			for (ReportsBean reportsBean : scoreCardList) {
				if(overallPercentage.containsKey(reportsBean.getAudit_month())){
					percentageList.add(reportsBean.getAudit_percentage());
					overallPercentage.put(reportsBean.getAudit_month(), percentageList);
				}else{
					percentageList = new ArrayList<Float>();
					percentageList.add(reportsBean.getAudit_percentage());
					overallPercentage.put(reportsBean.getAudit_month(),percentageList);
				}
			}
			ReportsBean bean = null;
			for(Map.Entry<Integer, List<Float>> entry:overallPercentage.entrySet()){
				bean = new ReportsBean();
				bean.setAudit_category("Overall Score");
				bean.setAudit_month(entry.getKey());
				bean.setAudit_percentage(calculateAverage(entry.getValue()));
				scoreCardList.add(bean);
			}
			
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		return scoreCardList;
	}
	
	private float calculateAverage(List <Float> percentage) {
		  float sum = 0f;
		  if(!percentage.isEmpty()) {
		    for (float mark : percentage) {
		        sum += mark;
		    }
		    return sum / percentage.size();
		  }
		  return sum;
		}
	
	public List<AuditBean> getAuditDetailsToExport(ScoreCardBO scoreBo) throws Exception {
		List<AuditBean> scoreCardList = new ArrayList<AuditBean>();
		try{
			scoreCardList = auditDao.getAuditDetailsToExport(scoreBo);
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		return scoreCardList;
	}
	
	/*public DataSource getAuditDataSource(List<ScoreCardBO> scoreCardList) throws IOException {
		Workbook xlsFile = new HSSFWorkbook(); // create a workbook
		CreationHelper helper = xlsFile.getCreationHelper();
		Sheet sheet = xlsFile.createSheet("Audit"); 
		Row row = sheet.createRow(0);// add a sheet to your workbook
		row.createCell(0).setCellValue("Category");
        row.createCell(1).setCellValue("Percentage");
		int rowIndex = 1;
		float scorePercentage = 0;
		for(ScoreCardBO scoreCardBO :scoreCardList ){
			row = sheet.createRow(rowIndex++);
	        row.createCell(0).setCellValue(scoreCardBO.getAudit_category());
	        row.createCell(1).setCellValue(scoreCardBO.getPercentage());
	        scorePercentage = scorePercentage+Float.valueOf(scoreCardBO.getPercentage());
	        System.out.println("getAuditDataSource : " + row);
		}
		row = sheet.createRow(rowIndex++);
		row.createCell(0).setCellValue("Average Audit Score");
		row.createCell(1).setCellValue((scorePercentage/scoreCardList.size()));
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		xlsFile.write(bos); // write excel data to a byte array
		bos.close();

		// Now use your ByteArrayDataSource as
		DataSource fds = new ByteArrayDataSource(bos.toByteArray(), "application/vnd.ms-excel");
		return fds;
	}*/
	
	public DataSource getAuditDataSource(List<ScoreCardBO> scoreCardList, List<AuditBean> auditValueList) throws IOException {
		Workbook xlsFile = new HSSFWorkbook(); // create a workbook
		CreationHelper helper = xlsFile.getCreationHelper();
		 // create style for header cells
        CellStyle style = xlsFile.createCellStyle();
        Font font = xlsFile.createFont();
        font.setFontName("Arial");
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        font.setColor(HSSFColor.WHITE.index);
        style.setFont(font);
        int initialsize = 1;
		Sheet sheet = xlsFile.createSheet("Audit"); 
		sheet.setDefaultColumnWidth(30);
		Row row = sheet.createRow(initialsize);// add a sheet to your workbook
		row.createCell(0).setCellValue("Category");
		row.getCell(0).setCellStyle(style);
		
        row.createCell(1).setCellValue("Audit Question");
        row.getCell(1).setCellStyle(style);
        
        row.createCell(2).setCellValue("FST");
        row.getCell(2).setCellStyle(style);
        
		int rowIndex = initialsize+1;
		float scorePercentage = 0;
		for(AuditBean auditBean : auditValueList){
			row = sheet.createRow(rowIndex++);
	        row.createCell(0).setCellValue(auditBean.getAuditCategory());
	        row.createCell(1).setCellValue(auditBean.getAuditQuestion());
	        row.createCell(2).setCellValue(auditBean.getAuditAnswer());
	        //scorePercentage = scorePercentage+Float.valueOf(scoreCardBO.getPercentage());
	        System.out.println("getAuditDataSource : " + row);
		}
		rowIndex = rowIndex + 5;
		row = sheet.createRow(rowIndex++);
		row.createCell(0).setCellValue("Category");
		row.getCell(0).setCellStyle(style);
		
        row.createCell(1).setCellValue("FST");
        row.getCell(1).setCellStyle(style);
        
        row.createCell(2).setCellValue("Weightage");
        row.getCell(2).setCellStyle(style);
        
		for(ScoreCardBO scoreCardBO :scoreCardList ){
				row = sheet.createRow(rowIndex++);
					 	row.createCell(0).setCellValue(scoreCardBO.getAudit_category());
				        row.createCell(1).setCellValue(scoreCardBO.getPercentage());
				        row.createCell(2).setCellValue(scoreCardBO.getWeitage_percentage());
				        //row.createCell(3).setCellValue(categoryPercentage * weightagePercenatage);
				        scorePercentage = scorePercentage+Float.valueOf(scoreCardBO.getPercentage());
				        System.out.println("getAuditDataSource : " + row);
		}
		row = sheet.createRow(rowIndex++);
		row.createCell(0).setCellValue("Average Audit Score");
		row.createCell(1).setCellValue((scorePercentage/scoreCardList.size()));
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		xlsFile.write(bos); // write excel data to a byte array
		bos.close();

		// Now use your ByteArrayDataSource as
		DataSource fds = new ByteArrayDataSource(bos.toByteArray(), "application/vnd.ms-excel");
		return fds;
	}
	
	public List<AuditBean> getAuditValue(long auditMasterId) {
		List<AuditBean> auditValueList = new ArrayList<AuditBean>();
		auditValueList = auditDao.getAuditValue(auditMasterId);
		return auditValueList;
	}

	public long selectAuditId(int storeId) {
		long auditId = auditDao.selectAuditId(storeId);
		return auditId;
	}
	public List<CompletedAuditsVO> getCompletedAuditDetails(int userId) {
		// TODO Auto-generated method stub
		return auditDao.getCompletedAuditDetails(userId);
	}
	public List<UserPreviousAuditHytory> getUserAuditDetails(String userId) {
		// TODO Auto-generated method stub
		return auditDao.getUserAuditDetails(userId);
	}
	public List<ScoreCardBO> getCircleList(){
		List<ScoreCardBO> circleList= null;
		try{
			circleList = auditDao.getCircleList();
		}catch(Exception e){
			e.printStackTrace();
		}
		return circleList;
	}
	public List<ScoreCardBO> getCircleListForUser(int userId){
		List<ScoreCardBO> circleList= null;
		try{
			circleList = auditDao.getCircleListforUser(userId);
		}catch(Exception e){
			e.printStackTrace();
		}
		return circleList;
	}
	public List<String> getCategoryList(){
		List<String> categoryList= null;
		try{
			categoryList = auditDao.getCategoryList();
		}catch(Exception e){
			e.printStackTrace();
		}
		return categoryList;
	}
	public List<ScoreCardBO> getCircleScoreList(int circleId,String month,String category) {
		List<ScoreCardBO> auditCategoryList = null;
		try {
			if(category.equals("Overall Score")){
				auditCategoryList=auditDao.getCircleScoreList(circleId, month);
			}else{
				auditCategoryList=auditDao.getCircleScoreList(circleId, month, category);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return auditCategoryList;
	}
	
	public List<UserBean> getUserListforReport() {
		// TODO Auto-generated method stub
		return auditDao.getUserListforReport();
	}
	public List<UserBean> getUserListforReportStore(List<String> storeList) {
		List<UserBean> userBean=new ArrayList<>();
		List<UserBean> userBeanList=new ArrayList<>();
		for(String store:storeList)
		{
			userBean=auditDao.getUserListforReportStoreWise(store);
			for(UserBean user:userBean)
			{
				userBeanList.add(user);
			}
		}
		return userBeanList;
	}
	
	
	
	public List<ScoreCardBO> getAuditorReport(int auditorId, String month){
		return auditDao.getAuditorReport(auditorId, month);
	}
	
	public List<Store> getStoreObject(){
		List<Store> storeList= null;
		try{
			storeList = auditDao.getStoreObject();
		}catch(Exception e){
			e.printStackTrace();
		}
		return storeList;
	}
	public List<String> getAllCompletedStoreList() {
		return auditDao.getAllCompletedStoreList();
	}
	public List<String> getAllCompletedStoreListByCircle(int userId)
	{
		return auditDao.getStoreIdListComplete(userId);
	}

	public String getAuditorNameById(int user_id) {
		return auditDao.getAuditorNameById(user_id);
		
	}

	public List<ScoreCardBO> getMICOReport(int micoId, String month) {
		return auditDao.getMICOReport(micoId, month);
	}
	
	
	public List<UserBean> getMICOUserList() {
		// TODO Auto-generated method stub
		return auditDao.getMICOUserList();
	}
	
	public List<UserBean> getMICOUserListReport(List<String> storeList) {
		List<UserBean> userBean=new ArrayList<>();
		List<UserBean> userBeanList=new ArrayList<>();
		for(String store:storeList)
		{
			userBean=auditDao.getMICOUserListReport(store);
			for(UserBean user:userBean)
			{
				userBeanList.add(user);
			}
		}
		return userBeanList;
	}
}
