package com.vowmee.service;

import com.vowmee.bean.UserBean;
import com.vowmee.dao.LoginDAO;
import com.vowmee.dao.RegisterDAO;

public class SetPasswordService {
	LoginDAO loginDao;
	RegisterDAO registerDao;

	public LoginDAO getLoginDao() {
		return loginDao;
	}

	public void setLoginDao(LoginDAO loginDao) {
		this.loginDao = loginDao;
	}
	
	/*public void savePassword(String username,String otp, String password){
		try{
			UserBean userBean = loginDao.getUserDetailByName(username);
			System.out.println("userBean "+userBean);
			if(userBean!=null){
				if(userBean.getOtp().equals(otp) && userBean.getUsername().equalsIgnoreCase(username)){
					userBean.setPassword(password);
					loginDao.updatePassword(userBean);
				}
			}
				
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	public int getUserDetailByName(String userName){
		int id=0;
		try{
			id = registerDao.getUserDetailByName(userName);
		}catch(Exception e){
			e.printStackTrace();
		}
		return id;
	}
	
	public int updateSecurityCodeId(int id,String securityCode){
		int updateId=0;
		try{
			updateId = registerDao.updateSecurityCodeId(id,securityCode);
		}catch(Exception e){
			e.printStackTrace();
		}
		return updateId;
	}
	public UserBean getUserDetailById(int id){
		UserBean userBean = null;
		try{
			userBean = registerDao.getUserDetailById(id);
		}catch(Exception e){
			e.printStackTrace();
		}
		return userBean;
	}
	public int updateForgotPassword(UserBean userBean){
		int result=0;
		try{
			result=registerDao.updateForgotPassword(userBean);
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
	
	public int updateRegisterSetPassword(UserBean userBean){
		int result=0;
		try{
			result=registerDao.updateRegisterSetPassword(userBean);
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}

	public RegisterDAO getRegisterDao() {
		return registerDao;
	}

	public void setRegisterDao(RegisterDAO registerDao) {
		this.registerDao = registerDao;
	}
}
