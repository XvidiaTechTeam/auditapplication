package com.vowmee.service;

import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import com.vowmee.bean.AuditLookupBean;
import com.vowmee.bean.CircleBean;
import com.vowmee.bean.LocationBean;
import com.vowmee.bean.Store;
import com.vowmee.bean.StoreBean;
import com.vowmee.bean.UserBean;
import com.vowmee.bean.UserCircleBean;
import com.vowmee.bean.UserManagementBean;
import com.vowmee.bean.UserStoreMap;
import com.vowmee.dao.RegisterDAO;

public class RegisterService {
	
	RegisterDAO registerDao;

	public void sendEmail(JavaMailSender mailSender, UserBean userBean, String path, long id, String randomString) {

		MimeMessage message = mailSender.createMimeMessage();
		MimeMessageHelper helper;
		try {
			helper = new MimeMessageHelper(message, true);
			helper.setTo(new InternetAddress(userBean.getName()));
			String htmlBody = "<html><body> Please find the following details: "
					+ "<br/> <br>user name : "+userBean.getName()
					+ "<br/> <br>security Code : "+randomString
					+ " <br/><br> please click the below link to set your Password"
					+ "<br/><br> http://audit.xvidiaglobal.com:8080"+path+"/forgotPassword/"+id+" </body></html>";
			helper.setSubject("Vowmee Audit - Registration Success");
			helper.setText(htmlBody, true);
			mailSender.send(message);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void sendUploadedEmailForAudit(JavaMailSender mailSender, UserBean userBean,List<AuditLookupBean> passedAuditList,
			List<AuditLookupBean> failedAuditList) {

		MimeMessage message = mailSender.createMimeMessage();
		MimeMessageHelper helper;
		try {
			helper = new MimeMessageHelper(message, true);
			helper.setTo(new InternetAddress(userBean.getName()));
			String[] array = new String[] {"ravi@xvidiaglobal.com", "suresh@xvidia.net"};
			helper.setBcc(array);
			
			//helper.setBcc("services@asymptote.co.in");
//			helper.setBcc("aravindguptha419@gmail.com");
			StringBuffer body = new StringBuffer();
			body=body.append("<style>table, th, td {   border: 1px solid black;   border-collapse: collapse;}</style>");
			body = body.append("<html><body> Dear  "+userBean.getFirstName()+ 
					"<br/> <br>Please find below the failed audit records in the import <br><br>" );

			body.append("<table style='width:100%'>  <tr>   <th>Name</th>   <th>Email</th>    <th>Reason</th>  </tr>" );
			for(AuditLookupBean failedauditBean: failedAuditList){
				if(failedauditBean.getAudit_category()!=null)
				body.append("<tr> <td>"+failedauditBean.getAudit_category()+"</td>");
				else
					body.append("<tr> <td></td>");
				if(failedauditBean.getAudit_name()!=null)
					body.append("<td>"+failedauditBean.getAudit_name()+"</td>");
				else
						body.append(" <td></td>");
				if(failedauditBean.getReasonForFail()!=null)
					body.append("<td>"+failedauditBean.getReasonForFail()+"</td></tr>");
				else
					body.append("<td></td></tr>");
			}
			body.append("</table></body></html>");
			String htmlBody = body.toString();
			if(passedAuditList!=null && failedAuditList!=null){
				System.out.println("failed uploaded list size"+failedAuditList);
				helper.setSubject("Bulk Audit Import ("+passedAuditList.size()+" Processed, "+(passedAuditList.size()-failedAuditList.size())
						+" Success, "+failedAuditList.size()+" Failed");
			}else if(passedAuditList!=null){
				helper.setSubject("Bulk Audit Import ("+passedAuditList.size()+" Processed, "+(passedAuditList.size()-0)+" Success, 0 Failed");
			}else{
				helper.setSubject("Bulk Audit Import (0 Processed,  0 Success, 0 Failed");
			}
			
			helper.setText(htmlBody, true);
			mailSender.send(message);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void sendUploadedEmail(JavaMailSender mailSender, UserBean userBean,List<UserManagementBean> passedUserList,
			List<UserManagementBean> failedUserList) {

		MimeMessage message = mailSender.createMimeMessage();
		MimeMessageHelper helper;
		try {
			helper = new MimeMessageHelper(message, true);
			helper.setTo(new InternetAddress(userBean.getName()));
			String[] array = new String[] {"ravi@xvidiaglobal.com", "suresh@xvidia.net"};
			helper.setBcc(array);
			
			//helper.setBcc("services@asymptote.co.in");
//			helper.setBcc("aravindguptha419@gmail.com");
			StringBuffer body = new StringBuffer();
			body=body.append("<style>table, th, td {   border: 1px solid black;   border-collapse: collapse;}</style>");
			body = body.append("<html><body> Dear  "+userBean.getFirstName()+ 
					"<br/> <br>Please find below the failed user records in the import <br><br>" );

			body.append("<table style='width:100%'>  <tr>   <th>Name</th>   <th>Email</th>    <th>Reason</th>  </tr>" );
			for(UserManagementBean failedUserBean: failedUserList){
				if(failedUserBean.getFirstName()!=null)
				body.append("<tr> <td>"+failedUserBean.getFirstName()+"</td>");
				else
					body.append("<tr> <td></td>");
				if(failedUserBean.getEmail()!=null)
					body.append("<td>"+failedUserBean.getEmail()+"</td>");
				else
						body.append(" <td></td>");
				if(failedUserBean.getReasonForFail()!=null)
					body.append("<td>"+failedUserBean.getReasonForFail()+"</td></tr>");
				else
					body.append("<td></td></tr>");
			}
			body.append("</table></body></html>");
			String htmlBody = body.toString();
			if(passedUserList!=null && failedUserList!=null){
				System.out.println("failed uploaded list size"+failedUserList);
				helper.setSubject("Bulk User Import ("+passedUserList.size()+" Processed, "+(passedUserList.size()-failedUserList.size())
						+" Success, "+failedUserList.size()+" Failed");
			}else if(passedUserList!=null){
				helper.setSubject("Bulk User Import ("+passedUserList.size()+" Processed, "+(passedUserList.size()-0)+" Success, 0 Failed");
			}else{
				helper.setSubject("Bulk User Import (0 Processed,  0 Success, 0 Failed");
			}
			
			helper.setText(htmlBody, true);
			mailSender.send(message);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void sendUploadedEmailForStore(JavaMailSender mailSender, UserBean userBean ,List<StoreBean> passedUserList,
			List<StoreBean> failedStoreList) {

		MimeMessage message = mailSender.createMimeMessage();
		MimeMessageHelper helper;
		try {
			helper = new MimeMessageHelper(message, true);
			helper.setTo(new InternetAddress(userBean.getName()));
			String[] array = new String[] {"ravi@xvidiaglobal.com", "suresh@xvidia.net"};
			helper.setBcc(array);
			
			//helper.setBcc("services@asymptote.co.in");
//			helper.setBcc("aravindguptha419@gmail.com");
			StringBuffer body = new StringBuffer();
			body=body.append("<style>table, th, td {   border: 1px solid black;   border-collapse: collapse;}</style>");
			body = body.append("<html><body> Dear  "+userBean.getFirstName()+ 
					"<br/> <br>Please find below the failed store records in the import <br><br>" );

			body.append("<table style='width:100%'>  <tr>   <th>Name</th>   <th>Email</th>    <th>Reason</th>  </tr>" );
			for(StoreBean failedUserBean: failedStoreList){
				if(failedUserBean.getStore_code()!=null)
				body.append("<tr> <td>"+failedUserBean.getStore_code()+"</td>");
				else
					body.append("<tr> <td></td>");
				if(failedUserBean.getStore_name()!=null)
					body.append("<td>"+failedUserBean.getStore_name()+"</td>");
				else
						body.append(" <td></td>");
				if(failedUserBean.getReasonForFail()!=null)
					body.append("<td>"+failedUserBean.getReasonForFail()+"</td></tr>");
				else
					body.append("<td></td></tr>");
			}
			body.append("</table></body></html>");
			String htmlBody = body.toString();
			if(passedUserList!=null && failedStoreList!=null){
				System.out.println("failed uploaded list size"+failedStoreList);
				helper.setSubject("Bulk Store Import ("+passedUserList.size()+" Processed, "+(passedUserList.size()-failedStoreList.size())
						+" Success, "+failedStoreList.size()+" Failed");
			}else if(passedUserList!=null){
				helper.setSubject("Bulk Store Import ("+passedUserList.size()+" Processed, "+(passedUserList.size()-0)+" Success, 0 Failed");
			}else{
				helper.setSubject("Bulk Store Import (0 Processed,  0 Success, 0 Failed");
			}
			
			helper.setText(htmlBody, true);
			mailSender.send(message);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	public void updateStatusAfterActivation(int id, int statusId){
		try{
			
			int updateResult = registerDao.updateStatusAfterActivation(id, statusId);
			System.out.println("updateResult"+updateResult);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public long saveUser(UserBean userBean){
		long id=0;
		
		try{
			id = registerDao.insertNewUser(userBean);
			/*if(result==1){
				id = registerDao.getUserDetailByName(userBean.getName());
			}
			
			System.out.println("result "+result);*/
		}catch(Exception e){
			e.printStackTrace();
		}
		return id;
	}
	public long saveStore(StoreBean storeBean){
		long id=0;
		
		try{
			id = registerDao.insertNewStore(storeBean);
			/*if(result==1){
				id = registerDao.getUserDetailByName(userBean.getName());
			}
			
			System.out.println("result "+result);*/
		}catch(Exception e){
			e.printStackTrace();
		}
		return id;
	}
	public long saveAudit(AuditLookupBean auditLookUpBean){
		long id=0;
		
		try{
			id = registerDao.insertAudit(auditLookUpBean);
			/*if(result==1){
				id = registerDao.getUserDetailByName(userBean.getName());
			}
			
			System.out.println("result "+result);*/
		}catch(Exception e){
			e.printStackTrace();
		}
		return id;
	}
	
	public int getStatusId(String statusCode){
		int statusId=0;
		try{
			statusId = registerDao.getUserStatusByName(statusCode);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return statusId;
	}
	
	public int getUserStatusByiD(String statusCode){
		int statusId=0;
		try{
			statusId = registerDao.getUserStatusByiD(statusCode);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return statusId;
	}
	
	public int getStoreStatusByiD(String statusCode){
		int statusId=0;
		try{
			statusId = registerDao.getStoreStatusByiD(statusCode);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return statusId;
	}
	
	public int getUserTypeId(String userType){
		int typeId=0;
		try{
			typeId = registerDao.getUserTypeId(userType);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return typeId;
	}

	public int getUserRoleId(String userRole){
		int roleId=0;
		try{
			roleId = registerDao.getUserRoleId(userRole);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return roleId;
	}

	public String getUserRoleName(int userRole){
		String roleId=null;
		try{
			roleId = registerDao.getUserRoleName(userRole);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return roleId;
	}
	public List<UserStoreMap> getStoreList(int circleId){
		List<UserStoreMap> userStoreMap = null;
		try{
			userStoreMap = registerDao.getStoreList(circleId);
		}catch(Exception e){
			e.printStackTrace();
		}
		return userStoreMap;
	}
	

	
	
	public RegisterDAO getRegisterDao() {
		return registerDao;
	}

	public void setRegisterDao(RegisterDAO registerDao) {
		this.registerDao = registerDao;
	}
	public boolean checkForExistingUser(String userName){
		boolean exist=false;
		try{
			exist = registerDao.checkForExistingUser(userName);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return exist;
	}
	public boolean checkForExistingAudit(String category,String question){
		boolean exist=false;
		try{
			exist = registerDao.checkForExistingAudit(category,question);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return exist;
	}
	public boolean checkForExistingStore(String storeCode){
		boolean exist=false;
		try{
			exist = registerDao.checkForExistingStore(storeCode);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return exist;
	}
	
	public List<UserBean> getTypeList(){
		List<UserBean> auditList=null;
		try{
			auditList = registerDao.getTypeList();
		}catch(Exception e){
			e.printStackTrace();
		}
		return auditList;
	}
	
	public List<UserBean> getStatusList(){
		List<UserBean> auditList=null;
		try{
			auditList = registerDao.getStatusList();
		}catch(Exception e){
			e.printStackTrace();
		}
		return auditList;
	}
	public List<CircleBean> getCircleList(){
		List<CircleBean> auditList=null;
		try{
			auditList = registerDao.getCircleList();
		}catch(Exception e){
			e.printStackTrace();
		}
		return auditList;
	}
	public List<AuditLookupBean> getAuditLookUp(){
		List<AuditLookupBean> auditList=null;
		try{
			auditList = registerDao.getAuditList();
		}catch(Exception e){
			e.printStackTrace();
		}
		return auditList;
	}
	public List<LocationBean> getLocationList(){
		List<LocationBean> auditList=null;
		try{
			auditList = registerDao.getLocationList();
		}catch(Exception e){
			e.printStackTrace();
		}
		return auditList;
	}
	public List<UserBean> getRoleList(){
		List<UserBean> auditList=null;
		try{
			auditList = registerDao.getRoleUserList();
		}catch(Exception e){
			e.printStackTrace();
		}
		return auditList;
	}
	public void userStoreMapping(List<UserStoreMap> userStoreList) {
		try{
			registerDao.userStoreMapping(userStoreList);
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public void userCircleMapping(List<UserCircleBean> userCircleList) {
		try{
			registerDao.userCircleMapping(userCircleList);
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public void removeUserStoreMapping(List<String> userStoreList,int userId) {
		try{
			StringBuffer removeUser = new StringBuffer();
			if(userStoreList!=null && userStoreList.size()>0){
				for(String userStores:userStoreList){
					if(removeUser.length()>0){
						removeUser.append(",").append(userStores);
					}else{
						removeUser.append(userStores);
					}
				}
				registerDao.removeUserStoreMapping(removeUser,userId);
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	public List<Store> removeUserCircleMapping(List<String> userCircleList,int userId) {
		List<Store> storeList=new ArrayList<>();
		try{
			StringBuffer removeUser = new StringBuffer();
			if(userCircleList!=null && userCircleList.size()>0){
				for(String userCircles:userCircleList){
					if(removeUser.length()>0){
						removeUser.append(",").append(userCircles);
					}else{
						removeUser.append(userCircles);
					}
				}
				registerDao.removeUserCircleMapping(removeUser,userId);
				 storeList=registerDao.getStoreList(removeUser);
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return storeList;
	}

}
