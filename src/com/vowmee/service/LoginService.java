package com.vowmee.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.vowmee.bean.CircleBean;
import com.vowmee.bean.UserBean;
import com.vowmee.bean.UserStatusBean;
import com.vowmee.dao.LoginDAO;
import com.vowmee.dao.RegisterDAO;
import com.vowmee.dao.UserManagementDao;

public class LoginService {
	LoginDAO loginDao;
	RegisterDAO registerDao;
	UserManagementDao userManageDao;

	public UserManagementDao getUserManageDao() {
		return userManageDao;
	}

	public void setUserManageDao(UserManagementDao userManageDao) {
		this.userManageDao = userManageDao;
	}

	public LoginDAO getLoginDao() {
		return loginDao;
	}

	public void setLoginDao(LoginDAO loginDao) {
		this.loginDao = loginDao;
	}
	
	public boolean isValidUser(String username,String password, HttpServletRequest request){
		boolean validUser = false;
		try{
			UserBean userBean = loginDao.getUserDetailByName(username);
			if(userBean != null){
				String userRole = loginDao.getRoleName(userBean.getUser_role());
				//List<CircleBean>circle=userManageDao.findCircleForUserId(String.valueOf(userBean.getId()));
				
				UserStatusBean userStatus = loginDao.getUserStatusById(userBean.getUser_status());
				//UserRoleBean userRole = loginDao.getUserRoleById(userBean.getUser_role());
				//UserTypeBean userType = loginDao.getUserTypeById(userBean.getUser_type());
				String getCompletedCount = getCompletedCount(userBean.getId());
				String getIncompletedCount = getIncompletedCount(userBean.getId());
				request.getSession().setAttribute("getCompletedCount", getCompletedCount);
				request.getSession().setAttribute("getIncompletedCount", getIncompletedCount);
				
				request.getSession().setAttribute("loggedInUser", userBean.getFirstName());
				request.getSession().setAttribute("loginBean", userBean);
				//List<UserBean> userWithRole = loginDao.getUserList(userBean);
				/*if(userWithRole.size()>0){
					request.getSession().setAttribute("loginBean", userWithRole.get(0));
				}else{
					
				}*/
				
				
				System.out.println("userBean "+userBean);
				request.getSession().setAttribute("username",username);
				request.getSession().setAttribute("userId",userBean.getId());
				request.getSession().setAttribute("firstname",userBean.getFirstName());
				request.getSession().setAttribute("mobile",userBean.getMobile());
				request.getSession().setAttribute("email",userBean.getName());
				request.getSession().setAttribute("userRole",userRole);
				//request.getSession().setAttribute("userCircle",circle);
				//request.getSession().setAttribute("userType",userType.getType_name());
				
				if(userStatus.getStatus_code().equalsIgnoreCase("AC")){
					
					if(userBean.getPassword().equals(password)){
						validUser = true;
						if(userRole.equalsIgnoreCase("admin")){
							request.getSession().setAttribute("admin","admin");
						}else if(userRole.equalsIgnoreCase("Auditor/Trainer")){
								request.getSession().setAttribute("auditor","auditor");
							
						}else if(userRole.equalsIgnoreCase("SuperAdmin")){
							request.getSession().setAttribute("superAdmin","superAdmin");
						}else{
								request.getSession().setAttribute("management","management");
												
							}
						}
						/*if(userType.getType_code().equals("AD")){
							request.getSession().setAttribute("admin","admin");
						}else if(userType.getType_code().equals("NM")){
							if(userRole.equalsIgnoreCase("Auditor/Trainer")){
								request.getSession().setAttribute("auditor","auditor");
							}else{
								request.getSession().setAttribute("management","management");
							}
						}*/
					}
					
				else if(userStatus.getStatus_code().equalsIgnoreCase("RE")){
					if(userBean.getPassword().equals(password)){
						validUser = true;
						request.getSession().setAttribute("registered","registered");
					}
					
				}else if(userStatus.getStatus_code().equalsIgnoreCase("CR")){
					request.getSession().setAttribute("yet_to_activate","yet_to_activate");
				}else{
					request.getSession().setAttribute("inactive","inactive");
				}
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return validUser;
	}
	
	public boolean isAdmin(HttpServletRequest request){
		boolean isAdmin=false;
		try{
			String userType = (String) request.getSession().getAttribute("userType");
			if(userType.equalsIgnoreCase("admin")){
				isAdmin=true;
			}
			//if(userBean.getUser)
		}catch(Exception e){
			e.printStackTrace();
		}
		return isAdmin;
	}
	
	public boolean isRole(HttpServletRequest request){
		boolean isRole=false;
		try{
			String userRole = (String) request.getSession().getAttribute("userRole");
			if(userRole.equalsIgnoreCase("Auditor/Trainer")){
				isRole=true;
			}
			//if(userBean.getUser)
		}catch(Exception e){
			e.printStackTrace();
		}
		return isRole;
	}

	public RegisterDAO getRegisterDao() {
		return registerDao;
	}

	public void setRegisterDao(RegisterDAO registerDao) {
		this.registerDao = registerDao;
	}
	public String getCompletedCount(int userId) {
		String completedCount = loginDao.getCompletedCount(userId);
		return completedCount;
	}
	public String getIncompletedCount(int userId) {
		String completedCount = loginDao.getIncompletedCount(userId);
		return completedCount;
	}
}
