package com.vowmee.service;

import java.util.Date;

import javax.activation.DataSource;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

public class EmailService {

	public void commonSendEmail(JavaMailSender mailSender, String to, String htmlBody, String subject) {

		MimeMessage message = mailSender.createMimeMessage();
		MimeMessageHelper helper;
		try {
			helper = new MimeMessageHelper(message, true);
			helper.setTo(new InternetAddress(to));
			helper.setSubject(subject);
			helper.setText(htmlBody, true);
			mailSender.send(message);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void commonSendEmailAttachment(JavaMailSender mailSender, String to, String htmlBody, String subject, DataSource datasource
			,HttpServletRequest request) {
		MimeMessage message = mailSender.createMimeMessage();
		MimeMessageHelper helper;
		try {
			String storeName = (String) request.getSession().getAttribute("storeName");
			String userName = (String) request.getSession().getAttribute("firstname");
			helper = new MimeMessageHelper(message, true);
			/*message.addRecipient(Message.RecipientType.TO,new InternetAddress(to));
			message.setSubject(subject);
			message.setText(htmlBody,"text/html; charset=utf-8");*/
			helper.setTo(new InternetAddress(to));
			helper.setSubject(subject);
			helper.setText(htmlBody, true);
			helper.addAttachment(userName+"_"+storeName+"_"+new Date()+".xls", datasource);
			/*MimeBodyPart messageBodyPart2 = new MimeBodyPart();
			messageBodyPart2.setDataHandler(new DataHandler(datasource));  
			//messageBodyPart2.setContent(htmlBody,"text/html; charset=utf-8");
		    messageBodyPart2.setFileName("Audit");
		    Multipart multipart = new MimeMultipart();
		    multipart.addBodyPart(messageBodyPart2);
			//FileSystemResource file = new FileSystemResource("C:\\Ayyanar\\backup\\Vowmee\\vowmee Nov 2\\Vowmee_Audit\\WebContent/WEB-INF/attachments/temp.xls");
			//helper.addAttachment(file.getFilename(), file);
		    message.setContent(multipart );*/
			mailSender.send(message);

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
