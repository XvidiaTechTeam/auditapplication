package com.vowmee.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.vowmee.bean.AuditBean;
import com.vowmee.bean.AuditLookupBean;
import com.vowmee.bean.AuditMaster;
import com.vowmee.bean.CompletedAuditsVO;
import com.vowmee.bean.ReportsBean;
import com.vowmee.bean.ScoreCardBO;
import com.vowmee.bean.Store;
import com.vowmee.bean.StoreBean;
import com.vowmee.bean.UserBean;
import com.vowmee.bean.UserPreviousAuditHytory;
import com.vowmee.bean.UserRoleBean;
import com.vowmee.bean.WeitageBean;

public class AuditDAO {

	JdbcTemplate template;

	public void setTemplate(JdbcTemplate template) {
		this.template = template;
	}
	
	public List<String> getStoreList(){
		List<String> auditList=null;
		try{
			String sql="SELECT store_name FROM vowmeeasymp.vowmee_store_master";
			auditList = template.queryForList(sql, String.class);
		}catch(Exception e){
			e.printStackTrace();
		}
		return auditList;
	}
	public List<String> getStoreListById(StringBuilder buildId){
		List<String> auditList=null;
		try{
			String sql="SELECT store_name FROM vowmeeasymp.vowmee_store_master where id in ("+buildId+")";
			auditList = template.queryForList(sql, String.class);
		}catch(Exception e){
			e.printStackTrace();
		}
		return auditList;
	}
	
	
	
	public List<String> getStoreIdList(int userId){
		List<String> auditList=null;
		try{
			String sql="SELECT storeid FROM vowmeeasymp.vowmee_user_store_mapping where userid="+userId;
			auditList = template.queryForList(sql, String.class);
		}catch(Exception e){
			e.printStackTrace();
		}
		return auditList;
	}
	
	public List<String> getStoreIdListComplete(int userId){
		List<String> auditList=null;
		try{
			String sql="SELECT a.storeid FROM vowmeeasymp.vowmee_user_store_mapping a,vowmeeasymp.vowmee_audit_master b where b.audit_complete='1' and a.storeid=b.store and a.userid="+userId;
			auditList = template.queryForList(sql, String.class);
		}catch(Exception e){
			e.printStackTrace();
		}
		return auditList;
	}
	
	
	public int getUserId(String email){
		int userId = 0;
		try{
			String query = "SELECT id FROM vowmeeasymp.vowmee_user_master WHERE `name` = '"+email+"' ";
			List<Integer> rows = template.query(query,new RowMapper<Integer>(){  
			    @Override  
			    public Integer mapRow(ResultSet rs, int rownumber) throws SQLException {  
			    	Integer e = null;
			    	if(rs!= null){
			    		e = rs.getInt(1);
			    	}
			        return e;  
			    }  
			    });
			for(Integer user : rows){
				userId = user;
			}
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		
		return userId;
	}
	
	public Map<String, String> getAuditCatergoryStatus(long auditMasterId) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT a.audit_category,totalcount,count(*) as count FROM vowmeeasymp.vowmee_audit_lookup a , vowmeeasymp.vowmee_audit_detail b, ");
		sql.append("(SELECT audit_category,count(*) totalcount FROM vowmeeasymp.vowmee_audit_lookup  group by audit_category) as lookup where ");
		sql.append("a.id=b.audit_lkp and b.audit="+auditMasterId+"  and lookup.audit_category= a.audit_category group by a.audit_category ");
		final Map<String, String> categoryMap = new TreeMap<String, String>();
		List<String> categoryList = template.query(sql.toString(),new RowMapper<String>(){  
		    @Override  
		    public String mapRow(ResultSet rs, int rownumber) throws SQLException {
		    	if(rs.getInt("totalcount") == rs.getInt("count")){
		    		categoryMap.put(rs.getString("audit_category"),"green");
		    	}else{
		    		categoryMap.put(rs.getString("audit_category"),"red");
		    	}
		         return "";
		    }  
		    });
				
				
		return categoryMap;
		
	}
	
	
	public List<String> getAuditCategory(){
		List<String> auditCategoryList=null;
		try{
			String sql="SELECT distinct(audit_category) FROM vowmeeasymp.vowmee_audit_lookup";
			auditCategoryList = template.queryForList(sql, String.class);
		}catch(Exception e){
			e.printStackTrace();
		}
		return auditCategoryList;
	}
	
	public int updateAuditStatus(long auditMasterId,String agentName, String agentEmail, String agentMobile) {
		String query = "UPDATE `vowmeeasymp`.`vowmee_audit_master` SET `audit_complete` = 1, agent_name='"+agentName
				+"', agent_email='"+agentEmail +"', agent_mobile='"+agentMobile +"' WHERE `id` = '"+auditMasterId+"'";
		return template.update(query);
	}
	
	public List<AuditLookupBean> getAuditQuestions(String auditCategory){
		String sql="select * from vowmeeasymp.vowmee_audit_lookup where audit_category=?";
		List<AuditLookupBean> auditQuestionsList = template.query(sql, new Object[]{auditCategory},new BeanPropertyRowMapper(AuditLookupBean.class));
		return auditQuestionsList;
	}
	
	public List<AuditLookupBean> getAuditQuestionsReplace(String auditCategory){
		String sql="select * from vowmeeasymp.vowmee_audit_lookup where audit_category="+auditCategory;
		List<AuditLookupBean> auditQuestionsList = template.query(sql,new BeanPropertyRowMapper(AuditLookupBean.class));
		return auditQuestionsList;
	}
	
	
	public int insertAuditData(String query){
		return template.update(query);
	}

	public List<AuditLookupBean> selectAllAuditName() {
		String sql = "select id,audit_category, audit_name from vowmeeasymp.vowmee_audit_lookup";
			List<AuditLookupBean> rows = template.query(sql,new RowMapper<AuditLookupBean>(){  
			    @Override  
			    public AuditLookupBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
			    	AuditLookupBean e=new AuditLookupBean(); 
			    	e.setId(rs.getInt(1));
			        e.setAudit_category(rs.getString(2));  
			        e.setAudit_name(rs.getString(3));
			        return e;  
			    }  
			    });  
			System.out.println(rows);
		return rows;
	}
	
	
	
	public boolean getExistedAudit(long auditMasterId, int auditLookupId){
		boolean flag = false;
		try{
			String query = "select audit,audit_lkp from vowmeeasymp.vowmee_audit_detail where audit='"+auditMasterId+"' and audit_lkp='"+auditLookupId+"'";
			List<AuditBean> rows = template.query(query,new RowMapper<AuditBean>(){  
			    @Override  
			    public AuditBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
			    	AuditBean e = new AuditBean();
			    	if(rs!= null){
				    	e.setAudit(rs.getInt(1));
				        e.setAuditLookup(rs.getInt(2));  
				        //e.setAudit_name(rs.getString(3));
			    	}
			        return e;  
			    }  
			    });
			if(rows.isEmpty()){
				flag= true;
			}
			for(AuditBean auditMaster: rows){
				if((auditMaster.getAudit()==auditMasterId && auditMaster.getAuditLookup() == auditLookupId)){
					flag = false;
				}else{
					flag= true;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		
		return flag;
	}
	
	
	/*public long insertAuditMaster(String storeNameUnique,int id, int storeId){
		final String storeName = storeNameUnique;
		final int idFinal=id;
		final int storeIdFinal=storeId;
		long uniqueId=0;
		KeyHolder keyHolder = new GeneratedKeyHolder();
		final String INSERT_SQL = "INSERT INTO `vowmeeasymp`.`vowmee_audit_master`(`audit_name`,`store`,`auditor`,`audit_complete`) values(?,?,?,?)";
		template.update(
			    new PreparedStatementCreator() {
			        public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
			            PreparedStatement ps =
			                connection.prepareStatement(INSERT_SQL, new String[] {"id"});
			            ps.setString(1, storeName);
			            ps.setInt(2, storeIdFinal);
			            ps.setInt(3, idFinal);
			            ps.setBoolean(4, false);
			            return ps;
			        }
			    },
			    keyHolder);
		System.out.println("keyHolder "+ keyHolder.getKey());
		uniqueId = (long) keyHolder.getKey();
		
		return uniqueId;
	}*/
	
	public long insertAuditMaster(String storeNameUnique,int id, int storeId){
		final String storeName = storeNameUnique;
		final int idFinal=id;
		final int storeIdFinal=storeId;
		long uniqueId=0;
		KeyHolder keyHolder = new GeneratedKeyHolder();
		final String INSERT_SQL = "INSERT INTO `vowmeeasymp`.`vowmee_audit_master`(`audit_name`,`store`,`auditor`,`audit_date`,`audit_complete`) values(?,?,?,?,?)";
		template.update(
			    new PreparedStatementCreator() {
			        public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
			            PreparedStatement ps =
			                connection.prepareStatement(INSERT_SQL, new String[] {"id"});
			            ps.setString(1, storeName);
			            ps.setInt(2, storeIdFinal);
			            ps.setInt(3, idFinal);
			            ps.setDate(4, new java.sql.Date(System.currentTimeMillis()));
			            ps.setBoolean(5, false);
			            return ps;
			        }
			    },
			    keyHolder);
		System.out.println("keyHolder "+ keyHolder.getKey());
		uniqueId = (long) keyHolder.getKey();
		
		return uniqueId;
	}
	
	public List<AuditBean> selectAuditPreview(long auditId, String auditCategory) {
		String auditDetail = "SELECT a.audit_value, b.id, b.audit_category, b.audit_name from vowmeeasymp.vowmee_audit_detail a, vowmeeasymp.vowmee_audit_lookup b Where a.audit_lkp = b.id and a.audit = "+auditId+" and b.audit_category = '"+auditCategory+"'";
		List<AuditBean> rows = template.query(auditDetail,new RowMapper<AuditBean>(){  
		    @Override  
		    public AuditBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	AuditBean e=new AuditBean();
		    	if(rs.getBoolean(1)){
		    		e.setAuditYes("Yes");
		    	}else{
		    		e.setAuditNo("No");
		    	}
		    	e.setAuditValue(rs.getBoolean(1));
		    	e.setLookupId(rs.getInt(2));
		    	e.setAuditCategory(rs.getString(3));
		        e.setAuditQuestion(rs.getString(4));  
		        return e;  
		    }  
		    });
		return rows;
	}
	
	public List<ScoreCardBO> scoreCardCalculation(long auditMasterId) {
		
		List<WeitageBean> getWeightageList = getWeightage();
		final Map<String,Integer> weightageMap = new HashMap<String,Integer>();
		
		if(getWeightageList!=null){
			for(WeitageBean weightage: getWeightageList){
				weightageMap.put(weightage.getCategory(), Integer.parseInt(weightage.getWeightage()));
			}
		}
		
		String auditDetail = "select mst.id, audit_category, "
				+ " sum(audit_value) / count(cat.id)*100 percentage "
				+ "  from vowmeeasymp.vowmee_audit_master mst,"
				+ "       vowmeeasymp.vowmee_audit_lookup cat,"
				+ "       vowmeeasymp.vowmee_audit_detail det"
				+ " where mst.id ="+ auditMasterId+"  and mst.id = det.audit and det.audit_lkp = cat.id and det.audit_value>=0"
				+ "  group by mst.audit_name, audit_category";
		List<ScoreCardBO> rows = template.query(auditDetail,new RowMapper<ScoreCardBO>(){  
		    @Override  
		    public ScoreCardBO mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	ScoreCardBO e=new ScoreCardBO();
		    	e.setAudit_id(rs.getInt(1));
		    	e.setAudit_category(rs.getString(2));
		    	e.setPercentage(rs.getString(3));
		    	System.out.println("insd "+weightageMap.get(rs.getString(2))+" 3 values "+rs.getString(3));
		    	if(weightageMap.get(rs.getString(2))!=null){
		    		Float weightage_percentage = ((weightageMap.get(rs.getString(2)))*Float.parseFloat(rs.getString(3)))/100;
		    		String weightage = weightage_percentage.toString();
			    	e.setWeitage_percentage((Math.round(new Double(weightage) * 100.0) / 100.0)+"");
		    	}
		    	
		    	
		    	
		    	
		        return e;  
		    }  
		    });
		return rows;
	}
	
	public List<WeitageBean> getWeightage() {
		String auditDetail = "select category,weightage from vowmeeasymp.vowmee_weightage";
		List<WeitageBean> rows = template.query(auditDetail,new RowMapper<WeitageBean>(){  
		    @Override  
		    public WeitageBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	WeitageBean e=new WeitageBean();
		    	e.setCategory(rs.getString(1));
		    	e.setWeightage(rs.getString(2));
		    	
		        return e;  
		    }  
		    });
		return rows;
	}
	
	
	
	public List<AuditBean> selectAuditPreviewList(long auditId, String auditCategory) {
		String auditDetail = "SELECT a.audit_value, b.id, b.audit_category, b.audit_name from vowmeeasymp.vowmee_audit_detail a, vowmeeasymp.vowmee_audit_lookup b Where a.audit_lkp = b.id and a.audit = "+auditId+" and b.audit_category = "+auditCategory;
		List<AuditBean> rows = template.query(auditDetail,new RowMapper<AuditBean>(){  
		    @Override  
		    public AuditBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	AuditBean e=new AuditBean();
		    	if(rs.getBoolean(1)){
		    		e.setAuditYes("Yes");
		    	}else{
		    		e.setAuditNo("No");
		    	}
		    	e.setAuditValue(rs.getBoolean(1));
		    	e.setLookupId(rs.getInt(2));
		    	e.setAuditCategory(rs.getString(3));
		        e.setAuditQuestion(rs.getString(4));  
		        return e;  
		    }  
		    });
		return rows;
	}
	
	public List<String> getAuditPreviewList(long auditId){
		List<String> auditCategoryList=null;
		try{
			String sql="SELECT distinct(b.audit_category) from vowmeeasymp.vowmee_audit_detail a, vowmeeasymp.vowmee_audit_lookup b Where a.audit_lkp = b.id and a.audit ="+auditId;
			auditCategoryList = template.queryForList(sql, String.class);
		}catch(Exception e){
			e.printStackTrace();
		}
		return auditCategoryList;	
	}
	
	/*public List<String> getScorePercentage(long auditId){
		List<String> auditCategoryList=null;
		try{
			String sql="SELECT a.percentage from vowmeeasymp.vowmee_score_card_cal a Where a.audit_id ="+auditId;
			auditCategoryList = template.queryForList(sql, String.class);
		}catch(Exception e){
			e.printStackTrace();
		}
		return auditCategoryList;	
	}
	*/
	
	public List<String> getScorePercentage(long storeId){
		List<String> auditCategoryList=null;
		try{
			String sql="SELECT sum(a.percentage)/count(a.percentage) as percentage"
					+ " from vowmeeasymp.vowmee_score_card_cal a Where a.store_id ='"+storeId+"'"
							+ " and MONTH(audit_completed_date) = MONTH(CURDATE())";
			auditCategoryList = template.queryForList(sql, String.class);
		}catch(Exception e){
			e.printStackTrace();
		}
		return auditCategoryList;	
	}
	public List<Store> selectStoreById(StringBuilder buildId) {
		String sql = "SELECT a.id, a.store_name,b.circle_name FROM vowmeeasymp.vowmee_store_master a, vowmeeasymp.vowmee_circle_master b where a.id in ("+buildId+")"
				+ "and a.circle=b.id order by b.circle_name";
			List<Store> rows = template.query(sql,new RowMapper<Store>(){  
			    @Override  
			    public Store mapRow(ResultSet rs, int rownumber) throws SQLException {  
			    	Store e=new Store(); 
			    	e.setId(rs.getInt(1));
			        e.setStore_name(rs.getString(2));  
			        e.setCircleName(rs.getString(3));
			        return e;  
			    }  
			    });  
			System.out.println(rows);
		return rows;
	}
	
	
	public List<Store> selectStoreReportById(int buildId) {
		String sql = "SELECT a.id, a.store_name,b.circle_name FROM vowmeeasymp.vowmee_store_master a, vowmeeasymp.vowmee_circle_master b where a.id ="+buildId+" and a.circle=b.id";
			List<Store> rows = template.query(sql,new RowMapper<Store>(){  
			    @Override  
			    public Store mapRow(ResultSet rs, int rownumber) throws SQLException {  
			    	Store e=new Store(); 
			    	e.setId(rs.getInt(1));
			        e.setStore_name(rs.getString(2));  
			        e.setCircleName(rs.getString(3));
			        return e;  
			    }  
			    });  
			System.out.println(rows);
		return rows;
	}
	
	
	public List<AuditBean> selectAllAuditPreviewList(long auditId) {
		String auditDetail = "SELECT a.audit_value, b.id, b.audit_category, b.audit_name from vowmeeasymp.vowmee_audit_detail a, vowmeeasymp.vowmee_audit_lookup b Where a.audit_lkp = b.id and a.audit = "+auditId;
		List<AuditBean> rows = template.query(auditDetail,new RowMapper<AuditBean>(){  
		    @Override  
		    public AuditBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	AuditBean e=new AuditBean();
		    	if(rs.getString(1).equals("1")){
		    		e.setAuditYes("Yes");
		    	}else if(rs.getString(1).equals("0")){
		    		e.setAuditNo("No");
		    	}else{
		    		e.setAuditNA("N/A");
		    	}
		    	e.setAuditValue(rs.getBoolean(1));
		    	e.setAuditFlag(rs.getInt(1));
		    	e.setLookupId(rs.getInt(2));
		    	e.setAuditCategory(rs.getString(3));
		        e.setAuditQuestion(rs.getString(4));  
		        return e;  
		    }  
		    });
		return rows;
	}
	
	public int getStoreIdByName(String storeName){
		String sql="select id from vowmeeasymp.vowmee_store_master where store_name=?";
		Object obj = template.queryForObject(sql, new Object[]{storeName},new BeanPropertyRowMapper<Store>(Store.class));
		int id = ((Store)obj).getId();
		return id;
		 
	}
	
	public String getStoreNameById(int storeId){
		String sql="select store_name from vowmeeasymp.vowmee_store_master where id=?";
		Object obj = template.queryForObject(sql, new Object[]{storeId},new BeanPropertyRowMapper<Store>(Store.class));
		String storeName = ((Store)obj).getStore_name();
		return storeName;
		 
	}
	
	public List<AuditMaster> getIncompletedCount(StringBuilder stores, int userId) {
		String sql="SELECT store, audit_complete FROM vowmeeasymp.vowmee_audit_master where vowmee_audit_master.store in ("+stores+") and vowmee_audit_master.auditor = "+userId+" and MONTH(audit_date) = MONTH(CURDATE())";
		List<AuditMaster> rows = template.query(sql,new RowMapper<AuditMaster>(){  
		    @Override  
		    public AuditMaster mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	AuditMaster e=new AuditMaster();
		    		if(rs.getInt(2) == 0){
		    			e.setAuditComplete(false);
		    		}else if(rs.getInt(2) == 1){
		    			e.setAuditComplete(true);
		    		}
		    		
		    		e.setStore(rs.getInt(1));
		        return e;  
		    }  
		    });
		return rows;
	}
	
	public void insertScoreCardCal(List<ScoreCardBO> scoreCardList, int storeId) {
		for(ScoreCardBO scoreBean: scoreCardList){
				 String query = "INSERT INTO `vowmeeasymp`.`vowmee_score_card_cal` (`store_id`, `audit_id`, "
				 		+ "`audit_category`, `percentage`,`audit_completed_date`,`weitage_percentage`) VALUES ('"
						 +storeId+"','"+scoreBean.getAudit_id()
						 +"','"+scoreBean.getAudit_category()+"','"+scoreBean.getPercentage()
						 +"',NOW(),'"+scoreBean.getWeitage_percentage()+"')";
					template.update(query);
			
		}
	}
	
	/*public List<ReportsBean> scoreCardList(ScoreCardBO scoreBo) throws Exception {
		String start_dt = scoreBo.getFromDate();
		//DateFormat formatter = new SimpleDateFormat("DD/MM/yyyy"); 
		SimpleDateFormat sdf = new SimpleDateFormat("M/dd/yyyy");
		String dateInString = scoreBo.getFromDate();
		Date date = sdf.parse(dateInString);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String finalString = dateFormat.format(date);
		System.out.println(finalString);
		//System.out.println(date);
		SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-mm-dd");
		Date date = (Date)newFormat.parse(scoreBo.getFromDate());
		String finalString = newFormat.format(date);
		System.out.println("final String "+finalString);
		
		String start_dt1 = scoreBo.getToDate();
		Date date1 = sdf.parse(start_dt1);
		String finalString1 = dateFormat.format(date1);
		System.out.println("final String "+finalString1);
		String sql="select audit_category, percentage, EXTRACT(MONTH FROM audit_completed_date)AS audit_month,weitage_percentage"
				+ " FROM vowmeeasymp.vowmee_score_card_cal where "
				+ "store_id ="+ scoreBo.getStore_id()+" and (audit_completed_date BETWEEN '"+finalString+"' AND '"+finalString1+"')";
		System.out.println("sql : "+sql);
		List<ReportsBean> rows = template.query(sql,new RowMapper<ReportsBean>(){  
		    @Override  
		    public ReportsBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	ReportsBean e=new ReportsBean();
		    		e.setAudit_category(rs.getString(1));
		    		e.setAudit_percentage(Float.parseFloat(rs.getString(2)));
		    		e.setAudit_month(Integer.parseInt(rs.getString(3)));
		    		e.setWeitage_percentage(rs.getString(4));
		        return e;  
		    }  
		    });
		return rows;
	}*/
	
	public List<ReportsBean> scoreCardList(ScoreCardBO scoreBo) throws Exception {
		String month = scoreBo.getMonth();
		String sql="select audit_category, percentage, EXTRACT(MONTH FROM audit_completed_date)AS audit_month,weitage_percentage"
				+ " FROM vowmeeasymp.vowmee_score_card_cal where "
				+ "store_id ="+ scoreBo.getStore_id()+" and MONTH(audit_completed_date) BETWEEN "+(Integer.parseInt(month)-2)+" And "+month;
		System.out.println("sql : "+sql);
		List<ReportsBean> rows = template.query(sql,new RowMapper<ReportsBean>(){  
		    @Override  
		    public ReportsBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	ReportsBean e=new ReportsBean();
		    		e.setAudit_category(rs.getString(1));
		    		e.setAudit_percentage(Float.parseFloat(rs.getString(2)));
		    		e.setAudit_month(Integer.parseInt(rs.getString(3)));
		    		e.setWeitage_percentage(rs.getString(4));
		        return e;  
		    }  
		    });
		return rows;
	}
	/*public List<ReportsBean> scoreCardReportList(ScoreCardBO scoreBo) throws Exception {
		String month = scoreBo.getMonth();
		String sql="select audit_category, avg(percentage), EXTRACT(MONTH FROM audit_completed_date)AS audit_month"
				+ " FROM vowmeeasymp.vowmee_score_card_cal where "
				+ "store_id ="+ scoreBo.getStore_id()+" and MONTH(audit_completed_date) BETWEEN "+(Integer.parseInt(month)-2)+" And "+month +" group by audit_category, audit_month"
						+ " order by audit_month";
		System.out.println("sql : "+sql);
		List<ReportsBean> rows = template.query(sql,new RowMapper<ReportsBean>(){  
		    @Override  
		    public ReportsBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	ReportsBean e=new ReportsBean();
		    		e.setAudit_category(rs.getString(1));
		    		e.setAudit_percentage(Float.parseFloat(rs.getString(2)));
		    		e.setAudit_month(Integer.parseInt(rs.getString(3)));
		        return e;  
		    }  
		    });
		return rows;
	}*/
	
	public List<ReportsBean> scoreCardReportList(ScoreCardBO scoreBo) throws Exception {
		String month = scoreBo.getMonth();
		String sql="select b.weightage, audit_category, ROUND(avg(percentage),2), EXTRACT(MONTH FROM audit_completed_date)AS audit_month"
				+ " FROM vowmeeasymp.vowmee_score_card_cal, vowmeeasymp.vowmee_weightage b where "
				+ "store_id ="+ scoreBo.getStore_id()+" and MONTH(audit_completed_date) BETWEEN "+(Integer.parseInt(month)-2)+" And "+month +" and audit_category = b.category group by audit_category, audit_month"
						+ " order by audit_category";
		System.out.println("sql : "+sql);
		List<ReportsBean> rows = template.query(sql,new RowMapper<ReportsBean>(){  
		    @Override  
		    public ReportsBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	ReportsBean e=new ReportsBean();
		    		e.setAudit_category(rs.getString(1));
		    		e.setAudit_percentage(Float.parseFloat(rs.getString(2)));
		    		e.setAudit_month(Integer.parseInt(rs.getString(3)));
		    		e.setWeitage_percentage(rs.getString(4));
		        return e;  
		    }  
		    });
		return rows;
	}
	
/*	public List<ReportsBean> scoreCardReportList(ScoreCardBO scoreBo) throws Exception {
		String month = scoreBo.getMonth();
		String sql="select audit_category, avg(percentage), EXTRACT(MONTH FROM audit_completed_date)AS audit_month"
				+ " FROM vowmeeasymp.vowmee_score_card_cal where "
				+ "store_id ="+ scoreBo.getStore_id()+" and MONTH(audit_completed_date) BETWEEN "+(Integer.parseInt(month)-2)+" And "+month +" group by audit_category, audit_month"
						+ " order by audit_month";
		System.out.println("sql : "+sql);
		List<ReportsBean> rows = template.query(sql,new RowMapper<ReportsBean>(){  
		    @Override  
		    public ReportsBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	ReportsBean e=new ReportsBean();
		    		e.setAudit_category(rs.getString(1));
		    		e.setAudit_percentage(Float.parseFloat(rs.getString(2)));
		    		e.setAudit_month(Integer.parseInt(rs.getString(3)));
		        return e;  
		    }  
		    });
		return rows;
	}*/
	/*public List<AuditBean> getAuditDetailsToExport(ScoreCardBO scoreBo) throws Exception {
		String start_dt = scoreBo.getFromDate();
		//DateFormat formatter = new SimpleDateFormat("DD/MM/yyyy"); 
		SimpleDateFormat sdf = new SimpleDateFormat("M/dd/yyyy");
		String dateInString = scoreBo.getFromDate();
		Date date = sdf.parse(dateInString);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String finalString = dateFormat.format(date);
		System.out.println(finalString);
		//System.out.println(date);
		SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-mm-dd");
		Date date = (Date)newFormat.parse(scoreBo.getFromDate());
		String finalString = newFormat.format(date);
		System.out.println("final String "+finalString);
		
		String start_dt1 = scoreBo.getToDate();
		Date date1 = sdf.parse(start_dt1);
		String finalString1 = dateFormat.format(date1);
		System.out.println("final String "+finalString1);
		String sql ="select mst.id, audit_category, audit_value,cat.audit_name"
				+ " from vowmeeasymp.vowmee_audit_master mst, "
				+ " vowmeeasymp.vowmee_audit_lookup cat,"
				+ " vowmeeasymp.vowmee_audit_detail det "
				+ "where mst.store = "+ scoreBo.getStore_id()+" and (audit_date BETWEEN '"+finalString+"' AND '"+finalString1+"')"
				+ "  and mst.id = det.audit and det.audit_lkp = cat.id";
				
	String sql="select audit_category, percentage, EXTRACT(MONTH FROM audit_completed_date)AS audit_month,weitage_percentage"
				+ " FROM vowmeeasymp.vowmee_score_card_cal where "
				+ "store_id ="+ scoreBo.getStore_id()+" and (audit_completed_date BETWEEN '"+finalString+"' AND '"+finalString1+"')";
		List<AuditBean> rows = template.query(sql,new RowMapper<AuditBean>(){  
		    @Override  
		    public AuditBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	AuditBean e=new AuditBean();
		    		e.setAudit(rs.getInt(1));
		    		e.setAuditCategory(rs.getString(2));
		    		e.setAuditValue(rs.getBoolean(3));
		    		if(e.isAuditValue()){
		    			e.setAuditAnswer("1");
		    		}else{
		    			e.setAuditAnswer("0");
		    		}
		    		e.setAuditQuestion(rs.getString(4));
		        return e;  
		    }  
		    });
		return rows;
	}*/
	
	public List<AuditBean> getAuditDetailsToExport(ScoreCardBO scoreBo) throws Exception {
		String month = scoreBo.getMonth();
		String sql ="select distinct mst.id, cat.audit_category, audit_value,cat.audit_name, MONTH(scrd.audit_completed_date) as month, "
				+ "scrd.audit_completed_date from vowmeeasymp.vowmee_audit_master mst, "
				+ " vowmeeasymp.vowmee_audit_lookup cat,"
				+ " vowmeeasymp.vowmee_audit_detail det, vowmeeasymp.vowmee_score_card_cal scrd "
				+ "where mst.store = "+ scoreBo.getStore_id()+" and MONTH(scrd.audit_completed_date) BETWEEN "+(Integer.parseInt(month)-2)+" And "+month
				+ "  and mst.id = det.audit and det.audit_lkp = cat.id and mst.id=scrd.audit_id order by month";
				
	/*String sql="select audit_category, percentage, EXTRACT(MONTH FROM audit_completed_date)AS audit_month,weitage_percentage"
				+ " FROM vowmeeasymp.vowmee_score_card_cal where "
				+ "store_id ="+ scoreBo.getStore_id()+" and (audit_completed_date BETWEEN '"+finalString+"' AND '"+finalString1+"')";*/
		List<AuditBean> rows = template.query(sql,new RowMapper<AuditBean>(){  
		    @Override  
		    public AuditBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	AuditBean e=new AuditBean();
		    		e.setAudit(rs.getInt(1));
		    		e.setAuditCategory(rs.getString(2));
		    		e.setAuditValue(rs.getBoolean(3));
		    		if(rs.getInt(3)== 1){
		    			e.setAuditAnswer("Yes");
		    		}else if(rs.getInt(3)== 0){
		    			e.setAuditAnswer("No");
		    		}else{
		    			e.setAuditAnswer("N/A");
		    		}
		    		e.setAuditQuestion(rs.getString(4));
		    		e.setMonth(rs.getString(5));

		    		e.setAuditCompleteDate(rs.getString(6));
		        return e;  
		    }  
		    });
		return rows;
	}
	public List<AuditBean> getAuditValue(long auditMasterId) {
		String sql="select a.audit,a.audit_lkp,b.id,b.audit_category,a.audit_value,b.audit_name from "
				+ "vowmeeasymp.vowmee_audit_detail a,vowmeeasymp.vowmee_audit_lookup b where a.audit_lkp=b.id and a.audit="+auditMasterId+";";
		List<AuditBean> rows = template.query(sql,new RowMapper<AuditBean>(){  
		    @Override  
		    public AuditBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	AuditBean e=new AuditBean();
		    		e.setAuditCategory(rs.getString(4));
		    		e.setAuditQuestion(rs.getString(6));
		    		if(rs.getInt(5)== 1){
		    			e.setAuditAnswer("Yes");
		    		}else if(rs.getInt(5)== 0){
		    			e.setAuditAnswer("No");
		    		}else{
		    			e.setAuditAnswer("N/A");
		    		}
		        return e;  
		    }  
		    });
		return rows;
	}

	public long selectAuditId(int storeId) {
		String sql = "SELECT MAX(id) FROM vowmeeasymp.vowmee_audit_master where store ="+storeId;
		return template.queryForObject(sql, long.class );
	}
	
/*	public List<CompletedAuditsVO> getCompletedAuditDetails(int userId) {
		// TODO Auto-generated method stub
		
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT distinct f.circle_name, d.store_name,  c.audit_completed_date, ROUND(avg(c.percentage),2) as percentage ");
		sql.append("FROM vowmeeasymp.vowmee_audit_detail a, vowmeeasymp.vowmee_audit_master b, vowmeeasymp.vowmee_score_card_cal c,");
		sql.append(" vowmeeasymp.vowmee_store_master d, vowmeeasymp.vowmee_audit_lookup e, vowmeeasymp.vowmee_circle_master f where a.audit=b.id and b.auditor="+userId);
		sql.append(" and b.audit_complete=1 and c.audit_id=a.audit and d.id=b.store and e.id=a.audit_lkp and d.circle=f.id group by f.circle_name, d.store_name ");
		sql.append("order by c.audit_completed_date");
		
		List<CompletedAuditsVO> auditList = template.query(sql.toString(),new RowMapper<CompletedAuditsVO>(){  
		    @Override  
		    public CompletedAuditsVO mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	CompletedAuditsVO vo=new CompletedAuditsVO();
		    		vo.setCircle(rs.getString("circle_name"));
		    		vo.setStoreName(rs.getString("store_name"));
		    		vo.setAuditDate(rs.getString("audit_completed_date"));
		    		vo.setPercentage(rs.getString("percentage"));
		        return vo;  
		    }  
		    });
		return auditList;

	}*/
	
	public List<UserPreviousAuditHytory> getUserAuditDetails(String userId) {
		// TODO Auto-generated method stub
		
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT field,oldvalue,newvalue,createddate FROM vowmeeasymp.vowmee_audit_detail_history where parentid="+userId);
		
		List<UserPreviousAuditHytory> auditList = template.query(sql.toString(),new RowMapper<UserPreviousAuditHytory>(){  
		    @Override  
		    public UserPreviousAuditHytory mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	UserPreviousAuditHytory vo=new UserPreviousAuditHytory();
		    		vo.setAction(rs.getString(1));
		    		vo.setNewValue(rs.getString(2));
		    		vo.setOldValue(rs.getString(3));
		    		vo.setCreatedDate(rs.getDate(4)+"");
		        return vo;  
		    }  
		    });
		return auditList;

	}
	
	public List<CompletedAuditsVO> getCompletedAuditDetails(int userId) {
		// TODO Auto-generated method stub
		
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT distinct c.audit_id, f.circle_name, d.store_name,  c.audit_completed_date, ROUND(avg(c.percentage),2) as percentage ");
		sql.append("FROM vowmeeasymp.vowmee_audit_detail a, vowmeeasymp.vowmee_audit_master b, vowmeeasymp.vowmee_score_card_cal c,");
		sql.append(" vowmeeasymp.vowmee_store_master d, vowmeeasymp.vowmee_audit_lookup e, vowmeeasymp.vowmee_circle_master f where a.audit=b.id and b.auditor="+userId);
		sql.append(" and b.audit_complete=1 and c.audit_id=a.audit and d.id=b.store and e.id=a.audit_lkp and d.circle=f.id group by c.audit_id, f.circle_name, d.store_name ");
		sql.append("order by c.audit_completed_date");
		
		List<CompletedAuditsVO> auditList = template.query(sql.toString(),new RowMapper<CompletedAuditsVO>(){  
		    @Override  
		    public CompletedAuditsVO mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	CompletedAuditsVO vo=new CompletedAuditsVO();
		    		vo.setCircle(rs.getString("circle_name"));
		    		vo.setStoreName(rs.getString("store_name"));
		    		vo.setAuditDate(rs.getString("audit_completed_date"));
		    		vo.setPercentage(rs.getString("percentage"));
		        return vo;  
		    }  
		    });
		return auditList;

	}
	
	public List<ScoreCardBO> getCircleList() {

		String auditDetail = "select circle_name,id from vowmeeasymp.vowmee_circle_master order by circle_name";
		List<ScoreCardBO> rows = template.query(auditDetail, new RowMapper<ScoreCardBO>() {
			@Override
			public ScoreCardBO mapRow(ResultSet rs, int rownumber) throws SQLException {
				ScoreCardBO e = new ScoreCardBO();

				e.setCircle_id(rs.getInt(2));
				e.setCircle_name(rs.getString(1));

				return e;
			}
		});
		return rows;
	}
	
	public List<ScoreCardBO> getCircleListforUser(int userId) {

		String auditDetail = "select b.circle_name,b.id from vowmeeasymp.vowmee_user_circle_mapping a,vowmeeasymp.vowmee_circle_master b where a.circleId=b.id and a.userId='"+userId+"'";
		List<ScoreCardBO> rows = template.query(auditDetail, new RowMapper<ScoreCardBO>() {
			@Override
			public ScoreCardBO mapRow(ResultSet rs, int rownumber) throws SQLException {
				ScoreCardBO e = new ScoreCardBO();

				e.setCircle_id(rs.getInt(2));
				e.setCircle_name(rs.getString(1));

				return e;
			}
		});
		return rows;
	}
	

	public List<ScoreCardBO> getCircleScoreList(int circleId,String month,String category) {
		List<ScoreCardBO> auditCategoryList = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("select sum(s.percentage)/count(s.percentage),m.store_name,EXTRACT(MONTH FROM s.audit_completed_date)AS audit_month from vowmeeasymp.vowmee_score_card_cal s, ");
			sql.append("vowmeeasymp.vowmee_store_master m where m.id=s.store_id and  m.circle="+circleId +" and ");
			sql.append(" MONTH(s.`audit_completed_date`) BETWEEN "+(Integer.parseInt(month)-2)+" And "+month);
			sql.append(" and audit_category = '"+category+"' group by m.store_name, audit_month order by audit_month");
			
			auditCategoryList = template.query(sql.toString(), new RowMapper<ScoreCardBO>() {
				@Override
				public ScoreCardBO mapRow(ResultSet rs, int rownumber) throws SQLException {
					ScoreCardBO e = new ScoreCardBO();
					
					e.setPercentage(rs.getString(1));
					e.setStoreName(rs.getString(2));
					e.setMonth(rs.getString(3));

					return e;
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
		return auditCategoryList;
	}
	public List<ScoreCardBO> getCircleScoreList(int circleId,String month) {
		List<ScoreCardBO> auditCategoryList = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("select sum(s.percentage)/count(s.percentage),m.store_name,EXTRACT(MONTH FROM s.audit_completed_date)AS audit_month from vowmeeasymp.vowmee_score_card_cal s, ");
			sql.append("vowmeeasymp.vowmee_store_master m where m.id=s.store_id and  m.circle="+circleId +" and ");
			sql.append(" MONTH(s.`audit_completed_date`) BETWEEN "+(Integer.parseInt(month)-2)+" And "+month);
			sql.append(" group by m.store_name, audit_month order by audit_month");
			
			auditCategoryList = template.query(sql.toString(), new RowMapper<ScoreCardBO>() {
				@Override
				public ScoreCardBO mapRow(ResultSet rs, int rownumber) throws SQLException {
					ScoreCardBO e = new ScoreCardBO();
					
					e.setPercentage(rs.getString(1));
					e.setStoreName(rs.getString(2));
					e.setMonth(rs.getString(3));

					return e;
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
		return auditCategoryList;
	}
	
	public List<String> getCategoryList() {
		List<String> auditCategoryList = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT distinct audit_category FROM vowmeeasymp.vowmee_audit_lookup order by audit_category");
			
			auditCategoryList = template.queryForList(sql.toString(), String.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return auditCategoryList;
	}

	public List<UserBean> getUserListforReport(){
		String sql="select distinct a.name, a.id,a.firstName,a.lastName from vowmeeasymp.vowmee_user_master a, vowmeeasymp.vowmee_audit_master b where a.id = b.auditor and b.audit_complete = 1 and  MONTH(CURDATE()) = MONTH(b.audit_date)";
		
		List<UserBean> rows = template.query(sql,new RowMapper<UserBean>(){  
		    @Override  
		    public UserBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	UserBean e=new UserBean(); 
		    	e.setId(rs.getInt(2));
		        e.setName(rs.getString(1)); 
		        e.setFirstName(rs.getString(3));
		        e.setLastName(rs.getString(4));
		        return e;  
		    }  
		    });  
		System.out.println(rows);
	return rows;
}
	
	public List<UserBean> getUserListforReportStoreWise(String store){
		String sql="select distinct a.name, a.id,a.firstName,a.lastName from vowmeeasymp.vowmee_user_master a, vowmeeasymp.vowmee_audit_master b where a.id = b.auditor and b.audit_complete = 1 and  MONTH(CURDATE()) = MONTH(b.audit_date) and b.store='"+store+"'";
		
		List<UserBean> rows = template.query(sql,new RowMapper<UserBean>(){  
		    @Override  
		    public UserBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	UserBean e=new UserBean(); 
		    	e.setId(rs.getInt(2));
		        e.setName(rs.getString(1)); 
		        e.setFirstName(rs.getString(3));
		        e.setLastName(rs.getString(4));
		        return e;  
		    }  
		    });  
		System.out.println(rows);
	return rows;
}

	public List<ScoreCardBO> getAuditorReport(int auditorId, String month) {
		System.out.println("auditorId " +auditorId);
		String sql="select a.store_id, EXTRACT(MONTH FROM a.audit_completed_date)AS audit_month,  sum(a.percentage)/count(a.percentage) from vowmeeasymp.vowmee_audit_master b, vowmeeasymp.vowmee_score_card_cal a where a.audit_id = b.id and b.auditor = '"+auditorId+"' and MONTH(b.`audit_date`) BETWEEN '"+(Integer.parseInt(month)-2)+"' And '"+month+"'"
				+ " group by a.store_id, audit_month order by audit_month";
		
		List<ScoreCardBO> rows = template.query(sql,new RowMapper<ScoreCardBO>(){  
		    @Override  
		    public ScoreCardBO mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	ScoreCardBO e=new ScoreCardBO(); 
		    	e.setStore_id(rs.getInt(1));
		    	e.setMonth(rs.getString(2));
		        e.setPercentage(rs.getString(3));  
		        return e;  
		    }  
		    });  
		System.out.println(rows);
	return rows;
	}
	
	public List<Store> getStoreObject() {
		String sql="SELECT id, store_name FROM vowmeeasymp.vowmee_store_master";
		
		List<Store> rows = template.query(sql,new RowMapper<Store>(){  
		    @Override  
		    public Store mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	Store e=new Store(); 
		    	e.setId(rs.getInt(1));
		        e.setStore_name(rs.getString(2));  
		        return e;  
		    }  
		    });  
		System.out.println(rows);
	return rows;
	}

public List<String> getAllCompletedStoreList(){
		List<String> auditList=null;
		try{
			String sql="SELECT distinct store FROM vowmeeasymp.vowmee_audit_master where audit_complete = '1'";
			auditList = template.queryForList(sql, String.class);
		}catch(Exception e){
			e.printStackTrace();
		}
		return auditList;
	}

public List<String> getAllCompletedStoreListByCircle(long circle){
	List<String> auditList=null;
	try{
		String sql="Select distinct a.store FROM vowmeeasymp.vowmee_audit_master a, vowmeeasymp.vowmee_store_master b, vowmeeasymp.vowmee_circle_master c where audit_complete = '1'and "
				+ "   a.store=b.id and b.circle='"+circle+"'";
		auditList = template.queryForList(sql, String.class);
	}catch(Exception e){
		e.printStackTrace();
	}
	return auditList;
}

/*public List<Integer> getAuditLookUpCount(){
	List<Integer> auditCategoryList=null;
	try{
		String sql="SELECT count(id) FROM vowmeeasymp.vowmee_audit_lookup";
		auditCategoryList = template.queryForList(sql, Integer.class);
	}catch(Exception e){
		e.printStackTrace();
	}
	return auditCategoryList;	
}
*/
public List<Integer> getAuditLookUpCount(){
	List<Integer> auditCategoryList=null;
	try{
		String sql="SELECT count(id) FROM vowmeeasymp.vowmee_audit_lookup where UPPER(flag)='Y'";
		auditCategoryList = template.queryForList(sql, Integer.class);
	}catch(Exception e){
		e.printStackTrace();
	}
	return auditCategoryList;	
}

/*public List<Integer> getMasterLookUpCount(long auditMasterId) {
	List<Integer> auditCategoryList=null;
	try{
		String sql="SELECT count(id) FROM vowmeeasymp.vowmee_audit_detail where audit = '"+auditMasterId+"'";
		auditCategoryList = template.queryForList(sql, Integer.class);
	}catch(Exception e){
		e.printStackTrace();
	}
	return auditCategoryList;
}*/
public List<Integer> getMasterLookUpCount(long auditMasterId) {
	List<Integer> auditCategoryList=null;
	try{
		String sql="SELECT count(a.id) FROM vowmeeasymp.vowmee_audit_detail a,vowmeeasymp.vowmee_audit_lookup b where audit = '"+auditMasterId+"'"
				+ " and b.id=a.audit_lkp and UPPER(b.flag)='Y'";
		auditCategoryList = template.queryForList(sql, Integer.class);
	}catch(Exception e){
		e.printStackTrace();
	}
	return auditCategoryList;
}

public List<Integer> getOptionLookUpCountforCategory(String category) {
	List<Integer> auditCategoryList=null;
	try{
		String sql="SELECT count(id) FROM vowmeeasymp.vowmee_audit_lookup  where audit_category='"+category+"'";
		auditCategoryList = template.queryForList(sql, Integer.class);
	}catch(Exception e){
		e.printStackTrace();
	}
	return auditCategoryList;
}

public List<Integer> getOptionAuditCountforCategory(String category,long auditMasterId) {
	List<Integer> auditCategoryList=null;
	try{
		String sql="SELECT count(a.id) FROM vowmeeasymp.vowmee_audit_lookup a,vowmeeasymp.vowmee_audit_detail b where a.audit_category='"+category+"'"
				+"and b.audit_lkp=a.id and b.audit='"+auditMasterId+"'";
		auditCategoryList = template.queryForList(sql, Integer.class);
	}catch(Exception e){
		e.printStackTrace();
	}
	return auditCategoryList;
}

public List<String> getMasterLookUpCountForOptional(long auditMasterId) {
	List<String> auditCategoryList=null;
	try{
		String sql="SELECT distinct b.audit_category FROM vowmeeasymp.vowmee_audit_detail a,vowmeeasymp.vowmee_audit_lookup b where audit = '"+auditMasterId+"'"
				+ " and b.id=a.audit_lkp and UPPER(b.flag)='N'";
		auditCategoryList = template.queryForList(sql, String.class);
	}catch(Exception e){
		e.printStackTrace();
	}
	return auditCategoryList;
}

public String getAuditorNameById(int user_id) {
	String sql="select firstName, lastName, name from vowmeeasymp.vowmee_user_master where id=?";
	Object obj = template.queryForObject(sql, new Object[]{user_id},new BeanPropertyRowMapper<UserBean>(UserBean.class));
	String user = ((UserBean)obj).getFirstName()+" "+((UserBean)obj).getLastName()+" (Email: "+((UserBean)obj).getName()+" )";
	return user;
}

public List<ScoreCardBO> getMICOReport(int micoId, String month) {
	String sql="select a.store_id, EXTRACT(MONTH FROM a.audit_completed_date)AS audit_month,  sum(a.percentage)/count(a.percentage) from vowmeeasymp.vowmee_audit_master b, vowmeeasymp.vowmee_score_card_cal a where a.audit_id = b.id  and MONTH(b.`audit_date`) BETWEEN '"+(Integer.parseInt(month)-2)+"' And '"+month+"'"
			+ " and a.store_id in (SELECT c.storeid FROM vowmeeasymp.vowmee_user_store_mapping c where c.userid="+micoId+")"
			+" group by a.store_id, audit_month order by audit_month";
	
	List<ScoreCardBO> rows = template.query(sql,new RowMapper<ScoreCardBO>(){  
	    @Override  
	    public ScoreCardBO mapRow(ResultSet rs, int rownumber) throws SQLException {  
	    	ScoreCardBO e=new ScoreCardBO(); 
	    	e.setStore_id(rs.getInt(1));
	    	e.setMonth(rs.getString(2));
	        e.setPercentage(rs.getString(3));  
	        return e;  
	    }  
	    });  
	System.out.println(rows);
	return rows;
}

public List<UserBean> getMICOUserList(){
	String sql="select distinct a.id, a.name, a.firstName,a.lastName from vowmeeasymp.vowmee_user_master a, vowmeeasymp.vowmee_audit_master b where user_role=6 and b.audit_complete = 1 and  MONTH(CURDATE()) = MONTH(b.audit_date) order by a.firstName";
	List<UserBean> rows = template.query(sql,new RowMapper<UserBean>(){  
	    @Override  
	    public UserBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
	    	UserBean e=new UserBean(); 
	    	e.setId(rs.getInt(1));
	        e.setName(rs.getString(2)); 
	        e.setFirstName(rs.getString(3));
	        e.setLastName(rs.getString(4));
	        return e;  
	    }  
	    });  
return rows;
}

public List<UserBean> getMICOUserListReport(String store){
	String sql="select distinct a.id, a.name, a.firstName,a.lastName from vowmeeasymp.vowmee_user_master a, vowmeeasymp.vowmee_audit_master b where user_role=6 and b.audit_complete = 1 and  MONTH(CURDATE()) = MONTH(b.audit_date) and b.store='"+store+"'"+ "order by a.firstName";
	List<UserBean> rows = template.query(sql,new RowMapper<UserBean>(){  
	    @Override  
	    public UserBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
	    	UserBean e=new UserBean(); 
	    	e.setId(rs.getInt(1));
	        e.setName(rs.getString(2)); 
	        e.setFirstName(rs.getString(3));
	        e.setLastName(rs.getString(4));
	        return e;  
	    }  
	    });  
return rows;
}

}
