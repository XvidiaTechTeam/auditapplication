package com.vowmee.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.vowmee.bean.AuditHistoryBean;
import com.vowmee.bean.AuditLookupBean;
import com.vowmee.bean.CircleBean;
import com.vowmee.bean.LocationBean;
import com.vowmee.bean.Store;
import com.vowmee.bean.StoreBean;
import com.vowmee.bean.UserBean;
import com.vowmee.bean.UserCircleVO;
import com.vowmee.bean.UserManagementBean;
import com.vowmee.bean.UserRoleBean;
import com.vowmee.bean.UserStatusBean;
import com.vowmee.bean.UserStoreMap;
import com.vowmee.bean.UserTypeBean;
import com.vowmee.utill.AuditHistoryUtil;

public class UserManagementDao {

	private JdbcTemplate jdbcTemplate;

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	public List<StoreBean> getStoreforStoreCode(String storeCode) {

		String sql = "select id,store_code,store_name,circle,store_status from vowmeeasymp.vowmee_store_master a where a.store_code='"+storeCode+"'";
		
		
		List<StoreBean> rows = jdbcTemplate.query(sql,new RowMapper<StoreBean>(){  
		    @Override  
		    public StoreBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	StoreBean e=new StoreBean();  
		        e.setId(rs.getInt(1));
		        e.setStore_code(rs.getString(2));
		        e.setStore_name(rs.getString(3));
		       e.setCircle(rs.getInt(4));
		       
		        e.setStore_status(rs.getInt(5));
		        return e;  
		    }  
		    });  
		System.out.println(rows);
		return rows;
	
	}
	
	public boolean checkForExistingUser(String email){
		boolean flag = false;
		try{
			String query = "SELECT name FROM vowmeeasymp.vowmee_user_master WHERE `name` = '"+email+"' ";
			List<String> rows = jdbcTemplate.query(query,new RowMapper<String>(){  
			    @Override  
			    public String mapRow(ResultSet rs, int rownumber) throws SQLException {  
			    	String e = null;
			    	if(rs!= null){
			    		e = rs.getString(1);
			    	}
			        return e;  
			    }  
			    });
			for(String emailId : rows){
				if(!emailId.equals(email)){
					flag = false;
				}else{
					flag= true;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		
		return flag;
	}
	
	public boolean checkForExistingStore(String storeCode){
		boolean flag = false;
		try{
			String query = "SELECT store_code FROM vowmeeasymp.vowmee_store_master WHERE `store_code` = '"+storeCode+"' ";
			List<String> rows = jdbcTemplate.query(query,new RowMapper<String>(){  
			    @Override  
			    public String mapRow(ResultSet rs, int rownumber) throws SQLException {  
			    	String e = null;
			    	if(rs!= null){
			    		e = rs.getString(1);
			    	}
			        return e;  
			    }  
			    });
			for(String store : rows){
				if(!store.equals(storeCode)){
					flag = false;
				}else{
					flag= true;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		
		return flag;
	}
	
	public boolean checkForExistingAudit(String auditCategory,String auditName){
		boolean flag = false;
		try{
			String query = "SELECT audit_category, audit_name FROM vowmeeasymp.vowmee_audit_lookup WHERE `audit_category` = '"+auditCategory+"' ";
			List<AuditLookupBean> rows = jdbcTemplate.query(query,new RowMapper<AuditLookupBean>(){  
			    @Override  
			    public AuditLookupBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
			    	AuditLookupBean audit=new AuditLookupBean();
			    	
			    	audit.setAudit_category(rs.getString(1));
			       audit.setAudit_name(rs.getString(2));
			        return audit;  
			    
			    }
			    });
			for(AuditLookupBean auditCat : rows){
				if(auditCat.getAudit_category().equals(auditCategory)){
					if(auditCat.getAudit_name().equals(auditName))
						flag=true;
					
				}else{
					flag=false;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		
		return flag;
	}
	
	public int insertUser(String json){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		int randomNumber = (int)(Math.random()*90000);
		String randomString = String.valueOf(randomNumber);
		UserManagementBean usermanagementBean = new Gson().fromJson(json, UserManagementBean.class);
		String query = "INSERT INTO `vowmeeasymp`.`vowmee_user_master` (`name`, `password`, `user_status`, `user_type`, `user_role`, `mobile`) VALUES ('"+usermanagementBean.getEmail()+"','"+randomString+"','"+usermanagementBean.getStatus()+"','"+usermanagementBean.getUsertype()+"','"+usermanagementBean.getUserrole()+"','"+usermanagementBean.getMobile()+"')";
		return jdbcTemplate.update(query);
	}
	
	public int updateUser(String json){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		UserManagementBean usermanagementBean = new Gson().fromJson(json, UserManagementBean.class);
		String query = "UPDATE `vowmeeasymp`.`vowmee_user_master` SET `mobile` = '"+usermanagementBean.getMobile()+"', `user_role` = '"+usermanagementBean.getUserrole()+"', `user_type` = '"+usermanagementBean.getUsertype()+"', `user_status` = '"+usermanagementBean.getStatus()+"' WHERE `name` = '"+usermanagementBean.getEmail()+"'; ";
		return jdbcTemplate.update(query);
	}
	
	public int updateUser(UserBean userBean){
		String query = "UPDATE `vowmeeasymp`.`vowmee_user_master` SET `mobile` = '"+userBean.getMobile()+"', "
				+ "`firstName` = '"+userBean.getFirstName()+"', "
				+ "`lastName` = '"+userBean.getLastName()+"', "
				
				+ "`user_role` = '"+userBean.getUser_role() +"', `user_status` = '"+userBean.getUser_status()+"' WHERE "
						+ "`id` = '"+userBean.getId()+"'; ";
		return jdbcTemplate.update(query);
	}
	
	public int updateStore(StoreBean storeBean){
		String query = "UPDATE `vowmeeasymp`.`vowmee_store_master` SET `store_name` = '"+storeBean.getStore_name()+"', "
				+ "`circle` = '"+storeBean.getCircle()+"', "
				+ "`store_name` = '"+storeBean.getStore_name()+"', "
				+ "`store_status` = '"+storeBean.getStore_status()+"', "
				+ "`store_desc` = '"+storeBean.getStore_name()+"' WHERE "
				+ "`store_code` = '"+storeBean.getStore_code()+"'; ";
		return jdbcTemplate.update(query);
	}
	
	
	public int updateUserProfile(UserBean userBean){
		String query = "UPDATE `vowmeeasymp`.`vowmee_user_master` SET `mobile` = '"+userBean.getMobile()+"', "
				+ "`firstName` = '"+userBean.getFirstName()+"', "
				+ "`lastName` = '"+userBean.getLastName()+" "
				+"' WHERE "	+ "`id` = "+userBean.getId();
		return jdbcTemplate.update(query);
	}
	
	public List<UserManagementBean> selectAllUser(){
		String sql = "SELECT name,user_status, user_type, user_role, mobile FROM vowmeeasymp.vowmee_user_master";
		List<UserManagementBean> rows = jdbcTemplate.query(sql,new RowMapper<UserManagementBean>(){  
		    @Override  
		    public UserManagementBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	UserManagementBean e=new UserManagementBean();  
		        e.setEmail(rs.getString(1));  
		        e.setMobile(rs.getString(5));
		        e.setUserrole(rs.getInt(4));
		        e.setUsertype(rs.getInt(3));
		        e.setStatus(rs.getInt(2));
		        return e;  
		    }  
		    });  
		System.out.println(rows);
		return rows;
	}
	
	//
	
	public List<UserBean> getUserList(int id){
		//final String idFinal = id+"";
		/*String sql = "select a.name,a.firstName,r.role_name,t.type_name,s.status_name,c.circle_name,a.id,a.lastName,a.mobile,"
				+ "a.user_role,a.user_status,a.user_type,a.user_circle from "
				+ "vowmeeasymp.vowmee_user_master a,vowmeeasymp.vowmee_circle_master c,"
				+ "	vowmeeasymp.vowmee_user_role r,vowmeeasymp.vowmee_user_status s, vowmeeasymp.vowmee_user_type t"
				+ "	where a.user_role=r.id and a.user_circle=c.id and a.user_status=s.id and a.user_type = t.id and a.id != "+id;*/
		String sql = "select a.name,a.firstName,r.role_name,s.status_name,a.id,a.lastName,a.mobile,"
				+ "a.user_role,a.user_status,a.user_type from "
				+ "vowmeeasymp.vowmee_user_master a,"
				+ "	vowmeeasymp.vowmee_user_role r,vowmeeasymp.vowmee_user_status s"
				+ "	where a.user_role=r.id and  a.user_status=s.id and a.id != "+id +" and"
						+ " upper(r.role_name)!='SUPERADMIN' ORDER BY a.id DESC";
		List<UserBean> rows = jdbcTemplate.query(sql,new RowMapper<UserBean>(){  
		    @Override  
		    public UserBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	UserBean e=new UserBean();  
		       e.setName(rs.getString(1));
		       List<String> storeList = getStoreList(rs.getInt(5)+"");
		       List<String> circleList = getCircleList(rs.getInt(5)+"");
		       
		       e.setFirstName(rs.getString(2));
		       e.setUserRole(rs.getString(3));
		    //   e.setUserType(rs.getString(4));
		       e.setUserStatus(rs.getString(4));
		       e.setId(rs.getInt(5));
		       e.setLastName(rs.getString(6));
		       e.setMobile(rs.getString(7));
		       e.setUser_role(rs.getInt(8));
		       e.setUser_status(rs.getInt(9));
		       e.setUser_type(rs.getInt(10));
		       e.setUserCircle(circleList);
		       e.setStoreName(storeList);
		        return e;  
		    }  
		    });  
		System.out.println(rows);
		return rows;
	}
	
	public List<String> getStoreList(String id){
		List<String> storeList = new ArrayList<>();
		try{
			List<Store> storeNameList = findStoreForUserId(id);
			for(Store storeName : storeNameList){
				storeList.add(storeName.getStore_name());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return storeList;
	}
	
	public List<String> getCircleList(String id){
		List<String> circleList = new ArrayList<>();
		try{
			List<CircleBean> circleNameList = findCircleForUserId(id);
			for(CircleBean CircleName : circleNameList){
				circleList.add(CircleName.getCircle_name());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return circleList;
	}
	public List<UserManagementBean> selectPerticularUser(String name){
		String sql = "SELECT name,user_status, user_type, user_role, mobile FROM vowmeeasymp.vowmee_user_master where name = '"+name+"'";
		List<UserManagementBean> rows = jdbcTemplate.query(sql,new RowMapper<UserManagementBean>(){  
		    @Override  
		    public UserManagementBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	UserManagementBean e=new UserManagementBean();  
		        e.setEmail(rs.getString(1));  
		        e.setMobile(rs.getString(5));
		        e.setUserrole(rs.getInt(4));
		        e.setUsertype(rs.getInt(3));
		        e.setStatus(rs.getInt(2));
		        return e;  
		    }  
		    });  
		System.out.println(rows);
		return rows;
	}
	
	public List<UserBean> selectPerticularUserForProfile(UserBean userBean){
		String sql = "select a.name,a.firstName,r.role_name,t.type_name,s.status_name,a.id,a.lastName,a.mobile,"
				+ "a.user_role,a.user_status,a.user_type, from "
				+ "vowmeeasymp.vowmee_user_master a,"
				+ "	vowmeeasymp.vowmee_user_role r,vowmeeasymp.vowmee_user_status s, vowmeeasymp.vowmee_user_type t"
				+ "	where a.user_role=r.id  and a.user_status=s.id and a.user_type = t.id"
				+" and a.id = "+userBean.getId();
		List<UserBean> rows = jdbcTemplate.query(sql,new RowMapper<UserBean>(){  
		    @Override  
		    public UserBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	UserBean e=new UserBean();  
		       e.setName(rs.getString(1));
		       e.setFirstName(rs.getString(2));
		       e.setUserRole(rs.getString(3));
		       e.setUserType(rs.getString(4));
		       e.setUserStatus(rs.getString(5));
		       
		       e.setId(rs.getInt(6));
		       e.setLastName(rs.getString(7));
		       e.setMobile(rs.getString(8));
		       e.setUser_role(rs.getInt(9));
		       e.setUser_status(rs.getInt(10));
		       e.setUser_type(rs.getInt(11));
		   
		        return e;  
		    }  
		    });  
		System.out.println(rows);
		return rows;
	}
	
	
	public List<UserRoleBean> selectAllRole(){
		String sql = "SELECT id, role_name FROM vowmeeasymp.vowmee_user_role;";
		List<UserRoleBean> rows = jdbcTemplate.query(sql,new RowMapper<UserRoleBean>(){  
		    @Override  
		    public UserRoleBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	UserRoleBean e=new UserRoleBean();  
		        e.setId(rs.getInt(1));  
		        e.setRole(rs.getString(2).toUpperCase());
		        return e;  
		    }  
		    });  
		System.out.println(rows);
		return rows;
	}
	
	public List<UserTypeBean> selectAllUserType(){
		String sql = "SELECT id, type_name FROM vowmeeasymp.vowmee_user_type";
		List<UserTypeBean> rows = jdbcTemplate.query(sql,new RowMapper<UserTypeBean>(){  
		    @Override  
		    public UserTypeBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	UserTypeBean e=new UserTypeBean();  
		        e.setId(rs.getInt(1));  
		        e.setType_name(rs.getString(2).toUpperCase());
		        return e;  
		    }  
		    });  
		System.out.println(rows);
		return rows;
	}
	
	public List<UserStatusBean> selectAllUserStatus(){
		String sql = "SELECT id, status_name FROM vowmeeasymp.vowmee_user_status;";
		List<UserStatusBean> rows = jdbcTemplate.query(sql,new RowMapper<UserStatusBean>(){  
		    @Override  
		    public UserStatusBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	UserStatusBean e=new UserStatusBean();  
		        e.setId(rs.getInt(1));  
		        e.setUserStatus(rs.getString(2).toUpperCase());
		        return e;  
		    }  
		    });  
		System.out.println(rows);
		return rows;
	}
	
	public List<LocationBean> selectAllLocation(){
		String sql = "SELECT id, name FROM vowmeeasymp.vowmee_location_master;";
		List<LocationBean> rows = jdbcTemplate.query(sql,new RowMapper<LocationBean>(){  
		    @Override  
		    public LocationBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	LocationBean e=new LocationBean();  
		        e.setId(rs.getInt(1));  
		        e.setName(rs.getString(2));
		        return e;  
		    }  
		    });  
		System.out.println(rows);
		return rows;
	}

	public void saveUploadUser(List<UserManagementBean> userUploadList) {
		for(UserManagementBean usermanagementBean: userUploadList){
			 boolean flag = checkForExistingUser(usermanagementBean.getEmail());
			 if(!flag){
				 String query = "INSERT INTO `vowmeeasymp`.`vowmee_user_master`"
				 		+ " (`name`, `password`, `user_status`, `user_type`, `user_role`, `mobile`)"
				 		+ " VALUES ('"+usermanagementBean.getEmail()+"','"+usermanagementBean.getPassword()+"','"+usermanagementBean.getStatus()+"','"+usermanagementBean.getUsertype()+"','"+usermanagementBean.getUserrole()+"','"+usermanagementBean.getMobile()+"')";
					jdbcTemplate.update(query);
			 }
			
		}
	}
	
	public List<UserManagementBean> getExistedUsers(List<UserManagementBean> userUploadList){
		List<UserManagementBean> existedUserList = new ArrayList<UserManagementBean>();
	
		for(UserManagementBean usermanagementBean: userUploadList){
			 boolean flag = checkForExistingUser(usermanagementBean.getEmail());
			 if(flag){
				 usermanagementBean.setReasonForFail("Already Existed");
				 existedUserList.add(usermanagementBean);
			 }
		}
		return existedUserList;
	}
	
	public List<StoreBean> getExistedStores(List<StoreBean> storeUploadList){
		List<StoreBean> existedUserList = new ArrayList<StoreBean>();
	
		for(StoreBean storeBean: storeUploadList){
			 boolean flag = checkForExistingStore(storeBean.getStore_code());
			 if(flag){
				 storeBean.setReasonForFail("Already Existed");
				 existedUserList.add(storeBean);
			 }
		}
		return existedUserList;
	}
	
	public List<AuditLookupBean> getExistedAudit(List<AuditLookupBean> auditUploadList){
		List<AuditLookupBean> existedUserList = new ArrayList<AuditLookupBean>();
	
		for(AuditLookupBean auditBean: auditUploadList){
			 boolean flag = checkForExistingAudit(auditBean.getAudit_category(),auditBean.getAudit_name());
			 if(flag){
				 auditBean.setReasonForFail("Already Existed");
				 existedUserList.add(auditBean);
			 }
		}
		return existedUserList;
	}
	
	public Map<String, Long> insertNewUsers(List<UserManagementBean> userUploadList){
		Map<String,Long> userFinalIds =new HashMap<String,Long>(); 
	
		for(UserManagementBean usermanagementBean: userUploadList){
			long uniqueId=0;
			 boolean flag = checkForExistingUser(usermanagementBean.getEmail());
			 if(!flag){
				 final UserManagementBean userBeanFinal = usermanagementBean;
					
					KeyHolder keyHolder = new GeneratedKeyHolder();
					final String INSERT_SQL = "insert into vowmeeasymp.vowmee_user_master(name,mobile,user_status,password,"
							+ "user_role,lastName,firstName,created_time) values(?,?,?,?,?,?,?,?)";
					jdbcTemplate.update(
						    new PreparedStatementCreator() {
						        public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
						            PreparedStatement ps =
						                connection.prepareStatement(INSERT_SQL, new String[] {"id"});
						            ps.setString(1, userBeanFinal.getEmail());
						            ps.setString(2, userBeanFinal.getMobile());
						            ps.setInt(3, userBeanFinal.getStatus());
						            ps.setString(4,userBeanFinal.getPassword());
						           // ps.setInt(5, userBeanFinal.getUser_type());
						            if(userBeanFinal.getRoleName()!=null){
						            	 ps.setInt(5, Integer.parseInt(userBeanFinal.getRoleName()));
						            }else{
						            	 ps.setInt(5, 0);
						            }
						            
						           
						           
						            ps.setString(6, userBeanFinal.getLastName());
						            ps.setString(7, userBeanFinal.getFirstName());
						            ps.setDate(8, new java.sql.Date(System.currentTimeMillis()));
						            return ps;
						        }
						    },
						    keyHolder);
					System.out.println("keyHolder "+ keyHolder.getKey());
					uniqueId = (long) keyHolder.getKey();
					userFinalIds.put(userBeanFinal.getEmail(), uniqueId);
			 }
		}
		
		return userFinalIds;
	}
	
	
	public Map<String, Long> insertNewStores(List<StoreBean> storeUploadList){
		Map<String,Long> userFinalIds =new HashMap<String,Long>(); 
	
		for(StoreBean storeBean: storeUploadList){
			long uniqueId=0;
			 boolean flag = checkForExistingStore(storeBean.getStore_code());
			 if(!flag){
				 final StoreBean userBeanFinal = storeBean;
					
					KeyHolder keyHolder = new GeneratedKeyHolder();
					final String INSERT_SQL = "insert into vowmeeasymp.vowmee_store_master(store_code,store_name,store_desc,location,"
							+ "circle) values(?,?,?,?,?)";
					jdbcTemplate.update(
						    new PreparedStatementCreator() {
						        public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
						            PreparedStatement ps =
										connection.prepareStatement(INSERT_SQL, new String[] { "id" });
								ps.setString(1, userBeanFinal.getStore_code());
								ps.setString(2, userBeanFinal.getStore_name());
								ps.setString(3, userBeanFinal.getStore_name());
								if (userBeanFinal.getCircle_name() != null) {
									ps.setInt(4, Integer.parseInt(userBeanFinal.getLocation()));
								} else {
						            	 ps.setInt(4, 0);
						            }

								

								if (userBeanFinal.getCircle_name() != null) {
									ps.setInt(5, Integer.parseInt(userBeanFinal.getCircle_name()));
								} else {
						            	 ps.setInt(5, 0);
						            }
						           return ps;
						        }
						    },
						    keyHolder);
					System.out.println("keyHolder "+ keyHolder.getKey());
					uniqueId = (long) keyHolder.getKey();
					userFinalIds.put(userBeanFinal.getStore_code(), uniqueId);
			 }
		}
		
		return userFinalIds;
	}
	
	public Map<String, Long> insertNewAudit(List<AuditLookupBean> auditUploadList){
		Map<String,Long> userFinalIds =new HashMap<String,Long>(); 
	
		for(AuditLookupBean auditBean: auditUploadList){
			long uniqueId=0;
			 boolean flag = checkForExistingAudit(auditBean.getAudit_category(),auditBean.getAudit_name());
			 if(!flag){
				 final AuditLookupBean userBeanFinal = auditBean;
					
					KeyHolder keyHolder = new GeneratedKeyHolder();
					final String INSERT_SQL = "insert into vowmeeasymp.vowmee_audit_lookup(audit_category,audit_name,audit_desc,circle,"
							+ "flag) values(?,?,?,?,?)";
					jdbcTemplate.update(
						    new PreparedStatementCreator() {
						        public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
						            PreparedStatement ps =
						                connection.prepareStatement(INSERT_SQL, new String[] {"id"});
						            ps.setString(1, userBeanFinal.getAudit_category());
						            ps.setString(2, userBeanFinal.getAudit_name());
						            ps.setString(3, userBeanFinal.getAudit_name());
						            if(userBeanFinal.getCircle()!=null){
						            	 ps.setInt(4, Integer.parseInt(userBeanFinal.getCircle()));
						            }else{
						            	 ps.setInt(4, 0);
						            }
						            ps.setString(5, userBeanFinal.getFlag());
						           return ps;
						        }
						    },
						    keyHolder);
					System.out.println("keyHolder "+ keyHolder.getKey());
					uniqueId = (long) keyHolder.getKey();
					userFinalIds.put(userBeanFinal.getAudit_category(), uniqueId);
			 }
		}
		
		return userFinalIds;
	}
	
	
	
	public List<Store> findStoreForCircle(String circle){
		String sql = "SELECT id, store_name FROM vowmeeasymp.vowmee_store_master where circle='"+circle+"'";
		List<Store> rows = jdbcTemplate.query(sql,new RowMapper<Store>(){  
		    @Override  
		    public Store mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	Store e=new Store();  
		        e.setId(rs.getInt(1));  
		        e.setStore_name(rs.getString(2));
		        return e;  
		    }  
		    });  
		System.out.println(rows);
		return rows;
	}
	
	public List<Store> findStoreForUserId(String userId){
		String sql = "SELECT b.id, b.store_name  FROM vowmeeasymp.vowmee_user_store_mapping a,vowmeeasymp.vowmee_store_master b "
				+ "where a.userid='"+userId+"' and a.storeid=b.id";
		List<Store> rows = jdbcTemplate.query(sql,new RowMapper<Store>(){  
		    @Override  
		    public Store mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	Store e=new Store();  
		        e.setId(rs.getInt(1));  
		        e.setStore_name(rs.getString(2));
		        return e;  
		    }  
		    });  
		System.out.println(rows);
		return rows;
	}
	
	public List<CircleBean> findCircleForUserId(String userId){
		String sql = "SELECT b.id, b.circle_name  FROM vowmeeasymp.vowmee_user_circle_mapping a,vowmeeasymp.vowmee_circle_master b "
				+ "where a.userid='"+userId+"' and a.circleId=b.id";
		List<CircleBean> rows = jdbcTemplate.query(sql,new RowMapper<CircleBean>(){  
		    @Override  
		    public CircleBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	CircleBean e=new CircleBean();  
		        e.setId(rs.getInt(1));  
		        e.setCircle_name(rs.getString(2));
		        return e;  
		    }  
		    });  
		System.out.println(rows);
		return rows;
	}
	
	
	public List<Integer> getIdListOfStore(int userId){
		List<Integer> auditList=null;
		try{
			String sql="SELECT id FROM vowmeeasymp.vowmee_user_store_mapping where userid = "+userId;
			auditList = jdbcTemplate.queryForList(sql, Integer.class);
		}catch(Exception e){
			e.printStackTrace();
		}
		return auditList;
	}
	
	public List<UserBean> getAdminUserList(int id){
		//final String idFinal = id+"";
		/*String sql = "select a.name,a.firstName,r.role_name,t.type_name,s.status_name,c.circle_name,a.id,a.lastName,a.mobile,"
				+ "a.user_role,a.user_status,a.user_type,a.user_circle from "
				+ "vowmeeasymp.vowmee_user_master a,vowmeeasymp.vowmee_circle_master c,"
				+ "	vowmeeasymp.vowmee_user_role r,vowmeeasymp.vowmee_user_status s, vowmeeasymp.vowmee_user_type t"
				+ "	where a.user_role=r.id and a.user_circle=c.id and a.user_status=s.id and a.user_type = t.id and a.id != "+id;*/
		String sql = "select a.name,a.firstName,r.role_name,s.status_name,a.id,a.lastName,a.mobile,"
				+ "a.user_role,a.user_status,a.user_type,a.id from "
				+ "vowmeeasymp.vowmee_user_master a,"
				+ "	vowmeeasymp.vowmee_user_role r,vowmeeasymp.vowmee_user_status s"
				+ "	where a.user_role=r.id  and a.user_status=s.id and a.id != "+id 
						+ " ORDER BY a.id DESC";
		List<UserBean> rows = jdbcTemplate.query(sql,new RowMapper<UserBean>(){  
		    @Override  
		    public UserBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	UserBean e=new UserBean();  
		       e.setName(rs.getString(1));
		       List<String> storeList = getStoreList(rs.getInt(11)+"");
		       List<String>circleList=getCircleList(rs.getInt(11)+"");
		       e.setFirstName(rs.getString(2));
		       e.setUserRole(rs.getString(3));
		    //   e.setUserType(rs.getString(4));
		       e.setUserStatus(rs.getString(4));
		      
		       e.setId(rs.getInt(5));
		       e.setLastName(rs.getString(6));
		       e.setMobile(rs.getString(7));
		       e.setUser_role(rs.getInt(8));
		       e.setUser_status(rs.getInt(9));
		       e.setUser_type(rs.getInt(10));
		       e.setUserCircle(circleList);
		       e.setStoreName(storeList);
		        return e;  
		    }  
		    });  
		System.out.println(rows);
		return rows;
	}
	public List<UserBean> getUserListForMonth(int id, int date){
		//final String idFinal = id+"";
		/*String sql = "select a.name,a.firstName,r.role_name,t.type_name,s.status_name,c.circle_name,a.id,a.lastName,a.mobile,"
				+ "a.user_role,a.user_status,a.user_type,a.user_circle from "
				+ "vowmeeasymp.vowmee_user_master a,vowmeeasymp.vowmee_circle_master c,"
				+ "	vowmeeasymp.vowmee_user_role r,vowmeeasymp.vowmee_user_status s, vowmeeasymp.vowmee_user_type t"
				+ "	where a.user_role=r.id and a.user_circle=c.id and a.user_status=s.id and a.user_type = t.id and a.id != "+id;*/
		String sql = "select a.name,a.firstName,r.role_name,s.status_name,a.id,a.lastName,a.mobile,"
				+ "a.user_role,a.user_status,a.user_type,a.id from "
				+ "vowmeeasymp.vowmee_user_master a,"
				+ "	vowmeeasymp.vowmee_user_role r,vowmeeasymp.vowmee_user_status s"
				+ "	where a.user_role=r.id  and a.user_status=s.id and a.id != "+id +" and"
						+ " upper(r.role_name)!='SUPERADMIN' and created_time >= now()-interval "+date+" month ORDER BY created_time desc";
		List<UserBean> rows = jdbcTemplate.query(sql,new RowMapper<UserBean>(){  
		    @Override  
		    public UserBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	UserBean e=new UserBean();  
			       e.setName(rs.getString(1));
			       List<String> storeList = getStoreList(rs.getInt(11)+"");
			       List<String>circleList=getCircleList(rs.getInt(11)+"");
			       e.setFirstName(rs.getString(2));
			       e.setUserRole(rs.getString(3));
			    //   e.setUserType(rs.getString(4));
			       e.setUserStatus(rs.getString(4));
			      
			       e.setId(rs.getInt(5));
			       e.setLastName(rs.getString(6));
			       e.setMobile(rs.getString(7));
			       e.setUser_role(rs.getInt(8));
			       e.setUser_status(rs.getInt(9));
			       e.setUser_type(rs.getInt(10));
			       e.setUserCircle(circleList);
			       e.setStoreName(storeList);
			        return e;  
			    }  
		    });  
		System.out.println(rows);
		return rows;
	}
	
	public List<UserBean> getAdminUserListForMonth(int id,int date){
		//final String idFinal = id+"";
		/*String sql = "select a.name,a.firstName,r.role_name,t.type_name,s.status_name,c.circle_name,a.id,a.lastName,a.mobile,"
				+ "a.user_role,a.user_status,a.user_type,a.user_circle from "
				+ "vowmeeasymp.vowmee_user_master a,vowmeeasymp.vowmee_circle_master c,"
				+ "	vowmeeasymp.vowmee_user_role r,vowmeeasymp.vowmee_user_status s, vowmeeasymp.vowmee_user_type t"
				+ "	where a.user_role=r.id and a.user_circle=c.id and a.user_status=s.id and a.user_type = t.id and a.id != "+id;*/
		String sql = "select a.name,a.firstName,r.role_name,s.status_name,a.id,a.lastName,a.mobile,"
				+ "a.user_role,a.user_status,a.user_type from "
				+ "vowmeeasymp.vowmee_user_master a,"
				+ "	vowmeeasymp.vowmee_user_role r,vowmeeasymp.vowmee_user_status s"
				+ "	where a.user_role=r.id  and a.user_status=s.id and a.id != "+id 
						+ " and created_time >= now()-interval "+date+" month ORDER BY created_time desc";
		List<UserBean> rows = jdbcTemplate.query(sql,new RowMapper<UserBean>(){  
		    @Override  
		    public UserBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	UserBean e=new UserBean();  
			       e.setName(rs.getString(1));
			       List<String> storeList = getStoreList(rs.getInt(5)+"");
			       List<String>circleList=getCircleList(rs.getInt(5)+"");
			       e.setFirstName(rs.getString(2));
			       e.setUserRole(rs.getString(3));
			    //   e.setUserType(rs.getString(4));
			       e.setUserStatus(rs.getString(4));
			      
			       e.setId(rs.getInt(5));
			       e.setLastName(rs.getString(6));
			       e.setMobile(rs.getString(7));
			       e.setUser_role(rs.getInt(8));
			       e.setUser_status(rs.getInt(9));
			       e.setUser_type(rs.getInt(10));
			       e.setUserCircle(circleList);
			       e.setStoreName(storeList);
			        return e;  
			    }  
		    });  
		System.out.println(rows);
		return rows;
	}
	
	public List<StoreBean> getStoreList(){
		
		String sql = "select a.store_code, a.store_name, a.store_desc, c.circle_name, s.status_name,a.store_status from vowmeeasymp.vowmee_store_master a, vowmeeasymp.vowmee_circle_master c,vowmeeasymp.vowmee_user_status s where a.circle=c.id and s.id=a.store_status";
		List<StoreBean> rows = jdbcTemplate.query(sql,new RowMapper<StoreBean>(){  
		    @Override  
		    public StoreBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	StoreBean e=new StoreBean();  
		       e.setStore_code(rs.getString(1));
		       e.setStore_name(rs.getString(2));
		       e.setStore_desc(rs.getString(3));
		    //   e.setUserType(rs.getString(4));
		       e.setCircle_name(rs.getString(4));
		       e.setStoreStatus(rs.getString(5));
		       e.setStore_status(rs.getInt(6));
		        return e;  
		    }  
		    });  
		System.out.println(rows);
		return rows;
	}
public List<AuditLookupBean> getQuestionsList(){
		
		String sql = "select a.audit_category,a.audit_name,a.id from vowmeeasymp.vowmee_audit_lookup a ORDER BY a.audit_category desc";
		List<AuditLookupBean> rows = jdbcTemplate.query(sql,new RowMapper<AuditLookupBean>(){  
		    @Override  
		    public AuditLookupBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	AuditLookupBean e=new AuditLookupBean();  
		       e.setAudit_category(rs.getString(1));
		       e.setAudit_name(rs.getString(2));
		        return e;  
		    }  
		    });  
		System.out.println(rows);
		return rows;
	}
	
	public List<UserBean> getUserListFor1Week(int id, int date){
		//final String idFinal = id+"";
		/*String sql = "select a.name,a.firstName,r.role_name,t.type_name,s.status_name,c.circle_name,a.id,a.lastName,a.mobile,"
				+ "a.user_role,a.user_status,a.user_type,a.user_circle from "
				+ "vowmeeasymp.vowmee_user_master a,vowmeeasymp.vowmee_circle_master c,"
				+ "	vowmeeasymp.vowmee_user_role r,vowmeeasymp.vowmee_user_status s, vowmeeasymp.vowmee_user_type t"
				+ "	where a.user_role=r.id and a.user_circle=c.id and a.user_status=s.id and a.user_type = t.id and a.id != "+id;*/
		String sql = "select a.name,a.firstName,r.role_name,s.status_name,a.id,a.lastName,a.mobile,"
				+ "a.user_role,a.user_status,a.user_type,a.id from "
				+ "vowmeeasymp.vowmee_user_master a,"
				+ "	vowmeeasymp.vowmee_user_role r,vowmeeasymp.vowmee_user_status s"
				+ "	where a.user_role=r.id and  a.user_status=s.id and a.id != "+id +" and"
						+ " upper(r.role_name)!='SUPERADMIN' and created_time >= now()-interval "+date+" day ORDER BY created_time desc";
		List<UserBean> rows = jdbcTemplate.query(sql,new RowMapper<UserBean>(){  
		    @Override  
		    public UserBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	UserBean e=new UserBean();  
			       e.setName(rs.getString(1));
			       List<String> storeList = getStoreList(rs.getInt(11)+"");
			       List<String>circleList=getCircleList(rs.getInt(11)+"");
			       e.setFirstName(rs.getString(2));
			       e.setUserRole(rs.getString(3));
			    //   e.setUserType(rs.getString(4));
			       e.setUserStatus(rs.getString(4));
			      
			       e.setId(rs.getInt(5));
			       e.setLastName(rs.getString(6));
			       e.setMobile(rs.getString(7));
			       e.setUser_role(rs.getInt(8));
			       e.setUser_status(rs.getInt(9));
			       e.setUser_type(rs.getInt(10));
			       e.setUserCircle(circleList);
			       e.setStoreName(storeList);
			        return e;  
			    }  
		    });  
		System.out.println(rows);
		return rows;
	}
	
	public List<UserBean> getAdminUserListFor1Week(int id,int date){
		//final String idFinal = id+"";
		/*String sql = "select a.name,a.firstName,r.role_name,t.type_name,s.status_name,c.circle_name,a.id,a.lastName,a.mobile,"
				+ "a.user_role,a.user_status,a.user_type,a.user_circle from "
				+ "vowmeeasymp.vowmee_user_master a,vowmeeasymp.vowmee_circle_master c,"
				+ "	vowmeeasymp.vowmee_user_role r,vowmeeasymp.vowmee_user_status s, vowmeeasymp.vowmee_user_type t"
				+ "	where a.user_role=r.id and a.user_circle=c.id and a.user_status=s.id and a.user_type = t.id and a.id != "+id;*/
		String sql = "select a.name,a.firstName,r.role_name,s.status_name,a.id,a.lastName,a.mobile,"
				+ "a.user_role,a.user_status,a.user_type from "
				+ "vowmeeasymp.vowmee_user_master a,"
				+ "	vowmeeasymp.vowmee_user_role r,vowmeeasymp.vowmee_user_status s"
				+ "	where a.user_role=r.id and a.user_status=s.id and a.id != "+id 
						+ " and created_time >= now()-interval "+date+" day ORDER BY created_time desc";
		List<UserBean> rows = jdbcTemplate.query(sql,new RowMapper<UserBean>(){  
		    @Override  
		    public UserBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	UserBean e=new UserBean();  
			       e.setName(rs.getString(1));
			       List<String> storeList = getStoreList(rs.getInt(5)+"");
			       List<String>circleList=getCircleList(rs.getInt(5)+"");
			       e.setFirstName(rs.getString(2));
			       e.setUserRole(rs.getString(3));
			    //   e.setUserType(rs.getString(4));
			       e.setUserStatus(rs.getString(4));
			      
			       e.setId(rs.getInt(5));
			       e.setLastName(rs.getString(6));
			       e.setMobile(rs.getString(7));
			       e.setUser_role(rs.getInt(8));
			       e.setUser_status(rs.getInt(9));
			       e.setUser_type(rs.getInt(10));
			       e.setUserCircle(circleList);
			       e.setStoreName(storeList);
			        return e;  
			    }  
		    });  
		System.out.println(rows);
		return rows;
	}
	
	public List<UserCircleVO> getCircle(){
		String sql = "SELECT id,circle_code,circle_name FROM vowmeeasymp.vowmee_circle_master";
		List<UserCircleVO> rows = jdbcTemplate.query(sql,new RowMapper<UserCircleVO>(){  
		    @Override  
		    public UserCircleVO mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	UserCircleVO e=new UserCircleVO();  
		        e.setId(rs.getInt(1));
		        e.setCircleCode(rs.getString(2));
		        e.setCircleName(rs.getString(3));
		        return e;  
		    }  
		    });  
		System.out.println(rows);
		return rows;
	}
	
	public List<Store> selectAllStores(){
		String sql = "SELECT id, store_name FROM vowmeeasymp.vowmee_store_master;";
		List<Store> rows = jdbcTemplate.query(sql,new RowMapper<Store>(){  
		    @Override  
		    public Store mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	Store e=new Store();  
		        e.setId(rs.getInt(1));  
		        e.setStore_name(rs.getString(2));
		        return e;  
		    }  
		    });  
		System.out.println(rows);
		return rows;
	}
	
	public List<CircleBean> selectAllCircle(){
		String sql = "SELECT id, circle_name FROM vowmeeasymp.vowmee_circle_master;";
		List<CircleBean> rows = jdbcTemplate.query(sql,new RowMapper<CircleBean>(){  
		    @Override  
		    public CircleBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	CircleBean e=new CircleBean();  
		        e.setId(rs.getInt(1));  
		        e.setCircle_name(rs.getString(2));
		        return e;  
		    }  
		    });  
		System.out.println(rows);
		return rows;
	}
	
	
}
