package com.vowmee.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.vowmee.bean.AuditHistoryBean;

public class AuditHistoryDAO {

	JdbcTemplate template;

	public void setTemplate(JdbcTemplate template) {
		this.template = template;
	}
	public long insertAuditHistory(AuditHistoryBean auditHistory){
		final AuditHistoryBean auditHistoryFinal = auditHistory;
		long uniqueId=0;
		KeyHolder keyHolder = new GeneratedKeyHolder();
		try{
			final String INSERT_SQL = "insert into vowmeeasymp.vowmee_audit_detail_history(parentid,createdby,field,oldvalue,"
					+ "newvalue,createddate) values(?,?,?,?,?,?)";
			template.update(
				    new PreparedStatementCreator() {
				        public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				            PreparedStatement ps =
				                connection.prepareStatement(INSERT_SQL, new String[] {"id"});
				            ps.setInt(1, auditHistoryFinal.getParentid());
				            ps.setInt(2, auditHistoryFinal.getCreatedby());
				            ps.setString(3, auditHistoryFinal.getField());
				            ps.setString(4,auditHistoryFinal.getOldvalue());
				           // ps.setInt(5, userBeanFinal.getUser_type());
				            ps.setString(5, auditHistoryFinal.getNewvalue());
				            ps.setDate(6, new java.sql.Date(System.currentTimeMillis()));
				           
				            return ps;
				        }
				    },
				    keyHolder);
			System.out.println("keyHolder "+ keyHolder.getKey());
			uniqueId = (long) keyHolder.getKey();
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return uniqueId;
	}
}
