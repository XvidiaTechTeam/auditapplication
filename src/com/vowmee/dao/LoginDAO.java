package com.vowmee.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.vowmee.bean.UserBean;
import com.vowmee.bean.UserRoleBean;
import com.vowmee.bean.UserStatusBean;
import com.vowmee.bean.UserTypeBean;

public class LoginDAO {
	JdbcTemplate template;

	public void setTemplate(JdbcTemplate template) {
		this.template = template;
	}
	
	public UserBean getUserDetailByName(String username){
		String sql="select name,password,user_status,user_role,mobile,lastName,"
				+ "firstName,id from vowmeeasymp.vowmee_user_master where UPPER(name)= '"+username.toUpperCase()+"'";
		List<UserBean> rows = template.query(sql,new RowMapper<UserBean>(){  
		    @Override  
		    public UserBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	UserBean e=new UserBean();  
		       e.setName(rs.getString(1));
		      e.setPassword(rs.getString(2));
		      e.setUser_status(rs.getInt(3));
		      e.setUser_role(rs.getInt(4));
		      e.setMobile(rs.getString(5));
		      e.setLastName(rs.getString(6));
		      e.setFirstName(rs.getString(7));
		      e.setId(rs.getInt(8));
		        return e;  
		    }  
		    });  
		System.out.println(rows);
		UserBean userBean = null;
		if(rows.size()>0){
			userBean = rows.get(0);
		}
		
		return userBean;
		//return template.queryForObject(sql, new Object[]{username.toUpperCase()},new BeanPropertyRowMapper<UserBean>(UserBean.class));
	}
	
	public UserStatusBean getUserStatusById(int statusId){
		String sql="select * from vowmeeasymp.vowmee_user_status where id=?";
		return template.queryForObject(sql, new Object[]{statusId},new BeanPropertyRowMapper<UserStatusBean>(UserStatusBean.class));
		 
	}
	public UserTypeBean getUserTypeById(int userTypeId){
		String sql="select * from vowmeeasymp.vowmee_user_type where id=?";
		return template.queryForObject(sql, new Object[]{userTypeId},new BeanPropertyRowMapper<UserTypeBean>(UserTypeBean.class));
		 
	}
	public UserRoleBean getUserRoleById(int roleId){
		String sql="select * from vowmeeasymp.vowmee_user_role where id=?";
		return template.queryForObject(sql, new Object[]{roleId},new BeanPropertyRowMapper<UserRoleBean>(UserRoleBean.class));
		 
	}
	public int updatePassword(UserBean userBean){
		String sql="update vowmeeasymp.vowmee_user_master set password='"+userBean.getPassword()+"' where name='"+userBean.getName()+"'";
		return template.update(sql);
	}
	
	public String getCircleName(int circleId){
		String auditList=null;
		try{
			String sql="SELECT circle_name FROM vowmeeasymp.vowmee_circle_master where id = "+circleId;
			auditList = template.queryForObject(sql,new RowMapper<String>(){  
			    @Override  
			    public String mapRow(ResultSet rs, int rownumber) throws SQLException {  
			        return rs.getString(1);  
			    }  
			    });  
		}catch(Exception e){
			e.printStackTrace();
		}
		return auditList;
	}
	
	public String getRoleName(int roleId){
		String sql = "SELECT role_name FROM vowmeeasymp.vowmee_user_role where id="+roleId;
		String  rows = template.queryForObject(sql,new RowMapper<String >(){  
		    @Override  
		    public String  mapRow(ResultSet rs, int rownumber) throws SQLException {  
		        return rs.getString(1);  
		    }  
		    });  
		System.out.println(rows);
		return rows;
	}
	
	public List<UserBean> getUserList(UserBean userBean){
		String sql = "select a.name,a.firstName,r.role_name,t.type_name,s.status_name,a.id,a.lastName,a.mobile,"
				+ "a.user_role,a.user_status,a.user_typefrom "
				+ "vowmeeasymp.vowmee_user_master a,"
				+ "	vowmeeasymp.vowmee_user_role r,vowmeeasymp.vowmee_user_status s, vowmeeasymp.vowmee_user_type t"
				+ "	where a.user_role="+userBean.getUser_role()
				+" and a.user_status="+userBean.getUser_status()+" and a.user_type = "+userBean.getUser_type()
				+" and a.id = "+userBean.getId();
		List<UserBean> rows = template.query(sql,new RowMapper<UserBean>(){  
		    @Override  
		    public UserBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	UserBean e=new UserBean();  
		       e.setName(rs.getString(1));
		       e.setFirstName(rs.getString(2));
		       e.setUserRole(rs.getString(3));
		       e.setUserType(rs.getString(4));
		       e.setUserStatus(rs.getString(5));
		       e.setId(rs.getInt(6));
		       e.setLastName(rs.getString(7));
		       e.setMobile(rs.getString(8));
		       e.setUser_role(rs.getInt(9));
		       e.setUser_status(rs.getInt(10));
		       e.setUser_type(rs.getInt(11));
		      
		        return e;  
		    }  
		    });  
		System.out.println(rows);
		return rows;
	}
	
	public String getCompletedCount(int userId) {
		String sql = "SELECT count(audit_complete) from vowmeeasymp.vowmee_audit_master where audit_complete= 1 and auditor = " + userId;
		String completeCount = template.queryForObject(sql,new RowMapper<String >(){  
		    @Override  
		    public String  mapRow(ResultSet rs, int rownumber) throws SQLException { 
		    	int count = rs.getInt(1);
		    	
		        return String.valueOf(count);  
		    }  
		    });
		
		return completeCount;
	}
	
	public String getIncompletedCount(int userId) {
		String sql = "SELECT count(audit_complete) from vowmeeasymp.vowmee_audit_master where audit_complete= 0 and auditor = " + userId;
		String completeCount = template.queryForObject(sql,new RowMapper<String >(){  
		    @Override  
		    public String  mapRow(ResultSet rs, int rownumber) throws SQLException { 
		    	int count = rs.getInt(1);
		    	
		        return String.valueOf(count);  
		    }  
		    });
		
		return completeCount;
	}
}
