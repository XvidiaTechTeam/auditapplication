package com.vowmee.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.vowmee.bean.AuditLookupBean;
import com.vowmee.bean.CircleBean;
import com.vowmee.bean.LocationBean;
import com.vowmee.bean.Store;
import com.vowmee.bean.StoreBean;
import com.vowmee.bean.UserBean;
import com.vowmee.bean.UserCircleBean;
import com.vowmee.bean.UserManagementBean;
import com.vowmee.bean.UserRoleBean;
import com.vowmee.bean.UserStatusBean;
import com.vowmee.bean.UserStoreMap;
import com.vowmee.bean.UserTypeBean;

public class RegisterDAO {
	JdbcTemplate template;

	public void setTemplate(JdbcTemplate template) {
		this.template = template;
	}

	public int save(UserBean userBean, int statusId, int userTypeId, int userRoleId){
		String sql="insert into vowmeeasymp.vowmee_user_master(name,mobile,user_status,password,user_type,user_role) values('"+userBean.getName()+"','"+userBean.getMobile()+"','"+statusId+"','"+userBean.getPassword()+"','"+userTypeId+"','"+userRoleId+"')";
		return template.update(sql);
	}
	
	public long insertNewUser(UserBean userBean){
		final UserBean userBeanFinal = userBean;
		long uniqueId=0;
		KeyHolder keyHolder = new GeneratedKeyHolder();
		final String INSERT_SQL = "insert into vowmeeasymp.vowmee_user_master(name,mobile,user_status,password,"
				+ "user_role,securityCode,lastName,firstName,created_time) values(?,?,?,?,?,?,?,?,?)";
		template.update(
			    new PreparedStatementCreator() {
			        public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
			            PreparedStatement ps =
			                connection.prepareStatement(INSERT_SQL, new String[] {"id"});
			            ps.setString(1, userBeanFinal.getName());
			            ps.setString(2, userBeanFinal.getMobile());
			            ps.setInt(3, userBeanFinal.getUser_status());
			            ps.setString(4,userBeanFinal.getPassword());
			           // ps.setInt(5, userBeanFinal.getUser_type());
			            ps.setInt(5, userBeanFinal.getUser_role());
			            ps.setString(6, userBeanFinal.getSecurityCode());
			            ps.setString(7, userBeanFinal.getLastName());
			            ps.setString(8, userBeanFinal.getFirstName());
			            ps.setDate(9, new java.sql.Date(System.currentTimeMillis()));
			           
			            return ps;
			        }
			    },
			    keyHolder);
		System.out.println("keyHolder "+ keyHolder.getKey());
		uniqueId = (long) keyHolder.getKey();
		
		return uniqueId;
	}
	public long insertNewStore(StoreBean storeBean){
		final StoreBean storeBeanFinal = storeBean;
		long uniqueId=0;
		KeyHolder keyHolder = new GeneratedKeyHolder();
		final String INSERT_SQL = "insert into vowmeeasymp.vowmee_store_master(store_code,store_name,store_desc,location,"
				+ "circle,store_status) values(?,?,?,?,?,?)";
		template.update(
			    new PreparedStatementCreator() {
			        public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
			            PreparedStatement ps =
			                connection.prepareStatement(INSERT_SQL, new String[] {"id"});
			            ps.setString(1, storeBeanFinal.getStore_code());
			            ps.setString(2, storeBeanFinal.getStore_name());
			            ps.setString(3, storeBeanFinal.getStore_name());
			            ps.setString(4,storeBeanFinal.getLocation());
			           // ps.setInt(5, userBeanFinal.getUser_type());
			            ps.setInt(5, storeBeanFinal.getCircle());
			            ps.setInt(6, storeBeanFinal.getStore_status());
			      
			            return ps;
			        }
			    },
			    keyHolder);
		System.out.println("keyHolder "+ keyHolder.getKey());
		uniqueId = (long) keyHolder.getKey();
		
		return uniqueId;
	}
	
	public long insertAudit(AuditLookupBean auditLookupBean)
	{
		final AuditLookupBean auditBean=auditLookupBean;
		long uniqueId=0;
		KeyHolder keyholder=new GeneratedKeyHolder();
		final String sql="Insert into vowmeeasymp.vowmee_audit_lookup(audit_category,audit_name,audit_desc,circle,flag) values(?,?,?,?,?)";
		template.update(new PreparedStatementCreator() {
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				 PreparedStatement ps =
			                connection.prepareStatement(sql, new String[] {"id"});
				 ps.setString(1, auditBean.getAudit_category());
				 ps.setString(2, auditBean.getAudit_name());
				 ps.setString(3, auditBean.getAudit_desc()); 
				 ps.setString(4, auditBean.getCircle());
				 ps.setString(5, auditBean.getFlag());
				 return ps;
							}
		},keyholder);
		System.out.println("keyHolder "+ keyholder.getKey());
		uniqueId = (long) keyholder.getKey();
		
		return uniqueId;
		
	}
	
	public int getUserDetailByName(String username){
		String sql="select id from vowmeeasymp.vowmee_user_master where name=?";
		Object obj = template.queryForObject(sql, new Object[]{username},new BeanPropertyRowMapper<UserBean>(UserBean.class));
		int id = ((UserBean)obj).getId();
		return id;
		 
	}

	public int getUserStatusByName(String statusCode){
		String sql="select id from vowmeeasymp.vowmee_user_status where status_code=?";
		Object obj = template.queryForObject(sql, new Object[]{statusCode},new BeanPropertyRowMapper<UserStatusBean>(UserStatusBean.class));
		int id = ((UserStatusBean)obj).getId();
		return id;
		 
	}
	
	public int getUserStatusByiD(String statusCode){
		String sql="select id from vowmeeasymp.vowmee_user_status where status_name=?";
		Object obj = template.queryForObject(sql, new Object[]{statusCode},new BeanPropertyRowMapper<UserStatusBean>(UserStatusBean.class));
		int id = ((UserStatusBean)obj).getId();
		return id;
		 
	}
	
	public int getStoreStatusByiD(String statusCode){
		String sql="select id from vowmeeasymp.vowmee_user_status where status_name=?";
		Object obj = template.queryForObject(sql, new Object[]{statusCode},new BeanPropertyRowMapper<UserStatusBean>(UserStatusBean.class));
		int id = ((UserStatusBean)obj).getId();
		return id;
		 
	}
	
	public int getUserTypeId(String userType){
		String sql="select id from vowmeeasymp.vowmee_user_type where type_code=?";
		Object obj = template.queryForObject(sql, new Object[]{userType},new BeanPropertyRowMapper<UserTypeBean>(UserTypeBean.class));
		int id = ((UserTypeBean)obj).getId();
		return id;
		 
	}
	public int updateSecurityCodeId(int id,String securityCode){
		String sql="update vowmeeasymp.vowmee_user_master set securityCode='"+securityCode+"' where id='"+id+"'";
		return template.update(sql);
	}

	public int getUserRoleId(String userRole){
		String sql="select id from vowmeeasymp.vowmee_user_role where role_code=?";
		Object obj = template.queryForObject(sql, new Object[]{userRole},new BeanPropertyRowMapper<UserRoleBean>(UserRoleBean.class));
		int id = ((UserRoleBean)obj).getId();
		return id;
		 
	}
	
	public String getUserRoleName(int userRole){
		String sql="select role_name from vowmeeasymp.vowmee_user_role where id=?";
		Object obj = template.queryForObject(sql, new Object[]{userRole},new BeanPropertyRowMapper<UserRoleBean>(UserRoleBean.class));
		String role_name = ((UserRoleBean)obj).getRole_name();
		return role_name;
		 
	}
	public UserBean getUserDetailById(int id){
		String sql="select name,password,user_status,user_role,mobile,lastName,firstName,id from vowmeeasymp.vowmee_user_master where id=?";
		return template.queryForObject(sql, new Object[]{id},new BeanPropertyRowMapper<UserBean>(UserBean.class));
	}
	public int updateStatusAfterActivation(int id,int statusId){
		String sql="update vowmeeasymp.vowmee_user_master set user_status='"+statusId+"' where id='"+id+"'";
		return template.update(sql);
	}
	
	public int updateForgotPassword(UserBean userBean){
		String sql="update vowmeeasymp.vowmee_user_master set password='"+userBean.getPassword()+"' where name='"+userBean.getName()+"'";
		return template.update(sql);
	}
	
	public int updateRegisterSetPassword(UserBean userBean){
		String sql="update vowmeeasymp.vowmee_user_master set password='"+userBean.getPassword()+"',user_status='"+userBean.getUser_status() +"' where name='"+userBean.getName()+"'";
		return template.update(sql);
	}
	public boolean checkForExistingUser(String email){
		boolean flag = false;
		try{
			String query = "SELECT name FROM vowmeeasymp.vowmee_user_master WHERE name = '"+email+"' ";
			List<String> rows = template.query(query,new RowMapper<String>(){  
			    @Override  
			    public String mapRow(ResultSet rs, int rownumber) throws SQLException {  
			    	String e = null;
			    	if(rs!= null){
			    		e = rs.getString(1);
			    	}
			        return e;  
			    }  
			    });
			for(String emailId : rows){
				if(!emailId.equals(email)){
					flag = false;
				}else{
					flag= true;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		
		return flag;
	}
	
	public boolean checkForExistingAudit(String category,String question)
	{
		boolean flag=false;
		try{
		String sql="SELECT audit_name from vowmeeasymp.vowmee_audit_lookup where audit_category='"+category+"'";
		List<String>rows=template.query(sql, new RowMapper<String>(){

			@Override
			public String mapRow(ResultSet rs, int rownumber) throws SQLException {
				String e=null;
				if(rs!=null)
				{
					e=rs.getString(1);
				}
				return e;
			}
			
		});
		for(String s:rows)
		{
			if(question.equalsIgnoreCase(s))
				flag=true;
		}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return flag;
	}

	public boolean checkForExistingStore(String storeCode){
		boolean flag = false;
		try{
			String query = "SELECT store_code FROM vowmeeasymp.vowmee_store_master WHERE store_code = '"+storeCode+"' ";
			List<String> rows = template.query(query,new RowMapper<String>(){  
			    @Override  
			    public String mapRow(ResultSet rs, int rownumber) throws SQLException {  
			    	String e = null;
			    	if(rs!= null){
			    		e = rs.getString(1);
			    	}
			        return e;  
			    }  
			    });
			for(String emailId : rows){
				if(!emailId.equals(storeCode)){
					flag = false;
				}else{
					flag= true;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		
		return flag;
	}

	public List<String> getRoleList(){
		List<String> auditList=null;
		try{
			String sql="SELECT role_name FROM vowmeeasymp.vowmee_user_role";
			auditList = template.queryForList(sql, String.class);
		}catch(Exception e){
			e.printStackTrace();
		}
		return auditList;
	}
	
	public List<UserBean> getTypeList(){
		List<UserBean> auditList=null;
		try{
			String sql="SELECT id,type_name FROM vowmeeasymp.vowmee_user_type";
			auditList = template.query(sql,new RowMapper<UserBean>(){  
			    @Override  
			    public UserBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
			    	UserBean e=new UserBean();  
			       e.setUser_type(rs.getInt(1));
			       e.setUserType(rs.getString(2));
			        return e;  
			    }  
			    });  
		}catch(Exception e){
			e.printStackTrace();
		}
		return auditList;
	}
	
	public List<UserBean> getStatusList(){
		List<UserBean> auditList=null;
		try{
			String sql="SELECT id,status_name FROM vowmeeasymp.vowmee_user_status";
			auditList = template.query(sql,new RowMapper<UserBean>(){  
			    @Override  
			    public UserBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
			    	UserBean e=new UserBean();  
			       e.setUser_status(rs.getInt(1));
			       e.setUserStatus(rs.getString(2));
			        return e;  
			    }  
			    });  
		}catch(Exception e){
			e.printStackTrace();
		}
		return auditList;
	}
	public List<CircleBean> getCircleList(){
		List<CircleBean> auditList=null;
		try{
			String sql="SELECT id,circle_name FROM vowmeeasymp.vowmee_circle_master";
			auditList = template.query(sql,new RowMapper<CircleBean>(){  
			    @Override  
			    public CircleBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
			    	CircleBean e=new CircleBean();  
			       e.setId(rs.getInt(1));
			       e.setCircle_name(rs.getString(2));
			        return e;  
			    }  
			    });  
		}catch(Exception e){
			e.printStackTrace();
		}
		return auditList;
	}
	public List<AuditLookupBean> getAuditList(){
		List<AuditLookupBean> auditList=null;
		try{
			String sql="SELECT distinct(audit_category) FROM vowmeeasymp.vowmee_audit_lookup";
			auditList = template.query(sql,new RowMapper<AuditLookupBean>(){  
			    @Override  
			    public AuditLookupBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
			    	AuditLookupBean e=new AuditLookupBean();  
			       
			       e.setAudit_name(rs.getString(1));
			        return e;  
			    }  
			    });  
		}catch(Exception e){
			e.printStackTrace();
		}
		return auditList;
	}
	public List<LocationBean>getLocationList()
	{
		List<LocationBean> locationList=new ArrayList<>();
		try{
			String sql="SELECT id,name from vowmeeasymp.vowmee_location_master";
			locationList=template.query(sql, new RowMapper<LocationBean>(){
				@Override
				public LocationBean mapRow(ResultSet rs,int rownumber)  throws SQLException{
					LocationBean e=new LocationBean();
					e.setId(rs.getInt(1));
					e.setName(rs.getString(2));
					return e;
				}
			});
					
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return locationList;
	}
	
	public List<UserBean> getRoleUserList(){
		String sql = "SELECT id,role_name FROM vowmeeasymp.vowmee_user_role";
		List<UserBean> rows = template.query(sql,new RowMapper<UserBean>(){  
		    @Override  
		    public UserBean mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	UserBean e=new UserBean();  
		       e.setUser_role(rs.getInt(1));
		       e.setUserRole(rs.getString(2));
		        return e;  
		    }  
		    });  
		System.out.println(rows);
		return rows;
	}
	
	public List<UserStoreMap> getStoreList(int circleId){
		String sql = "SELECT id,store_name FROM vowmeeasymp.vowmee_store_master where circle = "+circleId;
		List<UserStoreMap> rows = template.query(sql,new RowMapper<UserStoreMap>(){  
		    @Override  
		    public UserStoreMap mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	UserStoreMap e=new UserStoreMap();  
		       e.setId(rs.getInt(1));
		       e.setStore_name(rs.getString(2));
		        return e;  
		    }  
		    });  
		System.out.println(rows);
		return rows;
	}
	
	
	
	public void userStoreMapping(List<UserStoreMap> userStoreList) {
		try{
			
			
			for(UserStoreMap userStoreMap: userStoreList){
				
				 boolean flag = checkForExistingStore(userStoreMap.getUserid(),userStoreMap.getStoreid());
				 if(!flag){
					 String query = "INSERT INTO `vowmeeasymp`.`vowmee_user_store_mapping` "
						 		+ "(`userid`, `storeid`) "
						 		+ "VALUES ("+userStoreMap.getUserid()+","+userStoreMap.getStoreid()+")";
						 template.update(query);
				 }
				
		}
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public void userCircleMapping(List<UserCircleBean> userCircleList) {
		try{
			
			
			for(UserCircleBean userCircleMap: userCircleList){
				
				 boolean flag = checkForExistingCircle(userCircleMap.getUserid(),userCircleMap.getCircleid());
				 if(!flag){
					 String query = "INSERT INTO `vowmeeasymp`.`vowmee_user_circle_mapping` "
						 		+ "(`userId`, `circleId`) "
						 		+ "VALUES ("+userCircleMap.getUserid()+","+userCircleMap.getCircleid()+")";
						 template.update(query);
				 }
				
		}
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public boolean checkForExistingStore(long userId, int storeId){
		boolean flag = false;
		try{
			String query = "SELECT userId FROM vowmeeasymp.vowmee_user_store_mapping WHERE `userid` = "+userId+" and `storeid`= "+storeId;
			System.out.println(query);
			List<Integer> rows = template.query(query,new RowMapper<Integer>(){  
			    @Override  
			    public Integer mapRow(ResultSet rs, int rownumber) throws SQLException {  
			    	Integer e = null;
			    	if(rs!= null){
			    		e = rs.getInt(1);
			    	}
			        return e;  
			    }  
			    });
			for(Integer userFromId : rows){
				if(userFromId!=userId){
					flag = false;
				}else{
					flag= true;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		
		return flag;
	}
	public boolean checkForExistingCircle(long userId, int storeId){
		boolean flag = false;
		try{
			String query = "SELECT userId FROM vowmeeasymp.vowmee_user_circle_mapping WHERE `userId` = "+userId+" and `circleId`= "+storeId;
			System.out.println(query);
			List<Integer> rows = template.query(query,new RowMapper<Integer>(){  
			    @Override  
			    public Integer mapRow(ResultSet rs, int rownumber) throws SQLException {  
			    	Integer e = null;
			    	if(rs!= null){
			    		e = rs.getInt(1);
			    	}
			        return e;  
			    }  
			    });
			for(Integer userFromId : rows){
				if(userFromId!=userId){
					flag = false;
				}else{
					flag= true;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		
		return flag;
	}
	public void removeUserStoreMapping(StringBuffer userStoreList,int userId) {
		try{
		 String query = "DELETE FROM `vowmeeasymp`.`vowmee_user_store_mapping` WHERE  storeid in"+
						 		 "("+userStoreList+") and userid="+userId;
						 template.update(query);
				
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	public void removeUserCircleMapping(StringBuffer userCircleList,int userId) {
		try{
		 String query = "DELETE FROM `vowmeeasymp`.`vowmee_user_circle_mapping` WHERE  circleId in"+
						 		 "("+userCircleList+") and userId="+userId;
						 template.update(query);
				
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public List<Store> getStoreList(StringBuffer userCircleList){
		String sql = "SELECT id,store_name FROM vowmeeasymp.vowmee_store_master where circle in "+"("+userCircleList+")";
		List<Store> rows = template.query(sql,new RowMapper<Store>(){  
		    @Override  
		    public Store mapRow(ResultSet rs, int rownumber) throws SQLException {  
		    	Store e=new Store();  
		       e.setId(rs.getInt(1));
		       e.setStore_name(rs.getString(2));
		        return e;  
		    }  
		    });  
		System.out.println(rows);
		return rows;
	}
	
}
