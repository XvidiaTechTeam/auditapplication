package com.vowmee.auditHistory;

import com.vowmee.bean.AuditHistoryBean;
import com.vowmee.dao.AuditHistoryDAO;

public class AuditHistoryService {
	
	AuditHistoryDAO auditHistoryDao;

	public AuditHistoryDAO getAuditHistoryDao() {
		return auditHistoryDao;
	}

	public void setAuditHistoryDao(AuditHistoryDAO auditHistoryDao) {
		this.auditHistoryDao = auditHistoryDao;
	}

	public void insertAuditHistory(AuditHistoryBean auditHistoryBean){
		
		try{
			auditHistoryDao.insertAuditHistory(auditHistoryBean);
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
}
