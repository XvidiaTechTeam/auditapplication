package com.vowmee.utill;
/**
 * This class used for store key and value like same as Map
 * 
 * @author Elan
 *
 */
public class BuildMap {
	
	private String key;
	private String value;
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
