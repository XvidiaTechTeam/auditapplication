package com.vowmee.utill;

import com.vowmee.bean.AuditHistoryBean;

public class AuditHistoryUtil {
	
	public AuditHistoryBean auditHistoryBean(int parentid,int createdby
			,String field,String oldvalue,String newvalue){
		AuditHistoryBean auditHistoryBean = new AuditHistoryBean();
		try{
			auditHistoryBean.setParentid(parentid);
			auditHistoryBean.setCreatedby(createdby);
			auditHistoryBean.setNewvalue(newvalue);
			auditHistoryBean.setField(field);
			auditHistoryBean.setOldvalue(oldvalue);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return auditHistoryBean;
	}

}
