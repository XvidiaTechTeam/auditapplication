package com.vowmee.utill;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.commons.CommonsMultipartFile;


/**
 * @author Elan
 */

public class ExcelSheetReader {
	/** This list contain the rows value, first list index should be Column Detail*/
	private static List<List<Cell>> rowDataList = null;
	/**	This list should contain the all the column name with index in buildMap object */
	private static List<BuildMap> columnList = null;
	

	/**
	 * This method is used to read the data's from an excel file.
	 * @param fileName - Name of the excel file.
	 */
	@SuppressWarnings("resource")
	public static void readExcelFile(InputStream fileInputStream, String fileType,int index) {
		try {
			int startCell =0;
			clear();
			Workbook workBook = null;
			if(fileType.equalsIgnoreCase(".xlsx"))
				workBook = new XSSFWorkbook(fileInputStream);
			else 
				workBook = new HSSFWorkbook(fileInputStream);

			Sheet Sheet = workBook.getSheetAt(index);
			
			rowDataList = new ArrayList<List<Cell>>();
			
			/** Iterate the rows and cells of the spreadsheet to get all the datas. */
			Iterator<Row> rowIterator = Sheet.rowIterator();
			int headerlength = 0;
			while (rowIterator.hasNext()) {
				
				Row row = rowIterator.next();
				List<Cell> cellList = new ArrayList<Cell>();
				
				if(rowDataList.size() == 0) {
					Iterator<Cell> iterator = row.cellIterator();
					while (iterator.hasNext()) {
						Cell cell = iterator.next();
						startCell = cell.getColumnIndex();
						break;
					}
					Iterator<Cell> iterator1 = row.cellIterator();
					while (iterator1.hasNext()) {
						Cell cell = iterator1.next();
						
						cellList.add(cell);
					}
					headerlength = cellList.size();
				} else {
					for (int idx=startCell; idx<=headerlength; idx++) {
						cellList.add(row.getCell(idx));
					}
				}
				rowDataList.add(cellList);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		/** Call the printToConsole method to print the cell data in the console. */
		buildHeaderList(rowDataList);
	}

	/**
	 * This method is used to build column header list.
	 * @param cellList - List of the data's in the spreadsheet.
	 */
	private static void buildHeaderList(List<List<Cell>> cellList) {
		
		for (int i = 0; i < cellList.size(); i++) {
			List<Cell> columnData = (List<Cell>) cellList.get(i);
			
			if(i == 0) {
				columnList = new ArrayList<BuildMap>();
				BuildMap buildMap = null;
				
				for (int j = 0; j < columnData.size(); j++) {
					Cell cell = (Cell) columnData.get(j);
					
					buildMap = new BuildMap();
					
					buildMap.setKey(cell.toString().trim());
					buildMap.setValue(String.valueOf(j));
					columnList.add(buildMap);
				}
			}
		}
	}
	
	public static int getColumnIndex(String columnName) {
		int columnIndex = -1;
		if(columnList != null) {
			
			for(int idx=0; idx<columnList.size(); idx++) {
				BuildMap map = columnList.get(idx);
				if(map.getKey().equalsIgnoreCase(columnName)) {
					if(map.getValue() != null) {
						columnIndex = Integer.parseInt(map.getValue());
						break;
					}
				}
			}
		}
		return columnIndex;
	}
	
	public static String getValue(List<Cell> row, String columnName) {
		String value = null;
		int columnIndex = getColumnIndex(columnName);
		if(columnIndex != -1) {
			if(row.get(columnIndex) != null) {
				value = row.get(columnIndex).toString().trim();				
			}
		}
		return value;
	}
	public static String getStringValue(List<Cell> row, String columnName) {
		String value = null;
		int columnIndex = getColumnIndex(columnName);
		if(columnIndex != -1) {
			if(row.get(columnIndex) != null) {				
				row.get(columnIndex).setCellType(Cell.CELL_TYPE_STRING);				
				value = row.get(columnIndex).toString().trim();				
			}
		}
		return value;
	}
	
	private static void clear() {
		if(rowDataList != null)
			rowDataList.clear();
		if(columnList != null)
			columnList.clear();
	}
	
	
	public static List<List<Cell>> getRowDataList() {
		return rowDataList;
	}

	public static void setRowDataList(List<List<Cell>> rowDataList) {
		ExcelSheetReader.rowDataList = rowDataList;
	}

	public static List<BuildMap> getColumnList() {
		return columnList;
	}

	public static void setColumnList(List<BuildMap> columnList) {
		ExcelSheetReader.columnList = columnList;
	}
	
	public static List<List<org.apache.poi.ss.usermodel.Cell>> returnRowDataList(String fileType,int sheetNo,CommonsMultipartFile filePath) throws Exception{

		List<List<org.apache.poi.ss.usermodel.Cell>> rowDataList = null;

		try{

			ExcelSheetReader.readExcelFile(filePath.getInputStream(), fileType,sheetNo);
			rowDataList = ExcelSheetReader.getRowDataList();

		}catch(FileNotFoundException e){
			e.printStackTrace();
			//this.addActionError(e.getMessage());
			throw e;
		}catch(Exception e){
			e.printStackTrace();
			//this.addActionError(e.getMessage());
			throw e;
		}

		return rowDataList;
	}
	/*public static void main(String[] args) {
		new ExcelSheetReader().readExcelFile(fileName);
	}*/
}
