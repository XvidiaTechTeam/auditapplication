package com.vowmee.utill;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import com.vowmee.bean.ScoreCardBO;

public class AuditorExcelBuilder extends AbstractExcelView{

	
	 @Override
	    protected void buildExcelDocument(Map<String, Object> model,
	            HSSFWorkbook workbook, HttpServletRequest request, HttpServletResponse response)
	            throws Exception {
	        // get data model which is passed by the Spring container
	        List<ScoreCardBO> reportsLists = (List<ScoreCardBO>) model.get("auditorReportExportList");
	        String user = (String) model.get("user");
	        // create a new Excel sheet
	        HSSFSheet sheet = workbook.createSheet("Auditor Wise Report");
	        sheet.setDefaultColumnWidth(30);
	        
	        
	         
	        // create style for header cells
	        CellStyle style = workbook.createCellStyle();
	        Font font = workbook.createFont();
	        font.setFontName("Arial");
	        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
	        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
	        font.setColor(HSSFColor.WHITE.index);
	        style.setFont(font);
	         
	        
	        int initialsize = 1;
	        HSSFRow auditorHeader = sheet.createRow(initialsize);
	        auditorHeader.createCell(0).setCellValue(user);
	        auditorHeader.getCell(0).setCellStyle(style);
	        
	        initialsize = initialsize+2;
	        HSSFRow header = sheet.createRow(initialsize);
	        header.createCell(0).setCellValue("Store Name");
	        header.getCell(0).setCellStyle(style);
	         
	        header.createCell(1).setCellValue("Month");
	        header.getCell(1).setCellStyle(style);
	        
	        header.createCell(2).setCellValue("Audit Score");
	        header.getCell(2).setCellStyle(style);
	         
	        
	        sheet.addMergedRegion(new CellRangeAddress(1,1,0,2));
	         
	        // create data rows
	        int rowCount = initialsize+1;
	        float scorePercentage = 0;
	        
	        
	        HSSFRow aRow = null;
	        
	        for (ScoreCardBO scoreCardBo : reportsLists) {
	        	aRow = sheet.createRow(rowCount++);
	            aRow.createCell(0).setCellValue(scoreCardBo.getStoreName());
	            aRow.createCell(1).setCellValue(scoreCardBo.getMonth());
	            aRow.createCell(2).setCellValue(Math.round(Float.parseFloat(scoreCardBo.getPercentage())* 100.0) / 100.0);
	            //scorePercentage = scorePercentage+Float.valueOf(reportBean.getAudit_percentage());
	        }
	        aRow = sheet.createRow(rowCount++);
	       /* aRow.createCell(0).setCellValue("Average Audit Score");
	        aRow.createCell(1).setCellValue((scorePercentage/reportsLists.size()));*/
	        workbook.close();
	    }
}
