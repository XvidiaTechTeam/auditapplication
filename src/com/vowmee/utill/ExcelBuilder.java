package com.vowmee.utill;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import com.vowmee.bean.AuditBean;
import com.vowmee.bean.ReportsBean;
 
/**
 * This class builds an Excel spreadsheet document using Apache POI library.
 * @author www.codejava.net
 *
 */
public class ExcelBuilder extends AbstractExcelView {
 
    @Override
    protected void buildExcelDocument(Map<String, Object> model,
            HSSFWorkbook workbook, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        // get data model which is passed by the Spring container
        List<ReportsBean> reportsLists = (List<ReportsBean>) model.get("reportList");
        List<AuditBean> reportsListWithCategory = (List<AuditBean>) model.get("auditDetailsList");
        String circleStore = (String) model.get("circleStore");
        Map<Integer, String> monthMap = (TreeMap<Integer, String>) model.get("monthMap");
        System.out.println("reportsListWithCategory "+reportsListWithCategory);
        
        // create a new Excel sheet
        HSSFSheet sheet = workbook.createSheet("Summary");
        sheet.setDefaultColumnWidth(30);
        
        
         
        // create style for header cells
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        font.setColor(HSSFColor.WHITE.index);
        style.setFont(font);
        int initialsize = 1;
        HSSFRow storeHeader = sheet.createRow(initialsize);
        storeHeader.createCell(0).setCellValue(circleStore);
        storeHeader.getCell(0).setCellStyle(style);
        
        initialsize = initialsize+1;
        HSSFRow dataHeader = sheet.createRow(initialsize);
        // create data rows
        int rowCount = initialsize+1;
        float scorePercentage = 0;
        
        HSSFRow aRow = null;
        dataHeader = sheet.createRow(rowCount++);
         
        dataHeader.createCell(0).setCellValue("Category");
        dataHeader.getCell(0).setCellStyle(style);
        
        dataHeader.createCell(1).setCellValue("Month");
        dataHeader.getCell(1).setCellStyle(style);
        
        dataHeader.createCell(2).setCellValue("Percentage");
        dataHeader.getCell(2).setCellStyle(style);
        
        dataHeader.createCell(3).setCellValue("Weightage Percentage");
        dataHeader.getCell(3).setCellStyle(style);
        sheet.addMergedRegion(new CellRangeAddress(1,1,0,3));
        
        for (ReportsBean reportBean : reportsLists) {
        	aRow = sheet.createRow(rowCount++);
            aRow.createCell(0).setCellValue(reportBean.getAudit_category());
            aRow.createCell(1).setCellValue(monthMap.get(reportBean.getAudit_month()));
            aRow.createCell(2).setCellValue(Math.round(reportBean.getAudit_percentage()* 100.0) / 100.0);
            aRow.createCell(3).setCellValue(reportBean.getWeitage_percentage());
            //scorePercentage = scorePercentage+Float.valueOf(reportBean.getAudit_percentage());
        }
       
        HSSFSheet sheet1 = workbook.createSheet("Details");
        sheet1.setDefaultColumnWidth(30);
        
        
         
        // create style for header cells
        CellStyle style1 = workbook.createCellStyle();
        Font font1 = workbook.createFont();
        font1.setFontName("Arial");
        style1.setFillPattern(CellStyle.SOLID_FOREGROUND);
        font1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        font1.setColor(HSSFColor.WHITE.index);
        style1.setFont(font);
        int initialsize1 = 1;
        HSSFRow storeHeader1 = sheet1.createRow(initialsize1);
        storeHeader1.createCell(0).setCellValue(circleStore);
        storeHeader1.getCell(0).setCellStyle(style1);
        
        initialsize1 = initialsize1+2;
        HSSFRow dataHeader1 = sheet1.createRow(initialsize1);
        dataHeader1.createCell(0).setCellValue("Category");
        dataHeader1.getCell(0).setCellStyle(style);
        
        dataHeader1.createCell(1).setCellValue("Month");
        dataHeader1.getCell(1).setCellStyle(style);
        
        dataHeader1.createCell(2).setCellValue("Audit Question");
        dataHeader1.getCell(2).setCellStyle(style);
        
        dataHeader1.createCell(3).setCellValue("Audit Answer");
        dataHeader1.getCell(3).setCellStyle(style);
        
        dataHeader1.createCell(4).setCellValue("Audit Completed Date");
        dataHeader1.getCell(4).setCellStyle(style);
         
        sheet1.addMergedRegion(new CellRangeAddress(1,1,0,4));
        int rowCount1 = initialsize1+1;
        HSSFRow aRow1 = null;
        
        for(AuditBean auditBean : reportsListWithCategory){
        	aRow1 = sheet1.createRow(rowCount1++);
        	aRow1.createCell(0).setCellValue(auditBean.getAuditCategory());
        	aRow1.createCell(1).setCellValue(monthMap.get(Integer.parseInt(auditBean.getMonth())));
        	aRow1.createCell(2).setCellValue(auditBean.getAuditQuestion());
        	/*if(auditBean.getAuditAnswer() == "1"){
        		aRow1.createCell(3).setCellValue("Yes");
        	}else if(auditBean.getAuditAnswer() == "0"){
        		aRow1.createCell(3).setCellValue("No");
        	}else if(auditBean.getAuditAnswer() == "-1"){
        		aRow1.createCell(3).setCellValue("N/A");
        	}*/
        	aRow1.createCell(3).setCellValue(auditBean.getAuditAnswer());
        	aRow1.createCell(4).setCellValue(auditBean.getAuditCompleteDate());
        	
            //scorePercentage = scorePercentage+Float.valueOf(scoreCardBO.getPercentage());
    	}
        workbook.close();
    }
    
  
}