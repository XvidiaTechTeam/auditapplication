package com.vowmee.bean;

public class UserRoleBean {
	
	private int id;
	private String role;
	private String parentRole;
	private String role_code;
	private String role_name;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getParentRole() {
		return parentRole;
	}
	public void setParentRole(String parentRole) {
		this.parentRole = parentRole;
	}
	@Override
	public String toString() {
		return "UserRoleBean [id=" + id + ", role=" + role + ", parentRole=" + parentRole + ", role_code=" + role_code
				+ ", role_name=" + role_name + "]";
	}
	public String getRole_code() {
		return role_code;
	}
	public void setRole_code(String role_code) {
		this.role_code = role_code;
	}
	public String getRole_name() {
		return role_name;
	}
	public void setRole_name(String role_name) {
		this.role_name = role_name;
	}

}
