package com.vowmee.bean;

public class AuditCategory {
	private String audit_category;

	public String getAudit_category() {
		return audit_category;
	}

	public void setAudit_category(String audit_category) {
		this.audit_category = audit_category;
	}

	@Override
	public String toString() {
		return "AuditCategory [audit_category=" + audit_category + "]";
	}
	

}
