package com.vowmee.bean;

public class AuditMaster {
	
	private int id;
	private String auditName;
	private String auditDesc;
	private int store;
	private String agent_name;
	private String agent_email;
	private String agent_mobile;
	
	public boolean isAuditComplete() {
		return auditComplete;
	}
	public void setAuditComplete(boolean auditComplete) {
		this.auditComplete = auditComplete;
	}
	private boolean auditComplete;
	
	@Override
	public String toString() {
		return "AuditMaster [id=" + id + ", auditName=" + auditName + ", auditDesc=" + auditDesc + ", store=" + store
				+ ", agent_name=" + agent_name + ", agent_email=" + agent_email + ", agent_mobile=" + agent_mobile
				+ ", auditComplete=" + auditComplete + "]";
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAuditName() {
		return auditName;
	}
	public void setAuditName(String auditName) {
		this.auditName = auditName;
	}
	public String getAuditDesc() {
		return auditDesc;
	}
	public void setAuditDesc(String auditDesc) {
		this.auditDesc = auditDesc;
	}
	public int getStore() {
		return store;
	}
	public void setStore(int store) {
		this.store = store;
	}
	public String getAgent_name() {
		return agent_name;
	}
	public void setAgent_name(String agent_name) {
		this.agent_name = agent_name;
	}
	public String getAgent_email() {
		return agent_email;
	}
	public void setAgent_email(String agent_email) {
		this.agent_email = agent_email;
	}
	public String getAgent_mobile() {
		return agent_mobile;
	}
	public void setAgent_mobile(String agent_mobile) {
		this.agent_mobile = agent_mobile;
	}
	

}