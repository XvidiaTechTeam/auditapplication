package com.vowmee.bean;

public class ScoreCardBO {
	private int store_id;
	private int audit_id;
	private String audit_category;
	private String percentage;
	private int id;
	private String fromDate;
	private String toDate;
	private String month;
	private String weitage_percentage;
	private int circle_id;
	private String circle_name;
	private String store_name;
	private String storeName;
	
	
	public int getStore_id() {
		return store_id;
	}
	public void setStore_id(int store_id) {
		this.store_id = store_id;
	}
	public int getAudit_id() {
		return audit_id;
	}
	public void setAudit_id(int audit_id) {
		this.audit_id = audit_id;
	}
	public String getAudit_category() {
		return audit_category;
	}
	public void setAudit_category(String audit_category) {
		this.audit_category = audit_category;
	}
	public String getPercentage() {
		return percentage;
	}
	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "ScoreCardBO [store_id=" + store_id + ", audit_id=" + audit_id + ", audit_category=" + audit_category
				+ ", percentage=" + percentage + ", id=" + id + ", fromDate=" + fromDate + ", toDate=" + toDate
				+ ", month=" + month + ", weitage_percentage=" + weitage_percentage + ", circle_id=" + circle_id
				+ ", circle_name=" + circle_name + ", store_name=" + store_name + ", storeName=" + storeName + "]";
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getWeitage_percentage() {
		return weitage_percentage;
	}
	public void setWeitage_percentage(String weitage_percentage) {
		this.weitage_percentage = weitage_percentage;
	}
	public int getCircle_id() {
		return circle_id;
	}
	public void setCircle_id(int circle_id) {
		this.circle_id = circle_id;
	}
	public String getCircle_name() {
		return circle_name;
	}
	public void setCircle_name(String circle_name) {
		this.circle_name = circle_name;
	}
	public String getStore_name() {
		return store_name;
	}
	public void setStore_name(String store_name) {
		this.store_name = store_name;
	}
	public String getStoreName() {
		return storeName;
	}
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((month == null) ? 0 : month.hashCode());
		result = prime * result + ((storeName == null) ? 0 : storeName.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ScoreCardBO other = (ScoreCardBO) obj;
		if (month == null) {
			if (other.month != null)
				return false;
		} else if (!month.equals(other.month))
			return false;
		if (storeName == null) {
			if (other.storeName != null)
				return false;
		} else if (!storeName.equals(other.storeName))
			return false;
		return true;
	}
	
	
}
