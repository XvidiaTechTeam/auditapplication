package com.vowmee.bean;

public class UserPreviousAuditHytory {

	private String action;
	private String oldValue;
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getOldValue() {
		return oldValue;
	}
	public void setOldValue(String oldValue) {
		this.oldValue = oldValue;
	}
	public String getNewValue() {
		return newValue;
	}
	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	private String newValue;
	private String createdDate;
	@Override
	public String toString() {
		return "UserPreviousAuditHytory [action=" + action + ", oldValue=" + oldValue + ", newValue=" + newValue
				+ ", createdDate=" + createdDate + "]";
	}
	
	
}
