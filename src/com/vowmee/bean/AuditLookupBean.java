package com.vowmee.bean;

public class AuditLookupBean {
	
	private int id;
	private String audit_category;
	private String audit_name;
	private String audit_desc;
	private String circle;
	private String flag;
	private String reasonForFail;
	public String getReasonForFail() {
		return reasonForFail;
	}
	public void setReasonForFail(String reasonForFail) {
		this.reasonForFail = reasonForFail;
	}
	public int getId() {
		return id;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getCircle() {
		return circle;
	}
	public void setCircle(String circle) {
		this.circle = circle;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAudit_category() {
		return audit_category;
	}
	public void setAudit_category(String audit_category) {
		this.audit_category = audit_category;
	}
	public String getAudit_name() {
		return audit_name;
	}
	public void setAudit_name(String audit_name) {
		this.audit_name = audit_name;
	}
	public String getAudit_desc() {
		return audit_desc;
	}
	public void setAudit_desc(String audit_desc) {
		this.audit_desc = audit_desc;
	}
	@Override
	public String toString() {
		return "AuditLookupBean [id=" + id + ", audit_category=" + audit_category + ", audit_name=" + audit_name
				+ ", audit_desc=" + audit_desc + "]";
	}
	
	
}
