package com.vowmee.bean;

public class StoreBean {
	private String store_name;
	private int id;
	private String store_code;
	private String store_desc;
	private String circle_name;
	private String storeStatus;
	private String old_store_status;
	private int store_status;
	private int circle;
	public int getCircle() {
		return circle;
	}
	public void setCircle(int circle) {
		this.circle = circle;
	}
	public int getStore_status() {
		return store_status;
	}
	public String getOld_store_status() {
		return old_store_status;
	}
	public void setOld_store_status(String old_store_status) {
		this.old_store_status = old_store_status;
	}
	public String getStoreStatus() {
		return storeStatus;
	}
	public void setStoreStatus(String storeStatus) {
		this.storeStatus = storeStatus;
	}
	public void setStore_status(int store_status) {
		this.store_status = store_status;
	}
	private String location;
	private String old_store_name;
	private String old_circle_name;
	public String getOld_circle_name() {
		return old_circle_name;
	}
	public void setOld_circle_name(String old_circle_name) {
		this.old_circle_name = old_circle_name;
	}
	public String getOld_store_name() {
		return old_store_name;
	}
	public void setOld_store_name(String old_store_name) {
		this.old_store_name = old_store_name;
	}
	private String reasonForFail;
	
	public String getReasonForFail() {
		return reasonForFail;
	}
	public void setReasonForFail(String reasonForFail) {
		this.reasonForFail = reasonForFail;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getCircle_name() {
		return circle_name;
	}
	public void setCircle_name(String circle_name) {
		this.circle_name = circle_name;
	}
	public String getStore_name() {
		return store_name;
	}
	public void setStore_name(String store_name) {
		this.store_name = store_name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getStore_code() {
		return store_code;
	}
	public void setStore_code(String store_code) {
		this.store_code = store_code;
	}
	public String getStore_desc() {
		return store_desc;
	}
	public void setStore_desc(String store_desc) {
		this.store_desc = store_desc;
	}

}
