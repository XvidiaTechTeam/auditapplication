package com.vowmee.bean;

public class ReportsBean {
	
	private String audit_category;
	private int audit_month;
	private float audit_percentage;
	private String weitage_percentage;
	private String month;
	public String getAudit_category() {
		return audit_category;
	}
	public void setAudit_category(String audit_category) {
		this.audit_category = audit_category;
	}
	public int getAudit_month() {
		return audit_month;
	}
	public void setAudit_month(int audit_month) {
		this.audit_month = audit_month;
	}
	public float getAudit_percentage() {
		return audit_percentage;
	}
	public void setAudit_percentage(float audit_percentage) {
		this.audit_percentage = audit_percentage;
	}
	@Override
	public String toString() {
		return "ReportsBean [audit_category=" + audit_category + ", audit_month=" + audit_month + ", audit_percentage="
				+ audit_percentage + ", weitage_percentage=" + weitage_percentage + ", month=" + month + "]";
	}
	public String getWeitage_percentage() {
		return weitage_percentage;
	}
	public void setWeitage_percentage(String weitage_percentage) {
		this.weitage_percentage = weitage_percentage;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((audit_category == null) ? 0 : audit_category.hashCode());
		result = prime * result + ((month == null) ? 0 : month.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReportsBean other = (ReportsBean) obj;
		if (audit_category == null) {
			if (other.audit_category != null)
				return false;
		} else if (!audit_category.equals(other.audit_category))
			return false;
		if (month == null) {
			if (other.month != null)
				return false;
		} else if (!month.equals(other.month))
			return false;
		return true;
	}
}
