package com.vowmee.bean;

public class AuditHistoryBean {
	private int parentid;
	private int createdby;
	private String field;
	private String oldvalue;
	private String newvalue;
	public int getParentid() {
		return parentid;
	}
	public void setParentid(int parentid) {
		this.parentid = parentid;
	}
	public int getCreatedby() {
		return createdby;
	}
	public void setCreatedby(int createdby) {
		this.createdby = createdby;
	}
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public String getOldvalue() {
		return oldvalue;
	}
	public void setOldvalue(String oldvalue) {
		this.oldvalue = oldvalue;
	}
	public String getNewvalue() {
		return newvalue;
	}
	public void setNewvalue(String newvalue) {
		this.newvalue = newvalue;
	}
	@Override
	public String toString() {
		return "AuditHistoryBean [parentid=" + parentid + ", createdby=" + createdby + ", field=" + field
				+ ", oldvalue=" + oldvalue + ", newvalue=" + newvalue + "]";
	}

}
