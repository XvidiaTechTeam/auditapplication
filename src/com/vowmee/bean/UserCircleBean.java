package com.vowmee.bean;

public class UserCircleBean {
	private long userid;
	private int circleid;
	private String circle_name;
	private int id;
	public long getUserid() {
		return userid;
	}
	public void setUserid(long userid) {
		this.userid = userid;
	}
	
	public int getCircleid()
	{
		return circleid;
	}
	public void setCircleid(int circleid)
	{
		this.circleid=circleid;
	}
	public String getCircle_name()
	{
		return circle_name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setCircle_name(String circle_name) {
		this.circle_name = circle_name;
	}

}
