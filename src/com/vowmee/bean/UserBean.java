package com.vowmee.bean;

import java.util.List;

public class UserBean {
	
	private int id;
	private String name;
	private String mobile;
	private String password;
	private int user_status;
	private int user_type;
	private int user_role;
	private String securityCode;
	private String userRole;
	private String userType;
	private String userStatus;
	private List<String> userCircle;
	private String firstName;
	private String lastName;
	private List<String> storeName;
	private String old_firstName;
	private String old_lastName;
	private String old_mobile;
	private String old_userRole;
	
	private String old_userStatus;
	private String testValue;
	private List<String> removeStoreName;
	private List<String> removeCircleName;
	private String filterTimeForSelectUser;
	public String getFilterTimeForSelectUser() {
		return filterTimeForSelectUser;
	}
	public void setFilterTimeForSelectUser(String filterTimeForSelectUser) {
		this.filterTimeForSelectUser = filterTimeForSelectUser;
	}
	public List<String> getRemoveStoreName() {
		return removeStoreName;
	}
	public void setRemoveStoreName(List<String> removeStoreName) {
		this.removeStoreName = removeStoreName;
	}
	//private int managerid;
	private String confirmPassword;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getUser_status() {
		return user_status;
	}
	public void setUser_status(int user_status) {
		this.user_status = user_status;
	}
	public int getUser_type() {
		return user_type;
	}
	public void setUser_type(int user_type) {
		this.user_type = user_type;
	}
	public int getUser_role() {
		return user_role;
	}
	public void setUser_role(int user_role) {
		this.user_role = user_role;
	}
	@Override
	public String toString() {
		return "UserBean [id=" + id + ", name=" + name + ", mobile=" + mobile + ", password=" + password
				+ ", user_status=" + user_status + ", user_type=" + user_type + ", user_role=" + user_role
				+ ", securityCode=" + securityCode + ", userRole=" + userRole + ", userType=" + userType
				+ ", userStatus=" + userStatus 
				+ ", firstName=" + firstName + ", lastName=" + lastName + ", storeName=" + storeName
				+ ", old_firstName=" + old_firstName + ", old_lastName=" + old_lastName + ", old_mobile=" + old_mobile
				+ ", old_userRole=" + old_userRole + ", old_userStatus="
				+ old_userStatus + ", testValue=" + testValue + ", removeStoreName=" + removeStoreName
				+ ", filterTimeForSelectUser=" + filterTimeForSelectUser + ", confirmPassword=" + confirmPassword + "]";
	}
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	public String getSecurityCode() {
		return securityCode;
	}
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}
	public String getUserRole() {
		return userRole;
	}
	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getUserStatus() {
		return userStatus;
	}
	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}
	public List<String> getUserCircle() {
		return userCircle;
	}
	public void setUserCircle(List<String> userCircle) {
		this.userCircle = userCircle;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public List<String> getStoreName() {
		return storeName;
	}
	public void setStoreName(List<String> storeName) {
		this.storeName = storeName;
	}
	public String getOld_firstName() {
		return old_firstName;
	}
	public void setOld_firstName(String old_firstName) {
		this.old_firstName = old_firstName;
	}
	public String getOld_lastName() {
		return old_lastName;
	}
	public void setOld_lastName(String old_lastName) {
		this.old_lastName = old_lastName;
	}
	public String getOld_mobile() {
		return old_mobile;
	}
	public void setOld_mobile(String old_mobile) {
		this.old_mobile = old_mobile;
	}
	public String getOld_userRole() {
		return old_userRole;
	}
	public void setOld_userRole(String old_userRole) {
		this.old_userRole = old_userRole;
	}
	
	public List<String> getRemoveCircleName() {
		return removeCircleName;
	}
	public void setRemoveCircleName(List<String> removeCircleName) {
		this.removeCircleName = removeCircleName;
	}
	public String getOld_userStatus() {
		return old_userStatus;
	}
	public void setOld_userStatus(String old_userStatus) {
		this.old_userStatus = old_userStatus;
	}
	public String getTestValue() {
		return testValue;
	}
	public void setTestValue(String testValue) {
		this.testValue = testValue;
	}

	
}
