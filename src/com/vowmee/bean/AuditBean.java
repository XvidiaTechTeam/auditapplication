package com.vowmee.bean;

public class AuditBean {
	
	private int audit;
	private int auditLookup;
	private boolean auditValue;
	private int auditFlag;
	private String auditQuestion;
	private String auditAnswer;
	private String auditYes;
	private String auditNo;
	private String auditCategory;
	private int lookupId;
	private String auditNA;
	private String month;
	private String auditCompleteDate;
	
	
	public int getAuditFlag() {
		return auditFlag;
	}
	public void setAuditFlag(int auditFlag) {
		this.auditFlag = auditFlag;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public int getAudit() {
		return audit;
	}
	public void setAudit(int audit) {
		this.audit = audit;
	}
	public int getAuditLookup() {
		return auditLookup;
	}
	public void setAuditLookup(int auditLookup) {
		this.auditLookup = auditLookup;
	}
	public boolean isAuditValue() {
		return auditValue;
	}
	public void setAuditValue(boolean auditValue) {
		this.auditValue = auditValue;
	}
	@Override
	public String toString() {
		return "AuditBean [audit=" + audit + ", auditLookup=" + auditLookup + ", auditValue=" + auditValue
				+ ", auditQuestion=" + auditQuestion + ", auditAnswer=" + auditAnswer + ", auditYes=" + auditYes
				+ ", auditNo=" + auditNo + ", auditCategory=" + auditCategory + ", lookupId=" + lookupId + ", auditNA="
				+ auditNA + ", month=" + month + ", auditCompleteDate=" + auditCompleteDate + "]";
	}
	public String getAuditQuestion() {
		return auditQuestion;
	}
	public void setAuditQuestion(String auditQuestion) {
		this.auditQuestion = auditQuestion;
	}
	public String getAuditAnswer() {
		return auditAnswer;
	}
	public void setAuditAnswer(String auditAnswer) {
		this.auditAnswer = auditAnswer;
	}
	public String getAuditYes() {
		return auditYes;
	}
	public void setAuditYes(String auditYes) {
		this.auditYes = auditYes;
	}
	public String getAuditNo() {
		return auditNo;
	}
	public void setAuditNo(String auditNo) {
		this.auditNo = auditNo;
	}
	public String getAuditCategory() {
		return auditCategory;
	}
	public void setAuditCategory(String auditCategory) {
		this.auditCategory = auditCategory;
	}
	public int getLookupId() {
		return lookupId;
	}
	public void setLookupId(int lookupId) {
		this.lookupId = lookupId;
	}
	public String getAuditNA() {
		return auditNA;
	}
	public void setAuditNA(String auditNA) {
		this.auditNA = auditNA;
	}
	public String getAuditCompleteDate() {
		return auditCompleteDate;
	}
	public void setAuditCompleteDate(String auditCompleteDate) {
		this.auditCompleteDate = auditCompleteDate;
	}
	

}
