package com.vowmee.bean;

public class UserTypeBean {
	
	private int id;
	private String type_name;
	private String type_code;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getType_code() {
		return type_code;
	}
	public void setType_code(String type_code) {
		this.type_code = type_code;
	}
	public String getType_name() {
		return type_name;
	}
	public void setType_name(String type_name) {
		this.type_name = type_name;
	}
	@Override
	public String toString() {
		return "UserTypeBean [id=" + id + ", type_name=" + type_name + ", type_code=" + type_code + "]";
	}

}
