package com.vowmee.bean;

public class WeitageBean {
	
	private String category;
	private String weightage;
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getWeightage() {
		return weightage;
	}
	public void setWeightage(String weightage) {
		this.weightage = weightage;
	}
	@Override
	public String toString() {
		return "WeitageBean [category=" + category + ", weightage=" + weightage + "]";
	}

}
