package com.vowmee.bean;

public class UserCircleVO {
	private String circleCode;
	private int id;
	private String circleName;
	private String circleDesc;
	public String getCircleCode() {
		return circleCode;
	}
	public void setCircleCode(String circleCode) {
		this.circleCode = circleCode;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCircleName() {
		return circleName;
	}
	public void setCircleName(String circleName) {
		this.circleName = circleName;
	}
	public String getCircleDesc() {
		return circleDesc;
	}
	public void setCircleDesc(String circleDesc) {
		this.circleDesc = circleDesc;
	}
	@Override
	public String toString() {
		return "UserCircleVO [circleCode=" + circleCode + ", id=" + id + ", circleName=" + circleName + ", circleDesc="
				+ circleDesc + "]";
	}

}
