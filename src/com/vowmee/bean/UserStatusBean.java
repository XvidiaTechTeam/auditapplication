package com.vowmee.bean;

public class UserStatusBean {

	private int id;
	private String userStatus;
	private String status_code;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUserStatus() {
		return userStatus;
	}
	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}
	@Override
	public String toString() {
		return "UserStatusBean [id=" + id + ", userStatus=" + userStatus + ", status_code=" + status_code + "]";
	}
	public String getStatus_code() {
		return status_code;
	}
	public void setStatus_code(String status_code) {
		this.status_code = status_code;
	}
}
