package com.vowmee.bean;

public class UserStoreMap {
	private long userid;
	private int storeid;
	private String store_name;
	private int id;
	public long getUserid() {
		return userid;
	}
	public void setUserid(long userid) {
		this.userid = userid;
	}
	public int getStoreid() {
		return storeid;
	}
	public void setStoreid(int storeid) {
		this.storeid = storeid;
	}
	@Override
	public String toString() {
		return "UserStoreMap [userid=" + userid + ", storeid=" + storeid + ", store_name=" + store_name + ", id=" + id
				+ "]";
	}
	public String getStore_name() {
		return store_name;
	}
	public void setStore_name(String store_name) {
		this.store_name = store_name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	

}
