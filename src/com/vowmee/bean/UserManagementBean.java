package com.vowmee.bean;

import java.util.List;

public class UserManagementBean {
	
	private String email;
	private String password;
	private String mobile;
	private int userrole;
	private int usertype;
	private String managerId;
	private String locationId;
	private String contactId;
	private int status;
	private String circleName;
	private String storeName;
	private String roleName;
	private String storeType;
	private String firstName;
	private String lastName;
	private List<String> storeList;
	private List<String> circleList;
	public List<String> getCircleList() {
		return circleList;
	}
	public void setCircleList(List<String> circleList) {
		this.circleList = circleList;
	}
	private String reasonForFail;
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public int getUserrole() {
		return userrole;
	}
	public void setUserrole(int userrole) {
		this.userrole = userrole;
	}
	public int getUsertype() {
		return usertype;
	}
	public void setUsertype(int usertype) {
		this.usertype = usertype;
	}
	public String getManagerId() {
		return managerId;
	}
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	public String getLocationId() {
		return locationId;
	}
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	public String getContactId() {
		return contactId;
	}
	public void setContactId(String contactId) {
		this.contactId = contactId;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "UserManagementBean [email=" + email + ", password=" + password + ", mobile=" + mobile + ", userrole="
				+ userrole + ", usertype=" + usertype + ", managerId=" + managerId + ", locationId=" + locationId
				+ ", contactId=" + contactId + ", status=" + status + ", circleName=" + circleName + ", storeName="
				+ storeName + ", roleName=" + roleName + ", storeType=" + storeType + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", storeList=" + storeList + ", reasonForFail=" + reasonForFail + "]";
	}
	public String getCircleName() {
		return circleName;
	}
	public void setCircleName(String circleName) {
		this.circleName = circleName;
	}
	public String getStoreName() {
		return storeName;
	}
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	
	public String getStoreType() {
		return storeType;
	}
	public void setStoreType(String storeType) {
		this.storeType = storeType;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public List<String> getStoreList() {
		return storeList;
	}
	public void setStoreList(List<String> storeList) {
		this.storeList = storeList;
	}
	public String getReasonForFail() {
		return reasonForFail;
	}
	public void setReasonForFail(String reasonForFail) {
		this.reasonForFail = reasonForFail;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((reasonForFail == null) ? 0 : reasonForFail.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserManagementBean other = (UserManagementBean) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (reasonForFail == null) {
			if (other.reasonForFail != null)
				return false;
		} else if (!reasonForFail.equals(other.reasonForFail))
			return false;
		return true;
	}
	
}
